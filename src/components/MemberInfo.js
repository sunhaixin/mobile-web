import React from 'react';
import { GENDER_MAP } from 'const';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { AgeUtil } from 'routes/SettingAge';
import { List, Modal } from 'antd-mobile';

const Item = List.Item;
const Alert = Modal.alert;

function MemberInfo({ dispatch, createTime, extra, pathname, IndexPageSize, IndexPageBottom, recordSize, recordBottom, reportSize, reportBottom, ...rest }) {
  let titleimg = { position: 'relative', top: '-1px'};
  let auth =  JSON.parse(sessionStorage.getItem("state"));
  let evalstyle = { float: 'right', fontSize: 15, position: 'relative', right: 16, color: recordSize, height: 29, borderBottom: recordBottom,top: 7};
  let ages = auth.birthday? AgeUtil(new Date(auth.birthday.slice(0,11).replace(/-/g, "/")),new Date()): '';
  if(pathname == "/IndexPage" || /IndexPage\/\w+/.test(pathname)){
     dispatch({
       type: 'scale/save',
       payload:{
        IndexPageSize: '#a94929',
        IndexPageBottom: '2px solid #a94929',
        recordSize: 'black',
        recordBottom: '2px solid #fff',
        reportSize: 'black',
        reportBottom: '2px solid #fff',
       }
     })
  }else if(pathname == "/scale_record" || /scale_record\/\w+/.test(pathname)){
    dispatch({
      type: 'scale/save',
      payload:{
       IndexPageSize: 'black',
       IndexPageBottom: '2px solid #fff',
       recordSize: '#a94929',
       recordBottom: '2px solid #a94929',
       reportSize: 'black',
       reportBottom: '2px solid #fff',
      }
    })
  }else if(pathname == "/report" || /report\/\w+/.test(pathname)){
    dispatch({
      type: 'scale/save',
      payload:{
       IndexPageSize: 'black',
       IndexPageBottom: '2px solid #fff',
       recordSize: 'black',
       recordBottom: '2px solid #fff',
       reportSize: '#a94929',
       reportBottom: '2px solid #a94929',
      }
    })
  }
  return (
      <List style={{
        position: 'fixed',
        top: 0,
        width:'100%',
        zIndex: 10000
      }}>
           <Item
            extra={
            <img style={{ width: 28, height: 28}} 
            onClick={() =>{
              if(/IndexPage\/\w+/.test(pathname)){
                Alert('离开', '你将离开当前测评！', [
                  { text: '取消'},
                  { text: '确定', onPress: () => 
                  dispatch(routerRedux.replace({
                    pathname: '/IndexPage/detail',
                  }))}])
              }else{
                dispatch(routerRedux.replace({
                  pathname: '/IndexPage/detail',
              }))}
            }}
            src={require('../assets/avatar.svg')} />}>
            {auth? auth.name: ""}&nbsp;&nbsp;&nbsp;&nbsp;
            {auth? GENDER_MAP[auth.gender]: ""}&nbsp;&nbsp;&nbsp;&nbsp;{ages}
          </Item>
          <Item
            extra={
           <div 
           style={{ fontSize: 15, color: reportSize, height: 29, width: 60, float: 'right',borderBottom: reportBottom, position: 'relative', top: 7}} 
           onClick={() =>{
              if(/IndexPage\/\w+/.test(pathname)){
                Alert('离开', '你将离开当前测评！', [
                  { text: '取消'},
                  { text: '确定', onPress: () => 
                  dispatch(routerRedux.replace({
                    pathname: '/report',
                  }))}])
              }else{
                dispatch(routerRedux.replace({
                  pathname: '/report',
              }))}
           }}>
           <img style={titleimg} src="http://wapzh.tcm-ai.com/static/baogao.75477a75.svg" />
           &nbsp;&nbsp;报告</div>}>
            <div>
              <div style={{ float: 'left'}}>
              <div style={{ fontSize: 15, color: IndexPageSize, height: 29, borderBottom: IndexPageBottom, position: 'relative', top: 7 }} 
              onClick={() =>{
                if(/IndexPage\/\w+/.test(pathname)){
                  Alert('离开', '你将离开当前测评！', [
                    { text: '取消'},
                    { text: '确定', onPress: () =>{
                    dispatch(routerRedux.replace({
                      pathname: '/IndexPage',
                    }))}}])
                }else{
                  dispatch(routerRedux.replace({
                    pathname: '/IndexPage',
                }))}
              }}> 
              <img style={titleimg} src={require('../assets/qianhuan.svg')} />
              &nbsp;&nbsp;测评</div>
              </div>
              <div 
              style={evalstyle} 
              onClick={() =>{
                if(/IndexPage\/\w+/.test(pathname)){
                  Alert('离开', '你将离开当前测评！', [
                    { text: '取消'},
                    { text: '确定', onPress: () => 
                    dispatch(routerRedux.replace({
                      pathname: '/scale_record',
                    }))}])
                }else{
                  dispatch(routerRedux.replace({
                    pathname: '/scale_record',
                }))}
              }}> 
              <img style={titleimg} src="http://wapzh.tcm-ai.com/static/jilu.77139d5a.svg" />
              &nbsp;&nbsp;记录</div>
            </div>
          </Item>
      </List>
  );
}

export default connect(({ scale }) => ({ ...scale }))(MemberInfo);