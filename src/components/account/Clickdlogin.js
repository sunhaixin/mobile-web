import React from 'react';
import { connect } from 'dva';
import { CardList } from 'const';
import { WingBlank, WhiteSpace, List, InputItem, Button, Toast, Picker } from 'antd-mobile';
import { createForm } from 'rc-form';
import IndexBottom from './IndexBottom';
import ClicksmsCode from './ClicksmsCode';
import styles from './cc.less';
import axios from 'axios';
import { apiBaseUrl } from '../../config';
import { fetchErrorMsg, IdNumverReg } from "routes/SettingAge";

const Clickdlogin = ({ dispatch, form, location, clock, localtype, W3T, ...rest }) =>{
    window.document.title = '\u200E';
    const { getFieldProps, getFieldValue, setFieldsValue, getFieldError, validateFields } = form;
    let Imessage, Iplaceholder, ImaxLength, Itype_;
    let localid = JSON.parse(localStorage.getItem("Datas"));
    let localidnumber = localid && localid.idNumber ? localid.idNumber : "";
    if(localid){
      if(localid.type == 1){
        localtype = ["1"];
      }else if(localid.type == 2){
        localtype = ["2"];
      }else if(localid.type == 3){
        localtype = ["3"];
      }else if(localid.type == 5){
        localtype = ["5"];
      }else{
        localtype = ["0"];
      }
    }else{
       if(W3T && localtype){
        if(localtype[0] == 0){
          Imessage = '请填写身份证';
          Iplaceholder = '请填写身份证号,一旦填写不可修改';
          ImaxLength = 18;
          Itype_= "string"
        }else if(localtype[0] == 1){
          Imessage = '请填写体检号';
          Iplaceholder = '请填写体检卡号,一旦填写不可修改';
          ImaxLength = 30;
          Itype_= "string"
        }else if(localtype[0] == 2){
          Imessage = '请填写社保号';
          Iplaceholder = '请填写社保卡号,一旦填写不可修改';
          ImaxLength = 30;
          Itype_= "string"
        }else if(localtype[0] == 3){
          Imessage = '请填写就诊号';
          Iplaceholder = '请填写就诊卡号,一旦填写不可修改';
          ImaxLength = 30;
          Itype_= "string"
        }else if(localtype[0] == 5){
          Imessage = '请填写手机号';
          Iplaceholder = '请填写手机号,一旦填写不可修改';
          ImaxLength = 11;
          Itype_= "number"
        }
       }
    }
    
    const checkSmsCode = () =>
        validateFields((errors, value) =>{
          if(errors) {
            const fieldNames = Object.keys(errors);
            if(fieldNames[0]){
              return Toast.fail(getFieldError(fieldNames[0]));
            }
          }else{
            let IdId = parseInt(value.idtype);
            if(IdId == 0 || !IdId){
                if(IdNumverReg(getFieldValue('idNumber')) == 404){
                   setFieldsValue({["idNumber"]: null});
                }else{
                  dispatch({
                    type: "relation/findbyid",
                    payload: {
                      idNumber: getFieldValue('idNumber'),
                      hid: localStorage.getItem("hid")
                    },
                  });
                }
            }else{
                if(IdId == 5){
                  if(!(/^1[345678]\d{9}$/.test(getFieldValue('idNumber')))){
                    setFieldsValue({["idNumber"]: null});
                    return Toast.fail('手机号码有误，请重填',1);
                  }
                  if(!rest.codecode){
                    return Toast.fail('请输入验证码', 1);
                  }else if(rest.codecode.length !==6){
                    return Toast.fail('验证码格式不正确',1)
                  }
                  axios.get(`${apiBaseUrl}rbac/wap/checkSms`, {
                    params: {
                      mobile: getFieldValue('idNumber'),
                      code: rest.codecode,
                      hid: localStorage.getItem("hid")
                    }
                  })
                  .then(function (res) {
                    if(res.data){
                      if(res.data.code !=200){
                        Toast.fail(res.data.message, 1);
                      }else{
                        clearInterval(clock);
                        dispatch({
                          type: 'scale/save',
                          payload: {
                            backGround: "#fff",
                            btn: "获取验证码",
                            clock: "",
                            fontSizecolor: "#aa4929",
                            isDisabled_: false,
                            wait: 60
                        }});
                        dispatch({
                          type: "relation/findbyid",
                          payload: {
                            idNumber: getFieldValue('idNumber'),
                            type: getFieldValue('idtype'),
                            hid: localStorage.getItem("hid")
                          },
                        });
                      }
                    }
                  }).catch(function (error) {
                    return fetchErrorMsg(error.message)
                  });
                }else if(IdId == 1 || IdId == 2|| IdId ==3){
                  const regtj = /^[0-9a-zA-Z]*$/g;
                  if(!regtj.test(getFieldValue('idNumber'))){
                    setFieldsValue({["idNumber"]: null});
                    return Toast.fail('卡号只能由字母数字组成', 1);
                  }
                  dispatch({
                    type: "relation/findbyid",
                    payload: {
                      idNumber: getFieldValue('idNumber'),
                      type: getFieldValue('idtype'),
                      hid: localStorage.getItem("hid")
                    },
                  });
                }
            }
      }
    });
    return (
        <WingBlank size="md">
            <WhiteSpace size="xl" />
            <Picker
            cols={1}
            onOk={(val) =>{
              dispatch({
                type: "scale/save",
                payload: {codetype: val[0], localtype: val}
              });
              if(localid){
                if(val[0] == localid.type || val[0] == "" && localid.type === null){
                  setFieldsValue({["idNumber"]: localidnumber})
                }else{
                  localStorage.removeItem("Datas");
                  setFieldsValue({["idNumber"]: null})
                }
              }else{
                  setFieldsValue({["idNumber"]: null})
              }
              }
            }
            data={CardList}
            {...getFieldProps('idtype', {
              initialValue: localtype ||  ["5"] ,
              validate: [{
                trigger: false,
                rules: [{ required: true, message: '请选择认证方式' }],
              }],
            })}>
                <List.Item arrow="horizontal">认证方式</List.Item>
            </Picker>
            <InputItem
            {...getFieldProps('idNumber', {
              initialValue: localidnumber,
              validate: [{
                trigger: false,
                rules: [{ required: true, message:  Imessage }],
              }],
            })}
            placeholder={Iplaceholder}
            type={Itype_}
            maxLength={ImaxLength}
            />
            {localtype&&localtype[0] == 5?
            <ClicksmsCode
            mobile={getFieldValue('idNumber')}/>: ""}
            <WhiteSpace size="md" />
            <div style={{ height: 40, background: '#efefef', zIndex: 1, width: '100%', position: 'relative', bottom: 20}}></div>
            <Button
              type="primary"
              onClick={checkSmsCode}
              style={{ position: 'relative', bottom: 20}}
              className={styles.loginButton}
            >
            下一步
            </Button>
            <IndexBottom/>
        </WingBlank>
    );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(Clickdlogin));
