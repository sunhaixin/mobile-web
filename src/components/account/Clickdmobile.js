import React from 'react';
import { connect } from 'dva';
import { WingBlank, WhiteSpace, List, InputItem, DatePicker, Button, Picker,  Toast } from 'antd-mobile';
import { createForm } from 'rc-form';
import { DateTimes } from 'const';
import IndexBottom from './IndexBottom';

const Clickdmobile = ({dispatch, form ,isDisabled }) => {
  const { getFieldProps, getFieldValue, getFieldError, validateFields } = form;
  const genderList = [
    { label: '男', value: 1 },
    { label: '女', value: 2 },
  ];

  const nowTimeStamp = Date.now();
  const minDate = new Date(1900, 1, 1);
  const maxDate = new Date(nowTimeStamp);
  let localid = JSON.parse(localStorage.getItem("Datas"));
  let Idtype = localid && localid.type ? localid.type: "";
  let Idnumber = localid && localid.idNumber ? localid.idNumber: "";
  let INames, Imessage, Iplaceholder, ImaxLength, DefYear, DefMonth, DefDay, DefGendr, DefDates;
    if(Idtype == 0 || !Idtype){
      INames = '身份证号';
      Imessage = '请填写身份证';
      Iplaceholder = '请填写身份证号,一旦填写不可修改';
      ImaxLength = 18;
      if(Idnumber){
        if(Idnumber.length == 18){
          DefYear = Idnumber.slice(6,10);
          DefMonth = Idnumber.slice(10,12);
          DefDay = Idnumber.slice(12,14);
          if(Idnumber.slice(16,17) % 2 ==1){
            DefGendr = [1]
          }else{
            DefGendr = [2]
          }
          DefDates = new Date(DefYear, DefMonth - 1,DefDay )
        }else if(Idnumber.length == 15){
          DefYear = 19 + Idnumber.slice(6,8);
          DefMonth = Idnumber.slice(8,10);
          DefDay = Idnumber.slice(10,12);
          if(Idnumber.slice(14,15) % 2 ==1){
            DefGendr = [1]
          }else{
            DefGendr = [2]
          }
          DefDates = new Date(DefYear, DefMonth - 1,DefDay )
        }
      }
    }else if(Idtype == 1){
      INames = '体检卡号';
      Imessage = '请填写体检号';
      Iplaceholder = '请填写体检卡号,一旦填写不可修改';
      ImaxLength = 30;
    }else if(Idtype == 2){
      INames = '社保卡号';
      Imessage = '请填写社保号';
      Iplaceholder = '请填写社保卡号,一旦填写不可修改';
      ImaxLength = 30;
    }else if(Idtype == 3){
      INames = '就诊卡号';
      Imessage = '请填写就诊号';
      Iplaceholder = '请填写就诊卡号,一旦填写不可修改';
      ImaxLength = 30;
    }else if(Idtype == 5){
      INames = '手机卡号';
      Imessage = '请填写手机号';
      Iplaceholder = '请填写手机卡号,一旦填写不可修改';
      ImaxLength = 11;
    }
  const signup = () =>
    validateFields((errors, value) => {
      if (errors) {
        const fieldNames = Object.keys(errors);
        if (fieldNames[0]) {
          return Toast.fail(getFieldError(fieldNames[0]));
        }
      }else{
          dispatch(({ type: 'relation/save', payload:{
            isDisabled: true
          }}))
          setTimeout(() => {
              dispatch(({ type: 'relation/save', payload:{
                isDisabled: false
            }}))
          }, 3000);
          dispatch({
            type: "relation/create",
            payload: {
              idNumber: getFieldValue('idNumber'),
              name: getFieldValue('name'),
              gender: getFieldValue('gender'),
              birthday: DateTimes(value.birthday).split(" ")[0],
              type: !Idtype?"":Idtype,
              hid: localStorage.getItem("hid")
            },
          });
        }
  })

  return (
    <div style={{ position: 'absolute', top: '20%', width: '100%'}}>
        <WingBlank><div style={{ fontSize: '0.22rem'}}>欢迎使用智能云中医</div></WingBlank>
        <WhiteSpace size="xl" />
        <WhiteSpace size="xl" />
        <WingBlank size="md">
          <WhiteSpace size="xl" />
          <InputItem
            {...getFieldProps('idNumber', {
              initialValue: Idnumber,
              validate: [{
                trigger: false,
                rules: [{ required: true, message: {Imessage} }],
              }],
            })}
            disabled={true}
            placeholder={Iplaceholder}
            type="string"
            maxLength={ImaxLength}
            ><div style={{color:'black'}}>{INames}</div></InputItem>
          <InputItem
              {...getFieldProps('name', {
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请输入姓名' }],
                }],
              })}
              placeholder="请输入姓名"
            >姓名</InputItem>
          <Picker
              cols={1}
              data={genderList}
              {...getFieldProps('gender', {
                initialValue: DefGendr,
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请选择性别' }],
                }],
              })}
            >
              <List.Item arrow="horizontal">性别</List.Item>
            </Picker>
          <DatePicker
              {...getFieldProps('birthday', {
                initialValue: DefDates,
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请选择出生年月' }],
                }],
              })}
              mode="date"
              title="请选择"
              minDate={minDate}
              maxDate={maxDate}
            >
              <List.Item arrow="horizontal">出生年月</List.Item>
            </DatePicker>
          <WhiteSpace size="md" />
          <WhiteSpace size="md" />
          <Button type="primary" disabled={isDisabled} onClick={signup}>
            下一步
          </Button>
          <IndexBottom/>
        </WingBlank>
    </div>
  );
}

export default connect(({ relation }) => ({ ...relation }))(createForm()(Clickdmobile));
