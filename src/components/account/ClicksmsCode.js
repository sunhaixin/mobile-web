import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import axios from 'axios';
import { apiBaseUrl } from '../../config';
import { InputItem, Button, Toast } from 'antd-mobile';
import { fetchErrorMsg } from 'routes/SettingAge';

const ClicksmsCode = ({ dispatch, form, mobile, btn, wait, isDisabled_, clock, backGround, fontSizecolor }) => {
  const { getFieldProps, validateFields, getFieldValue } = form;
  if (!mobile) {
    clearInterval(clock);
    dispatch({
      type: 'scale/save',
      payload: {
        backGround: '#fff',
        btn: '获取验证码',
        clock,
        fontSizecolor: '#aa4929',
        isDisabled_: false,
        wait: 120,
      } });
        // 当每次进入该页面时把button的设为"获取验证码"
  }
  const sendSmsCode = () => {
    validateFields(() => {
      if (!mobile) {
        return Toast.fail('请输入正确手机号', 0.5);
      }
      axios.get(`${apiBaseUrl}rbac/wap/sendSms`, {
        params: {
          mobile,
        },
      })
        .then((res) => {
          if (res.data) {
            if (res.data.code != 200) {
              Toast.fail(res.data.message, 1);
              clearInterval(clock);
              dispatch({
                type: 'scale/save',
                payload: {
                  backGround: '#fff',
                  btn: '获取验证码',
                  wait: 120,
                  isDisabled_: false,
                  fontSizecolor: '#aa4929',
                } });
            } else {
              Toast.success('获取验证码成功', 1);
            }
          } else {
            return fetchErrorMsg(res.data);
          }
        }).catch((error) => {
          clearInterval(clock);
          dispatch({
            type: 'scale/save',
            payload: {
              backGround: '#fff',
              btn: '获取验证码',
              wait: 120,
              isDisabled_: false,
              fontSizecolor: '#aa4929',
            } });
          return fetchErrorMsg(error.message);
        });
      btn = `${wait}S`;
      clock = setInterval(doLoop, 1000);
      dispatch({
        type: 'scale/save',
        payload: {
          clock,
          isDisabled_: true,
          backGround: '#727171',
          fontSizecolor: '#fff',
        } });
    });
  };
  function doLoop() { // 验证码倒计时
    dispatch({
      type: 'scale/save',
      payload: {
        backGround: '#727171',
        fontSizecolor: '#fff',
        wait: wait--,
      } });
    if (wait > 0) {
      btn = `${wait}S`;
      dispatch({
        type: 'scale/save',
        payload: {
          btn, wait,
        } });
    } else {
      clearInterval(clock);
      dispatch({
        type: 'scale/save',
        payload: {
          backGround: '#fff',
          btn: '获取验证码',
          clock: '',
          fontSizecolor: '#aa4929',
          isDisabled_: false,
          wait: 120,
        } });
    }
  }
  if (getFieldValue('code') || !getFieldValue('code')) {
    dispatch({
      type: 'scale/save',
      payload: {
        codecode: getFieldValue('code'),
      },
    });
  }
  return (
    <InputItem
      {...getFieldProps('code', {
        validate: [{
          trigger: false,
          rules: [{ required: true, message: '请输入验证码' }],
        }],
      })}
      maxLength={6}
      placeholder="请输入验证码"
      extra={
        <Button
          type="primary"
          size="small"
          onClick={sendSmsCode}
          disabled={isDisabled_}
          style={{ background: backGround, color: fontSizecolor }}
        >
          {btn}
        </Button>
        }
    />
  );
};

export default connect(({ scale }) => ({ ...scale }))(createForm()(ClicksmsCode));
