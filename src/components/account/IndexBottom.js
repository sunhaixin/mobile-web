import React from 'react';
import { Link } from 'dva/router';
import { WhiteSpace, Flex } from 'antd-mobile';
import styles from './cc.less';


export default function IndexBottom() {

    return (
      <div className={styles.indexbottom}>
        <Flex justify="center">
              <div style={{ color: '#656565'}}>使用即代表您同意并接受</div>
              <Link to="/useragree">《智能云中医用户协议》</Link>
        </Flex>
        <WhiteSpace size="md" />
        <Flex justify="center">
            <div style={{ color: '#ababab'}}>版本号：1.1.2 pre-release</div>
        </Flex>
      </div>
    );
}
