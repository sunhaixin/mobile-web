import React from 'react';
import { Flex, WingBlank } from 'antd-mobile';
import styles from './cc.less';

export default function UserAgree(props) {
   
    return (
        <div style={{ background: '#efefef',  userSelect:'none'}}>
             <WingBlank>
                    <Flex justify="center">
                        <h2 style={{ color: 'black', marginTop: 20, fontWeight: 'normal'}}>智能云中医用户协议</h2>
                    </Flex>
                    <Flex><span>一、声明</span></Flex>  
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、本协议是用户与上海道生医疗科技有限公司（以下简称“道生医疗”）之间关于使用智能云中医软件PC端、移动客户端及其他现在或将来推出的基于智能云中医服务的平台（以下简称“智能云中医”）所订立的协议。用户在使用智能云中医所提供的任何服务前，须事先仔细阅读、充分理解本用户协议。如您同意本用户协议或者您事实上已存在使用智能云中医的行为，则将被视为您完全接受本用户协议所包含的全部内容、条款及相关隐私声明，即被视为用户确认自己具有独立的行为能力，明确自己所享有的相应权利，能够独立承担法律责任，本协议即构成对双方有约束力的法律文件；如您不同意本用户协议，您将不得使用或应主动取消智能云中医所提供的服务。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、未满18岁的用户或限制民事行为能力人，必须在父母或者监护人监护之下使用智能云中医。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;3、智能云中医所有服务的所有权、运营权和解释权均归道生医疗所有。用户特此声明，已经完全理解本用户协议所有的内容，并不存在任何重大误解；同时，认可本用户协议并不存在显失公平的情形。道生医疗有权不另行通知即对本用户协议随时做任何修改及更新。</span>
                    </p>
                    <Flex><span>二、定义</span></Flex>   
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、智能云中医：指包括智能云中医软件PC端、移动客户端及其他现在或将来推出的基于智能云中医服务的平台。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、用户：指阅读并同意本用户协议内容，经过智能云中医注册程序成为智能云中医各项服务使用者的单位或个人。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;3、医师：指同意并承诺遵守智能云中医使用限定，向智能云中医用户采集用户信息、提供咨询解答、医疗知识传播服务的，并持有卫生局签发的《医师资格证书》、《医师执业证书》的医生。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;4、服务：指由智能云中医所有、控制和运营的，可在电脑、移动端等各种设备上运行的软件、公众号、小程序、内容和数据资料。包括但不限于文本、图片、音频、视频、软件等信息和资料。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;5、用户个人信息：指包括但不限于下列信息：用户真实姓名、身份证号、手机电话、用户个人健康和医疗信息、身高、体重、性别、年龄、以及慢性病史情况等。</span>
                    </p>
                    <Flex><span>三、隐私政策</span></Flex>   
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、智能云中医非常重视对用户个人隐私信息的保护。用户所提供的个人信息等资料以及智能云中医所保留的用户个人资料，将受到中国有关隐私的法律以及智能云中医隐私政策的特别保护。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、用户可以通过注册账号的方式使用智能云中医的服务，注册智能云中医时，根据要求提供个人信息。用户注册成功后，必须妥善保管自己的账号和密码，因用户本人泄露而造成的任何损失由用户本人负责。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;3、智能云中医严格保护用户个人信息的安全。我们使用各种安全技术和程序来保护用户的个人信息不被未经授权的访问、使用或泄露，智能云中医不会向任何人出售或出借用户的个人信息，除非事先得到用户的许可。但对于基于当前技术在合理水平下无法解决的信息安全问题，比方黑客恶意攻击等，智能云中医及道生医疗不承担任何责任。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;4、为服务用户的目的，智能云中医可能通过使用用户的个人信息，向用户提供服务，包括但不限于向用户发出活动和服务信息等。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;5、随着服务的变化，道生医疗有权对隐私条款不时进行修改，且不再另行通知。更新后的隐私条款一旦发布即有效代替旧版本的隐私政策。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;6、用户的个人信息将在下述情况下部分或全部被披露：</span>
                    </p>
                   
                    <p className={styles.user_p}>
                        <ul>
                            <li>经用户同意，向第三方披露；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>根据法律的有关规定，或者行政或司法机关的要求，向第三方或者行政、司法机关披露；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>如用户出现违反中国有关法律或者网站政策的情况，需要向第三方披露的；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>基于学术研究而使用。</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>为提供用户所要求的产品和服务，而必须和第三方分享用户的个人信息的；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>在不透露单个用户隐私资料的前提下，智能云中医有权对整个用户数据库进行分析。</li>
                        </ul>
                    </p>
                    
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;7、如果有明确证据表明用户所提供的信息存在不符合法律政策或者不真实情况，智能云中医有权无须通知用户对信息进行删除、更改等处理。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;8、用户请勿在使用智能云中医时公开透露各类财产账户、银行卡、信用卡、第三方支付账户及对应密码等重要资料，否则由此带来的损失由用户自行承担责任。</span>
                    </p>
                    <Flex><span>四、双方的权利与义务</span></Flex>   
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、为便于用户使用智能云中医，智能云中医有义务维护平台服务的正常运行，提升及改进技术。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、用户有权利使用自己的账户随时登陆智能云中医服务。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;3、智能云中医应在具有资质的医师指导下合理使用。智能云中医发挥功效之前提，是在医师的指导下起到合理范围内的辅助医师诊断决策之作用。智能云中医使用过程中所产生的相应信息，仅作为用户或者医师的参考，不可以独立替代医师就病情的决策判断、诊断治疗方案或完整的治疗建议。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;4、因下述情况造成不良后果及责任的，智能云中医及道生医疗不承担任何独立或连带责任：</span>
                    </p>
                   
                    <p className={styles.user_p}>
                        <ul>
                            <li>用户提供的有关信息不真实、不准确或不完整；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>用户将该产品提供的判断、建议或报告作为医师针对病情的决策判断、最终诊断治疗方案或完整的治疗建议替代医师常规诊断治疗；</li>
                        </ul>
                    </p>
                    <p className={styles.user_p}>
                        <ul>
                            <li>医师诊断有误或使用该产品不当等。</li>
                        </ul>
                    </p>
                    
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;5、如用户损害智能云中医的合法权益、或用户导致智能云中医遭受任何来自第三方的纠纷、诉讼、索赔要求等，用户须向智能云中医及道生医疗赔偿相应的损失，并需要对用户的行为产生的一切后果负全部法律责任。</span>
                    </p>
                    <Flex><span>五、知识产权</span></Flex>   
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、除法律规定外，未经道生医疗书面形式的明确许可，任何单位或个人不得以任何方式全部或部分复制、转载、引用、链接、抓取或以其他方式使用智能云中医的信息内容（包括但不限于：源程序和文档、界面设计、养生方案、文案等），如有违反本条协议者，一经核实，道生医疗坚决追究其法律责任，并要求赔偿因其行为所造成的一切经济损失。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、道生医疗为智能云中医的开发者，拥有智能云中医所有内容及资源的著作权等合法权利，受国家法律保护，有权不时地对本协议及智能云中医的内容进行修改。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;3、智能云中医所刊登的资料信息（诸如文字、图表、标识、按钮图标、图像、声音文件片段、数字下载、数据编辑和软件），均受国家版权法保护。</span>
                    </p>
                    <Flex><span>六、其他</span></Flex>   
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;1、因不可抗力（如火灾、水灾、暴动、骚乱、战争、自然灾害等）导致智能云中医服务中断或者用户数据损坏、丢失等，智能云中医及道生医疗不承担任何责任。</span>
                    </p>
                    <p className={styles.user_p}>
                        <span>&nbsp;&nbsp;2、本用户协议的订立、执行和解释及争议的解决均应适用在中华人民共和国大陆地区适用之有效法律。如缔约方就本用户协议内容或其执行发生任何争议,双方应友好协商解决；协商不成的,双方在此同意由智能云中医运营方上海道生医疗科技有限公司所在地的人民法院管辖。</span>
                    </p>
             </WingBlank>
        </div>
    );
}
