import Clickdlogin from './account/Clickdlogin';
import Clickdmobile from './account/Clickdmobile';
import UserAgree from './account/UserAgree';
import IndexBottom from './account/IndexBottom';


export {
	Clickdlogin,
	Clickdmobile,
	UserAgree,
	IndexBottom
}