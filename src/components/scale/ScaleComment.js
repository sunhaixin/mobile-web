import React from 'react';
import { Flex, WingBlank, WhiteSpace, List, TextareaItem } from 'antd-mobile';

const { Item } = List;
export default function Scalecomment({ comment}) {
  return (
    <WingBlank size="md">
      <List>
        <Item>
          <Flex justify="center">备注</Flex>
          <WhiteSpace />
            <TextareaItem
              value={comment}
              rows={5}
              style={{ border: '1px solid #ddd' }}
              editable={false}
            />
        </Item>
        
      </List>
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}