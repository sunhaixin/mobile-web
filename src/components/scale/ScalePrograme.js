import React from 'react';
import { Flex, WingBlank, List, WhiteSpace } from 'antd-mobile';

const { Item } = List;
export default function ScalePrograme({ ProgrameRec, ProgrameInt}) {
  return (
    <WingBlank size="md">
      <List>
        <Item>
          <Flex justify="center">选择方案</Flex>
        </Item>
        { !ProgrameInt ? "" : ProgrameInt.map(item => (
          <AgreeItem
            key={item.Id}
            onChange={e =>
              dispatch({
                type: 'scale/savePrograme',
                payload: {
                  type: 'programeint',
                  value: item.Id,
                },
              })}
          >
            <Flex align="center" justify="between">
              {item.Title}
              <Flex align="center">
                <a style={{ color: '#aa4929'}} href={item.Url} target="_blank">查看方案</a>
                <Icon type="right" style={{ fill: '#ccc' }} />
              </Flex>
            </Flex>
          </AgreeItem>
        ))}
        { !ProgrameRec ? "" : ProgrameRec.map(item => (
          <AgreeItem
            key={item.Id}
            onChange={e =>
              dispatch({
                type: 'scale/savePrograme',
                payload: {
                  type: 'programerec',
                  value: item.Id,
                },
              })}
          >
            {item.Url ?<Flex align="center" justify="between">
                          {item.Title}
                          <Flex align="center">
                            <a style={{ color: '#aa4929'}} href={item.Url} target="_blank">查看方案</a>
                            <Icon type="right" style={{ fill: '#ccc' }} />
                          </Flex>
                        </Flex>
              :<Flex align="center" justify="between"
                style={{ border: '1px solid #ddd', padding: "10px", textIndent: "2em" }}
                >
                {item.Detail.replace(/@/g," \r ").replace(/;/g," \r ")}
                </Flex>
            }
          </AgreeItem>
        ))}
      </List>
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}
