import React from 'react';
import { connect } from 'dva';
import { WingBlank, WhiteSpace, Flex, List, TextareaItem, Icon } from 'antd-mobile';

const { Item } = List;

 function ScaleProgrameSelect({list, vp }) {
  const { 
    type, recType, Programe,
    programeInt, programeRec, 
    programeRecHtml, 
    RecDetail} = list;
  
  let programe, redetail, htmlstyle = { margin: '0 auto', overflowY: 'scroll',  height: 900, padding: 35};
  if(Programe){
     programe = Programe.replace(/@/g,"\r\n").replace(/;/g,"\r\n").replace(/~/g,"\r\n").replace(/\|/g,"\r\n\r\n");
  }

  if(RecDetail){
     redetail =  RecDetail.replace(/@/g,"\r\n").replace(/;/g,"\r\n").replace(/~/g,"\r\n").replace(/\|/g,"\r\n\r\n");
  }
  return (
    <WingBlank size="md">
      <List>
        <Item style={{marginBottom: '1em'}}>
          <Flex justify="center">方案</Flex>
        </Item>
        {type == "phy" || type == "phy2" || type == "prepre" || type == "normal" ? 
					<div>  
              {recType == "text" ? 
                <WingBlank size="md">
                <List>
                  <Item>
                    <WhiteSpace />
                      <TextareaItem
                        value = {programe}
                        autoHeight
                        style={{ border: '1px solid #ddd', textIndent: "2em" }}
                        editable={false}
                        rows ={50}
                      />
    
                  </Item>
                </List>
              </WingBlank>: 
              <WingBlank size="sm"> 
                {(recType == "html" || recType == "div" ? <div 
                style={htmlstyle}
                dangerouslySetInnerHTML = {{ __html: Programe }} />: "")}
              </WingBlank> 
             }
					</div>
					:(type == "pre" || type == "partum")?
          <div>
              {recType == "text" ? 
                <WingBlank size="md">
                <List>
                  <Item>
                    <WhiteSpace />
                      <TextareaItem
                        value = {redetail}
                        autoHeight
                        style={{ border: '1px solid #ddd', textIndent: "2em" }}
                        editable={false}
                        rows ={50}
                      />
    
                  </Item>
                </List>
              </WingBlank>:  
               <WingBlank size="sm"> 
                {(recType == "html" || recType == "div" ? <div 
                style={htmlstyle}
                dangerouslySetInnerHTML = {{ __html: programeRecHtml }} />: "")}
               </WingBlank>
             }
          </div>:
          <div>
            { !programeRec ? "" : programeRec.map((item,i) => (
              <div style={{lineHeight: "2em"}} key={i}>
                {item.url ?<Flex align="center" justify="between" style={{marginLeft: '0.15rem', borderBottom: '1px solid #ccc'}}>
                              <div style={{width: "70%",overflow: "hidden",whiteSpace: "normal",textOverflow: "ellipsis"}}>
                              {item.title=="自定义方案"?"个体化养生方案":item.prog}
                              </div>
                              <Flex align="center">
                                <a style={{ color: '#aa4929'}} href={item.url} target="_blank">查看方案</a>
                                <Icon type="right" style={{ fill: '#ccc' }} />
                              </Flex>
                            </Flex>
                  :
                  <div>
                    {item.programe}
                  </div>
                }
              </div>
           ))}
           <div style={{height: '0.5em'}}>
           </div>
            {vp && vp == 2? "":!programeInt ? "" : programeInt.map((item,i) => (
            <div style={{lineHeight: "2em"}} key={i}>
              <Flex align="center" justify="between" style={{marginLeft: '0.15rem', borderBottom: '1px solid #ccc'}}>
                <div style={{width: "70%",}}>
                {item.title=="自定义方案"?"个体化干预方案":item.prog}
                </div>
                <Flex align="center">
                  <a style={{ color: '#aa4929'}} href={item.url} target="_blank">查看方案</a>
                  <Icon type="right" style={{ fill: '#ccc' }} />
                </Flex>
              </Flex>
            </div>
            )) }
            </div>}
        <WhiteSpace size='md' />
      </List>
    </WingBlank>
  );
}
export default connect(({ report }) => ({
  ...report,
}))(ScaleProgrameSelect);
