import React from 'react';
import { Flex, WingBlank, WhiteSpace, List, Progress } from 'antd-mobile';

const { Item, Item: { Brief } } = List;
const scoreLabels = ['平和质', '气虚质', '阳虚质', '阴虚质', '痰湿质', '湿热质', '血瘀质', '气郁质', '特禀质'];

export default function ScaleReport({ report1, report2, report3, title, ...rest }) { 
  window.scrollTo(0, 0);
  let Report1, Report2;
  if(report1){
     if(report1.split(';')[1]){
        if(report1.indexOf("倾向是") != - 1 && report1.split(';')[1].indexOf("是") != - 1){
          if(report1.indexOf("基本是") != -1 && report1.indexOf("倾向是") != -1){
             Report1 = report1;
          }else{
             if(report1[0] == "是"){
              Report1 = report1.substring(1,25)
             }else{
              Report1 = report1.substring(0,25)
             }
          }
        }else if(report1.indexOf("是") != - 1 ){
          let a = report1.split('是');
          let b = a[2] ?  a[2]: '';
          let b1 = a[3] ?  a[3]: '';
          Report1 = a[1] + b + b1
        }else{
          Report1 = report1;
        }
     }else{
        if(report1.split('是')[0] == "倾向"){
          Report1 = report1;
        }else if(report1.split('是')[0] == ""){
          Report1 = report1.substring(1,25)
        }else{
          Report1 = report1;
        }
     }
  }

  if(report2){
      Report2 = report2.replace(/@/g," "); 
  }else if(report2 == ""){
      Report2 = "请咨询相关医生";
  }

  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">{title}</Flex>
          <WhiteSpace/>
        </Item>
        <Item >
          体质类型：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {Report1}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          易发疾病：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {Report2}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          体质得分：
          <Brief>
            {report3 &&
              report3.split(';').map((percent, i) => (
                <div key={i}>
                  <div style={{ display: 'inline-block', width: "0.8rem" }}>
                    {scoreLabels[i]}
                  </div>
                  <div style={{ display: 'inline-block', width: "1.5rem" }}>
                    <Progress position="normal" unfilled="hide" percent={percent} />
                  </div>
                  <span style={{ marginLeft: "0.1rem", color: '#aa4929',width: "2em" }}>
                    {percent}
                  </span>
                  <WhiteSpace />
                </div>
              ))}
          </Brief>
        </Item>
        {rest.children}
      </List>
    </WingBlank>
  );
}
