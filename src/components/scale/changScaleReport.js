import React from 'react';
import { Flex, WingBlank, WhiteSpace, List, TextareaItem } from 'antd-mobile';
import styles from './scalereport.css';
// 中成药辅助辨证
const { Item, Item: { Brief } } = List;

export default function changScaleReport(props) {
  window.scrollTo(0, 0);
  return (
    <WingBlank size="md" classname={styles.itemfather}>
      <List wrap>
        <Item>
            <Flex justify="center">中成药辅助辨证报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item >
          主诉：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
              {props.mainCom}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          主症：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {props.primSym}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          兼症：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
              {props.acpnSym}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          舌象：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
              {props.tongueCon}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          脉象：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
              {props.pulseCon}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          辨证结果：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {props.synResult}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          辨病结果：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {props.disResult}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
         <Item >
          用药建议：
          
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}  >
              <TextareaItem 
              rows={15}
              editable={false}
              value={props.medSug} 
              autoHeight/>
          </Brief>
        </Item>
        <Item >
          调养建议：
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
            {props.comment}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
      </List>
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}
