import React from 'react';
import { Flex, WingBlank, WhiteSpace, List, Progress } from 'antd-mobile';

const { Item, Item: { Brief } } = List;
let scoreLabels = [], list = [];

export default function chd2ScaleReport({ hint, conclusion, explanation, performance, scores, ...rest }) {
  window.scrollTo(0, 0);
  let report1 = "平人质;阳热质;阴虚质;阳虚质;痰湿质;气滞质;气虚质;肺气虚质;肾气虚质;脾气虚质";
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
            <Flex justify="center">7-12岁儿童体质辨识报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item >
          尊敬的（监护人）先生/女士：
          <WhiteSpace/>
          <div style={{ 'white-space': 'normal', 'text-indent': '2em' }}>
                本测验基于中医理论，结合7-12岁儿童心身特征，由中国中医科学院中医临床基础医学研究所建立而成，为标准化的中医儿童体质测量工具，可参考此结果了解孩子的体质状况，在医生的指导下进行个体化调养。{hint}
          </div>
          
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          体质结果：
          <WhiteSpace/>
          <div style={{ 'white-space': 'normal', 'text-indent': '2em' }}>
                {conclusion},{explanation}
          </div>
          
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          主要表现：
          <WhiteSpace/>
          <div style={{ 'white-space': 'normal', 'text-indent': '2em' }}>
                {performance}
          </div>
          <WhiteSpace size="lg" />
        </Item>
        {scores? 
          scores.split(';').map((c, i) => {
            list = scores.split(';');
          }): ''
         }
         {report1? 
          report1.split(';').map((c, i) => {
            scoreLabels = report1.split(';');
          }): ''
         }
        <Item >
         辨识分析图：
         <WhiteSpace/>
          <Brief>
            {scoreLabels &&
              scoreLabels.map((percent, i) => (
                <div key={i}>
                  <div style={{ display: 'inline-block', width: "0.8rem" }}>
                    {scoreLabels[i]}
                  </div>
                  <div style={{ display: 'inline-block', width: "1.5rem" }}>
                    <Progress position="normal" unfilled="hide" percent={list[i]*100} />
                  </div>
                  <span style={{ marginLeft: "0.1rem", color: '#aa4929' }}>
                    {((Math.round(list[i]*10000))/100).toFixed(2)}%
                  </span>
                  <WhiteSpace />
                </div>
              ))}
          </Brief>
        </Item>
      </List>
    </WingBlank>
  );
}
