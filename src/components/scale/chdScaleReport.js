import React from 'react';
import { Flex, WingBlank, WhiteSpace, List, Progress } from 'antd-mobile';

const { Item, Item: { Brief } } = List;
let scoreLabels = [], list = [];

export default function chdScaleReport({ report1, report2, report3, report4, report5, ...rest }) {
  window.scrollTo(0, 0);
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
            <Flex justify="center">儿童体质辨识报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item >
          体质类型：
          <div style={{ 'text-indent': '2em'}}>
            {report3}
          </div>
          <div style={{ 'white-space': 'normal', 'text-indent': '2em' }}>
            {report4}
          </div>
          
          <WhiteSpace size="lg" />
        </Item>
        {report2? 
          report2.split(';').map((c, i) => {
            list = report2.split(';');
          }): ''
         }
         {report1? 
          report1.split(';').map((c, i) => {
            scoreLabels = report1.split(';');
          }): ''
         }
        <Item >
          体质得分：
          <Brief>
            {scoreLabels &&
              scoreLabels.map((percent, i) => (
                <div key={i}>
                  <div style={{ display: 'inline-block', width: "0.8rem" }}>
                    {scoreLabels[i]}
                  </div>
                  <div style={{ display: 'inline-block', width: "1.5rem" }}>
                    <Progress position="normal" unfilled="hide" percent={list[i]*100} />
                  </div>
                  <span style={{ marginLeft: "0.1rem", color: '#aa4929' }}>
                    {((Math.round(list[i]*10000))/100).toFixed(2)}%
                  </span>
                  <WhiteSpace />
                </div>
              ))}
          </Brief>
        </Item>
        {rest.children}
      </List>
    </WingBlank>
  );
}
