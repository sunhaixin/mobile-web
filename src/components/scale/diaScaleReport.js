import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import { SCALE_DIA_VALUE} from 'const';

const { Item } = List;

export default function diaScaleReport(props) {
    window.scrollTo(0, 0);
    const { PatientValue } = props;
    let normal = [], tongues = [], main = [];
    if(PatientValue){
      if (PatientValue.patientValue1 && PatientValue.patientValue1.indexOf("|") == -1) {
        SCALE_DIA_VALUE.filter((y) =>{PatientValue.patientValue1.split(',').filter((x) =>{
        if (y.id == x) {main.push(y)}})})
        main.forEach((z) =>{ 
          if(z.id>=202 && z.id<=219){
            tongues.push(z.title);
          }else{
            normal.push(z.title);
          }
        })
      }
    }

  return (
       <WingBlank size="md">
      <List wrap>
        <Item>
            <Flex justify="center">糖尿病辅助辨证报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item>
          症状
            <WhiteSpace/>
            <div style={{whiteSpace: "normal", textIndent: "2em", color: "#888"}}>{[...new Set(normal)].join(";")};{[...new Set(tongues)].join(";")}</div>
          <WhiteSpace size="lg" />
        </Item>
        {PatientValue.allTP == "" || PatientValue.allTP == "尚未进行客观化信息采集" ? "":
          <Item>
          舌面脉客观化信息
            <WhiteSpace/>
            <div style={{whiteSpace: "normal", textIndent: "2em"}}>
            {PatientValue && PatientValue.allTP == null? "尚未完成客观化信息采集":
            <textarea 
            disabled={true}
            rows="4" cols="40" 
            style={{ fontSize: "13px",color: "#888", lineHeight: 2, background: '#fff',border: 'none'}}
            value= {PatientValue.allTP}
            />}
            </div>
        </Item>}
        <Item>
          辨证分型
            <WhiteSpace/>
            <div style={{whiteSpace: "normal", textIndent: "2em"}}>{props.report1}</div>
          <WhiteSpace size="lg" />
        </Item>
      </List>
    </WingBlank>
  );
}