import React from 'react';
import { Flex, WingBlank, WhiteSpace, List  } from 'antd-mobile';
import styles from './scalereport.css';

const { Item, Item: { Brief } } = List;

export default function faceScaleReport(props) {
  window.scrollTo(0, 0);
  let Zygomatic, Orbital, Image1, Image2;
  if(props.zygomatic == ""){
    Zygomatic = "无";
  }else{
    Zygomatic = props.zygomatic;
  }
  if(props.orbital == ""){
    Orbital = "无";
  }else{
    Orbital = props.orbital;
  }
  if (props.image1 && props.image1.indexOf("//",6) !=-1) {
    Image1 = props.image1.replace("//","/");
  }
  if (props.image2 && props.image2.indexOf("//",6) !=-1) {
    Image2 = props.image2.replace("//","/");
  }
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
            <Flex justify="center">面色数据采集报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item >
          面色照片：
          <div className={styles.imgbrief}> 
            <img src={Image1 || props.image1? props.image1.toLocaleLowerCase(): ""} />
            <img src={Image2 || props.image2? props.image2.toLocaleLowerCase(): ""} />
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          面色唇色参数：
          <Brief>
             <div className={styles.mytablewrap}>
              <table  className={styles.mytable} >
                <tr>
                  <td>色彩空间</td>
                  <td>测量标准</td>
                  <td>总体</td>
                  <td>额头</td>
                  <td>脸颊(左)</td>
                  <td>脸颊(右)</td>
                  <td>眼眶</td>
                  <td>鼻</td>
                  <td>唇色</td>
                </tr>
                <tr>
                  <td className={styles.labl}></td>
                  <td>L</td>
                  <td>{props.zongtiL}</td>
                  <td>{props.etouL}</td>
                  <td>{props.lLianJiaL}</td>
                  <td>{props.rLianJiaL}</td>
                  <td>{props.yanKuangL}</td>
                  <td>{props.biL}</td>
                  <td>{props.chunSeL}</td>
                </tr>
                <tr>
                  <td className={styles.laba}>Lab</td>
                  <td>a</td>
                  <td>{props.zongtiA}</td>
                  <td>{props.etouA}</td>
                  <td>{props.lLianJiaA}</td>
                  <td>{props.rLianJiaA}</td>
                  <td>{props.yanKuangA}</td>
                  <td>{props.biA}</td>
                  <td>{props.chunSeA}</td>
                </tr>
                <tr>
                  <td className={styles.labb}></td>
                  <td>b</td>
                  <td>{props.zongtiB}</td>
                  <td>{props.etouB}</td>
                  <td>{props.lLianJiaB}</td>
                  <td>{props.rLianJiaB}</td>
                  <td>{props.yanKuangB}</td>
                  <td>{props.biB}</td>
                  <td>{props.chunSeB}</td>
                </tr>

                <tr>
                  <td>颜色类别</td>
                  <td></td>
                 <td>{props.zongtiC}</td>
                  <td>{props.etouC}</td>
                  <td>{props.lLianJiaC}</td>
                  <td>{props.rLianJiaC}</td>
                  <td>{props.yanKuangC}</td>
                  <td>{props.biC}</td>
                  <td>{props.chunSeC}</td>
                </tr>
              </table>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          面色信息输出：
          <Brief>
          <div className={styles.mytablewrap}>
              <table  className={styles.mytable} >
                <tr>
                  <td>面色</td>
                  <td> <div className={styles.tezheng}>
                    <div className={styles.zheng}>
                      局部特征
                    </div>
                    <div className={styles.bian}>
                      两颧红
                    </div>
                    <div className={styles.bian}>
                      眼眶黑
                    </div>
                  </div></td>
                  <td>光泽</td>
                  <td>唇色</td>
                </tr>
                <tr>
                  <td>{props.zongtiC}</td>
                  <td>
                    <div className={styles.tezhengs}>
                      <div className={styles.bians}>{Zygomatic}</div>
                      <div className={styles.bians}>{Orbital}</div>
                    </div>
                  </td>
                  <td>{props.luster}</td>
                  <td>{props.chunSeC}</td>
                </tr>
              </table>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
      </List>
    </WingBlank>
  );
}
