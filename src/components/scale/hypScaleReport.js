import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';

const { Item } = List;

export default function hypScaleReport(props) {
  window.scrollTo(0, 0);
  let styles = {whiteSpace: "normal", textIndent: "2em"};
  const { PatientValue } = props;
  return (
    <WingBlank size="md">
      <List wrap>
         <Item>
            <Flex justify="center">高血压病辅助辨证报告</Flex>
            <WhiteSpace/>
        </Item>
        <Item> 
          主诉
            <WhiteSpace/>
            <div style={styles}>{PatientValue? <div>{PatientValue.patientValue2}</div> : ""}</div>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          症状
            <WhiteSpace/>
            <div style={styles}>{PatientValue? <div>{PatientValue.patientValue1};{PatientValue.patientValue3}</div>: ""}</div>
          <WhiteSpace size="lg" />
        </Item>
        {PatientValue.allTP == ""|| PatientValue.allTP == "尚未进行客观化信息采集" ? "":
          <Item>
          舌面脉客观化信息
            <WhiteSpace/>
            <div style={styles}>
            {PatientValue && PatientValue.allTP == null? "尚未完成客观化信息采集":
            <textarea 
            disabled={true}
            rows="4" cols="40" 
            style={{ fontSize: "13px",color: "#888", lineHeight: 2, background: '#fff',border: 'none'}}
            value= {PatientValue.allTP}
            />}
            </div>
        </Item>}
        <Item>
          辨证分型
            <WhiteSpace/>
            <div style={styles}>{props.report1}</div>
          <WhiteSpace size="lg" />
        </Item>
      </List>
    </WingBlank>
  );
}
