import ScaleComment from './ScaleComment';

import ScaleReport from './ScaleReport';
import PreScaleReport from './preScaleReport';
import PartumScaleReport from './partumScaleReport';
import DiaScaleReport from './diaScaleReport';
import TcmScaleReport from './tcmScaleReport';
import XinScaleReport from './xinScaleReport';
import HypScaleReport from './hypScaleReport';
import ChdScaleReport from './chdScaleReport';
import Chd2ScaleReport from './chd2ScaleReport';
import PrePreScaleReport from './prepreScaleReport';
import WzxyScaleReport from './wzxyScaleReport';

import ScalePrograme from './ScalePrograme';
import ScaleProgrameSelect from './ScaleProgrameSelect';

import AlphaReport from './changScaleReport';
import PulseReport from './pulseScaleReport';
import TongueReport from './tongueScaleReport';
import FaceReport from './faceScaleReport';

export { 
	ScaleReport, 
	PreScaleReport,
	PartumScaleReport,
	DiaScaleReport, 
	TcmScaleReport,
	XinScaleReport, 
	HypScaleReport, 
	ChdScaleReport,
	Chd2ScaleReport,
	PrePreScaleReport,
	WzxyScaleReport,

	ScalePrograme, 
	ScaleProgrameSelect, 
	ScaleComment,

	AlphaReport,
	FaceReport,
	PulseReport,
	TongueReport,
};
