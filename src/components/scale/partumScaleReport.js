import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import styles from './scalereport.css';
import { SCALE_PARTUM_VALUE, Array_itmes } from 'const';

const { Item, Item: { Brief } } = List;
let scoreLabels = [];

export default function partumScaleReport({ report1, report2, report3, PatientValue, ...rest }) {
  window.scrollTo(0, 0);
  let list = [0,0], percent = [], value;
  if(PatientValue){
     let arr = PatientValue.patientValue;
     if(arr && arr.length !=0){
       if(arr.split("|")[5]){
        value = Array_itmes(PatientValue, arr.split("|")[5].split(";"), SCALE_PARTUM_VALUE)
       }
     }
  }
  return (
    <WingBlank size="md">
      <List wrap>
         <Item>
          <Flex justify="center">产后女性健康状态测评报告</Flex>
          <WhiteSpace/>
        </Item>
        <Item >
          症状：
          <WhiteSpace/>
          <div className={styles.white}> 
             {value}
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item multipleLine wrap>
          脏腑健康状态评估结果：
          <WhiteSpace/>
            <div className={styles.white}>
            {report1}
            </div>
          <WhiteSpace size="lg" />
        </Item>
        {report2 ? report2.split(';').map((c, i) => {
              list[i] = c; 
              scoreLabels = list[0]==0?0:list[0].split(",");
              percent = list[1]==0?0:list[1].split(",");

          }): ''
        }
         <Item >
          体质得分：
          <WhiteSpace/>
          <Brief>
            {scoreLabels &&
              scoreLabels.map((c, i) => (
                <div key={i}>
                  <div style={{ display: 'inline-block', width: "0.8rem" }}>
                    {scoreLabels[i]}
                  </div>
                   <div style={{ display: 'inline-block', width: "1.5rem", height:"0.1rem", background:'#ddd'}}>
                     {percent[i] >= 50 && percent[i] <=75? (
                        <div style={{ height: '100%', width: percent[i], background:'#D97A60', float: 'left' }}></div>
                      ):""}
                     {percent[i] >= 25 && percent[i] <=49? (
                        <div style={{ height: '100%', width: percent[i], background:'#D9AC92', float: 'left' }}></div>
                      ):""}
                     {percent[i] >75? (
                        <div style={{ height: '100%', width: percent[i], background:'#A7482E', float: 'left' }}></div>
                      ):""}
                     {percent[i] <25? (
                        <div style={{ height: '100%', width: percent[i], background:'', float: 'left' }}></div>
                      ):""}

                  </div>
                  <span style={{ marginLeft: "0.2rem", color: '#aa4929' }}>
                    {percent[i]}
                  </span>
                  <WhiteSpace />
                </div>
              ))
            }
          </Brief>
          <div className={styles.content}>注：分值越高代表对人的影响越大，不同项目之间的分值比较无意义。</div>
           <Brief> 
              <div>
                <div className={styles.feizhi} style={{background: '#D9AC92'}}></div>&nbsp;浅棕色 表示轻度影响。
              </div>
              <div>
                <div className={styles.feizhi} style={{background: '#D97A60'}}></div>&nbsp;棕色 表示中度影响。
              </div>
              <div>
                <div className={styles.feizhi} style={{background: '#A7482E'}}></div>&nbsp;深棕色  表示影响较大。
              </div>
           </Brief>
        </Item>
        {rest.children}
      </List>
    </WingBlank>
  );
}
