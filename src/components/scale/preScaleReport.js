import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import styles from './scalereport.css';
import { SCALE_PRE_VALUE, Array_itmes } from 'const';

const { Item, Item: { Brief } } = List;
let scoreLabels = [], percent = [];
export default function preScaleReport({ report1, report2, report3, PatientValue, ...rest }) {
  window.scrollTo(0, 0);
  let list = [0,0], value;
  if(PatientValue){
    let a= PatientValue.patientValue;
     if(a && a.split("|").length == 7){
        let arr = a.split("|")[6].split(",");
        value = Array_itmes(a,arr,SCALE_PRE_VALUE);
     }
  }
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">孕期女性健康状态测评报告</Flex>
          <WhiteSpace/>
        </Item>
        <Item >
          症状：
          <div className={styles.white}>
            {value}    
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item multipleLine wrap>
          脏腑健康状态评估结果：
            <div className={styles.white}>
            {report1}
            </div>
          <WhiteSpace size="lg" />
        </Item>
        {report2? report2.split(';').map((c, i) => {
            list[i] = c.split(":");
            scoreLabels[i] = list[i][0]==0?0:list[i][0];
            percent[i] = list[i][1]==0?0:list[i][1];
          })
          : ''
        }
        <Item >
          体质得分：
          <Brief>
            {scoreLabels &&
              scoreLabels.map((c, i) => (

                <div key={i}>
                  <div style={{ display: 'inline-block', width: "0.8rem" }}>
                    {scoreLabels[i]}
                  </div>
                 <div style={{ display: 'inline-block', width: "1.5rem", height:"0.1rem", background:'#ddd'}}>
                     {percent[i] >= 30 && percent[i] <=49? (<div className={styles.pref} style={{  width: percent[i], background:'#D9AC92'}}></div>):""}
                     {percent[i] >= 50 && percent[i] <70? (<div className={styles.pref} style={{   width: percent[i], background:'#D97A60'}}></div>):""}
                     {percent[i] >=70? (<div className={styles.pref} style={{width: percent[i], background:'#A7482E'}}></div>):""}
                     {percent[i] <=30? (<div className={styles.pref} style={{width: percent[i], background:''}}></div>):""}

                  </div>
                  <span style={{ marginLeft: "0.2rem", color: '#aa4929' }}>
                    {percent[i]}
                  </span>
                  <WhiteSpace />
                </div>
              ))
            }
          </Brief>
          <div className={styles.content}>注：分值越高代表对人的影响越大，不同项目之间的分值比较无意义。</div>
           <Brief> 
              <div>
                <div className={styles.feizhi} style={{background: '#D9AC92'}}></div>_浅棕色 表示轻度影响。
              </div>
              <div>
                <div className={styles.feizhi} style={{background: '#D97A60'}}></div>_棕色 表示中度影响。
              </div>
              <div>
                <div className={styles.feizhi} style={{background: '#A7482E'}}></div>_深棕色  表示影响较大。
              </div>
           </Brief>
        </Item>
        {rest.children}
      </List>
    </WingBlank>
  );
}
