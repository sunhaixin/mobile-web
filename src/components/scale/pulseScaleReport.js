import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import styles from './scalereport.css';

const { Item, Item: { Brief } } = List;

export default function pulseScaleReport(props) {
  window.scrollTo(0, 0);
  let A,H31,H41,H51,T1T,W1t,W2t,T54;
  let Ar,H31r,H41r,H51r,T1Tr,W1tr,W2tr,T54r;
  let jin,liu, jinR,liuR;
  if (props.t) {
    A = parseInt((props.ad+props.ass)*1000)/1000;
    T54 = parseInt(props.t5/props.t4*1000)/1000;
    H31 = parseInt(props.h3/props.h1*1000)/1000;
    H41 = parseInt(props.h4/props.h1*1000)/1000;
    H51 = parseInt(props.h5/props.h1*1000)/1000;
    T1T = parseInt(props.t1/props.t*1000)/1000;
    W1t = parseInt(props.w1/props.t*1000)/1000;
    W2t = parseInt(props.w2/props.t*1000)/1000;
  }
  if (props.tr) {
    Ar = parseInt((props.adR+props.assR)*1000)/1000;
    T54r = parseInt(props.t5R/props.t4R*1000)/1000;
    H31r = parseInt(props.h3R/props.h1R*1000)/1000;
    H41r = parseInt(props.h4R/props.h1R*1000)/1000;
    H51r = parseInt(props.h5R/props.h1R*1000)/1000;
    T1Tr = parseInt(props.t1R/props.tr*1000)/1000;
    W1tr = parseInt(props.w1R/props.tr*1000)/1000;
    W2tr = parseInt(props.w2R/props.tr*1000)/1000;
  }
  if(props.pulseName){
    if(props.pulseName.indexOf("弦") != -1){
      jin = "弦";
    } else if(props.pulseName.indexOf("紧") != -1){
      jin = "紧";
    } else if(props.pulseName == ""){
      jin = "";
    } else {
      jin = "无弦、紧特征"
    }
    if(props.pulseName.indexOf("滑") != -1){
      liu = "滑";
    } else if(props.pulseName.indexOf("涩") != -1){
      liu = "涩";
    } else if(props.pulseName == ""){
      liu = "";
    } else {
      liu = "无滑、涩特征"
    }
  }

  if(props.pulseNameR){
    if(props.pulseNameR.indexOf("弦") != -1){
      jinR = "弦";
    } else if(props.pulseNameR.indexOf("紧") != -1){
      jinR = "紧";
    } else if(props.pulseNameR == ""){
      jinR = "";
    } else {
      jinR = "无弦、紧特征"
    }
    if(props.pulseNameR.indexOf("滑") != -1){
      liuR = "滑";
    } else if(props.pulseNameR.indexOf("涩") != -1){
      liuR = "涩";
    } else if(props.pulseNameR == ""){
      liuR = "";
    } else {
      liuR = "无滑、涩特征"
    }
  }
  
  let righthards, lefthards, t4s, t4sr; 
  if(props.h5R){
    righthards = props.h5R.toFixed(3);
  }
  if(props.h5){
    lefthards  = props.h5.toFixed(3);
  }
  if(props.t){
    t4s = props.t.toFixed(3);
  }
  if(props.tr){
    t4sr = props.tr.toFixed(3);
  }
  let ImageSubsetion1,ImageSubsetion2,ImageTrend1,ImageTrend2,ImageRhythm1,ImageRhythm2,ImageBest1,ImageBest2;

  if(props.imageSubsetion1 && props.imageSubsetion1.indexOf("//",6) !=-1){
    ImageSubsetion1 = props.imageSubsetion1.replace("//","/");
  }
  if(props.imageSubsetion2 && props.imageSubsetion2.indexOf("//",6) !=-1){
    ImageSubsetion2 = props.imageSubsetion2.replace("//","/");
  }

  if(props.imageTrend1 && props.imageTrend1.indexOf("//",6) !=-1){
    ImageTrend1 = props.imageTrend1.replace("//","/");
  }
  if(props.imageTrend2 && props.imageTrend2.indexOf("//",6) !=-1){
    ImageTrend2 = props.imageTrend2.replace("//","/");
  }
  if(props.imageRhythm1 && props.imageRhythm1.indexOf("//",6) !=-1){
    ImageRhythm1 = props.imageRhythm1.replace("//","/");
  }
  if(props.imageRhythm2 && props.imageRhythm2.indexOf("//",6) !=-1){
    ImageRhythm2 = props.imageRhythm2.replace("//","/");
  }
  if(props.imageBest1 && props.imageBest1.indexOf("//",6) !=-1){
    ImageBest1 = props.imageBest1.replace("//","/");
  }
  if(props.imageBest2 && props.imageBest2.indexOf("//",6) !=-1){
    ImageBest2 = props.imageBest2.replace("//","/");
  }
  
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">脉象数据采集报告</Flex>
          <WhiteSpace/>
        </Item>
      </List>
      {props.hands?<List wrap>
        <Item>
          <Flex justify="center">{props.hands}脉象数据采集报告</Flex>
        </Item>
        <Item>
          {props.hands}分段压脉图：
          <div className={styles.imgbrief}>
            <img src={ImageSubsetion1 || props.imageSubsetion1? props.imageSubsetion1.toLocaleLowerCase() : ""} />
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          {props.hands}脉象p-h趋势图：
          <div className={styles.imgbrief}>
            <img src={ImageTrend1 || props.imageTrend1? props.imageTrend1.toLocaleLowerCase(): ""} />
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          {props.hands}连续脉图（30秒）：
          <div className={styles.imgbrief}>
            <img src={ImageRhythm1 || props.imageRhythm1? props.imageRhythm1.toLocaleLowerCase(): ""} />
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item >
          {props.hands}最佳压力脉图：
          <div className={styles.imgbrief}>
            <img src={ImageBest1 || props.imageBest1? props.imageBest1.toLocaleLowerCase() : ""} />
          </div>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          {props.hands}最佳脉图参数：
          <Brief>
            
            <div className={styles.mytablewrap}>
              <table className={styles.mytable} >
                <tr>
                  <td>t1(s)</td>
                  <td>{props.t1}</td>
                  <td>h1(g)</td>
                  <td>{props.h1}</td>
                  <td>As(g*s)</td>
                  <td>{props.ass}</td>
                </tr>
                
                <tr>
                  <td>t2(s)</td>
                  <td>{props.t2}</td>
                  <td>h2(g)</td>
                  <td>{props.h2}</td>
                  <td>Ad(g*s)</td>
                  <td>{props.ad}</td>
                </tr>
                <tr>
                  <td>t3(s)</td>
                  <td>{props.t3}</td>
                  <td>h3(g)</td>
                  <td>{props.h3}</td>
                  <td>A(g*s)</td>
                  <td>{A}</td>
                </tr>
                <tr>
                  <td>t4(s)</td>
                  <td>{props.t4}</td>
                  <td>h4(g)</td>
                  <td>{props.h4}</td>
                  <td>t(s)</td>
                  <td>{t4s}</td>
                </tr>

                <tr>
                  <td>t5(s)</td>
                  <td>{props.t5}</td>
                  <td>h5(g)</td>
                  <td>{lefthards}</td>
                  <td>t5/t4</td>
                  <td>{T54}</td>
                </tr>
                <tr>
                  <td>h3/h1</td>
                  <td>{H31}</td>
                  <td>h4/h1</td>
                  <td>{H41}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>h5/h1</td>
                  <td>{H51}</td>
                  <td>t1/t</td>
                  <td>{T1T}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>w1/t</td>
                  <td>{W1t}</td>
                  <td>w2/t</td>
                  <td>{W2t}</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>w1</td>
                  <td>{props.w1}</td>
                  <td>w2</td>
                  <td>{props.w2}</td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </div>

          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          {props.hands}脉象信息输出：
          <Brief>
            <div className={styles.mytablewrap}>
              <table  className={styles.mytable} >
                <tr>
                  <td></td>
                  <td>脉位</td>
                  <td>脉率(次／分钟)</td>
                  <td>脉节律</td>
                  <td>脉力</td>
                  <td>紧张度</td>
                  <td>流利度</td>
                  <td>脉名提示</td>
                </tr>
                
                <tr>
                  <td>{props.hands}关部</td>
                  <td>{props.pulseSite}</td>
                  <td>{props.maiLv? props.maiLv.replace(/[^0-9]/ig,""):""}</td>
                  <td>{props.pulseNumber}</td>
                  <td>{props.maiLi}</td>
                  <td>{jin}</td>
                  <td>{liu}</td>
                  <td>{props.pulseName}</td>
                </tr>
                
               
              </table>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        
            </List>:""}
      <WhiteSpace size="lg" />
      {props.handsR?<List wrap>
              <Item>
                <Flex justify="center">{props.handsR}脉象数据采集报告</Flex>
              </Item>
              <Item>
                {props.handsR}分段压脉图：
                <div className={styles.imgbrief}>
                  <img src={ImageSubsetion2 || props.imageSubsetion2? props.imageSubsetion2.toLocaleLowerCase(): ""} />
                </div>
                <WhiteSpace size="lg" />
              </Item>
              <Item>
                {props.handsR}脉象p-h趋势图：
                <div className={styles.imgbrief}>
                  <img src={ImageTrend2 || props.imageTrend2? props.imageTrend2.toLocaleLowerCase(): ""} />
                </div>
                <WhiteSpace size="lg" />
              </Item>
              <Item>
                {props.handsR}连续脉图（30秒）：
                <div className={styles.imgbrief}>
                  <img src={ImageRhythm2 || props.imageRhythm2? props.imageRhythm2.toLocaleLowerCase(): ""} />
                </div>
                <WhiteSpace size="lg" />
              </Item>
              <Item>
                {props.handsR}最佳压力脉图：
                <div className={styles.imgbrief}>
                  <img src={ImageBest2 || props.imageBest2? props.imageBest2.toLocaleLowerCase(): ""} />
                </div>
                <WhiteSpace size="lg" />
              </Item>
              <Item>
                {props.handsR}最佳脉图参数：
                <Brief>
                  
                  <div className={styles.mytablewrap}>
                    <table  className={styles.mytable} >
                      <tr>
                        <td>t1(s)</td>
                        <td>{props.t1R}</td>
                        <td>h1(g)</td>
                        <td>{props.h1R}</td>
                        <td>As(g*s)</td>
                        <td>{props.assR}</td>
                      </tr>
                      
                      <tr>
                        <td>t2(s)</td>
                        <td>{props.t2R}</td>
                        <td>h2(g)</td>
                        <td>{props.h2R}</td>
                        <td>Ad(g*s)</td>
                        <td>{props.adR}</td>
                      </tr>
                      <tr>
                        <td>t3(s)</td>
                        <td>{props.t3R}</td>
                        <td>h3(g)</td>
                        <td>{props.h3R}</td>
                        <td>A(g*s)</td>
                        <td>{Ar}</td>
                      </tr>
                      <tr>
                        <td>t4(s)</td>
                        <td>{props.t4R}</td>
                        <td>h4(g)</td>
                        <td>{props.h4R}</td>
                        <td>t(s)</td>
                        <td>{t4sr}</td>
                      </tr>
      
                      <tr>
                        <td>t5(s)</td>
                        <td>{props.t5R}</td>
                        <td>h5(g)</td>
                        <td>{righthards}</td>
                        <td>t5/t4</td>
                        <td>{T54r}</td>
                      </tr>
                      <tr>
                        <td>h3/h1</td>
                        <td>{H31r}</td>
                        <td>h4/h1</td>
                        <td>{H41r}</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>h5/h1</td>
                        <td>{H51r}</td>
                        <td>t1/t</td>
                        <td>{T1Tr}</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>w1/t</td>
                        <td>{W1tr}</td>
                        <td>w2/t</td>
                        <td>{W2tr}</td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>w1</td>
                        <td>{props.w1R}</td>
                        <td>w2</td>
                        <td>{props.w2R}</td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>
                  </div>
      
                </Brief>
                <WhiteSpace size="lg" />
              </Item>
              <Item>
                {props.handsR}脉象信息输出：
                <Brief>
                  <div className={styles.mytablewrap}>
                    <table  className={styles.mytable} >
                      <tr>
                        <td></td>
                        <td>脉位</td>
                        <td>脉率(次／分钟)</td>
                        <td>脉节律</td>
                        <td>脉力</td>
                        <td>紧张度</td>
                        <td>流利度</td>
                        <td>脉名提示</td>
                      </tr>
                      
                      <tr>
                        <td>{props.handsR}关部</td>
                        <td>{props.pulseSiteR}</td>
                        <td>{props.maiLvR? props.maiLvR.replace(/[^0-9]/ig,""):""}</td>
                        <td>{props.pulseNumberR}</td>
                        <td>{props.maiLiR}</td>
                        <td>{jinR}</td>
                        <td>{liuR}</td>
                        <td>{props.pulseNameR}</td>
                      </tr>
                      
                     
                    </table>
                  </div>
                </Brief>
                <WhiteSpace size="lg" />
              </Item>           
      </List>:""}
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}
