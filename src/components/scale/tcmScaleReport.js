import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import { SCALE_TCM_VALUE } from 'const';

const { Item, Item: { Brief } } = List;

export default function tcmScaleReport(props) {
  window.scrollTo(0, 0);
  const { PatientValue, PatientValue:{ patientValue1 }, title } = props;
  let a = [], b = [], main = [];
  if (patientValue1 && patientValue1.indexOf("|") == -1) {
    SCALE_TCM_VALUE.filter((y) =>{patientValue1.split(',').filter((x) =>{
      if (y.id == x) {main.push(y)}})})
      main.forEach((z) =>{ 
        if(z.id>=102 && z.id<=144){
          b.push(z.title);
        }else{
          a.push(z.title);
        }
      })
  }
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">{title}</Flex>
          <WhiteSpace/>
        </Item>
        <Item >
          症状
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
             {a.join(";")};{b.join(";")}    
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        {PatientValue.allTP == ""|| PatientValue.allTP == "尚未进行客观化信息采集" ? "":
          <Item>
          舌面脉客观化信息
            <WhiteSpace/>
            <div style={{whiteSpace: "normal", textIndent: "2em"}}>
            {PatientValue && PatientValue.allTP == null? "尚未完成客观化信息采集":
            <textarea 
            disabled={true}
            rows="4" cols="40" 
            style={{ fontSize: "13px",color: "#888", lineHeight: 2, background: '#fff',border: 'none'}}
            value= {PatientValue.allTP}
            />}
            </div>
        </Item>}
        <Item>
          体质辨识结果
          <WhiteSpace/>
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
             {props.report2}
          </Brief>
          <WhiteSpace/>
        </Item>
        <Item >
          中医辨证结果
          <WhiteSpace/>
          <Brief style={{whiteSpace: "normal",textIndent: "2em"}}>
             {props.report1}
          </Brief>
          <WhiteSpace/>
        </Item>
       
      </List>
    </WingBlank>
  );
}
