import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import styles from './scalereport.css';

const { Item, Item: { Brief } } = List;

export default function tongueScaleReport(props) {
  window.scrollTo(0, 0);
  let Ecchymosis,Thickness,Exfoliation,CurdygreasinessNi,CurdygreasinessFu,Fatthin,Indentation,Pepeckneedling,Crack;
  let bianjian, SheS, tongueStyle = { borderLeft: 'none'};
  
  if (props.ecchymosis) {   
    Thickness=props.thickness.split("|")[0];
    Exfoliation=props.exfoliation.split("|")[0];
    Indentation=props.indentation.split("|")[0].substr(0,1);
    Pepeckneedling=props.pepeckNeedling.split("|")[0].substr(0,1);
    Crack=props.crack.split("|")[0].substr(0,1);

    if (props.quanSheC.indexOf("边尖红") != -1) {
      bianjian = "有";
      SheS = props.quanSheC.split(",")[0]
    }else {
      bianjian = "无";
      SheS = props.quanSheC
    }
    if (props.fatThin){
      if(props.fatThin.split("|")[0].indexOf("胖瘦") != -1){
        Fatthin = props.fatThin.split("|")[0].split("胖瘦")[1];
      }else{
        Fatthin = props.fatThin.split("|")[0];
      }
    }

    if(props.ecchymosis.split("|")[0]== "无瘀点"){
        Ecchymosis = "无";
    }else{
        Ecchymosis = "有";
    }
    if(props.curdyGreasiness){
      if(props.curdyGreasiness.split("|")[0].indexOf("腻") != -1){
         CurdygreasinessNi = "有";
         CurdygreasinessFu = "无";
      }else if(props.curdyGreasiness.split("|")[0].indexOf("腐") != -1){
         CurdygreasinessNi = "无";
         CurdygreasinessFu = "有";
      }else if(props.curdyGreasiness.split("|")[0].indexOf("无") != -1){
         CurdygreasinessNi = "无";
         CurdygreasinessFu = "无";
      }else if(props.curdyGreasiness.split("|")[0].indexOf("腻腐") != -1){
         CurdygreasinessNi = "有";
         CurdygreasinessFu = "有";
      }
    }
  }
  let Image1, Image2;
  if (props.image1 && props.image1.indexOf("//",6) !=-1) {
    Image1 = props.image1.replace("//","/");
  }
  if (props.image2 && props.image2.indexOf("//",6) !=-1) {
    Image2 = props.image2.replace("//","/");
  }
  
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">舌象数据采集报告</Flex>
          <WhiteSpace/>
        </Item>
        <Item >
          舌象照片：
          <div className={styles.imgbrief}>
              <div>
                  <img src={ Image1 || props.image1? props.image1.toLocaleLowerCase() : ""} />
              </div>  
              <div>
                  <img src={ Image2 || props.image2? props.image2.toLocaleLowerCase() : ""} /> 
              </div> 
          </div>
          <WhiteSpace size="lg" />
        </Item>
        
        <Item>
          舌色参数：
          <Brief>
            <div className={styles.mytablewrap}>
              <table  className={styles.mytable} >
                <tr>
                  <td>色彩空间</td>
                  <td>测量标准</td>
                  <td>全舌</td>
                  <td>舌中</td>
                  <td>舌根</td>
                  <td>舌边(左)</td>
                  <td>舌边(右)</td>
                  <td>舌尖</td>
                </tr>
                
                <tr>
                  <td className={styles.labl}></td>
                  <td>L</td>
                  <td>{props.quanSheL}</td>
                  <td>{props.sheZhongL}</td>
                  <td>{props.sheGenL}</td>
                  <td>{props.lSheBianL}</td>
                  <td>{props.rSheBianL}</td>
                  <td>{props.sheJianL}</td>
                </tr>
                <tr>
                  <td className={styles.laba}>Lab</td>
                  <td>a</td>
                  <td>{props.quanSheA}</td>
                  <td>{props.sheZhongA}</td>
                  <td>{props.sheGenA}</td>
                  <td>{props.lSheBianA}</td>
                  <td>{props.rSheBianA}</td>
                  <td>{props.sheJianA}</td>
                </tr>
                <tr>
                  <td className={styles.labb}></td>
                  <td>b</td>
                  <td>{props.quanSheB}</td>
                  <td>{props.sheZhongB}</td>
                  <td>{props.sheGenB}</td>
                  <td>{props.lSheBianB}</td>
                  <td>{props.rSheBianB}</td>
                  <td>{props.sheJianB}</td>
                </tr>

                <tr>
                  <td>舌色类别</td>
                  <td></td>
                  <td>{props.quanSheC}</td>
                  <td>{props.sheZhongC}</td>
                  <td>{props.sheGenC}</td>
                  <td>{props.lSheBianC}</td>
                  <td>{props.rSheBianC}</td>
                  <td>{props.sheJianC}</td>
                </tr>
              </table>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          苔色参数：
          <Brief>
            <div className={styles.mytablewrap}>
              <table  className={styles.mytable} >
                <tr>
                  <td>色彩空间</td>
                  <td>测量标准</td>
                  <td>全舌</td>
                  <td>舌中</td>
                  <td>舌根</td>
                  <td>舌边(左)</td>
                  <td>舌边(右)</td>
                  <td>舌尖</td>
                </tr>
                
                <tr>
                  <td className={styles.labl}></td>
                  <td>L</td>
                  <td>{props.coatQuanSheL}</td>
                  <td>{props.coatSheZhongL}</td>
                  <td>{props.coatSheGenL}</td>
                  <td>{props.coatLSheBianL}</td>
                  <td>{props.coatRSheBianL}</td>
                  <td>{props.coatSheJianL}</td>
                </tr>
                <tr>
                  <td className={styles.laba}>Lab</td>
                  <td>a</td>
                  <td>{props.coatQuanSheA}</td>
                  <td>{props.coatSheZhongA}</td>
                  <td>{props.coatSheGenA}</td>
                  <td>{props.coatLSheBianA}</td>
                  <td>{props.coatRSheBianA}</td>
                  <td>{props.coatSheJianA}</td>
                </tr>
                <tr>
                  <td className={styles.labb}></td>
                  <td>b</td>
                  <td>{props.coatQuanSheB}</td>
                  <td>{props.coatSheZhongB}</td>
                  <td>{props.coatSheGenB}</td>
                  <td>{props.coatLSheBianB}</td>
                  <td>{props.coatRSheBianB}</td>
                  <td>{props.coatSheJianB}</td>
                </tr>

                <tr>
                  <td>苔色类别</td>
                  <td></td>
                  <td>{props.coatQuanSheC}</td>
                  <td>{props.coatSheZhongC}</td>
                  <td>{props.coatSheGenC}</td>
                  <td>{props.coatLSheBianC}</td>
                  <td>{props.coatRSheBianC}</td>
                  <td>{props.coatSheJianC}</td>
                </tr>
              </table>
            </div>
          
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          舌象信息输出：
          <Brief>
            
            <div className={styles.mytablewrap}>
              <div className={styles.tonguetab}>
                <div className={styles.tonguetr}>

                  <div className={styles.se} style={{ borderBottom: 'none', borderRight: 'none', width: '1rem'}}>
                    舌色
                  </div>

                  <div className={styles.tezheng}>
                    <div className={styles.zheng}>
                      局部特征
                    </div>
                    <div className={styles.bian}>
                      边尖红
                    </div>
                    <div className={styles.bian}>
                      瘀点瘀斑
                    </div>
                  </div>

                  <div className={styles.coatQuanSheCse} style={{borderBottom: 'none', borderLeft: 'none' }}>
                    苔色
                  </div>

                  <div className={styles.shetai}>
                    <div className={styles.zhixing} style={tongueStyle}>
                      苔质
                    </div>
                    <div className={styles.item}>
                      厚薄
                    </div>
                    <div className={styles.item}>
                      腻
                    </div>
                    <div className={styles.item}>
                      腐
                    </div>
                    <div className={styles.item}>
                      苔剥
                    </div>
                  </div>

                  <div className={styles.shetai}>
                    <div className={styles.zhixing} style={tongueStyle}>
                      舌形
                    </div>
                    <div className={styles.item}>
                      胖瘦
                    </div>
                    <div className={styles.item}>
                      齿痕
                    </div>
                    <div className={styles.item}>
                      点刺
                    </div>
                    <div className={styles.item}>
                      裂纹
                    </div>
                  </div>
                </div>

                <div className={styles.tonguetr}>

                  <div className={styles.bigitem}>{SheS}</div>

                  <div className={styles.mediumitem} style={{ borderTop: 'none', borderLeft: 'none'}}>{bianjian}</div>
                  <div className={styles.mediumitem} style={{ borderTop: 'none', borderLeft: 'none'}}>{Ecchymosis}</div>

                  <div className={styles.coatQuanSheC} style={tongueStyle}>{props.coatQuanSheC}</div>

                  <div className={styles.smallitem} style={tongueStyle}>{Thickness}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{CurdygreasinessNi}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{CurdygreasinessFu}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{Exfoliation}</div>

                  <div className={styles.smallitem} style={tongueStyle}>{Fatthin}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{Indentation}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{Pepeckneedling}</div>
                  <div className={styles.smallitem} style={tongueStyle}>{Crack}</div>
                </div>

              </div>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        
      </List>
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}
