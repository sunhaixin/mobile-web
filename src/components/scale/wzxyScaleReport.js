import React from 'react';
import { Progress } from 'antd';
import { WingBlank, List, Flex, WhiteSpace } from 'antd-mobile';

const { Item, Item: { Brief } } = List;
let resultarr = ["肝","心","脾","肺","肾"]

let fzstyle = {
    background: '#a94929', 
    borderRadius: '8%',
    color: '#fff',
    marginLeft: 37, 
    width: 25, 
    textAlign: 'center'
}

export default function wzxyScaleReport(props) {
 
  return (
    <WingBlank size="md">
       <List wrap>
        <Item>
           <Flex justify="center">五脏相音健康状态测评报告</Flex>
           <WhiteSpace/>
        </Item>
        <Item >
          测评结果：（>=50为正常，分数越低越严重）
          <WhiteSpace/>
          <Brief>
                <Progress strokeColor={{ '0%': '#A8492E','100%': '#E9967A'}} type="circle" percent={9} format={percent => `${percent}分`} width={50} strokeWidth={10} style={{ float: 'left', marginLeft: 12}}/>
                <Progress type="circle" percent={33} format={percent => `${percent}分`} width={50} strokeWidth={10} strokeColor={{ '0%': '#A8492E','100%': '#E9967A'}} style={{ float: 'left', marginLeft: 12}}/>
                <Progress type="circle" percent={99} format={percent => `${percent}分`} width={50} strokeWidth={10} strokeColor={{ '0%': '#A8492E','100%': '#E9967A'}} style={{ float: 'left', marginLeft: 12}}/>
                <Progress type="circle" percent={34} format={percent => `${percent}分`} width={50} strokeWidth={10} strokeColor={{ '0%': '#A8492E','100%': '#E9967A'}} style={{ float: 'left', marginLeft: 12}}/>
                <Progress type="circle" percent={64} format={percent => `${percent}分`} width={50} strokeWidth={10} strokeColor={{ '0%': '#A8492E','100%': '#E9967A'}} style={{ float: 'left', marginLeft: 12}}/>
          </Brief>
          <WhiteSpace/>
          <Brief style={{ position: 'relative', left: -11}}>
                {resultarr.map((z,i)=>{
                    return(
                        <div style={{float: 'left'}}><div style={fzstyle}>{z}</div>&nbsp;&nbsp;</div>
                    )
                })}
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
          <Item>
          推荐的音乐：（共10首）
            <WhiteSpace/>
            <div style={{whiteSpace: "normal", textIndent: "2em"}}>

            </div>
        </Item>
       </List>
    </WingBlank>
  );
}
