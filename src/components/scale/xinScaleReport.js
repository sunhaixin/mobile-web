import React from 'react';
import { Flex, WingBlank, WhiteSpace, List } from 'antd-mobile';
import styles from './scalereport.css';

const { Item, Item: { Brief } } = List;

export default function xinScaleReport(props) {
  window.scrollTo(0, 0);
  let report1 = '', report2 = '', report3 = '', report4 = '', report5 = '';
  return (
    <WingBlank size="md">
      <List wrap>
        <Item>
          <Flex justify="center">{props.title}</Flex>
          <WhiteSpace/>
        </Item>
        <Item>
          测试结果：
          <Brief>
            
          <div className={styles.tab}>
            {props.report1
              ? props.report1.split(';').map((c, i) => {
                const list = props.report1.split(';');
                if (i == list.length - 1) {
                    for (var j = list.length - 1; j >= 0; j--) {
                      report1 = list[0];
                      report2 = list[1];
                      report3 = list[2];
                      report4 = list[3];
                      report5 = list[4];
                    }
                  }
              })
              : ''}
              
            <div className={styles.tr}>
              <div className={styles.td}>测评项目</div>
              <div className={styles.td}>{report1}</div>
              <div className={styles.td}>{report2}</div>
              <div className={styles.td}>{report3}</div>
              <div className={styles.td}>{report4}</div>
              <div className={styles.td}>{report5}</div>
            </div>

            {props.report2
              ? props.report2.split(';').map((c, i) => {
                const list = props.report2.split(';');
                if (i == list.length - 1) {
                    for (var j = list.length - 1; j >= 0; j--) {
                      report1 = list[0];
                      report2 = list[1];
                      report3 = list[2];
                      report4 = list[3];
                      report5 = list[4];
                    }
                  }
              })
              : ''}
            <div className={styles.tr}>
              <div className={styles.td}>评分</div>
              <div className={styles.td}>{report1}</div>
              <div className={styles.td}>{report2}</div>
              <div className={styles.td}>{report3}</div>
              <div className={styles.td}>{report4}</div>
              <div className={styles.td}>{report5}</div>
            </div>

            {props.report3
              ? props.report3.split(';').map((c, i) => {
                const list = props.report3.split(';');
                if (i == list.length - 1) {
                    for (var j = list.length - 1; j >= 0; j--) {
                      report1 = list[0];
                      report2 = list[1];
                      report3 = list[2];
                      report4 = list[3];
                      report5 = list[4];
                    }
                  }
              })
              : ''}
            <div className={styles.tr}>
              <div className={styles.td}>程度</div>
              <div className={styles.td}>{report1}</div>
              <div className={styles.td}>{report2}</div>
              <div className={styles.td}>{report3}</div>
              <div className={styles.td}>{report4}</div>
              <div className={styles.td}>{report5}</div>
            </div>


            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          判定标准：
          <Brief>
          <div className={styles.tab}>
            {props.title == '五态性格问卷测评(简)报告'?
             <div className={styles.ttr}>
              <div className={styles.ttd}>T分</div>
              <div className={styles.ttd}>＜12.50</div>
              <div className={styles.ttd}>12.50-25.00</div>
              <div className={styles.ttd}>25.00-75.00</div>
              <div className={styles.ttd}>75.00-87.50</div>
              <div className={styles.ttd}>＞87.50</div>
            </div>:
            <div className={styles.ttr}>
                <div className={styles.ttd}>T分</div>
                <div className={styles.ttd}>＜38.50</div>
                <div className={styles.ttd}>38.50-43.30</div>
                <div className={styles.ttd}>43.31-56.70</div>
                <div className={styles.ttd}>56.71-61.50</div>
                <div className={styles.ttd}>＞61.50</div>
            </div>}
            <div className={styles.ttr}>
              <div className={styles.ttd}>程度</div>
              <div className={styles.ttd}>极低</div>
              <div className={styles.ttd}>较低</div>
              <div className={styles.ttd}>中等</div>
              <div className={styles.ttd}>较高</div>
              <div className={styles.ttd}>极高</div>
            </div>
            </div>
          </Brief>
          <WhiteSpace size="lg" />
        </Item>
        <Item>
          人格(性格)特征提示：
          <div className={styles.white}>
           {props.report5}
          </div>
          <WhiteSpace size="lg" />
        </Item>
      </List>
      <WhiteSpace size="lg" />
    </WingBlank>
  );
}
