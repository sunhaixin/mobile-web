export const AGE_OF_BABY = 6;
export const AGE_OF_CHILD = 14;
export const AGE_OF_OLD = 65;   
export const GENDER_MAP = {
  1: '男',
  2: '女',
};
export const CardList = [
  { label: '手机号', value: '5' },
  { label: '身份证', value: '0' },
  { label: '体检卡', value: '1' },
  { label: '社保卡', value: '2' },
  { label: '就诊卡', value: '3' },
];

export const sheetType = {
  1: 'old',
  'old': 1,
  2: 'nine',
  'nine': 2,
  3: 'phy',
  'phy': 3,
  4: 'pre',
  'pre': 4,
  5: 'hyp',
  'hyp': 5,
  6: 'chd',
  'chd': 6,
  11: 'dia',
  'dia': 11,
  12: 'tcm',
  'tcm': 12,
  13: 'partum',
  'partum': 13,
  14: 'prepre',
  'prepre': 14,
  16: 'normal',
  'normal': 16,
  17: 'phy2',
  'phy2': 17,
  19: 'chd2',
  'chd2': 19,
};

//处理体检报告详情的显示问题
export const Array_itmes = (a, b, c)=> {
  let value = [];
  if(a){c.filter((x) =>{ 
    if(b.includes(x.id)) {
         value.push(x.title)
     }
  })}
  return [...new Set(value)].join(";")
}

export const DateTimes = (b) =>{
  let a = new Array(61);
  for(let i=0;i<a.length;i++){
    if( i < 10){
        a[i] = '0'+ i;
    }else{
        a[i] =  '' + i ;
    }
  }
  let evatimes = b.getFullYear() + '-' + a[(b.getMonth() + 1)] + '-' + a[b.getDate()] + ' ' + a[b.getHours()] + ':' + a[b.getMinutes()] + ':' + a[b.getSeconds()];
  return evatimes;
}

export const todayPartum = [
   "星期日",
   "星期一",
   "星期二",
   "星期三",
   "星期四",
   "星期五",
   "星期六"
];

export const sheets =[
  {
    type: 'old',
    text: '老年人体质辨识',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-old.430f2df6.svg',
    module: 1,
  },
  {
    type: 'tcm',
    text: '四诊合参体质辨识',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-scientific.ff46cc75.svg',
    module: 12,
  },
  {
    type: 'nine',
    text: '中医体质辨识',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-physique.e7c8c005.svg',
    module: 2,
  },
  {
    type: 'prepre',
    text: '备孕女性健康状态测评',
    icon:  'http://test.tcm-ai.com/wap/static/icon-sheet-prepre.2f2e388e.svg',
    module: 14,
  },
  {
    type: 'pre',
    text: '孕期女性健康状态测评',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-pregnancy.cf9a1d98.svg',
    module: 4,
  },
  {
    type: 'partum',
    text: '产后女性健康状态测评',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-postpartum.10242ff8.svg',
    module: 13,
  },
  {
    type: 'chd',
    text: '儿童体质辨识',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-children.c5eb4611.svg',
    module: 6,
  },
  {
    type: 'chd2',
    text: '儿童体质辨识',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-children.c5eb4611.svg',
    module: 19,
  },
  {
    type: 'hyp',
    text: '高血压病辅助辨证',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-hypertension.b47ff1e5.svg',
    module: 5,
  },
  {
    type: 'dia',
    text: '糖尿病辅助辨证',
    icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-diabetes.84586c0c.svg',
    module: 11,
  },
  {
    type: 'phy',
    text: '五态性格问卷测评(简)',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAACCCAYAAACKAxD9AAAACXBIWXMAAAsSAAALEgHS3X78AAATz0lEQVR42u1da2xTZ5p+WrWO3Rxv1w5DElta0tjNdCR7QkHOEgWcjkhjdoomHuh0Yiq1SqQyEtn+IDDQEZcf0KqFhfCDTbQDbSLYpWZnBTijtBNzWdVmoqBYsFB7JDa1Kd3OcS4L8QrbdQg/2B/2Ofj4lnN8ju3jcJ5fgC98/s7zvff3/Z55/PjxY0h46vGstAUSJCJIkIggQSKCBIkIEiQiSJCIIEEiggSJCBIkIkjgjOekLRAvwjPTmPLeQq2xEQAQmZ2h/ywRYQljIRrBw0gEFQSBu+NjuDs+hu+ujdGvr9ryTsGI8IyUdBIPfMPnMH5iAAAgq6zEQjTKeJ1YXo2qej3a9x2QJMJSJsH9gD9JOkTT3hOZnUFkdgYL0QhklYRkLC413L/jx/iJAUxeuUj/W1PXe3hlw0YAQOObnWjqeo9+zes4J0mEpWkbPDn9FYQSa9/fjvq1rQCAH738Ml5a24oKQon6ta2YGDqJyctOrH77XUkiCGGUXT9zCuf/cStOvrEep976BS4e3I8p762SSQQAUNbU4tef/SsWImE8jIRxe3QEMkKJhUgE5M0b+F//JNb/bj+eefZZjJ/ol4jA1yUb2d2LG5+fxv1vA/SJ/O7aGEY+6MXkZWfR1/QwEomvbXoKE0OfQkYoUUEo8cqGjbj3zSSUNbXQrlyF+rWtuPLxAYSnp+AbPo/wzLREhHxx6eB+mgCZ4Dp2uOiSIZL0QMmb12m1AAD161qzfu7GmVMSEfLB3fGxnCSgcF3ADWYlpWZnnvx5eorx2j3/ZIr0CD/5PdfGsBCNSETIVxcvhmJLhNR1haencOXjAyBv3qA9hy/3/DajkSkUaZ8qIpTKGFyMBMleg3blqrhN8OoqaFeuoqXAMn1DViknhK3wVBGhUOFZfkRgqirtq6vxMBKmDchF7YvZGVw6uF+KI3BB1Y9/wup9FSvqEYvFoFAoCr6m1Ajhw3AYE0Of4ts/u/AwHCdEeDoI8uYNLHu5AQ9SbAgAqKrXSRKBLUiSxLmv3PjhhcVDs//9zHMY6O8HSZIFX5fmp42QVVbSf789+gVuj45AWVOL8PQUbo+OgLx5I/7an0bSjEnKaOSr9p4KieB2u+EcHcX8/DxeqH8FP51/gP9Lscbjp7MSuo43cfcvtxEMBjHQ3w/Lhg0wm80FjSEk2whr39+Oe9/E1/aTf9iIZS83YJm+gbYXvtzzW5oYyUZj8OubvFTfkiZCLBbDWbsdPp8PAKDT6dDV3Q2FQoHJy07cHX/iflXV62Do2AxldQ1Wx2IYGhxEIBDAsMOBgN+PTputIKpCWV3DdBe/mYT21dUg/+s6ZASBxjc7MTF0EneuurAQCaeRgEJDm4XXOpZsGjrg98NutyMUCgEAOqxWzifb7XZj2OEAAKhUKthsNuj0ekHXuRCN4NRbHYx/e++LK3iYeOg/0sftAu3KVXgYCePLPbvSYgvUZyQbIQVOpxMDAwMIhUJQqVTo3bEjL/FuNpvRu2MHVCoVQqEQBgYG4HQKG4JO9RqeBJK+we0/jeDB9BTC00Gc/rUVFYQSFURmG8fVd0iSCBTm5uYwNDiIYDAIADCZTOiwWnmL9FgshmGHAx6PJ27gaTTo6u6GWq0WZN32ri2IJEUXOz/7t7jNQhBYiESgrKnF1ePHoH11Fa58nLkoRZIICfi8Xhw9cgTBYBByuRydNptgel2hUNDfJ5fLEQwGcfTIEfi8XkHWXpHkQipraiEjCFw93keTYWLoJNa9vx0LSeHlVPCNMJa9sVjo05oMk8kEnU5HS52hoSHeUic8M03nPyoIJdp+tx/f/tmVUANKTAydfOLVEMrsgSWe0cWylggkSWKgv58mwTqzGTt27iwICSio1Wrs2LkT6xI2h8fj4RVzUFbX0G6fjCDwMBLBxNCneGldKx1EavxV3HPQrlyFd/7dAWVNbXpQScfPiC1bGyHZopfL5eju7hbcomfjmQwODmJ+fj5vzyQ8M43z729Nq1GsSJx+WcI4DE9PQVlTi/q1rbg9+gUjC7liTQvvgtayI0Ku2ECp1kPFHADAYDBwtk0WohGM7O5llSJPDYAtRKNo3b7r6YojCHECxSqhFqIRjP++HzKCQPPWHpx66xcZK5mT8freA1BWx0vc+aJsiOB0OnEx4cNrNBp02mzQarWis1nO2u20+9puscBiye+khmemMXnZift3Avju2hga1rfTcYeqeh2I6hoYrZsFK2sXPREKFRtYCl6MkBA1EXxeL+x2O+bn5yGXy2Gz2WAwGssmrlFOaxclEYpxqqa8t3D/jj+tAKSCIFBVr4esspK37i0naSY6IgipZzM9fK/jHKOxdDHUGhtRVa9DXfPavNO85WDfiIoIQsQGPB4PAn4/2i0WWoJMeW/h+plTvIs3ZJWVqFvTAoN1M2dpIWaPRzRESI0N5OOLezweOEdHoVAosK2nh/7s9TOncOPz04KvudbYiNVvv8tJSgjxO5csEfieFJ/XC4fDgVAoBI1GwyCBq+8Qo7G0EKg1NqJ56zZOEkIMUVFREYGP7kw+XXK5HACwraeH/nwxSJCMVVve4dSYWkhbqGyIwNeapqSIQqGAubUVww4HQ5LcHR/DpQ/3F30zq17SobV3F2vpIKaYQ9GJwNe/psQqpV+HBgcRi8WwY+dOAPFQrb1ry6Lh2UJBVlmJ1u27UdfcUlYxh6IRQQj2n7Xb4fF4aDE6NzeHjz78EF1dXfTGFco45AquiaBSxxwEI8LkZSej9aqhzUJX6AqhDykSdNpsMJlMAACHwwGf14u9+/bR72OTrBErGRazm6j8AwVldQ3vrKNgRLh/x4+LB/czau4oGDo24dErRt4WMrU5ySQAgKNHjkCn18NqtZbUNhCaDJk8qedve+EbPp/2XmJ5Ndr3HeAdBeVFhGxFFckIqZZhZrk2b5/Z4/HgrN2eRoJYLIa9e/Zg27ZtNLG4qgVDx2Y6gzflvYnrZ06z7pjmgo2f9HGOSiZ7RdWzJFShezntkk3HT6T1SHABr1K1G2dOLSqGVaF7+PnPXsureIQkSTguXIDJZGKQAACCidKwZOnCJXJo6NiM5q3bQCyvhqyyEivWtGDjJ32CTysDgIsH93GeY6BQKNDV3Y2f/+y1nCSIG8hR3kMzeBGBrZ/+tw9CeX3/WbsdarUanTZbxhPDBw2vWzKeLC7WPlssRKO4mGfHMtu94xszEW3xqtPpRDAYzEgCACCDQeh0+XcBK5fXZBGzREF+z5T3VknmM5U1Eebm5uB2udBusRQsS3c3SwaSS2aSK8ZP9As26kZURCCWV7M7fRyNmItOZzxqmCPnoNVo6ILRJ6e8mv1D+X0/w6ZYiEbhOnZY0EllmVQE12GZbPeO4PDbBScCm9g6sbyac2DF4/HAsmFDTuMy02tcavsXohGMfNCLs91vY+SDXti7thRFdPuGz3GSCg1tFlYPme8ATl5EaGiz0EWV2dwarvX2brcbKpUqzUtIhSahMgL+J+5ePoUj1Cj8YonsfKRC+74DjGEaac9hfTvvwBJvG6G1dzdat+9KY+2jGi02HT/BOdDhmZiAqamJlXul0WjgTeT2AaCqXo+ql3QQO7hKnqp6PTYdP4FHNdo0adu6fRdae3fzXpMgvY8NbRaakVSgBwAePS/jJja9XszPzy8qDSjo9Hr4vF46sggAButmuI4dFjURIrMzuH/Hz+mQPHpehsCLy4AXl+HDjz4SPAchuNegUChgMBjiDzbptLIigs8HjUbDOhFlNpsRCoUYXckNbRZRTk/jKxWSq5oKkYgqiPtIZQI9ExOcPuf3+zmlX9VqNXQ6HdxuN1Ndbd+VU6eKAXfHubmp1F4WKj1dGCIkJEIwGMTc3BxrbyEUCkHPMUjUYbUiEAgwyKCsrkHr9t2iJgJ1AQfbvaEyt9TelgUR8lEPmXIHbKDVarHObIZzdJTRml7X3CJ6yZBtbE6x1ULBiJD8QNmqBzIYhEajyev/slqtUKvVOGu3M3IQDW0WbPykT7SeRPDrm5zUQiELXAtGhGT1wCZBxHfS6baeHgDAQH8/4/+rqtdj0z+fwKot74jebsi2L4VWCwUlglqtpk84G/UQJEk6SJSvOurq7gYQL1hJnWCy+u138e4f/hgvFFnfzjskKwTYpM2pvePiTZUsjpANpqYmDCfKydjEBvjqP7VajW09PRgaHETf0aNot1hgNpsZ35sc88j0MIJf38T9OwFMeW+KouSNco3ZBNlESwSDwRAngs9XtCHXVKcTNXbXMzEBy4YNWQ2t1JhD8t/vjo/BN3yuZOP9Y7EYw1AsWyJQ6iEYDMLn87GOGAoBs9kMk8mEYYcDjgsX4LhwAXq9HgajERqNhlV6u665BXXNLYL1TnKWBkVSCwUnQj7qQWjp0GmzocNqhc/ni7fHXbhAF4VSUKlUUKvV8QCVXp8mPWqNjdj4SR/jptalpBaKQgS26kGj1cYziQVo+1IoFIy6x7m5OYSSAl1kwrMJ+P3wXriAs3Y7TCYTo6MaiNc5VtXrEzWI/O2HXPUTxVQLRSECW/VQzI5g6vSnxjwoElKd1R6PB+vMZlgsFnp9lHQY+aCXNxmIHEUnxVQLBXUfU9VDsqjLhEwVR6WCyWTC3n370GmzwTMxgYH+fkaovKpej+atPfwlQi4iFFEtFI0IVJFprngCFUMoxq0pXAhB9VQePXKEQYaGNgsMHZt4fX+uK3iS50guGSJotVqoVKqcUkGtVkMul4tGKqTGJtRqNd1wmxykyjdamWtGE7VHKpWqaCN2ilbFTKVPc0kFo9HIKD0TC6io5dzcHOO+BlklkbeKqDWuXFQaFLMjumhEoIxEbw47QafX096F2KBWq2H95S9x1e1mkLWuuSUvqZCrkYbao2K620UjAqUe5ufns6oHg8EAuVxOt86LDdSY/uTaB1klgbo1LYIRgSrXK6ZaKCoR2KgHhUIBo9EIt8sFscLU1JQmtVY0r+X0HQ3r27N2VJVCLRSdCGzUQ7vFglAoJGqpkEpmrhdwNry+QVRqoehEYKMe1Go1TCYTnKOjBV0LNdfh5BvrYe/awukqHJ1Ox3BzuXRy1RobsxbXlkotFJ0IbL0HSiqkFqUKBarLiepzjMzO4Mbnp1mTQafX06V1tFRgWQWVqyOpVGqhJERgox7UajVdh8i2+JUL4hd/poeH+bS8yYjFu6hXrGnJWWpfKrVQEiJotVrI5XLMz8/njBlQ8f2zdntBJEIhVM1iAaTXendlfT3g99NT1Uoxp7kkbfHGhOjz5lAPCoUCNpsNgUBA8Es3V2Rx9zQ8GmMWS0C1bt+dc/YCtRfGEo3yLwkR6FL3Re5N1On16LBacdHpFNSLiPc9MEvda42NaP4NuyhhwO9n1FcuJg0MHZsWncRC7UUxUs6ZUJJ7Hw1GI+RyOUKhEEiSzCkKzWYzgonxfGwri1i5cInaxSnvLRDLq1lb/rFYDIFAgJEVzFW5FJ/VnJtgJEkiFApBLpeX7HKPkk1MoUQgm5NOTVQb6O8XPBdRa2zk5P5lKhaZvJRZdVW9pGM1FoDaA2MJb3gpGRHYqodkMhiNRgwMDJQ02OQcHYXJZKILVZJvck0lwcZD7Ka0lVotlEw1cFUPyWQA4tPWSJJktMMXA263G6FQCO1J5XSZxtqtWNOC13p3sSKBGNRCSSUCV/WQTAaqcihTI0uhQJIknKOjWGc2M26GSR1rZ+jYlJhwwm46mxjUQsmJwFU9JAelqBa3vqNH4XQ6C5q6jsViGOjvj6eiE1JoIRrBV32HGHGC1/ce4FyfIAa1UFLVkK96SA5M7di5k25kcbtcMLe2pnU2CSEJKBJQ5AOAr/oO0/OnDR2bEtVKBOfvFoNaKDkRAECfKEbx+Xx5uYZUI4vb7Ybb5YLb5YKpqQkmk4m3q0kNA0+9f9rVdwjfXRvL6xqfTB6IvsTX+IiCCAajkW4+yfcqG4VCAUuiz9Hj8cDtcuFqYjqbwWiEXqeDTq9nJSlisRj9HbFYjHGlAHV/MwB0Dp7hNQSboRZEcDFo6YmQMl2FTw0/NaTTbDaDJEn66r+riSwm1dGk0WrTSBGamwNJkggGg5DL5TAajWkNLgDQ/JseQcb0FmMKSlkRgZquQqkHoe5C1Gq1tGqIxWIIkiSjoymbdLJYLFlPqJBzmosxBaWsiJCsHjwTEwW5FFOhUECn16d1NJUShR6OVVbuYzb1sNQhNrUgGiLwmc1YjhCbWhCNaqBE5J2Ja/jLZ/+CB1/G74CqqtfB0LGZt3UuBkx5b2Hy0ijCszP46/ffo/YxUNf896JZnyjuhl6IRvCf//Qxvvdcy/h6PhdkiQnjJ/ozXsxFBaOEaKhdEqrh+plTWUkAAK5jh0s2vkaI35aNBADgGz4P3/A5iQjhmemcG0WfqkQgp5ywEI2wesjXeV7MtSRsBLZX59z/NoAv/uMPeO5vXiwbIsz/9X9YDdNYiEYx5b1V0mHiJSfCwwj7iuJrVy7jhxeIsiHCCz9E8HdlstbnUEZYs76t7CTC7PcBiQhsUNfcwur2VmJ5Nd741VtlZyOcOn+W1Xu59k8uOWOxql7PSjeKwcXiClklwWq8TnxONPF0EwGIX16Vq3eQTV+AWNG8tSfnBWgN69t539AmBEQRUHriU5/D3fExTHlvQVZZiVrjShitm8viap7FMHnZicnLTjoeUmtsTJsLLRFBQsnxrLQFEiQiSJCIIEEiggSJCBIkIkiQiCBBIoIEiQgSJCJIkIggQSKCBIkIEiQiSJCIIEEiggSB8f9a8yKpmXBYjQAAAABJRU5ErkJggg==',
    module: 3,
  },
  {
   type: 'phy2',
   text: '五态性格问卷测评',
   icon: 'http://test.tcm-ai.com/wap/static/icon-sheet-psychology.5d2b9a5c.svg',
   module: 17,
  },
  {
   type: 'wz',
   text: '五脏相音健康养生测评',
   icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMMAAADDCAYAAAA/f6WqAAAACXBIWXMAAAsSAAALEgHS3X78AAAWG0lEQVR4nO2dPYwjSRXH6wCJj4CZI+GI1hcgcQJpjURAgDRehzgYHylI6yVzwnrbF4LWS3DZ+LyRI7SegPi8gQMCvJ7gkAjQeRBIZDuTHdkYcSE6VN7/m3vb2x/V3VXd1e33k1Z3nvH0Z/3rvXr16tVbX3zxhRIEQamvyDMQhFeIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAGIGAQBiBgEAYgYBAF8TR6EUAbzXrejlKJ/Jzjl0+FqPfLlBYgYBCfENP4wD5VSIgahWRg2fq8RMXgAGtLNcLXe1uyaszT+a6XUBv+elXCJmZE93Spi3uu2lVITpdRp6ArO9c+Hq/WVZ9dbpPFv+P3Me93bRjdcrd9ydtEZEctQAfNed5DQO95XSvXnve5ouFovPLjWYzTouylfjW38dUHEUDLoYdPchCOl1Gze6249cJ0GMUKofeMPI2Ion5nhGY/gRvUrvt5j9v8XSqlFUxp/GBFDiWCckOZucE61mzJcrW88uYWND66bK0QM5XKc42xtuCJ7xkGgP7eUUtuz6bRxvXOViBgMGAeBbnwv4SdTA7yK+X/dSJ305LiOT9nnHffbz6bT2oRmfUTEYIDugcdBcAkX5w7+Iiq8uMOMqk1X4rb3x3U8wjiijXHFKYVnx0FAg9rl2XS6tHgNB4GIwRzdwD9K+LYWQiepdx6u1pt5r3vNBJXGZXigejadzmgQDpeJx//vIDR7H1ZDC2ImFsMMEUMK4yA4Rnjxg4Rv6gbeN2x0sxRRcSZJv8T5tkwcHViNfkgYlxBFYwe/NhAxxAD/fAC35wjf+p9S6quhv7iERTAaJwxX6xmiSvdTvqozOjO5OmfTKY0fRhDGAMLQ7t2zcRCQVZm5GtfUGRFDCFiCCTIqiWv87HtKqQ/Zzy9gETI1rOFqPdATajjmUejX+lyjrEIIQ8Jglm0Ea/EYYilFFPNet4XxTRs/8tZlk9wkgEYzClkC3dgnaFg8qqQ5P5tOB0XPixnp24aixxVFjxnHOAgGECCNWXa4v9iJwHmvO4GANE+Gq3Wi68b+LnyuSHzKTRIxvGokfbgP9OJeE0Hou1uEMb3Jw89KhCi0NRpE3W9WMSCXaWmaxu2TGA562afu6cdBoF/cx2gYulE8OJtOO1ENA3TqLAT1yoVanE2n2so9gHXQ9/4C0ancsKQ+4/UM81636nSTWw5WDOgdtyyF+ol2V9IiLk0aeOJetSieoiMoJAa4mFnSTZSFc1rj4AbQGBssmAgu4SIcZCwe4qaxUlHyjKG8SSk5KDHADVgyX/nJ2XRqNCAUjDCdTOQ4Cxhk5WDcJLhFGzY2+LEIwTqXGQ/4yKdU8IMQA2LqzxAyvcDYQFIUYtDhXoR8s5Klc9GRKdO1HaXQ+NDqOAgWbLb3ad0jQS7AxNiIpXFw/q2U+i4+m4RWBwhThycTiQus8fbGPSIaO2aIGCg/kNycNwnNI0TxXfazb6QdTy/+mfe6SwirxX6lB+pLn1fINdIyQAi0iH2fVi1CeJN5r7swyJHi/HW4Wv+05MssjcZZhgghJKZVlw1SOqjHbGP12zv4/Fl40ZCr1WywCFmEoHyaE3BBE92khS9CCK03aLEJqQv89wYTfz/A58/gXtDy0JNxEChEaShd29aKtjxjp69bOK+3NMpNYoPlSoQAq0TrCTpo7BvWkCOXhI6DYD8ojQr14piU9UnCUpgvoVVtmWbFESl6keMWd8PVOs867lrQGMuABkVmf1SmEJDoN8Bg/RLWaWTDxUFDp3UK4RVuunefIb9qkZBPZYs30sr1tTQlTN0IMWBCjSIipUSNmBWg3tyaANLgK9wwBtGiWI6D4AbZtmn3n+cadzGulb4GnZh3rwQxOqX2bhJ6yQ3i2qXMI4yDYAQRbNEjFxJfkpuU8TgDJs5EUcx73VloAVMSe7czqrofJjQf4jvtOpevqbVlQO+8pJll10Jg6x5usMLNq54QjX9BooBoR1HXqTcJQcp1WkTpHCvv4sYlE7hsd/EuahtxqrVlCA2YW47rFS3woq3PWdiyDBHHJQu2xHW/8XywHluLhzdiGqcYTZLh+WzLtM4uqK0Y0Et/jI/O/FWcZ4HGMXAhOFdiUF821Bl6b2fWrKz34ZJaigHu0RV6Iidp2DjHDIPkgcuiXC7FwM5BOUMLVz03s9TXGD/UaiFUXbNWFxDCpcPedAPXodWE6nRw7fbzFOMgoKoZthlBCHcyZrB6Qe3EgOgRJd8Vrk4Rc3yaIKtd75YEQrK3k4FF1zyHwbOid/LQ9vFdUzsx4IXqdIZHtid7WJh2YqMMjI/oBns2nfYptcOBIPTze46PXq1XSKOWbhKqV1h90EwII9vH9hGIfeZCEHCXdsitqk2nctClYoiQEA4m1RvjrZFtQWDijTqU2owdDl4MhyoEAvesAwRLy4PqGdyl2uQtHXRFPbb2odIKeWWEVg2ugSYVjYsoN41DtwxLLKA5+HXRLGDQ+PFSHIdcUW+CBTeNjBrlZD9LXadBr028S9RjlRraqNl5gdnmha2KChgnPEbtJNmnAOhnASHoZL+NrQxULCai/KcWW+w0k7pJMehd8VHy/SErXnuCKf4XSDu2wQJpHFI7KQRm2xe29qVD0YEXeId3kTlwgne8RWkZL/BGDBBC2vZOD4sKwofBag3Yu5BF3SWD6htaGM98qcTthRjgGpk2zodIO84MWxV28APmJFgx4lnecCtcI9PqGwusragUX8YM/YQKbFGMcg58J1hAX2l6cWiR/zEt8ofVukkqHlAW2l3CxiyjnBNnWTqcI7SBSud5fBFD1rqeLYPvvAasgu6p3s1+ecXBoH3ASjhesBpJ/2InaOM7J9ilc4O06yrGNxNMxuXZ+835O7WNL2Iow0ROsA9bqdEL+N0jvOxF3HZRMX9LZWc26KUjt9ZyhT4XKm/ksQ5ZLL0X+CIGp+5AFVYBlmAGEZhUrHgDRHaWbPNFcl0GJYp6kdM6ZNn83Qt8iSZlXTyT1WUYlWkV4Pt/ijSPVtGcJ6RdT3iMHuubnQNLtM0xRstqwSoPc/skhp3hd3c5UgYGZQzOdA8Ot6LvYjMUiIJKx0+QT1QGixwRuCzv6Lrovtc28EIMKENi2vOMssxawu++ce1rs6S/luvSlriX/WbjZQgClu04S5o3aiw9MfjqDuKuHG8m3dAz3EuwEPrnD3T9/4yH/o1S6s+O1vxyNkj6K2WpKM7RKUMQeHb/UEr9NsvfYWOTRwnv9DquOFkVeJfCjcmXPqtcfUWV6xIKWcUyDoLP8dDfQ379Mk+x3pRzVJb+jIZKlf2suWWhIsqneA/fPJtO3zH489fApCq9U6psssnRsTml0esZxkHQQSNpIaJEBYLv2hIGq6uUubQiZtLbLMauG/VV1p6SLVAqVBcJz6jDBPDaM0ItV6/2u7BJ07e+7VOkii1FnIVe+rNxEOQSBtsqyzjUyVJPYmfd573uDsc1yurUjRMRrEXWyauETiKqYNoGz62RYmi6ZdiXcU+qexThDlBJ+WVaA0cD1D1l6mwr3L9JhmK/ROqmgux6yF1KjOTktZII53ZQXaNxNN0ynKRFKvDyqWAvF8ZHSIeIFAabCEuNsEAIG7ZzTxYeI+mtbzBmGqFRvyGGkABa+N4EcyGm1nDT5CRH7yyDHozaqFkEP1o34lw5L0wY5E5dcWGY9pIFhcC5ROQlseHCOmhruAjlQx2z3j93TH8cBLrBvN3ERVFeWQYMeO9j6r+oX9rKuSnHHm4x1Ot5Qnqy6yrDktGlBSEoHGNi0DNrq/A7uHAkgJHFEpmXsIa13pgkCt/cJGpceVO0OVZfGOUJqVfC0LHzD9MaGFZxnSR9JyN6LccyZfmrvqZnSqlfnU2nf7R4buLKRoYpTeD5FJnyZtINbgktBrkPH7cILlOCtavwJ4PvuVhNl3hMWDTde3/u4NwKkSQbz3bjm3XxaQ102BIUbUgthw871epgDsFF1uYJwrNJbBzvoGNjNl+HlY9KyAwwxicxhH3hvk8PKkTLINbuMvyYduxtjsU1ptgSGu2F7U2lbi/EgIFzuBc98jiM1zYYnLtqjMqgAdV2k8Eq8cUyxA2WR55ah6OKd7WsfIlkE6lcDKGBc5gjqXgnlIUPliGtsUtZF6EUfJhnWLLBaBuFxC5M8n0q5FrHyVNi5C5naJuQKOed+1u5GOB77/3vcRBUfTmmXBm8zA3be842aWJoOxSjrfHKflbepy1yfavCTS/Q1gN31fuYhC5drulNO3bbofVwOX9TKV6JgbkdNiartg5j2KliwDqEi6Tv5OTcIHu109QG65Im789w49AybFDxLu34ttMxdmnHRBrLsUP3o/DiHswrKaSNeIOPYtg/IPbA8uIsJQHjnIu0mWAk1JlUiDBlYrDybeDYRWtZHI94lQbuoxjoARXt1a8cT/Ub1RLCKrVzC+fT7pFJLSLXNaLuWLA61NF5NVPuoxh4mDU3NENsIfs1ElZLKNWCDVfrQUFBPMExEqH9FFy5SLhXG64NdXQihhToAdno1V0mrCn470a9MBrz+yhbY4r+7j2TNdAYv8wcz9jbKgZA79ar+RKfLYONHn3pUgywDje0G1AaulDacLXW9/UAi/Cjimvt8Lv39Xcz7GM3wZ4OLqNItqJUIgYT2Mu8ayFJb1NC6ULdEz/OMuDXxbOGq7Ve4K/v721UEtT/3tY/w++MB8FwjwYurQLexUlRMcBt1Tlnu4qTHd/A19Aq+aVFxw1b9NzOBtI4xwOUbc98Hj1noHt//MtTMZBK37suU687lUsL56BOw7uUEl/FQA/KhouzdJ35CndpmVcQeWGV9Gwu+I/D1jZT9E69mxT0VQz0oGyIYVFGGjjK22ywy45zQcAtIyGUUXj41NL8hYghI/SgTGZ5E2GuUlmC0APZT8ZB8HtX50Gh4xdlCAEMkElcyEXCeGGfauNTgh7hpRjw0GncYGMA7DrkyNEv+VtKqV/r8pY25zm0NUCRMD2Q/W+JvevIkotE7/K5hWNZx+fcJDLJtlyltoUUj0RgxZbYk+BHaKwvdU9exHWCCDasIp7em+4PjtMu6Nw0kWdDDNQhVb5LTxTeFh5G4/kUcfdW0XKGWYoEFzjHAtfaYT9rsaJoV2gImyQ3ge0T3We96QJlI2/Y97YQh4v6THQO2nq3kBjwHF7io5flKb2uwo0yjtrHfGDhZdAmGYX2MEg4/oB294x70axEZRuLW64jUhLaiMPTHtCbuEiRrX0ZUu5pkrdebehYI6xifO5rFW/fq3DP8AALJ59hs40ZBrhWrQN6vRkaZWyPx0tUqi8bczhAcGNachH7MtCWuIWtZwQTi2nolNTopYukamAZuGl910I0g7Z8yrUvc8Jxt+jBKylegB1Glc0eF26lFnfhMDHGai9subyu8HpxDxo/RR4KNzS8hBF277Gy8AfWRlUlBDBAgMDKNbBxjq17uh04+1zKvg4r3SiHf2CjAcNV2dgIFaLHG5SQ/5QIGhhtsGJjwm+BQXPhcQiERXWxnA30beC9GPBCri2Xm9QNuFNkIo6FUUc+JJxhnPGE7UCUC7hHLQdjhcKTdq6pyxpoejFWJs7Qkw7gLuXtSWkXH2+2b0WI9Sbj7vy3wNI9TgsEZDjeMXtnXlsFVRcxoMFp63DHll8Md2mRpyfFNbQ9rfbXRwXzTK4b3Jklwti2MkpHsOgXPqZfhKlTdQzqWSa2Br8Y9F5lCffBknxkq/e0DbN6C9NUEObyWbN0bBCu6mAVVJ3EwKyD7VL1A6xlTm0EbN/nRz5vDM6sXqrIcU+6176xsbEkY1Inq6BqWDeJRPDYVgIcetIOQpNpgpih0eTyycuEQr0GS1LpXmzOUXTqEkHi1EoM6PGoSp21gauJIFgqRZ02BB9gj4s3Zty1RUDeURs5W7ldvgi3lQR2XheroGpaUY9M+UnWQWISIUEs+QuGFVpgaWVt9j+GK0fpGvx+yDU6tiCEDl/QhODCXcw212o7gdqJAbFqqlJXKKYecWwSxDFeMLliS0xCeZtXEwfGWhu2nzUVJd4WFQLooPHr5/UBD3TUbeN0r3OTkkA+0F1XWZBwl/oQQttGjk5VsJysvyilfo5UcCu+PFyt8F7Xn5xNpz+rzQMCdS48TO7Sqa25Bw4iK/q4v1RK/dPjnUdN+btS6hdw9WwOaqM2ff9OmYURbFFbMcAffoSPtnJywufQ1uH7Sqkf6p7V9Uo5F2BcpZ/Vt5VS79h09RKex3twm+oUbKivm0QgffkUcxBtV34qrM8EbtPE9zwbNuhvuyocgLDt45hf72CFajPOasL+DANK1XC5QB5zC7dlEXVD8NF10iLAeOclLELLYf5UlGXYIcDRqlvAofaWQb2+/FHPeN5zHdtmVezavlgKXNMIk13nZVzTOAjCjaeU87qiEWJQX/qvHeTXlJIqAVdkggZ4wTJZSwkpwjL1IYJWmcJkq9cU7n1Spwm2KBojhiphqcoDCveyKhhWGyYsAAn/tAoRqi/HUCPbS2irRMRgGViLPmuwN2ySa4vcJqMeFL0vlY3psDHLxpXYTMG1bes2sZbEQYlh3uvSMk2e5LefXc5TAdsE9OT8X4vtZrqLqEbN4/aXENMGqeZbVy7gvNc9jljCqs81M9hHrhEcjBjmve6CZVKG2YcBs+yJYANYkXD27VXZvf2816WaT0cxX3k6XK1rlWeUh4MQQ4oQOPcy7JTTCOa9rrYEHxvcS+MF0eR9oPfgZZsIQTneJdM74BqZ3vNDuJmNpfFiyFhE4A7Ecyj0E1yjos+ydhyCGE4zfr+22ak5yNrTN/rZHIIYhHiyLp292+RnKWI4bLJGrWxsiO4thyCGrLvEeFv1wgFZI2eNfjaHIIYslSwuy55rqJhlzMbscdSm0kUeGi8GzBucG3x11/RoSRjMupve85Omz0Qf0gy0thAPY36t10PoXfmN3QC21RThct2AUxBOXiSEWbUQGm0V1AHmJrWoAjd+pHvG5XC1jquVdByqStdOaDCP6lBcLA6Wm9RhuwltkLcluUnCbXbm0mByqvDOQrZgdU77EHy/rgtuykTEYEBoJV0UfzubTn/iwTUOWB0j4trGBoWHgIjBEDS2JUu/DnMNwdD+bk7DkLBYfKFPWKjP61r4rCpEDBlgZRnDM7H/QSmWMBeY2LqihT1ZUrTZbqCU6k3rIaJmgne04Mf3vdN8RcSQkQhB7Cv6hZZjthMsiC2uyQqVYYkOARFDDiCIGVLDIzdsZ6FX6t0pgpUUkQrDN03f8CWk0vPbR8RQANpkXRpmMxAxCAKQrFVBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZBACIGQQAiBkEAIgZB0Cil/g9GvSjIY/emGAAAAABJRU5ErkJggg==',
   module: 20
  }
];
export const SCALE_OLD = [
  {
    "Id": 1,
    "Title": "您精力充沛吗？（指精神头足，乐于做事）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 1,
    "Code": "old_1",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 2,
    "Title": "您容易疲乏吗？（指体力如何，是否稍微活动一下或做一点家务劳动就感到累）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 2,
    "Code": "old_2",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 3,
    "Title": "您容易气短，呼吸短促，接不上气吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 3,
    "Code": "old_3",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 4,
    "Title": "您说话声音低弱无力吗?（指说话没有力气）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 4,
    "Code": "old_4",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 5,
    "Title": "您感到闷闷不乐、情绪低沉吗?（指心情不愉快，情绪低落）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 5,
    "Code": "old_5",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 6,
    "Title": "您容易精神紧张、焦虑不安吗?（指遇事是否心情紧张）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 6,
    "Code": "old_6",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 7,
    "Title": "您因为生活状态改变而感到孤独、失落吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 7,
    "Code": "old_7",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 8,
    "Title": "您容易感到害怕或受到惊吓吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 8,
    "Code": "old_8",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 9,
    "Title": "您感到身体超重不轻松吗?(感觉身体沉重) [BMI指数=体重（kg）/身高²（m²）]",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "BMI＜24|24≤BMI＜25|25≤BMI＜26|26≤BMI＜28|BMI≥28",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 9,
    "Code": "old_9",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 10,
    "Title": "您眼睛干涩吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 10,
    "Code": "old_10",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 11,
    "Title": "您手脚发凉吗?（不包含因周围温度低或穿的少导致的手脚发冷）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 11,
    "Code": "old_11",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 12,
    "Title": "您胃脘部、背部或腰膝部怕冷吗？（指上腹部、背部、腰部或膝关节等，有一处或多处怕冷）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 12,
    "Code": "old_12",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 13,
    "Title": "您比一般人耐受不了寒冷吗？（指比别人容易害怕冬天或是夏天的冷空调、电扇等）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 13,
    "Code": "old_13",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 14,
    "Title": "您容易患感冒吗?（指每年感冒的次数）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "一年＜2次|一年感冒2-4次|一年感冒5-6次|一年8次以上|几乎每月都感冒",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 14,
    "Code": "old_14",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 15,
    "Title": "您没有感冒时也会鼻塞、流鼻涕吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 15,
    "Code": "old_15",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 16,
    "Title": "您有口粘口腻，或睡眠打鼾吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 16,
    "Code": "old_16",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 17,
    "Title": "您容易过敏(对药物、食物、气味、花粉或在季节交替、气候变化时)吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "从来没有|一年1、2次|一年3、4次|一年5、6次|每次遇到上述原因都过敏",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 17,
    "Code": "old_17",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 18,
    "Title": "您的皮肤容易起荨麻疹吗? (包括风团、风疹块、风疙瘩)",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 18,
    "Code": "old_18",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 19,
    "Title": "您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗?（指皮肤在没有外伤的情况下出现青一块紫一块的情况）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 19,
    "Code": "old_19",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 20,
    "Title": "您的皮肤一抓就红，并出现抓痕吗?（指被指甲或钝物划过后皮肤的反应）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 20,
    "Code": "old_20",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 21,
    "Title": "您皮肤或口唇干吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 21,
    "Code": "old_21",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 22,
    "Title": "您有肢体麻木或固定部位疼痛的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 22,
    "Code": "old_22",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 23,
    "Title": "您面部或鼻部有油腻感或者油亮发光吗?（指脸上或鼻子）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 23,
    "Code": "old_23",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 24,
    "Title": "您面色或目眶晦黯，或出现褐色斑块/斑点吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 24,
    "Code": "old_24",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 25,
    "Title": "您有皮肤湿疹、疮疖吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 25,
    "Code": "old_25",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 26,
    "Title": "您感到口干咽燥、总想喝水吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 26,
    "Code": "old_26",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 27,
    "Title": "您感到口苦或嘴里有异味吗?（指口苦或口臭）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 27,
    "Code": "old_27",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 28,
    "Title": "您腹部肥大吗?（指腹部脂肪肥厚）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "腹围\u003c80cm，相当于2.4尺|腹围80-85cm，2.4-2.55尺|腹围86-90cm，2.56-2.7尺|腹围91-105cm，2.71-3.15尺|腹围\u003e105cm\r\n或3.15尺",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 28,
    "Code": "old_28",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 29,
    "Title": "您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉的东西吗？（指不喜欢吃凉的食物，或吃了凉的食物后会不舒服）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 29,
    "Code": "old_29",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 30,
    "Title": "您有大便黏滞不爽、解不尽的感觉吗?(大便容易粘在马桶或便坑壁上)",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 30,
    "Code": "old_30",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 31,
    "Title": "您容易大便干燥吗?",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 31,
    "Code": "old_31",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 32,
    "Title": "您舌苔厚腻或有舌苔厚厚的感觉吗?（如果自我感觉不清楚可由调查员观察后填写）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 32,
    "Code": "old_32",
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 33,
    "Title": "您舌下静脉瘀紫或增粗吗？（可由调查员辅助观察后填写）",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choiceextend": "",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 33,
    "Code": "old_33",
    "Memo": "",
    "Isactive": 1
  }
];
export const SCALE_NINE =[
  {
    "Id": 1,
    "Title": "您容易失眠吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 1,
    "Code": "nine_1",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 2,
    "Title": "您容易忘事（健忘）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 2,
    "Code": "nine_2",
    "Physique": "平和质|血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 3,
    "Title": "您容易疲乏吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 3,
    "Code": "nine_3",
    "Physique": "平和质|气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 4,
    "Title": "您感到闷闷不乐、情绪低沉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 4,
    "Code": "nine_4",
    "Physique": "平和质|气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 5,
    "Title": "您比一般人耐受不了寒冷（冬天的寒冷，夏天的冷空调、电扇等）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 5,
    "Code": "nine_5",
    "Physique": "平和质|阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 6,
    "Title": "您精力充沛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 6,
    "Code": "nine_6",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 7,
    "Title": "您能适应外界自然和社会环境的变化吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 7,
    "Code": "nine_7",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 8,
    "Title": "您说话声音低弱无力吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 8,
    "Code": "nine_8",
    "Physique": "平和质|气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 9,
    "Title": "您喜欢安静、懒得说话吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 9,
    "Code": "nine_9",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 10,
    "Title": "您容易头晕或站起来晕眩吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 10,
    "Code": "nine_10",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 11,
    "Title": "您容易心慌吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 11,
    "Code": "nine_11",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 12,
    "Title": "您容易气短（呼吸短促，接不上气）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 12,
    "Code": "nine_12",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 13,
    "Title": "您比别人容易感冒吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 13,
    "Code": "nine_13",
    "Physique": "气虚质|阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 14,
    "Title": "您活动量稍大就容易出虚汗吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 14,
    "Code": "nine_14",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 15,
    "Title": "您受凉或吃（喝）凉的东西后，容易腹泻（拉肚子）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 15,
    "Code": "nine_15",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 16,
    "Title": "您吃（喝）凉的东西会感到不舒服或怕吃（喝）凉的东西吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 16,
    "Code": "nine_16",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 17,
    "Title": "您感到怕冷、衣服比别人穿得多吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 17,
    "Code": "nine_17",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 18,
    "Title": "您手脚发凉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 18,
    "Code": "nine_18",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 19,
    "Title": "您胃脘部背部或腰膝部怕冷吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 19,
    "Code": "nine_19",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 20,
    "Title": "您口唇的颜色比一般人红吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 20,
    "Code": "nine_20",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 21,
    "Title": "您容易便秘或大便干燥吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 21,
    "Code": "nine_21",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 22,
    "Title": "您感到口干咽燥、总想喝水吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 22,
    "Code": "nine_22",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 23,
    "Title": "您感到手脚心发热吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 23,
    "Code": "nine_23",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 24,
    "Title": "您感到眼睛干涩吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 24,
    "Code": "nine_24",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 25,
    "Title": "您感觉身体、脸上发热吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 25,
    "Code": "nine_25",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 26,
    "Title": "您皮肤或口唇干吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 26,
    "Code": "nine_26",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 27,
    "Title": "您面部两颧潮红或偏红吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 27,
    "Code": "nine_27",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 28,
    "Title": "您上眼睑比别人肿（上眼睑有轻微隆起的现象）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 28,
    "Code": "nine_28",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 29,
    "Title": "您嘴里有黏黏的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 29,
    "Code": "nine_29",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 30,
    "Title": "您平时痰多，特别是喉部总感到有痰堵着吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 30,
    "Code": "nine_30",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 31,
    "Title": "您感到胸闷或腹部胀满吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 31,
    "Code": "nine_31",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 32,
    "Title": "您感到身体沉重不轻松或不爽快吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 32,
    "Code": "nine_32",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 33,
    "Title": "您有额部油脂分泌多的现象吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 33,
    "Code": "nine_33",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 34,
    "Title": "您腹部肥满松软吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 34,
    "Code": "nine_34",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 35,
    "Title": "您舌苔厚腻或有舌苔厚厚的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 35,
    "Code": "nine_35",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 36,
    "Title": "您大便黏滞不爽、有解不干尽的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 36,
    "Code": "nine_36",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 37,
    "Title": "您容易生痤疮或疮疖吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 37,
    "Code": "nine_37",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 38,
    "Title": "您小便时尿道有发热感、尿色浓（深度）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 38,
    "Code": "nine_38",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 39,
    "Title": "您感到口苦或嘴有异味吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 39,
    "Code": "nine_39",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 41,
    "Title": "您的阴囊部位潮湿吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 41,
    "Code": "nine_41",
    "Physique": "湿热质",
    "Gender": 1,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 42,
    "Title": "您面部或鼻部有油腻感或者油亮发光吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 42,
    "Code": "nine_42",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 43,
    "Title": "您的两颧部有细微红丝吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 43,
    "Code": "nine_43",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 44,
    "Title": "您口唇颜色偏黯吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 44,
    "Code": "nine_44",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 45,
    "Title": "您容易有黑眼圈吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 45,
    "Code": "nine_45",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 46,
    "Title": "您的皮肤在不知不觉中会出现青紫瘀斑（皮下出血）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 46,
    "Code": "nine_46",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 47,
    "Title": "您身体上有哪里疼痛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 47,
    "Code": "nine_47",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 48,
    "Title": "您面色晦黯或容易出现褐斑吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 48,
    "Code": "nine_48",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 49,
    "Title": "您咽喉部有异物感，且吐之不出、咽之不下吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 49,
    "Code": "nine_49",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 50,
    "Title": "您多愁善感、感情脆弱吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 50,
    "Code": "nine_50",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 51,
    "Title": "您容易感到害怕或受到惊吓吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 51,
    "Code": "nine_51",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 52,
    "Title": "您容易精神紧张、焦虑不安吗？ ",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 52,
    "Code": "nine_52",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 53,
    "Title": "您斜肋部或乳房胀痛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 53,
    "Code": "nine_53",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 54,
    "Title": "您无缘无故叹气吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 54,
    "Code": "nine_54",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 55,
    "Title": "您容易过敏(对药物、食物、气味、花粉或季节交替、气候变化时)吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 55,
    "Code": "nine_55",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 56,
    "Title": "您有因季节变化、温度变化或异味等原因而咳喘的现象吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 56,
    "Code": "nine_56",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 57,
    "Title": "您没有感冒也会打喷嚏吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 57,
    "Code": "nine_57",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 58,
    "Title": "您没有感冒也会鼻塞、流鼻涕吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 58,
    "Code": "nine_58",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 59,
    "Title": "您的皮肤一抓就红，并出现抓痕吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 59,
    "Code": "nine_59",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 60,
    "Title": "您的皮肤因过敏出现过紫癜（紫红色瘀点、瘀斑）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 60,
    "Code": "nine_60",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 61,
    "Title": "您的皮肤容易起荨麻疹（风团、风疹块、风疙瘩）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 61,
    "Code": "nine_61",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  }
];
export const SCALE_NINE_X = [
  {
    "Id": 1,
    "Title": "您容易失眠吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 1,
    "Code": "nine_1",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 2,
    "Title": "您容易忘事（健忘）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 2,
    "Code": "nine_2",
    "Physique": "平和质|血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 3,
    "Title": "您容易疲乏吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 3,
    "Code": "nine_3",
    "Physique": "平和质|气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 4,
    "Title": "您感到闷闷不乐、情绪低沉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 4,
    "Code": "nine_4",
    "Physique": "平和质|气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 5,
    "Title": "您比一般人耐受不了寒冷（冬天的寒冷，夏天的冷空调、电扇等）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 5,
    "Code": "nine_5",
    "Physique": "平和质|阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 6,
    "Title": "您精力充沛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 6,
    "Code": "nine_6",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 7,
    "Title": "您能适应外界自然和社会环境的变化吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 7,
    "Code": "nine_7",
    "Physique": "平和质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 8,
    "Title": "您说话声音低弱无力吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 8,
    "Code": "nine_8",
    "Physique": "平和质|气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 9,
    "Title": "您喜欢安静、懒得说话吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 9,
    "Code": "nine_9",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 10,
    "Title": "您容易头晕或站起来晕眩吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 10,
    "Code": "nine_10",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 11,
    "Title": "您容易心慌吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 11,
    "Code": "nine_11",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 12,
    "Title": "您容易气短（呼吸短促，接不上气）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 12,
    "Code": "nine_12",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 13,
    "Title": "您比别人容易感冒吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 13,
    "Code": "nine_13",
    "Physique": "气虚质|阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 14,
    "Title": "您活动量稍大就容易出虚汗吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 14,
    "Code": "nine_14",
    "Physique": "气虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 15,
    "Title": "您受凉或吃（喝）凉的东西后，容易腹泻（拉肚子）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 15,
    "Code": "nine_15",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 16,
    "Title": "您吃（喝）凉的东西会感到不舒服或怕吃（喝）凉的东西吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 16,
    "Code": "nine_16",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 17,
    "Title": "您感到怕冷、衣服比别人穿得多吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 17,
    "Code": "nine_17",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 18,
    "Title": "您手脚发凉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 18,
    "Code": "nine_18",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 19,
    "Title": "您胃脘部背部或腰膝部怕冷吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 19,
    "Code": "nine_19",
    "Physique": "阳虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 20,
    "Title": "您口唇的颜色比一般人红吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 20,
    "Code": "nine_20",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 21,
    "Title": "您容易便秘或大便干燥吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 21,
    "Code": "nine_21",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 22,
    "Title": "您感到口干咽燥、总想喝水吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 22,
    "Code": "nine_22",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 23,
    "Title": "您感到手脚心发热吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 23,
    "Code": "nine_23",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 24,
    "Title": "您感到眼睛干涩吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 24,
    "Code": "nine_24",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 25,
    "Title": "您感觉身体、脸上发热吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 25,
    "Code": "nine_25",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 26,
    "Title": "您皮肤或口唇干吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 26,
    "Code": "nine_26",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 27,
    "Title": "您面部两颧潮红或偏红吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 27,
    "Code": "nine_27",
    "Physique": "阴虚质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 28,
    "Title": "您上眼睑比别人肿（上眼睑有轻微隆起的现象）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 28,
    "Code": "nine_28",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 29,
    "Title": "您嘴里有黏黏的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 29,
    "Code": "nine_29",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 30,
    "Title": "您平时痰多，特别是喉部总感到有痰堵着吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 30,
    "Code": "nine_30",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 31,
    "Title": "您感到胸闷或腹部胀满吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 31,
    "Code": "nine_31",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 32,
    "Title": "您感到身体沉重不轻松或不爽快吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 32,
    "Code": "nine_32",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 33,
    "Title": "您有额部油脂分泌多的现象吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 33,
    "Code": "nine_33",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 34,
    "Title": "您腹部肥满松软吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 34,
    "Code": "nine_34",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 35,
    "Title": "您舌苔厚腻或有舌苔厚厚的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 35,
    "Code": "nine_35",
    "Physique": "痰湿质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 36,
    "Title": "您大便黏滞不爽、有解不干尽的感觉吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 36,
    "Code": "nine_36",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 37,
    "Title": "您容易生痤疮或疮疖吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 37,
    "Code": "nine_37",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 38,
    "Title": "您小便时尿道有发热感、尿色浓（深度）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 38,
    "Code": "nine_38",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 39,
    "Title": "您感到口苦或嘴有异味吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 39,
    "Code": "nine_39",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 40,
    "Title": "您带下色黄（白带颜色发黄）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 40,
    "Code": "nine_40",
    "Physique": "湿热质",
    "Gender": 2,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 42,
    "Title": "您面部或鼻部有油腻感或者油亮发光吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 42,
    "Code": "nine_42",
    "Physique": "湿热质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 43,
    "Title": "您的两颧部有细微红丝吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 43,
    "Code": "nine_43",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 44,
    "Title": "您口唇颜色偏黯吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 44,
    "Code": "nine_44",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 45,
    "Title": "您容易有黑眼圈吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 45,
    "Code": "nine_45",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 46,
    "Title": "您的皮肤在不知不觉中会出现青紫瘀斑（皮下出血）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 46,
    "Code": "nine_46",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 47,
    "Title": "您身体上有哪里疼痛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 47,
    "Code": "nine_47",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 48,
    "Title": "您面色晦黯或容易出现褐斑吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 48,
    "Code": "nine_48",
    "Physique": "血瘀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 49,
    "Title": "您咽喉部有异物感，且吐之不出、咽之不下吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 49,
    "Code": "nine_49",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 50,
    "Title": "您多愁善感、感情脆弱吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 50,
    "Code": "nine_50",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 51,
    "Title": "您容易感到害怕或受到惊吓吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 51,
    "Code": "nine_51",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 52,
    "Title": "您容易精神紧张、焦虑不安吗？ ",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 52,
    "Code": "nine_52",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 53,
    "Title": "您斜肋部或乳房胀痛吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 53,
    "Code": "nine_53",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 54,
    "Title": "您无缘无故叹气吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 54,
    "Code": "nine_54",
    "Physique": "气郁质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 55,
    "Title": "您容易过敏(对药物、食物、气味、花粉或季节交替、气候变化时)吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 55,
    "Code": "nine_55",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 56,
    "Title": "您有因季节变化、温度变化或异味等原因而咳喘的现象吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 56,
    "Code": "nine_56",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 57,
    "Title": "您没有感冒也会打喷嚏吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 57,
    "Code": "nine_57",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 58,
    "Title": "您没有感冒也会鼻塞、流鼻涕吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 58,
    "Code": "nine_58",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 59,
    "Title": "您的皮肤一抓就红，并出现抓痕吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 59,
    "Code": "nine_59",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 60,
    "Title": "您的皮肤因过敏出现过紫癜（紫红色瘀点、瘀斑）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 60,
    "Code": "nine_60",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  },
  {
    "Id": 61,
    "Title": "您的皮肤容易起荨麻疹（风团、风疹块、风疙瘩）吗？",
    "Choicekey": "没有|很少|有时|经常|总是",
    "Choicememo": "根本不/从来没有|有一点/偶尔|有些/少数时间|相当/多数时间|非常/每天",
    "Choicevalue": "1|2|3|4|5",
    "Sortorder": 61,
    "Code": "nine_61",
    "Physique": "特禀质",
    "Gender": 3,
    "Memo": "",
    "Isactive": 1
  }
];
export const SCALE_PHY = [
  {
    Id: 1,
    Title: '不管别人对我有什么看法，我都不在乎',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 1,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 2,
    Title: '我对日常生活中感兴趣的事太多了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 2,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 3,
    Title: '我说话做事，不快不慢，从容不迫',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 3,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 4,
    Title: '我不爱流露我的情感',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 4,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 5,
    Title: '就是在人多热闹的场合，我也感到孤独，或者提不起兴趣',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 5,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 6,
    Title: '我的朋友们说我是个急性子',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 6,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 7,
    Title: '我态度乐观，对困难并不忧心忡忡',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 7,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 8,
    Title: '当我要发火的时候，我总尽力克制下来',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 8,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 9,
    Title: '我不喜欢交际，总避开人多的地方',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 9,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 10,
    Title: '我认为毫不动摇地维护自己的观点是必要的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 10,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 11,
    Title: '我的爱好很广，但我并不长期坚持某一项目',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 11,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 12,
    Title: '我处理问题，会周全考虑其正反两方面',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 12,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 13,
    Title: '我比较拘谨，为人处事很有节制',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 13,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 14,
    Title: '我常感到自己什么都不行',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 14,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 15,
    Title: '照我的意见做的事，即使失败了，我也并不追悔',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 15,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 16,
    Title: '在公共场所，我不怕陌生人，常跟生人交谈',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 16,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 17,
    Title: '别人说我做事有耐心，能坚持',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 17,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 18,
    Title: '我常担心会发生不幸事件',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 18,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 19,
    Title: '我要做的事，不论遇到什么困难，也要争取完成',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 19,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 20,
    Title: '我容易对一个事情做出决定',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 20,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 21,
    Title: '别人说我态度从容，举止稳妥',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 21,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 22,
    Title: '我的朋友们说我办事稳健谨慎',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 22,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 23,
    Title: '别人说我多愁善感，思虑较多',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 23,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 24,
    Title: '我爱打抱不平',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 24,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 25,
    Title: '在沉闷的场合，我能给大家添些生气，使气氛活跃起来',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 25,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 26,
    Title: '我不容易生气',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 26,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 27,
    Title: '我说话做事有条有理，慢吞吞的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 27,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 28,
    Title: '我的情绪时常波动',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 28,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 29,
    Title: '我总是昂首（头）挺胸',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 29,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 30,
    Title: '我平时说话时手势和表情丰富',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 30,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 31,
    Title: '我对人对事既热情又冷静',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 31,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 32,
    Title: '出风头的事，我不想干',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 32,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 33,
    Title: '我有时无缘无故的感到不安',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 33,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 34,
    Title: '当有人挑剔我做的事，我必定与他争论一番',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 34,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 35,
    Title: '别人说我开朗随和',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 35,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 36,
    Title: '我的态度往往是和悦而严肃的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 36,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 37,
    Title: '我不容易改变观点，但我却不为此与人争辩',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 37,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 38,
    Title: '我容易疲倦，且无精打彩',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 38,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 39,
    Title: '朋友们说我办事有魄力，敢顶撞',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 39,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 40,
    Title: '我对事物的反应很快，从这件事一下就联系到别的事上了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 40,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 41,
    Title: '无论是高兴或不高兴的事，我都坦然处之',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 41,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 42,
    Title: '我极少冒险',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 42,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 43,
    Title: '人家说我对人冷淡，缺乏热情',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 43,
    Memo: '',
    Isactive: 1,
  },
];
export const SCALE_PHY2 = [
  {
    Id: 1,
    Title: '凡是我认为正确的事情，我都要坚持',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 1,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 2,
    Title: '我对日常生活中感兴趣的事太多了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 2,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 3,
    Title: '别人对我特别好时，我常疑心他们另有目的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 3,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 4,
    Title: '好像我周围的人都不怎么了解我',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 4,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 5,
    Title: '不管别人对我有什么看法，我都不在乎',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 5,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 6,
    Title: '我与周围的人都合得来 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 6,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 7,
    Title: '我说话做事，很有分寸',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 7,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 8,
    Title: '我遇事镇静，不容易激动 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 8,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 9,
    Title: '我时常感到悲观失望',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 9,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 10,
    Title: '我读报纸时，对我所关心的事情看的详细，有的我只看标题',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 10,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 11,
    Title: '在排队的时候，有人插队，我就向他提意见，不惜与他争吵一番',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 11,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 12,
    Title: '我喜欢人多热闹的场合',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 12,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 13,
    Title: '我认为对任何人都不要太相信，比较安全',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 13,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 14,
    Title: '我喜欢独自一人',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 14,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 15,
    Title: '我经常是愉快的，很少忧虑',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 15,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 16,
    Title: '我经常是愉快的，很少忧虑',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 16,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 17,
    Title: '我说话做事，不快不慢，从容不迫',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 17,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 18,
    Title: '我不爱流露我的情感',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 18,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 19,
    Title: '我优柔寡断，不能当机立断，所以把许多机会都丢掉了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 19,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 20,
    Title: '有时我办事为达到目的，也找关系，但次数不多',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 20,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 21,
    Title: '我的朋友们说我是个急性子',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 21,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 22,
    Title: '我对任何事情都抱乐观态度，对困难并不忧心忡忡',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 22,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 23,
    Title: '我性情不急躁，也不疲踏(拖拉)',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 23,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 24,
    Title: '当我要发火的时候，我总尽力克制下来',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 24,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 25,
    Title: '我缺乏自信心',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 25,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 26,
    Title: '我认为毫不动摇地维护自己的观点是必要的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 26,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 27,
    Title: '对不同种类的游戏和娱乐，我都喜欢',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 27,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 28,
    Title: '我认为对人不能过于热情',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 28,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 29,
    Title: '我不愿意同别人讲话，即使他先开口，我也只应付一下',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 29,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 30,
    Title: '有时我也说一两句谎话',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 30,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 31,
    Title: '我不轻率作决定，一旦做出决定后，也不轻易更改',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 31,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 32,
    Title: '我的爱好很广，但我并不长期坚持某一项目',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 32,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 33,
    Title: '我处理问题，必定反复考虑其正反两方面',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 33,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 34,
    Title: '我的态度从容，举止安详',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 34,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 35,
    Title: '就是在人多热闹的场合，我也感到孤独，或者提不起兴趣',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 35,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 36,
    Title: '照我的意见做的事，即使失败了，我也并不追悔',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 36,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 37,
    Title: '在公共场所，我不怕陌生人，常跟生人交谈 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 37,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 38,
    Title: '我不愿针对别人的行为表示强烈的反对或同意',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 38,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 39,
    Title: '我不喜欢交际，总避开人多的地方',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 39,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 40,
    Title: '我认为一个人应具有不屈不挠的精神',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 40,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 41,
    Title: '我容易对一个事情做出决定',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 41,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 42,
    Title: '我很拘谨，我认为对事、对人都不能随随便便',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 42,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 43,
    Title: '我常感到自己什么都不行',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 43,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 44,
    Title: '太忙时，我就有些急躁',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 44,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 45,
    Title: '我要做的事，不管遇到什么困难，也要争取完成 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 45,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 46,
    Title: '有人夸奖我时，我就感到洋洋得意 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 46,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 47,
    Title: '我不容易生气 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 47,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 48,
    Title: '我性情温和，不愿与人争吵，也不与人深交',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 48,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 49,
    Title: '我常担心会发生不幸事件',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 49,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 50,
    Title: '我爱打抱不平',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 50,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 51,
    Title: '我活泼热情，主动交朋友',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 51,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 52,
    Title: '我觉得做事要有耐心，急也无用',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 52,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 53,
    Title: '我常常多愁善感，忧虑重重',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 53,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 54,
    Title: '要说服我改变主意是不容易的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 54,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 55,
    Title: '有人挑剔我工作中的毛病时，我就不积极了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 55,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 56,
    Title: '我对我的朋友和同事并不都是一样喜欢，对有的人好些，对有的人差些 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 56,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 57,
    Title: '我脚踏实地做事，但主动性不够 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 57,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 58,
    Title: '我的情绪时常波动 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 58,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 59,
    Title: '我总是昂首（头）挺胸 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 59,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 60,
    Title: '在沉闷的场合，我能给大家添些生气，使气氛活跃起来 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 60,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 61,
    Title: '我处理问题不偏不倚，所以很少出错误 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 61,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 62,
    Title: '我的朋友们说我办事稳健谨慎 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 62,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 63,
    Title: '我没有什么爱好，兴趣很窄 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 63,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 64,
    Title: '有人挑剔我的工作时，我必定与他争论一番 ',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 64,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 65,
    Title: '我常争取机会到外地参观访问',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 65,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 66,
    Title: '我说话做事不求快，慢腾腾的，有条有理',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 66,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 67,
    Title: '我有时无缘无故的感到不安',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 67,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 68,
    Title: '压是压不服我的，口服都不容易，更不用说心服',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 68,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 69,
    Title: '我说话时常指手划脚',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 69,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 70,
    Title: '出风头的事，我不想干',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 70,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 71,
    Title: '我宁愿一人呆在家里而不想出去访朋会友',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 71,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 72,
    Title: '我认为每人多少都有点私心，我自己也不例外',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 72,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 73,
    Title: '我想做的事，说干就干，恨不能立即做成',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 73,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 74,
    Title: '人少时我就感到寂寞',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 74,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 75,
    Title: '我常悠闲自得',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 75,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 76,
    Title: '我不容易改变观点，但我却不为此与人争辩',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 76,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 77,
    Title: '我容易疲倦，且无精打彩',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 77,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 78,
    Title: '我不怕打击',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 78,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 79,
    Title: '我认为不需要谨小慎微，不要过于注意小节',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 79,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 80,
    Title: '我对人处事都比较有节制',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 80,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 81,
    Title: '我对什么事都无所谓',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 81,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 82,
    Title: '别人说我开朗随和',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 82,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 83,
    Title: '我从不冒险',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 83,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 84,
    Title: '人家说我对人冷淡，缺乏热情',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 84,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 85,
    Title: '我对人对事既热情又冷静',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 85,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 86,
    Title: '朋友们说我办事有魄力，敢顶撞',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 86,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 87,
    Title: '我不拘谨，往往有些粗心',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 87,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 88,
    Title: '我的举止言行都很稳重',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 88,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 89,
    Title: '我不想大有作为而得过且过',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 89,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 90,
    Title: '我有时完不成当天的工作而拖到第二天',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 90,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 91,
    Title: '我处理事情快、果断、但不老练',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 91,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 92,
    Title: '我对人总是有礼貌而谦让的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 92,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 93,
    Title: '我宁愿依靠他人而不愿自立门面',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 93,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 94,
    Title: '我的态度往往是和悦而严肃的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 94,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 95,
    Title: '假如人们说我主观，我不以为然',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 95,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 96,
    Title: '我对事物的反应很快，从这件事一下就联系到别的事上了',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 96,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 97,
    Title: '我觉得察言观色而后行事，是必要的',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 97,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 98,
    Title: '我时常生闷气',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 98,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 99,
    Title: '无论是高兴或不高兴的事，我都坦然处之',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 99,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 100,
    Title: '我自信我的理想若能实现，就可以做出成绩',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 100,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 101,
    Title: '我喜欢说笑话和谈论有趣的事',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 101,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 102,
    Title: '我认为一个人一辈子很难一点违心的事都不做',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 102,
    Memo: '',
    Isactive: 1,
  },
  {
    Id: 103,
    Title: '我常沉思默想，有时想得脱离现实',
    Choicekey: '是|否',
    Choicevalue: '1|0',
    Sortorder: 103,
    Memo: '',
    Isactive: 1,
  },
];
export const SCALE_PRE = [
  {
    title: '五官及其他症状',
    subject: [
      {
        id: 's1',
        title: '我自觉：',
        item: [
          {
            id: '1',
            title: '脸色不好，较瘦，怀孕后体重无明显增加甚至有所减轻',
          },
          {
            id: '2',
            title: '体胖（参考医生意见）',
          },
          {
            id: '3',
            title: '脸色暗沉',
          },
          {
            id: '4',
            title: '脸色不好，没有光泽',
          },
        ],
      },
      {
        id: 's2',
        title: '我近来：',
        item: [
          {
            id: '5',
            title: '鼻干',
          },
          {
            id: '6',
            title: '唇干裂',
          },
          {
            id: '7',
            title: '流鼻血或鼻内有血丝',
          },
          {
            id: '8',
            title: '声音嘶哑或不能出声（排除感冒）',
          },
          {
            id: '9',
            title: '耳鸣',
          },
          {
            id: '10',
            title: '牙龈萎缩',
          },
        ],
      },
      {
        id: 's3',
        title: '我觉得：',
        item: [
          {
            id: '11',
            title: '渴不多饮',
          },
          {
            id: '12',
            title: '口干',
          },
          {
            id: '13',
            title: '口渴',
          },
          {
            id: '14',
            title: '口苦',
          },
        ],
      },
      {
        id: 's4',
        title: '我近来：',
        item: [
          {
            id: '15',
            title: '痰多',
          },
          {
            id: '16',
            title: '咳嗽',
          },
          {
            id: '17',
            title: '咳嗽痰黄',
          },
          {
            id: '18',
            title: '气喘',
          },
          {
            id: '19',
            title: '气喘，难以平卧',
          },
          {
            id: '20',
            title: '气喘痰多',
          },
        ],
      },
    ],
  },
  {
    title: '孕期特有症状',
    subject: [
      {
        id: 's5',
        title: '我的饮食：',
        item: [
          {
            id: '21',
            title: '贪食瓜果',
          },
          {
            id: '22',
            title: '喜欢吃冷的',
          },
          {
            id: '23',
            title: '食少',
          },
          {
            id: '24',
            title: '吃饭不香',
          },
        ],
      },
      {
        id: 's6',
        title: '我近来：',
        item: [
          {
            id: '25',
            title: '闻到某种食物味道时感觉恶心',
          },
          {
            id: '26',
            title: '反酸，或从胃里往上反气，带有食物的味道',
          },
          {
            id: '27',
            title: '常进食后即感腹痛腹胀，泻后缓解',
          },
          {
            id: '28',
            title: '胃中胀闷',
          },
        ],
      },
      {
        id: 's7',
        title: '我常：',
        item: [
          {
            id: '29',
            title: '呕吐',
          },
          {
            id: '30',
            title: '呕吐，吃不下饭',
          },
          {
            id: '31',
            title: '呕吐痰涎',
          },
          {
            id: '32',
            title: '频吐稀沫',
          },
        ],
      },
      {
        id: 's8',
        title: '我近来：',
        item: [
          {
            id: '33',
            title: '腹满腹胀',
          },
          {
            id: '34',
            title: '气胀，腹痛',
          },
          {
            id: '35',
            title: '腹痛',
          },
          {
            id: '36',
            title: '胁肋部胀或疼痛',
          },
        ],
      },
      {
        id: 's9',
        title: '我出现：',
        item: [
          {
            id: '37',
            title: '面部虚浮微肿',
          },
          {
            id: '38',
            title: '下眼睑肿',
          },
          {
            id: '39',
            title: '膝以下浮肿',
          },
          {
            id: '40',
            title: '两足自脚面渐肿至腿膝',
          },
          {
            id: '41',
            title: '先两足肿，渐至遍身，后及头面',
          },
          {
            id: '42',
            title: '一身及手足面目俱浮肿',
          },
        ],
      },
    ],
  },
  {
    title: '情绪及相关症状',
    subject: [
      {
        id: 's10',
        title: '我近来感觉：',
        item: [
          {
            id: '66',
            title: '头昏沉，头脑不是很清楚',
          },
          {
            id: '67',
            title: '头痛',
          },
          {
            id: '68',
            title: '头晕',
          },
          {
            id: '69',
            title: '头晕，视物不清',
          },
          {
            id: '70',
            title: '精神疲倦',
          },
        ],
      },
      {
        id: 's11',
        title: '我近来：',
        item: [
          {
            id: '71',
            title: '多梦',
          },
          {
            id: '72',
            title: '少眠多梦',
          },
          {
            id: '73',
            title: '彻夜不眠',
          },
          {
            id: '74',
            title: '失眠',
          },
          {
            id: '75',
            title: '健忘',
          },
        ],
      },
      {
        id: 's12',
        title: '我的情绪：',
        item: [
          {
            id: '76',
            title: '易怒',
          },
          {
            id: '77',
            title: '易受惊吓',
          },
          {
            id: '78',
            title: '思虑过度/压力大',
          },
          {
            id: '79',
            title: '情绪莫名低落',
          },
          {
            id: '80',
            title: '心烦，易激动',
          },
          {
            id: '81',
            title: '心烦憋闷',
          },
          {
            id: '82',
            title: '心烦不宁',
          },
          {
            id: '83',
            title: '心烦欲呕',
          },
        ],
      },
      {
        id: 's13',
        title: '我近来：',
        item: [
          {
            id: '91',
            title: '气短',
          },
          {
            id: '92',
            title: '气短懒言',
          },
          {
            id: '93',
            title: '心慌心跳快，感觉心脏搏动强烈，伴心前区不适感',
          },
          {
            id: '94',
            title: '心痛',
          },
          {
            id: '95',
            title: '胸闷',
          },
          {
            id: '96',
            title: '膈下或胸部不适，似乎有种被东西往上顶的感觉',
          },
          {
            id: '97',
            title: '胸膈胀满',
          },
        ],
      },
    ],
  },
  {
    title: '身体感觉及其他症状',
    subject: [
      {
        id: 's14',
        title: '我感觉：',
        item: [
          {
            id: '43',
            title: '手抖',
          },
          {
            id: '44',
            title: '容易抽筋',
          },
          {
            id: '45',
            title: '指甲苍白，全无血色',
          },
          {
            id: '46',
            title: '手足冷',
          },
          {
            id: '47',
            title: '两足冰冷',
          },
        ],
      },
      {
        id: 's15',
        title: '我近来有：',
        item: [
          {
            id: '48',
            title: '手足发麻',
          },
          {
            id: '49',
            title: '身体发麻',
          },
          {
            id: '50',
            title: '身痛（排除感冒）',
          },
          {
            id: '51',
            title: '肢体沉重伴关节疼痛',
          },
          {
            id: '52',
            title: '手或足易起疹子或小水泡',
          },
        ],
      },
      {
        id: 's16',
        title: '我感觉：',
        item: [
          {
            id: '53',
            title: '体倦乏力',
          },
          {
            id: '54',
            title: '体倦嗜卧',
          },
          {
            id: '55',
            title: '膝酸软',
          },
          {
            id: '56',
            title: '腰腿酸软',
          },
          {
            id: '57',
            title: '身子沉，懒于活动',
          },
        ],
      },
      {
        id: 's17',
        title: '我：',
        item: [
          {
            id: '84',
            title: '感觉全身发热或身体某些部位发热，人不舒服但体温正常',
          },
          {
            id: '85',
            title: '畏寒喜暖',
          },
          {
            id: '86',
            title: '畏寒肢冷',
          },
          {
            id: '87',
            title: '下午或夜间不适症状加重',
          },
          {
            id: '88',
            title: '睡觉时不能平卧',
          },
          {
            id: '89',
            title: '睡觉时出汗',
          },
          {
            id: '90',
            title: '容易出汗（活动时更明显）',
          },
        ],
      },
    ],
  },
  {
    title: '二便及其他症状',
    subject: [
      {
        id: 's18',
        title: '我感觉：',
        item: [
          {
            id: '58',
            title: '腰腹一圈痛（大致相当于带脉经行处）',
          },
          {
            id: '59',
            title: '腰胯酸软',
          },
          {
            id: '60',
            title: '腰痛',
          },
          {
            id: '61',
            title: '腹内重坠',
          },
          {
            id: '62',
            title: '腹内重坠伴小腹坠胀',
          },
          {
            id: '63',
            title: '腹痛坠胀',
          },
          {
            id: '65',
            title: '妊娠三四月，胎便作痛',
          },
        ],
      },
      {
        id: 's19',
        title: '我的大便情况：',
        item: [
          {
            id: '98',
            title: '便秘',
          },
          {
            id: '99',
            title: '便血',
          },
          {
            id: '100',
            title: '大便不成形',
          },
          {
            id: '101',
            title: '大便干结',
          },
          {
            id: '102',
            title: '易腹泻',
          },
        ],
      },
      {
        id: 's20',
        title: '我的小便情况：',
        item: [
          {
            id: '103',
            title: '欲小便而不得出，或量少而不爽利',
          },
          {
            id: '104',
            title: '小便不通或频数点滴而下',
          },
          {
            id: '105',
            title: '小便频数，点滴而痛',
          },
          {
            id: '106',
            title: '小便少',
          },
          {
            id: '107',
            title: '尿频，夜间尤甚',
          },
        ],
      },
      // {
      //   id: 's21',
      //   title: '我的舌脉(建议由医生填写)：',
      //   item: [
      //     {
      //       id: '108',
      //       title: '舌红少苔',
      //     },
      //     {
      //       id: '109',
      //       title: '苔黄',
      //     },
      //     {
      //       id: '110',
      //       title: '脉滑数',
      //     },
      //     {
      //       id: '111',
      //       title: '脉弦数',
      //     },
      //   ],
      // },
    ],
  },
];
export const SCALE_PRE_ITEM = {
  '2': '1',
  '1': '2',
  '98': '100',
  '100': '98;101',
  '101': '100',
}
export const SCALE_PRE_VALUE = [
  {
    id: '1',
    title: '脸色不好，较瘦，怀孕后体重无明显增加甚至有所减轻',
  },
  {
    id: '2',
    title: '体胖（参考医生意见）',
  },
  {
    id: '3',
    title: '脸色暗沉',
  },
  {
    id: '4',
    title: '脸色不好，没有光泽',
  },
  {
    id: '5',
    title: '鼻干',
  },
  {
    id: '6',
    title: '唇干裂',
  },
  {
    id: '7',
    title: '流鼻血或鼻内有血丝',
  },
  {
    id: '8',
    title: '声音嘶哑或不能出声（排除感冒）',
  },
  {
    id: '9',
    title: '耳鸣',
  },
  {
    id: '10',
    title: '牙龈萎缩',
  },
  {
    id: '11',
    title: '渴不多饮',
  },
  {
    id: '12',
    title: '口干',
  },
  {
    id: '13',
    title: '口渴',
  },
  {
    id: '14',
    title: '口苦',
  },
  {
    id: '15',
    title: '痰多',
  },
  {
    id: '16',
    title: '咳嗽',
  },
  {
    id: '17',
    title: '咳嗽痰黄',
  },
  {
    id: '18',
    title: '气喘',
  },
  {
    id: '19',
    title: '气喘，难以平卧',
  },
  {
    id: '20',
    title: '气喘痰多',
  },
  {
    id: '21',
    title: '贪食瓜果',
  },
  {
    id: '22',
    title: '喜欢吃冷的',
  },
  {
    id: '23',
    title: '食少',
  },
  {
    id: '24',
    title: '吃饭不香',
  },
  {
    id: '25',
    title: '闻到某种食物味道时感觉恶心',
  },
  {
    id: '26',
    title: '反酸，或从胃里往上反气，带有食物的味道',
  },
  {
    id: '27',
    title: '常进食后即感腹痛腹胀，泻后缓解',
  },
  {
    id: '28',
    title: '胃中胀闷',
  },
  {
    id: '29',
    title: '呕吐',
  },
  {
    id: '30',
    title: '呕吐，吃不下饭',
  },
  {
    id: '31',
    title: '呕吐痰涎',
  },
  {
    id: '32',
    title: '频吐稀沫',
  },
  {
    id: '33',
    title: '腹满腹胀',
  },
  {
    id: '34',
    title: '气胀，腹痛',
  },
  {
    id: '35',
    title: '腹痛',
  },
  {
    id: '36',
    title: '胁肋部胀或疼痛',
  },
  {
    id: '37',
    title: '面部虚浮微肿',
  },
  {
    id: '38',
    title: '下眼睑肿',
  },
  {
    id: '39',
    title: '膝以下浮肿',
  },
  {
    id: '40',
    title: '两足自脚面渐肿至腿膝',
  },
  {
    id: '41',
    title: '先两足肿，渐至遍身，后及头面',
  },
  {
    id: '42',
    title: '一身及手足面目俱浮肿',
  },
  {
    id: '66',
    title: '头昏沉，头脑不是很清楚',
  },
  {
    id: '67',
    title: '头痛',
  },
  {
    id: '68',
    title: '头晕',
  },
  {
    id: '69',
    title: '头晕，视物不清',
  },
  {
    id: '70',
    title: '精神疲倦',
  },
  {
    id: '71',
    title: '多梦',
  },
  {
    id: '72',
    title: '少眠多梦',
  },
  {
    id: '73',
    title: '彻夜不眠',
  },
  {
    id: '74',
    title: '失眠',
  },
  {
    id: '75',
    title: '健忘',
  },
  {
    id: '76',
    title: '易怒',
  },
  {
    id: '77',
    title: '易受惊吓',
  },
  {
    id: '78',
    title: '思虑过度/压力大',
  },
  {
    id: '79',
    title: '情绪莫名低落',
  },
  {
    id: '80',
    title: '心烦，易激动',
  },
  {
    id: '81',
    title: '心烦憋闷',
  },
  {
    id: '82',
    title: '心烦不宁',
  },
  {
    id: '83',
    title: '心烦欲呕',
  },
  {
    id: '91',
    title: '气短',
  },
  {
    id: '92',
    title: '气短懒言',
  },
  {
    id: '93',
    title: '心慌心跳快，感觉心脏搏动强烈，伴心前区不适感',
  },
  {
    id: '94',
    title: '心痛',
  },
  {
    id: '95',
    title: '胸闷',
  },
  {
    id: '96',
    title: '膈下或胸部不适，似乎有种被东西往上顶的感觉',
  },
  {
    id: '97',
    title: '胸膈胀满',
  },
  {
    id: '43',
    title: '手抖',
  },
  {
    id: '44',
    title: '容易抽筋',
  },
  {
    id: '45',
    title: '指甲苍白，全无血色',
  },
  {
    id: '46',
    title: '手足冷',
  },
  {
    id: '47',
    title: '两足冰冷',
  },
  {
    id: '48',
    title: '手足发麻',
  },
  {
    id: '49',
    title: '身体发麻',
  },
  {
    id: '50',
    title: '身痛（排除感冒）',
  },
  {
    id: '51',
    title: '肢体沉重伴关节疼痛',
  },
  {
    id: '52',
    title: '手或足易起疹子或小水泡',
  },
  {
    id: '53',
    title: '体倦乏力',
  },
  {
    id: '54',
    title: '体倦嗜卧',
  },
  {
    id: '55',
    title: '膝酸软',
  },
  {
    id: '56',
    title: '腰腿酸软',
  },
  {
    id: '57',
    title: '身子沉，懒于活动',
  },
  {
    id: '84',
    title: '感觉全身发热或身体某些部位发热，人不舒服但体温正常',
  },
  {
    id: '85',
    title: '畏寒喜暖',
  },
  {
    id: '86',
    title: '畏寒肢冷',
  },
  {
    id: '87',
    title: '下午或夜间不适症状加重',
  },
  {
    id: '88',
    title: '睡觉时不能平卧',
  },
  {
    id: '89',
    title: '睡觉时出汗',
  },
  {
    id: '90',
    title: '容易出汗（活动时更明显）',
  },
  {
    id: '58',
    title: '腰腹一圈痛（大致相当于带脉经行处）',
  },
  {
    id: '59',
    title: '腰胯酸软',
  },
  {
    id: '60',
    title: '腰痛',
  },
  {
    id: '61',
    title: '腹内重坠',
  },
  {
    id: '62',
    title: '腹内重坠伴小腹坠胀',
  },
  {
    id: '63',
    title: '腹痛坠胀',
  },
  {
    id: '65',
    title: '妊娠三四月，胎便作痛',
  },
  {
    id: '98',
    title: '便秘',
  },
  {
    id: '99',
    title: '便血',
  },
  {
    id: '100',
    title: '大便不成形',
  },
  {
    id: '101',
    title: '大便干结',
  },
  {
    id: '102',
    title: '易腹泻',
  },
  {
    id: '103',
    title: '欲小便而不得出，或量少而不爽利',
  },
  {
    id: '104',
    title: '小便不通或频数点滴而下',
  },
  {
    id: '105',
    title: '小便频数，点滴而痛',
  },
  {
    id: '106',
    title: '小便少',
  },
  {
    id: '107',
    title: '尿频，夜间尤甚',
  },
  {
    id: '108',
    title: '舌红少苔',
  },
  {
    id: '109',
    title: '苔黄',
  },
  {
    id: '110',
    title: '脉滑数',
  },
  {
    id: '111',
    title: '脉弦数',
  },
];
export const SCALE_CHD = [
  {
    title: '家长版测评',
    id: '1',
    subject: [
      {
        title: '平时你孩子怕冷吗（穿衣较多或常常出现手脚发凉）？',
        gender: '3',
        type: '1',
        code: '106',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '平时你孩子出汗多吗（如活动一下就出汗或睡觉时出汗多）？',
        gender: '3',
        type: '1',
        code: '107',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子平时老说累吗或走路时经常让抱抱？',
        gender: '3',
        type: '1',
        code: '108',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子在睡觉过程中有以下哪些情况？（请经常和孩子一块睡觉的家长回答此问题）',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '睡觉时翻来翻去',
            gender: '3',
            type: '1',
            code: '109',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '磨牙',
            gender: '3',
            type: '1',
            code: '110',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '容易惊醒',
            gender: '3',
            type: '1',
            code: '111',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '夜间哭闹',
            gender: '3',
            type: '1',
            code: '112',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '多梦',
            gender: '3',
            type: '1',
            code: '113',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '早上不容易起床',
            gender: '3',
            type: '1',
            code: '114',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '脾气性格：请判断您的孩子包括以下哪些情况？',
        gender: '3',
        type: '2',
        code: '115|116|117|118|119|120|121|122|123',
        choiceKey: '不爱说话|易被惊吓|经常打人或摔东西|经常在小要求不满足后易哭闹|受到批评后易哭|容易急躁/发脾气|胆子小|多动现象|多静少动',
      },
      {
        title: '女孩外阴：',
        gender: '2',
        type: '2',
        code: '124|125|126',
        choiceKey: '易外阴瘙痒|经常有异味|平时分泌物多',
      },
      {
        title: '皮肤情况：',
        gender: '3',
        type: '2',
        code: '127|128|129|130|131|132|133',
        choiceKey: '易冻疮伤|易反复湿疹|易皮肤瘙痒|易荨麻疹|易皮肤抓痕（消退时间长）|皮肤干燥或粗糙|皮肤高敏反应（蚊虫叮咬反应强烈）',
      },
      {
        title: '您孩子既往患病的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '毛细支气管炎',
            gender: '3',
            type: '1',
            code: '134',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '肺炎',
            gender: '3',
            type: '1',
            code: '135',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '哮喘病史',
            gender: '3',
            type: '1',
            code: '136',
            choiceKey: '2年以上|2年|1年',
            choiceValue: '3|2|1',
          },
          {
            title: '高热抽搐史',
            gender: '3',
            type: '1',
            code: '137',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '感冒',
            gender: '3',
            type: '1',
            code: '138',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '咳嗽',
            gender: '3',
            type: '1',
            code: '139',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '发热',
            gender: '3',
            type: '1',
            code: '140',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '做过儿童康复治疗',
            gender: '3',
            type: '2',
            code: '141|142|143',
            choiceKey: '运动方面|语言方面|智力方面',
          },
        ],
      },
      {
        title: '作为家长你认为你给你孩子穿的衣服与同龄相比：',
        gender: '3',
        type: '1',
        code: '144',
        choiceKey: '穿衣偏少|穿衣偏多 ',
        choiceValue: '1|2',
      },
      {
        title: '药物、食物及其它物品过敏史：',
        gender: '3',
        type: '1',
        code: '146',
        choiceKey: '2种以上|2种|1种',
        choiceValue: '3|2|1',
      },
      {
        title: '平时孩子的手脚情况：',
        gender: '3',
        type: '2',
        code: '147|148|149|150',
        choiceKey: '手足心热|手足心红赤|手足心脱皮|手足凉',
      },
      {
        title: '你孩子应用抗生素（包括口服、输液、灌肠）的频繁程度：',
        gender: '3',
        type: '1',
        code: '151',
        choiceKey: '经常应用|时常应用|偶尔应用',
        choiceValue: '3|2|1',
      },
      {
        title: '鼻部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '鼻塞/喷嚏',
            gender: '3',
            type: '1',
            code: '152',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '睡觉打呼噜',
            gender: '3',
            type: '1',
            code: '153',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '平时呼气音粗',
            gender: '3',
            type: '1',
            code: '154',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '流鼻血',
            gender: '3',
            type: '1',
            code: '155',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: ' 鼻痒（常揉鼻子） ',
            gender: '3',
            type: '1',
            code: '156',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '眼部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '平时眼屎多',
            gender: '3',
            type: '1',
            code: '157',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '眼痒（常揉眼睛）',
            gender: '3',
            type: '1',
            code: '158',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '易麦粒肿（即眼睑有易红肿）',
            gender: '3',
            type: '1',
            code: '159',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '口腔的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '口臭、口气酸腐、口气难闻',
            gender: '3',
            type: '1',
            code: '160',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口唇偏红',
            gender: '3',
            type: '1',
            code: '161',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口水多',
            gender: '3',
            type: '1',
            code: '162',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '咽喉的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '喉痰多',
            gender: '3',
            type: '1',
            code: '163',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '清嗓子（咽不舒适）',
            gender: '3',
            type: '1',
            code: '164',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '嗓子哑',
            gender: '3',
            type: '1',
            code: '165',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '患病情况：（经常是每月1次，时常是2-3月1次，偶尔是1年少于4次）',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '鼻炎',
            gender: '3',
            type: '1',
            code: '166',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '咽炎',
            gender: '3',
            type: '1',
            code: '167',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '中耳炎',
            gender: '3',
            type: '1',
            code: '168',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '乳蛾（扁桃体炎）',
            gender: '3',
            type: '1',
            code: '169',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口疮（口腔炎）',
            gender: '3',
            type: '1',
            code: '170',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '平时你的孩子掉头发吗？',
        gender: '3',
        type: '1',
        code: '171',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子平时吃东西有以下情况吗？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '偏食',
            gender: '3',
            type: '1',
            code: '172',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '拒食（拒绝正餐）',
            gender: '3',
            type: '1',
            code: '173',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '喜冷饮',
            gender: '3',
            type: '1',
            code: '174',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '多奶多肉食',
            gender: '3',
            type: '1',
            code: '175',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '异食它物（即非食用的物品如指甲、手指、衣物等）',
            gender: '3',
            type: '1',
            code: '176',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '平时你孩子有呕吐/干呕的情况吗？',
        gender: '3',
        type: '1',
        code: '177',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '腹部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '肠鸣漉漉（肚子咕噜响）',
            gender: '3',
            type: '1',
            code: '178',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '肚子胀',
            gender: '3',
            type: '1',
            code: '179',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '肚子疼或自诉肚子不舒服',
            gender: '3',
            type: '1',
            code: '180',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '小便的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '夜尿多',
            gender: '3',
            type: '1',
            code: '181',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿色黄',
            gender: '3',
            type: '1',
            code: '182',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿味腥臊重',
            gender: '3',
            type: '1',
            code: '183',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿液浑浊',
            gender: '3',
            type: '1',
            code: '184',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿频',
            gender: '3',
            type: '1',
            code: '185',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '遗尿（白天不自觉尿裤子或晚上尿床）',
            gender: '3',
            type: '1',
            code: '186',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '大便的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '平时大便次数',
            gender: '3',
            type: '1',
            code: '187',
            choiceKey: '日2次以上|日2次|日1次|2日1次|2日以上1次|不规律',
            // choiceValue: '2|0|0|0|2|0',
            choiceValue: '1|2|3|4|5|6',
          },
          {
            title: '大便不化（含食物残渣多）',
            gender: '3',
            type: '1',
            code: '188',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '干硬',
            gender: '3',
            type: '1',
            code: '189',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '干结如羊屎',
            gender: '3',
            type: '1',
            code: '190',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '便不成形或前干后稀',
            gender: '3',
            type: '1',
            code: '191',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '偏粘腻（马桶不好冲或屁股不容易擦干净）',
            gender: '3',
            type: '1',
            code: '192',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '平均每次拉的大便量',
            gender: '3',
            type: '1',
            code: '193',
            choiceKey: '偏少|偏多|正常',
            choiceValue: '1|2|3',
          },
          {
            title: '平时大便的气味（偏酸臭、偏腥臭或其它异味）',
            gender: '3',
            type: '1',
            code: '194',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：灰暗',
            gender: '3',
            type: '1',
            code: '195',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：发绿',
            gender: '3',
            type: '1',
            code: '196',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：黑或颜色不正',
            gender: '3',
            type: '1',
            code: '197',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
    ],
  },
  {
    title: '医师版测评',
    id: '2',
    subject: [
      {
        title: '头发：',
        gender: '3',
        type: '2',
        code: '198|199|200|201|202|203|204',
        choiceKey: '发穗|稀疏|发黄|纤细|斑秃|枕秃|干燥',
      },
      {
        title: '面色：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '面色萎黄',
            gender: '3',
            type: '1',
            code: '205',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '206|207|208',
            choiceKey: '苍白|花斑|恍白',
            choiceValue: '2|2|2',
          },
        ],
      },
      {
        title: '眼睛：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '眼部： 眼袋增重',
            gender: '3',
            type: '1',
            code: '209',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '210',
            choiceKey: '睑结膜充血',
          },
        ],
      },
      {
        title: '口腔：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '龋齿数量情况',
            gender: '3',
            type: '1',
            code: '211',
            choiceKey: '三颗及以上|两颗|一颗',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '212',
            choiceKey: '口唇红赤或潮红',
          },
        ],
      },
      {
        title: '咽喉：扁桃体大',
        gender: '3',
        type: '1',
        code: '213',
        choiceKey: 'Ⅲ度|Ⅱ度|Ⅰ度',
        choiceValue: '3|2|1',
      },
      {
        title: '舌：',
        gender: '3',
        type: '2',
        code: '214|215|216',
        choiceKey: '舌质红|地图舌|舌质淡苔薄白',
      },
      {
        title: '舌苔白厚腻：',
        gender: '3',
        type: '1',
        code: '217',
        choiceKey: '重度|中度|轻度',
        choiceValue: '3|2|1',
      },
      {
        title: '胸廓：',
        gender: '3',
        type: '2',
        code: '218|219|220|221',
        choiceKey: '鸡胸|漏斗胸|肋外翻|串珠',
      },
      {
        title: '腹部：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '腹胀',
            gender: '3',
            type: '1',
            code: '222',
            choiceKey: '+++|++|+',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '223',
            choiceKey: '肥大',
            choiceValue: '0',
          },
        ],
      },
      {
        title: '手足：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '爪甲不荣',
            gender: '3',
            type: '2',
            code: '224|225|226|227',
            choiceKey: '指甲扁平|竖纹|白斑|甲软',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '228|229|230|231|232',
            choiceKey: '手足心萎黄|手足凉|手足心热|倒刺|手足潮',
            choiceValue: '0|2|2|2|2',
          },
        ],
      },
    ],
  },
];
export const SCALE_CHD2 = [
   {
    subject:[
      {
        title: '与同龄孩子相比，您孩子平时的体力如何？',
        gender: '3',
        type: '1',
        choiceKey: '充沛|不足|一般',
        choiceValue: '1|2|3',
      },
      {
        title: '与同龄孩子相比，您觉得孩子平时的精神状态如何？',
        gender: '3',
        type: '1',
        choiceKey: '易亢奋|较饱满|不够饱满|不足，或易疲倦',
        choiceValue: '4|5|6|7',
      },
      {
        title: '您孩子平时的情绪更符合以下哪项？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '稳定程度',
            gender: '3',
            type: '1',
            choiceKey: '比较稳定|容易不稳定，易烦躁|不确定',
            choiceValue: '8|9|10',
          },
          {
            title: '情绪倾向',
            gender: '3',
            type: '2',
            code: '11|12|13|14|15',
            choiceKey: '易高兴|易发脾气|易忧虑|易惊恐|不确定',
          },
        ],
      },
      {
        title: '您和您周围的人觉得，您孩子的脾气性格更符合以下哪项？',
        gender: '3',
        type: '2',
        code: '16|17|18|19|20|21',
        choiceKey: '活泼开朗|较文静|较敏感|不愿冒险|格外喜欢冒险|不确定',
      },
      {
        title: '与同龄人相比，您孩子的体型如何？',
        gender: '3',
        type: '2',
        code: '22|23|24|25|26',
        choiceKey: '偏高|偏矮|偏胖|偏瘦|一般',
      },
      {
        title: '您孩子的头发质地如何？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '量',
            gender: '3',
            type: '1',
            choiceKey: '稠密|稀疏|不确定',
            choiceValue: '27|28|29',
          },
          {
            title: '颜色',
            gender: '3',
            type: '1',
            choiceKey: '黑亮|发黄|不确定',
            choiceValue: '30|31|32',
          },
          {
            title: '质地',
            gender: '3',
            type: '2',
            code: '33|34|35|36|37|38',
            choiceKey: '光泽有弹性|干枯|细软|偏早生白发瘦|头皮容易痒，或头屑较多|不确定',
          },
        ],
      },
      {
        title: '您孩子的脸色通常符合以下哪项？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '光泽度',
            gender: '3',
            type: '1',
            choiceKey: '有光泽|缺乏光泽|不确定',
            choiceValue: '39|40|41',
          },
          {
            title: '颜色',
            gender: '3',
            type: '1',
            choiceKey: '红润|偏白|偏黄|偏红|只有两颧偏红|不确定',
            choiceValue: '42|43|44|45|46|47',
          },
        ],
      },
      {
        title: '您孩子的口腔、嘴唇通常符合以下哪项？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '干湿度',
            gender: '3',
            type: '1',
            choiceKey: '干燥|湿润|不确定',
            choiceValue: '48|49|50',
          },
          {
            title: '嘴唇颜色',
            gender: '3',
            type: '1',
            choiceKey: '红润|偏白|偏黄|偏红',
            choiceValue: '51|52|53|54',
          },
        ],
      },
      {
        title: '您孩子的舌头通常符合以下哪项？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '舌体',
            gender: '3',
            type: '2',
            code: '55|56|57|58|59',
            choiceKey: '偏瘦|偏胖|舌边缘有齿痕|一般|不确定',
          },
          {
            title: '颜色',
            gender: '3',
            type: '1',
            choiceKey: '淡红|淡白|偏红|不确定',
            choiceValue: '60|61|62|63',
          },
          {
            title: '舌苔',
            gender: '3',
            type: '1',
            choiceKey: '薄白|薄黄|白腻|少或无苔|不确定',
            choiceValue: '64|65|66|67|68',
          },
        ],
      },
      {
        title: '您孩子的皮肤通常符合以下哪项？',
        gender: '3',
        type: '1',
        choiceKey: '润泽有弹性|干燥|不确定',
        choiceValue: '69|70|71',
      },
      {
        title: '与同龄人相比，您孩子的出汗情况通常符合以下哪项？',
        gender: '3',
        type: '2',
        code: '72|73|74|75|76|77',
        choiceKey: '不怎么活动也会出汗|睡眠中常出汗，醒来汗止|稍一活动容易出汗且出汗较多|出汗少|出汗情况一般|不确定',
      },
      {
        title: '您常摸到孩子的手、足的温度如何？',
        gender: '3',
        type: '1',
        choiceKey: '一般|手足常发凉|手足心常发热',
        choiceValue: '78|79|80',
      },
      {
        title: '您孩子的小便情况通常符合以下哪项？',
        gender: '3',
        type: '2',
        code: '81|82|83|84|85|86|87',
        choiceKey: '色黄量少|色黄量多|色清量少|色清量多|色和量均随喝水多少改变|夜尿多（每夜2次及以上）|不确定',
      },
      {
        title: '您孩子的大便情况通常符合以下哪项？',
        gender: '3',
        type: '2',
        code: '88|89|90|91|92|93',
        choiceKey: '正常成形|干硬|稀软或不成形|易腹泻|粘腻|不确定',
      },
      {
        title: '与同龄人相比，您孩子的饮水情况通常符合以下哪项？',
        gender: '3',
        type: '1',
        choiceKey: '冷热皆可|常饮热|常饮冷|不确定',
        choiceValue: '94|95|96|97',
      },
      {
        title: '与同龄人相比，您孩子的饮食情况通常符合以下哪项？',
        gender: '3',
        type: '2',
        code: '98|99|100|101',
        choiceKey: '食欲旺盛，比同龄孩子吃的多|饮食正常|食欲尚可，但进食后消化不良|食欲不振',
      },
      {
        title: '您孩子的睡眠情况通常如何？',
        gender: '3',
        type: '2',
        code: '102|103|104|105|106',
        choiceKey: '良好|睡眠轻浅不实|不易入睡|易困倦或睡得多|不确定',
      },
      {
        title: '您的孩子是否常有以下不适感？',
        gender: '3',
        type: '2',
        code: '107|108|109|110|111|112|113|114',
        choiceKey: '容易感冒|有时腹胀|有时腹痛|有时咳嗽|喉中常有痰|口舌易生疮|没什么不适|其他',
      },
    ]
   }
];
export const SCALE_CHD2_Item ={
  '1': '2;3',
  '2': '1;3',
  '3': '2;1',
  '4': '5;6;7',
  '5': '4;6;7',
  '6': '5;4;7',
  '7': '5;6;4',
  '8': '9;10',
  '9': '8;10',
  '10': '8;9',
  '27': '28;29',
  '28': '27;29',
  '29': '27;28',
  '30': '31;32',
  '31': '30;32',
  '32': '31;30',
  '39': '40;41',
  '40': '39;41',
  '41': '39;40',
  '42': '47;44;45;46;43',
  '43': '42;44;45;46;47',
  '44': '43;42;45;46;47',
  '45': '44;43;42;46;47',
  '46': '45;43;44;42;47',
  '47': '46;43;44;45;42',
  '48': '49;50',
  '49': '48;50',
  '50': '48;49',
  '51': '52;53;54',
  '52': '51;53;54',
  '53': '51;52;54',
  '54': '51;52;53',
  '60': '61;62;63',
  '61': '60;62;63',
  '62': '60;61;63',
  '63': '60;61;62',
  '64': '65;66;67;68',
  '65': '64;66;67;68',
  '66': '64;65;67;68',
  '67': '64;65;66;68',
  '68': '64;65;66;67',
  '69': '70;71',
  '70': '69;71',
  '71': '69;70',
  '78': '79;80',
  '79': '78;80',
  '80': '78;79',
  '94': '95;96;97',
  '95': '94;96;97',
  '96': '95;94;97',
  '97': '95;96;94',
}
export const SCALE_CHD_C = [
  {
    title: '家长版测评',
    id: '1',
    subject: [
      {
        title: '平时你孩子怕冷吗（穿衣较多或常常出现手脚发凉）？',
        gender: '3',
        type: '1',
        code: '106',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '平时你孩子出汗多吗（如活动一下就出汗或睡觉时出汗多）？',
        gender: '3',
        type: '1',
        code: '107',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子平时老说累吗或走路时经常让抱抱？',
        gender: '3',
        type: '1',
        code: '108',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子在睡觉过程中有以下哪些情况？（请经常和孩子一块睡觉的家长回答此问题）',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '睡觉时翻来翻去',
            gender: '3',
            type: '1',
            code: '109',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '磨牙',
            gender: '3',
            type: '1',
            code: '110',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '容易惊醒',
            gender: '3',
            type: '1',
            code: '111',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '夜间哭闹',
            gender: '3',
            type: '1',
            code: '112',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '多梦',
            gender: '3',
            type: '1',
            code: '113',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '早上不容易起床',
            gender: '3',
            type: '1',
            code: '114',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '脾气性格：请判断您的孩子包括以下哪些情况？',
        gender: '3',
        type: '2',
        code: '115|116|117|118|119|120|121|122|123',
        choiceKey: '不爱说话|易被惊吓|经常打人或摔东西|经常在小要求不满足后易哭闹|受到批评后易哭|容易急躁/发脾气|胆子小|多动现象|多静少动',
      },
      {
        title: '皮肤情况：',
        gender: '3',
        type: '2',
        code: '127|128|129|130|131|132|133',
        choiceKey: '易冻疮伤|易反复湿疹|易皮肤瘙痒|易荨麻疹|易皮肤抓痕（消退时间长）|皮肤干燥或粗糙|皮肤高敏反应（蚊虫叮咬反应强烈）',
      },
      {
        title: '您孩子既往患病的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '毛细支气管炎',
            gender: '3',
            type: '1',
            code: '134',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '肺炎',
            gender: '3',
            type: '1',
            code: '135',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '哮喘病史',
            gender: '3',
            type: '1',
            code: '136',
            choiceKey: '2年以上|2年|1年',
            choiceValue: '3|2|1',
          },
          {
            title: '高热抽搐史',
            gender: '3',
            type: '1',
            code: '137',
            choiceKey: '2次以上|2次|1次',
            choiceValue: '3|2|1',
          },
          {
            title: '感冒',
            gender: '3',
            type: '1',
            code: '138',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '咳嗽',
            gender: '3',
            type: '1',
            code: '139',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '发热',
            gender: '3',
            type: '1',
            code: '140',
            choiceKey: '平均每月1次 |每2-3月1次|1年1-3次',
            choiceValue: '3|2|1',
          },
          {
            title: '做过儿童康复治疗',
            gender: '3',
            type: '2',
            code: '141|142|143',
            choiceKey: '运动方面|语言方面|智力方面',
          },
        ],
      },
      {
        title: '作为家长你认为你给你孩子穿的衣服与同龄相比：',
        gender: '3',
        type: '1',
        code: '144',
        choiceKey: '穿衣偏少|穿衣偏多 ',
        choiceValue: '1|2',
      },
      {
        title: '药物、食物及其它物品过敏史：',
        gender: '3',
        type: '1',
        code: '146',
        choiceKey: '2种以上|2种|1种',
        choiceValue: '3|2|1',
      },
      {
        title: '平时孩子的手脚情况：',
        gender: '3',
        type: '2',
        code: '147|148|149|150',
        choiceKey: '手足心热|手足心红赤|手足心脱皮|手足凉',
      },
      {
        title: '你孩子应用抗生素（包括口服、输液、灌肠）的频繁程度：',
        gender: '3',
        type: '1',
        code: '151',
        choiceKey: '经常应用|时常应用|偶尔应用',
        choiceValue: '3|2|1',
      },
      {
        title: '鼻部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '鼻塞/喷嚏',
            gender: '3',
            type: '1',
            code: '152',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '睡觉打呼噜',
            gender: '3',
            type: '1',
            code: '153',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '平时呼气音粗',
            gender: '3',
            type: '1',
            code: '154',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '流鼻血',
            gender: '3',
            type: '1',
            code: '155',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: ' 鼻痒（常揉鼻子） ',
            gender: '3',
            type: '1',
            code: '156',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '眼部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '平时眼屎多',
            gender: '3',
            type: '1',
            code: '157',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '眼痒（常揉眼睛）',
            gender: '3',
            type: '1',
            code: '158',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '易麦粒肿（即眼睑有易红肿）',
            gender: '3',
            type: '1',
            code: '159',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '口腔的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '口臭、口气酸腐、口气难闻',
            gender: '3',
            type: '1',
            code: '160',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口唇偏红',
            gender: '3',
            type: '1',
            code: '161',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口水多',
            gender: '3',
            type: '1',
            code: '162',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '咽喉的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '喉痰多',
            gender: '3',
            type: '1',
            code: '163',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '清嗓子（咽不舒适）',
            gender: '3',
            type: '1',
            code: '164',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '嗓子哑',
            gender: '3',
            type: '1',
            code: '165',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '患病情况：（经常是每月1次，时常是2-3月1次，偶尔是1年少于4次）',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '鼻炎',
            gender: '3',
            type: '1',
            code: '166',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '咽炎',
            gender: '3',
            type: '1',
            code: '167',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '中耳炎',
            gender: '3',
            type: '1',
            code: '168',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '乳蛾（扁桃体炎）',
            gender: '3',
            type: '1',
            code: '169',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '口疮（口腔炎）',
            gender: '3',
            type: '1',
            code: '170',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '平时你的孩子掉头发吗？',
        gender: '3',
        type: '1',
        code: '171',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '你孩子平时吃东西有以下情况吗？',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '偏食',
            gender: '3',
            type: '1',
            code: '172',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '拒食（拒绝正餐）',
            gender: '3',
            type: '1',
            code: '173',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '喜冷饮',
            gender: '3',
            type: '1',
            code: '174',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '多奶多肉食',
            gender: '3',
            type: '1',
            code: '175',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '异食它物（即非食用的物品如指甲、手指、衣物等）',
            gender: '3',
            type: '1',
            code: '176',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '平时你孩子有呕吐/干呕的情况吗？',
        gender: '3',
        type: '1',
        code: '177',
        choiceKey: '经常|时常|偶尔',
        choiceValue: '3|2|1',
      },
      {
        title: '腹部的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '肠鸣漉漉（肚子咕噜响）',
            gender: '3',
            type: '1',
            code: '178',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '肚子胀',
            gender: '3',
            type: '1',
            code: '179',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '肚子疼或自诉肚子不舒服',
            gender: '3',
            type: '1',
            code: '180',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '小便的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '夜尿多',
            gender: '3',
            type: '1',
            code: '181',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿色黄',
            gender: '3',
            type: '1',
            code: '182',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿味腥臊重',
            gender: '3',
            type: '1',
            code: '183',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿液浑浊',
            gender: '3',
            type: '1',
            code: '184',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '尿频',
            gender: '3',
            type: '1',
            code: '185',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '遗尿（白天不自觉尿裤子或晚上尿床）',
            gender: '3',
            type: '1',
            code: '186',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
      {
        title: '大便的情况：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '平时大便次数',
            gender: '3',
            type: '1',
            code: '187',
            choiceKey: '日2次以上|日2次|日1次|2日1次|2日以上1次|不规律',
            // choiceValue: '2|0|0|0|2|0',
            choiceValue: '1|2|3|4|5|6',
          },
          {
            title: '大便不化（含食物残渣多）',
            gender: '3',
            type: '1',
            code: '188',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '干硬',
            gender: '3',
            type: '1',
            code: '189',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '干结如羊屎',
            gender: '3',
            type: '1',
            code: '190',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '便不成形或前干后稀',
            gender: '3',
            type: '1',
            code: '191',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '偏粘腻（马桶不好冲或屁股不容易擦干净）',
            gender: '3',
            type: '1',
            code: '192',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '平均每次拉的大便量',
            gender: '3',
            type: '1',
            code: '193',
            choiceKey: '偏少|偏多|正常',
            choiceValue: '1|2|3',
          },
          {
            title: '平时大便的气味（偏酸臭、偏腥臭或其它异味）',
            gender: '3',
            type: '1',
            code: '194',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：灰暗',
            gender: '3',
            type: '1',
            code: '195',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：发绿',
            gender: '3',
            type: '1',
            code: '196',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
          {
            title: '大便的颜色：黑或颜色不正',
            gender: '3',
            type: '1',
            code: '197',
            choiceKey: '经常|时常|偶尔',
            choiceValue: '3|2|1',
          },
        ],
      },
    ],
  },
  {
    title: '医师版测评',
    id: '2',
    subject: [
      {
        title: '头发：',
        gender: '3',
        type: '2',
        code: '198|199|200|201|202|203|204',
        choiceKey: '发穗|稀疏|发黄|纤细|斑秃|枕秃|干燥',
      },
      {
        title: '面色：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '面色萎黄',
            gender: '3',
            type: '1',
            code: '205',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '206|207|208',
            choiceKey: '苍白|花斑|恍白',
            choiceValue: '2|2|2',
          },
        ],
      },
      {
        title: '眼睛：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '眼部： 眼袋增重',
            gender: '3',
            type: '1',
            code: '209',
            choiceKey: '重度|中度|轻度',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '210',
            choiceKey: '睑结膜充血',
          },
        ],
      },
      {
        title: '口腔：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '龋齿数量情况',
            gender: '3',
            type: '1',
            code: '211',
            choiceKey: '三颗及以上|两颗|一颗',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '212',
            choiceKey: '口唇红赤或潮红',
          },
        ],
      },
      {
        title: '咽喉：扁桃体大',
        gender: '3',
        type: '1',
        code: '213',
        choiceKey: 'Ⅲ度|Ⅱ度|Ⅰ度',
        choiceValue: '3|2|1',
      },
      {
        title: '舌：',
        gender: '3',
        type: '2',
        code: '214|215|216',
        choiceKey: '舌质红|地图舌|舌质淡苔薄白',
      },
      {
        title: '舌苔白厚腻：',
        gender: '3',
        type: '1',
        code: '217',
        choiceKey: '重度|中度|轻度',
        choiceValue: '3|2|1',
      },
      {
        title: '胸廓：',
        gender: '3',
        type: '2',
        code: '218|219|220|221',
        choiceKey: '鸡胸|漏斗胸|肋外翻|串珠',
      },
      {
        title: '腹部：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '腹胀',
            gender: '3',
            type: '1',
            code: '222',
            choiceKey: '+++|++|+',
            choiceValue: '3|2|1',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '223',
            choiceKey: '肥大',
            choiceValue: '0',
          },
        ],
      },
      {
        title: '手足：',
        gender: '3',
        type: '3',
        subject: [
          {
            title: '爪甲不荣',
            gender: '3',
            type: '2',
            code: '224|225|226|227',
            choiceKey: '指甲扁平|竖纹|白斑|甲软',
          },
          {
            title: '其他',
            gender: '3',
            type: '2',
            code: '228|229|230|231|232',
            choiceKey: '手足心萎黄|手足凉|手足心热|倒刺|手足潮',
            choiceValue: '0|2|2|2|2',
          },
        ],
      },
    ],
  },
];
export const SCALE_HYP_TITLE = [
  {
    title: '消瘦',
    help: '体型：瘦 男性：身高(cm)-105=标准体重(Kg)；女性：身高(cm)-100=标准体重(Kg)(正常范围为标准体重±10%)',
    py: 'xs',
    id: '消瘦',
  },
  {
    title: '肥胖',
    help: '体型：胖 男性：身高(cm)-105=标准体重(Kg)；女性：身高(cm)-100=标准体重(Kg)(正常范围为标准体重±10%)',
    py: 'fp',
    id: '肥胖',
  },
  {
    title: '怕冷',
    help: '怕冷',
    py: 'pl',
    id: '畏寒',
  },
  {
    title: '四肢手足冰冷',
    py: 'szszpl',
    id: '肢冷',
  },
  {
    title: '自觉身体发热、面部发红，如坐于火炉旁',
    py: 'zjstfrmbfhrzyhlp',
    id: '烘热',
  },
  {
    title: '怕热',
    py: 'pr',
    id: '畏热',
  },
   {
    title: '按时发热，一日一次',
    py: 'asfryryc',
    id: '潮热',
  },
  {
    title: '两手心、脚心发热及自觉心胸烦热',
    py: 'lsxjxfrjzjxxfr',
    id: '五心烦热',
  },
  {
    title: '心烦、胸口发热',
    py: 'xfxkfr',
    id: '心胸烦热',
  },
  {
    title: '手脚心发热',
    py: 'sjxfr',
    id: '手足心热',
  },
  {
    title: '睡眠时出汗，醒来汗出即止',
    py: 'cmschxlhcjz',
    id: '盗汗',
  },
  {
    title: '白天无缘无故，不自主的出汗',
    py: 'btwywgbzzdch',
    id: '自汗',
  },
   {
    title: '左半身或右半身应汗出而不汗出',
    py: 'zbshybsyhcebce',
     id: '汗出偏沮',
  },
   {
    title: '生气时病情加重',
    py: 'sqspqjz',
    id: '怒则甚',
  },
  {
    title: '容易动怒、发脾气',
    py: 'rydnfpq',
    id: '善怒',
  },
  {
    title: '容易受惊吓',
    py: 'rysjx',
    id: '善惊恐',
  },
  {
    title: '情绪不宁、坐立不安',
    py: 'qxblzlba',
    id: '躁扰不安',
  },
  {
    title: '喜欢叹气或叹气后觉得舒服',
    py: 'xhtqhtqhjdsf',
    id: '善太息',
  },
  {
    title: '心烦',
    py: 'xf',
    id: '心烦',
  },
  {
    title: '失眠',
    py: 'sm',
    id: '失眠',
  },
  {
    title: '多梦',
    py: 'dm',
    id: '多梦',
  },
  {
    title: '睡后易醒',
    py: 'shyx',
    id: '睡后易醒',
  },
  {
    title: '睡梦中身体不自主的抖动、惊跳',
    py: 'smzstbzzdddjt',
    id: '睡中警惕',
  },
  {
    title: '头痛',
    py: 'tt',
    id: '头痛',   
  },
  {
    title: '头顶痛',
    py: 'ddt',
    id: '巅顶痛',
  },
  {
    title: '一侧头痛',
    py: 'yctd',
    id: '偏头痛',
  },
  {
    title: '前额头痛',
    py: 'qett',
    id: '前额头痛',
  },
  {
    title: '头隐隐作痛',
    py: 'tyt',
     id: '头隐痛',
  },
  {
    title: '头刺痛',
    py: 'tct',
    id: '头刺痛',
  },
  {
    title: '头胀痛',
    py: 'tzt',
    id: '头胀痛',
  },
  {
    title: '头目胀痛',
    py: 'tmzt',
    id: '头目胀痛',
  },
  {
    title: '平时不头痛，稍一劳累则头痛',
    py: 'lztt',
    id: '劳则头痛',
  },
  {
    title: '牙龈肿痛',
    py: 'yyzt',
    id: '牙龈肿痛',
  },
  {
    title: '胸痛',
    py: 'xt',
    id: '胸痛',
  },
  {
    title: '胸刺痛',
    py: 'xct',
    id: '胸刺痛',
  },
  {
    title: '胁痛',
    py: 'xt',
    id: '胁痛',
  },
  {
    title: '胁肋刺痛',
    py: 'xlct',
    id: '胁肋刺痛',
  },
  {
    title: '胁肋胀痛',
    py: 'xlzt',
    id: '胁肋胀痛',
  },
  {
    title: '腰痛',
    py: 'yt',
    id: '腰痛',
  },
  {
    title: '腰脊酸痛',
    py: 'yjst',
    id: '腰脊酸痛',
  },
  {
    title: '胸闷',
    py: 'xzmm',
    id: '胸中满闷',
  },
  {
    title: '胃脘部满闷不舒，有堵塞感',
    py: 'wrbmmbsydsg',
    id: '胃脘痞满',
  },
  {
    title: '腹胀',
    py: 'fzzm',
    id: '腹中胀满',
  },
  {
    title: '一侧或两侧胁部胀满不舒',
    py: 'ychlclbzmbq',
    id: '胁胀满',
  },
  {
    title: '面部浮肿',
    py: 'mbfz',
    id: '颜面浮肿',
  },
  {
    title: '双腿浮肿',
    py: 'stfz',
    id: '下肢浮肿',
  },
  {
    title: '全身浮肿',
    py: 'qsfz',
    id: '一身浮肿',
  },
  {
    title: '头皮麻木',
    py: 'tpmm',
    id: '头麻木',
  },
  {
    title: '嘴唇麻木',
    py: 'zcmm',
    id: '口唇麻木',
  },
  {
    title: '手指麻木',
    py: 'szmm',
    id: '手指麻木',
  }, 
   {
    title: '四肢麻木',
    py: 'ztmm',
    id: '肢体麻木',
  },
   {
    title: '自觉身体沉重',
    py: 'sz',
     id: '身重',
  },
  {
    title: '头重',
    py: 'tz',
    id: '头重',
  },
  {
    title: '头部沉重，如裹重物',
    py: 'tbczrgzw',
    id: '头重如裹',
  },
  {
    title: '头重脚轻',
    py: 'tzjq',
    id: '头重脚轻',
  },
  {
    title: '感觉四肢沉重',
    py: 'gjsscz',
     id: '四肢沉重',
  },
  {
    title: '关节酸楚',
    py: 'gjsc',
    id: '关节酸楚',
  },
  {
    title: '腰酸',
    py: 'ys',
    id: '腰酸',
  },
   {
    title: '腰膝软弱无力，不耐久行久立',
    py: 'yqrrwlbnjxjl',
    id: '腰膝酸软',
  },
   {
    title: '四肢无力',
    py: 'szfl',
    id: '四肢乏力',
  },
  {
    title: '自觉浑身疲惫无力',
    py: 'zjhspbwl',
    id: '倦怠乏力',
  },
  {
    title: '自觉精神困倦、疲乏，没有精神',
    py: 'zjjskjpfmyjs',
    id: '神疲',
  },
  {
    title: '疼痛部位固定在某处，不移不变',
    py: 'ttbwgdzmcbybb',
    id: '疼痛固定不移',
  },
  {
    title: '疼痛部位不局限在某处，游走不定，或走窜攻冲',
    py: 'ttbwbjxzmcyzbdhzcgc',
    id: '游走痛',
  },
   {
    title: '四肢不由自主的抽搐、颤动',
    py: 'szbyzzdccsd',
    id: '四肢掣动',
  },
  {
    title: '关节屈伸不便利',
    py: 'gjqsbl',
    id: '关节屈伸不利',
  },
  
  {
    title: '一侧肢体瘫痪',
    py: 'ycztth',
    id: '半身不遂',
  },
  {
    title: '下肢软弱无力，不堪任地，甚则肌肉萎缩',
    py: 'xzrrllbkrdscjrws',
    id: '两足痿弱',
  },
  {
    title: '手指轻微颤动',
    py: 'szqwcd',
    id: '手指蠕动',
  },
  {
    title: '头胀',
    py: 'tz',
    id: '头胀',
  },
  {
    title: '自觉头脑里面天旋地转，看东西也有旋转感，好像坐在车船上一样',
    py: 'zjtnlmtxdzkdxyyxzghxzccsyy',
    id: '眩晕',
  },
  {
    title: '自觉头脑里面天旋地转，好像坐在车船上一样',
    py: 'zjtnlmtxdzhxzccsyy',
    id: '头晕',
  },
  {
    title: '头脑昏沉',
    py: 'tnhc',
    id: '头昏',
  },
  {
    title: '吐字不清',
    py: 'yybl',
    id: '语言不利',
  },
  {
    title: '口眼歪斜（面瘫）',
    py: 'kywx',
    id: '口眼歪斜',
  },

   {
    title: '面色红',
    py: 'msh',
    id: '面色赤红',
  },
  {
    title: '面色苍白',
    py: 'mscb',
    id: '面色苍白',
  },
  {
    title: '面色暗黄',
    py: 'msah',
    id: '面色萎黄',
  },
  {
    title: '面色晦暗发黑，没有光泽',
    py: 'mshamygz',
    id: '面色黧黑',
  },

  {
    title: '颈项部肌肉僵硬不舒',
    py: 'jxbjrjybs',
    id: '颈项拘急',
  },
  {
    title: '口中淡而无味',
    py: 'kzdeww',
    id: '口淡',
  },
   {
    title: '口中自觉有苦味',
    py: 'kzzjykw',
   id: '口苦',
  },
   {
    title: '口臭',
    py: 'kc',
    id: '口臭',
  },
   {
    title: '口中粘腻',
    py: 'kznm',
    id: '口粘腻',
  },

  {
    title: '心慌',
    py: 'xh',
    id: '心悸',
  },
  {
    title: '呼吸比正常人短、快，自觉气不够用',
    py: 'hxbzcrdkzjqbgy',
    id: '气短',
  },
  {
    title: '呼吸急促，自觉气不够用，呼吸节律较快，呼吸不一定短',
    py: 'hxjczjqbgyhxjljkxjbydd',
    id: '气促、气喘',
  },
  {
    title: '平时痰多或者清晨容易吐痰',
    py: 'td',
    id: '痰多',
  },
  {
    title: '咯痰质地稠厚而粘',
    py: 'ltzdchez',
    id: '痰粘稠',
  },
  {
    title: '手指甲、脚趾甲颜色淡',
    py: 'szjjzjysd',
    id: '爪甲淡白',
  },
   {
    title: '手指甲、脚趾甲颜色青紫',
    py: 'szjjzjysqc',
    id: '爪甲青紫',
  },
   {
    title: '耳聋',
    py: 'el',
    id: '耳聋',
  },
  {
    title: '耳鸣',
    py: 'em',
    id: '耳鸣',
  },
  {
    title: '耳内胀闷，有堵塞感，听力减退',
    py: 'enzmydsgtljt',
    id: '耳塞',
  },
  {
    title: '眼睛胀',
    py: 'yjz',
    id: '目胀',
  },
  
  {
    title: '视物模糊',
    py: 'swmh',
    id: '视物模糊',
  },
  {
    title: '眼睛干涩',
    py: 'yjgs',
    id: '目干涩',
  },
  {
    title: '眼白发红',
    py: 'jbfh',
    id: '目赤',
  },
   {
    title: '口唇淡白',
    py: 'kcdd',
    id: '口唇淡',
  },
  
  {
    title: '口唇青紫',
    py: 'kcqc',
    id: '口唇紫',
  },
   {
    title: '经常流鼻血',
    py: 'jcxlb',
    id: '鼻衄',
  },
    {
    title: '自觉咽喉干燥',
    py: 'zjyhgz',
    id: '咽干',
  },
   {
    title: '不想说话，或者说话后觉得累而不想说话',
    py: 'bxshhzshhjdlebxsh',
    id: '懒言',
  },
  {
    title: '进食量虽多，却很容易饿',
    py: 'jslsdqhrye',
    id: '消谷善饥',
  },
  {
    title: '食欲减退或者食欲不好',
    py: 'syjthzsybh',
    id: '食欲不振',
  },
   {
    title: '泛酸水',
    py: 'fss',
    id: '吞酸',
  },
   {
    title: '自觉胃中饥饿感，吃东西后减轻，但不能多吃，不久之后又有这种饥饿感',
    py: 'zjwzjegcdxhjqdbndcbjzhyhzyjeg',
    id: '嘈杂',
  },
  {
    title: '嗝出气体，声音长而缓',
    py: 'gcqtsyceh',
    id: '嗳气',
  },
  {
    title: '打嗝，声音急而短促',
    py: 'dgsyjedc',
    id: '呃逆',
  },
   {
    title: '恶心',
    py: 'ex',
    id: '恶心',
  },
  {
    title: '呕吐',
    py: 'ot',
    id: '呕吐',
  },
  {
    title: '呕吐涎沫',
    py: 'odxm',
    id: '呕吐涎沫',
  },
   {
    title: '口干',
    py: 'kg',
    id: '口干',
  },
  {
    title: '口渴',
    py: 'kk',
    id: '口渴',
  },
  {
    title: '口渴喜欢冷的饮品',
    py: 'kkxhldyp',
    id: '渴喜冷饮',
  },
  {
    title: '口渴喜欢热的饮品',
    py: 'kkxhrdyp',
    id: '渴喜热饮',
  },
  {
    title: '口渴，喜饮水但不能多饮',
    py: 'kkxysdbndy',
    id: '渴欲饮水而不能多',
  },
  {
    title: '喝水量多，仍感口渴',
    py: 'hsldrgkk',
    id: '饮多不解渴',
  },
  {
    title: '大便干结',
    py: 'dbgj',
    id: '大便干',
  },
  {
    title: '便秘',
    py: 'bm',
    id: '大便秘结',
  },
  {
    title: '大便稀薄不成形',
    py: 'dbxbbcx',
    id: '大便溏',
  },
  {
    title: '大便排出不顺畅',
    py: 'dbpcbsc',
    id: '大便不畅',
  },
   {
    title: '小便颜色清、量多',
    py: 'xbysqld',
    id: '小便清长',
  },
  {
    title: '小便量少',
    py: 'xbls',
    id: '小便短少',
  },
   {
    title: '小便量少、色黄',
    py: 'xblssh',
    id: '小便短黄',
  },
   {
    title: '小便色黄',
    py: 'xbsh',
    id: '小便黄',
  },
  {
    title: '不管白天、晚上小便次数都增多',
    py: 'bgbtwsxbcsdzd',
    id: '小便频数',
  },
   {
    title: '小便排出不畅',
    py: 'xbpcbc',
    id: '小便涩',
  },
  {
    title: '小便时尿道有灼热感',
    py: 'xbsldyzrg',
    id: '小便灼热',
  },
  {
    title: '白天小便次数正常，晚上小便次数≥3次',
    py: 'btxbcszzwsxbcs',
    id: '夜尿多',
  },
   {
    title: '病情冬季加重夏季减轻',
    py: 'bqdjjzxjjq',
    id: '冬重夏轻',
  },
  {
    title: '病情夏季减轻冬季加重',
    py: 'bqxjjqdjjz',
    id: '夏重冬轻',
  },
  {
    title: '劳累后病情加重',
    py: 'llhbqjz',
    id: '劳则甚',
  },
  {
    title: '皮肤干燥粗糙状似鱼鳞',
    py: 'pfgzczzsyl',
    id: '肌肤甲错',
  },
  {
    title: '皮肤容易起瘀斑',
    py: 'pfryqyb',
    id: '紫癜',
  },
  {
    title: '记忆力减退，遇事容易忘记，或容易忘记以前的事情',
    py: 'jyljtysrywjhrywjyqdsq',
    id: '健忘',
  },
  {
    title: '筋肉跳动',
    py: 'jrtd',
    id: '筋惕肉瞤',
  },
];
export const SCALE_HYP = [
  {
    title: '全身症状',
    menu: [
      {
        title: '体型',
        item: [
          {
            title: '消瘦',
            help: '体型：瘦 男性：身高(cm)-105=标准体重(Kg)；女性：身高(cm)-100=标准体重(Kg)(正常范围为标准体重±10%)',
            py: 'xs',
            id: '消瘦',
          },
          {
            title: '肥胖',
            help: '体型：胖 男性：身高(cm)-105=标准体重(Kg)；女性：身高(cm)-100=标准体重(Kg)(正常范围为标准体重±10%)',
            py: 'fp',
            id: '肥胖',
          },
        ],
      },
      {
        title: '寒热',
        item: [
          {
            title: '怕冷',
            help: '怕冷',
            py: 'pl',
            id: '畏寒',
          },
          {
            title: '四肢手足冰冷',
            py: 'szszpl',
            id: '肢冷',
          },
          {
            title: '自觉身体发热、面部发红，如坐于火炉旁',
            py: 'zjstfrmbfhrzyhlp',
            id: '烘热',
          },
          {
            title: '怕热',
            py: 'pr',
            id: '畏热',
          },
          {
            title: '按时发热，一日一次',
            py: 'asfryryc',
            id: '潮热',
          },
          {
            title: '两手心、脚心发热及自觉心胸烦热',
            py: 'lsxjxfrjzjxxfr',
            id: '五心烦热',
          },
          {
            title: '心烦、胸口发热',
            py: 'xfxkfr',
            id: '心胸烦热',
          },
          {
            title: '手脚心发热',
            py: 'sjxfr',
            id: '手足心热',
          },
        ],
      },
      {
        title: '汗出',
        item: [
          {
            title: '睡眠时出汗，醒来汗出即止',
            py: 'cmschxlhcjz',
             id: '盗汗',
          },
          {
            title: '白天无缘无故，不自主的出汗',
            py: 'btwywgbzzdch',
            id: '自汗',
          },
          {
            title: '左半身或右半身应汗出而不汗出',
            py: 'zbshybsyhcebce',
             id: '汗出偏沮',
          },
        ],
      },
      {
        title: '情志',
        item: [
          {
            title: '生气时病情加重',
            py: 'sqspqjz',
             id: '怒则甚',
          },
          {
            title: '容易动怒、发脾气',
            py: 'rydnfpq',
             id: '善怒',
          },
          {
            title: '容易受惊吓',
            py: 'rysjx',
             id: '善惊恐',
          },
          {
            title: '情绪不宁、坐立不安',
            py: 'qxblzlba',
             id: '躁扰不安',
          },
          {
            title: '喜欢叹气或叹气后觉得舒服',
            py: 'xhtqhtqhjdsf',
             id: '善太息',
          },
          {
            title: '心烦',
            py: 'xf',
             id: '心烦',
          },
        ],
      },
      {
        title: '睡眠',
        item: [
          {
            title: '失眠',
            py: 'sm',
             id: '失眠',
          },
          {
            title: '多梦',
            py: 'dm',
             id: '多梦',
          },
          {
            title: '睡后易醒',
            py: 'shyx',
             id: '睡后易醒',
          },
          {
            title: '睡梦中身体不自主的抖动、惊跳',
            py: 'smzstbzzdddjt',
             id: '睡中警惕',
          },
        ],
      },
    ],
  },
  {
    title: '感觉异常症状',
    menu: [
      {
        title: '头面痛',
        item: [
          {
            title: '头痛',
            py: 'tt',
             id: '头痛',
          },
          {
            title: '头顶痛',
            py: 'ddt',
             id: '巅顶痛',
          },
          {
            title: '一侧头痛',
            py: 'yctd',
             id: '偏头痛',
          },
          {
            title: '前额头痛',
            py: 'qett',
             id: '前额头痛',
          },
          {
            title: '头隐隐作痛',
            py: 'tyyzt',
             id: '头隐痛',
          },
          {
            title: '头刺痛',
            py: 'tct',
             id: '头刺痛',
          },
          {
            title: '头胀痛',
            py: 'tzt',
             id: '头胀痛',
          },
          {
            title: '头目胀痛',
            py: 'tmzt',
             id: '头目胀痛',
          },
          {
            title: '平时不头痛，稍一劳累则头痛',
            py: 'psbttsyllztt',
             id: '劳则头痛',
          },
          {
            title: '牙龈肿痛',
            py: 'yyzt',
             id: '牙龈肿痛',
          },
        ],
      },
      {
        title: '胸胁腰痛',
        item: [
          {
            title: '胸痛',
            py: 'xt',
             id: '胸痛',
          },
          {
            title: '胸刺痛',
            py: 'xct',
             id: '胸刺痛',
          },
          {
            title: '胁痛',
            py: 'xt',
             id: '胁痛',
          },
          {
            title: '胁肋刺痛',
            py: 'xlct',
             id: '胁肋刺痛',
          },
          {
            title: '胁肋胀痛',
            py: 'xlzt',
             id: '胁肋胀痛',
          },
          {
            title: '腰痛',
            py: 'yt',
             id: '腰痛',
          },
          {
            title: '腰脊酸痛',
            py: 'yjst',
             id: '腰脊酸痛',
          },
        ],
      },
      {
        title: '肿胀',
        item: [
          {
            title: '胸闷',
            py: 'xm',
             id: '胸中满闷',
          },
          {
            title: '胃脘部满闷不舒，有堵塞感',
            py: 'wrbmmbsydsg',
             id: '胃脘痞满',
          },
          {
            title: '腹胀',
            py: 'fz',
             id: '腹中胀满',
          },
          {
            title: '一侧或两侧胁部胀满不舒',
            py: 'ychlclbzmbq',
             id: '胁胀满',
          },
          {
            title: '面部浮肿',
            py: 'mbfz',
             id: '颜面浮肿',
          },
          {
            title: '双腿浮肿',
            py: 'stfz',
             id: '下肢浮肿',
          },
          {
            title: '全身浮肿',
            py: 'qsfz',
             id: '一身浮肿',
          },
        ],
      },
      {
        title: '麻木',
        item: [
          {
            title: '头皮麻木',
            py: 'tpmm',
             id: '头麻木',
          },
          {
            title: '嘴唇麻木',
            py: 'zcmm',
             id: '口唇麻木',
          },
          {
            title: '手指麻木',
            py: 'szmm',
             id: '手指麻木',
          },
          {
            title: '四肢麻木',
            py: 'ztmm',
             id: '肢体麻木',
          },
        ],
      },
      {
        title: '酸重',
        item: [
          {
            title: '自觉身体沉重',
            py: 'sz',
             id: '身重',
          },
          {
            title: '头重',
            py: 'tz',
             id: '头重',
          },
          {
            title: '头部沉重，如裹重物',
            py: 'tbczrgzw',
             id: '头重如裹',
          },
          {
            title: '头重脚轻',
            py: 'tzjq',
             id: '头重脚轻',
          },
          {
            title: '感觉四肢沉重',
            py: 'gjsscz',
             id: '四肢沉重',
          },
          {
            title: '关节酸楚',
            py: 'gjsc',
             id: '关节酸楚',
          },
          {
            title: '腰酸',
            py: 'ys',
             id: '腰酸',
          },
          {
            title: '腰脊酸痛',
            py: 'yjst',
             id: '腰脊酸痛',
          },
          {
            title: '腰膝软弱无力，不耐久行久立',
            py: 'yqrrwlbnjxjl',
             id: '腰膝酸软',
          },
        ],
      },
      {
        title: '其他',
        item: [
          {
            title: '四肢无力',
            py: 'szfl',
             id: '四肢乏力',
          },
          {
            title: '自觉浑身疲惫无力',
            py: 'zjhspbwl',
             id: '倦怠乏力',
          },
          {
            title: '自觉精神困倦、疲乏，没有精神',
            py: 'zjjskjpfmyjs',
             id: '神疲',
          },
          {
            title: '疼痛部位固定在某处，不移不变',
            py: 'ttbwgdzmcbybb',
             id: '疼痛固定不移',
          },
          {
            title: '疼痛部位不局限在某处，游走不定，或走窜攻冲',
            py: 'ttbwbjxzmcyzbdhzcgc',
             id: '游走痛',
          },
        ],
      },
    ],
  },
  {
    title: '活动异常症状',
    menu: [
      {
        title: '活动异常',
        item: [
          {
            title: '四肢不由自主的抽搐、颤动',
            py: 'szbyzzdccsd',
             id: '四肢掣动',
          },
          {
            title: '关节屈伸不便利',
            py: 'gjqsbbl',
             id: '关节屈伸不利',
          },
          {
            title: '一侧肢体瘫痪',
            py: 'ycztth',
             id: '半身不遂',
          },
          {
            title: '下肢软弱无力，不堪任地，甚则肌肉萎缩',
            py: 'xzrrllbkrdscjrws',
             id: '两足痿弱',
          },
          {
            title: '手指轻微颤动',
            py: 'szqwcd',
             id: '手指蠕动',
          },
        ],
      },
    ],
  },
  {
    title: '头颈症状',
    menu: [
      {
        title: '头部',
        item: [
          {
            title: '头重',
            py: 'tz',
             id: '头重',
          },
          {
            title: '头部沉重，如裹重物',
            py: 'tzrg',
             id: '头重如裹',
          },
          {
            title: '头重脚轻',
            py: 'tzjq',
             id: '头重脚轻',
          },
          {
            title: '头胀',
            py: 'tz',
             id: '头胀',
          },
          {
            title: '头痛',
            py: 'tt',
             id: '头痛',
          },
          {
            title: '头隐隐作痛',
            py: 'tyt',
             id: '头隐痛',
          },
          {
            title: '头刺痛',
            py: 'tct',
             id: '头刺痛',
          },
          {
            title: '头胀痛',
            py: 'tzt',
             id: '头胀痛',
          },
          {
            title: '头目胀痛',
            py: 'tmzt',
             id: '头目胀痛',
          },
          {
            title: '平时不头痛，稍一劳累则头痛',
            py: 'lztt',
             id: '劳则头痛',
          },
          {
            title: '头顶痛',
            py: 'ddt',
             id: '巅顶痛',
          },
          {
            title: '一侧头痛',
            py: 'ptt',
             id: '偏头痛',
          },
          {
            title: '前额头痛',
            py: 'qett',
             id: '前额头痛',
          },
          {
            title: '自觉头脑里面天旋地转，看东西也有旋转感，好像坐在车船上一样',
            py: 'zjtnlmtxdzkdxyyxzghxzccsyy',
             id: '眩晕',
          },
          {
            title: '自觉头脑里面天旋地转，好像坐在车船上一样',
            py: 'zjtnlmtxdzhxzccsyy',
             id: '头晕',
          },
          {
            title: '头脑昏沉',
            py: 'tnhc',
             id: '头昏',
          },
          {
            title: '吐字不清',
            py: 'yybl',
             id: '语言不利',
          },
          {
            title: '头皮麻木',
            py: 'tmm',
             id: '头麻木',
          },
          {
            title: '口眼歪斜（面瘫）',
            py: 'kywx',
             id: '口眼歪斜',
          },
          {
            title: '记忆力减退，遇事容易忘记，或容易忘记以前的事情',
            py: 'jw',
             id: '健忘',
          },
        ],
      },
      {
        title: '面部',
        item: [
          {
            title: '面色红',
            py: 'msh',
             id: '面色赤红',
          },
          {
            title: '面色苍白',
            py: 'mscb',
             id: '面色苍白',
          },
          {
            title: '面色暗黄',
            py: 'msah',
             id: '面色萎黄',
          },
          {
            title: '面色晦暗发黑，没有光泽',
            py: 'mshamygz',
             id: '面色黧黑',
          },
          {
            title: '面部浮肿',
            py: 'ymfz',
             id: '颜面浮肿',
          },
        ],
      },
      {
        title: '颈部',
        item: [
          {
            title: '颈项部肌肉僵硬不舒',
            py: 'jxbjrjybs',
             id: '颈项拘急',
          },
        ],
      },
      {
        title: '口唇',
        item: [
          {
            title: '口中淡而无味',
            py: 'kzdeww',
             id: '口淡',
          },
          {
            title: '口中自觉有苦味',
            py: 'kzzjykw',
             id: '口苦',
          },
          {
            title: '口臭',
            py: 'kc',
             id: '口臭',
          },
          {
            title: '口中粘腻',
            py: 'kznm',
             id: '口粘腻',
          },
        ],
      },
    ],
  },
  {
    title: '腰背胸腹症状',
    menu: [
      {
        title: '胸部',
        item: [
          {
            title: '胸痛',
            py: 'xt',
             id: '胸痛',
          },
          {
            title: '胸刺痛',
            py: 'xct',
             id: '胸刺痛',
          },
          {
            title: '胸闷',
            py: 'xzmm',
             id: '胸中满闷',
          },
          {
            title: '心烦、胸口发热',
            py: 'xxfr',
             id: '心胸烦热',
          },
          {
            title: '两手心、脚心发热及自觉心胸烦热',
            py: 'wxfr',
             id: '五心烦热',
          },
          {
            title: '心烦',
            py: 'xf',
             id: '心烦',
          },
          {
            title: '心慌',
            py: 'xh',
             id: '心悸',
          },
          {
            title: '呼吸比正常人短、快，自觉气不够用',
            py: 'hxbzcrdkzjqbgy',
             id: '气短',
          },
          {
            title: '呼吸急促，自觉气不够用，呼吸节律较快，呼吸不一定短',
            py: 'hxjczjqbgyhxjljkxjbydd',
             id: '气促、气喘',
          },
          {
            title: '平时痰多或者清晨容易吐痰',
            py: 'pstdhzqcrydd',
             id: '痰多',
          },
          {
            title: '咯痰质地稠厚而粘',
            py: 'ltzdchez',
             id: '痰粘稠',
          },
        ],
      },
      {
        title: '脘腹部',
        item: [
          {
            title: '胃脘部满闷不舒，有堵塞感',
            py: 'wwpm',
             id: '胃脘痞满',
          },
          {
            title: '腹胀',
            py: 'fzzm',
             id: '腹中胀满',
          },
        ],
      },
      {
        title: '胁肋部',
        item: [
          {
            title: '胁肋刺痛',
            py: 'xlct',
             id: '胁肋刺痛',
          },
          {
            title: '胁肋胀痛',
            py: 'xlzt',
             id: '胁肋胀痛',
          },
          {
            title: '胁痛',
            py: 'xt',
             id: '胁痛',
          },
          {
            title: '一侧或两侧胁部胀满不舒',
            py: 'xzm',
             id: '胁胀满',
          },
        ],
      },
      {
        title: '腰背部',
        item: [
          {
            title: '腰酸',
            py: 'ys',
             id: '腰酸',
          },
          {
            title: '腰痛',
            py: 'yt',
             id: '腰痛',
          },
          {
            title: '腰膝软弱无力，不耐久行久立',
            py: 'yxsr',
             id: '腰膝酸软',
          },
          {
            title: '腰脊酸痛',
            py: 'yjst',
             id: '腰脊酸痛',
          },
        ],
      },
    ],
  },
  {
    title: '四肢关节症状',
    menu: [
      {
        title: '四肢/手足',
        item: [
          {
            title: '一侧肢体瘫痪',
            py: 'bsbs',
             id: '半身不遂',
          },
          {
            title: '四肢手足冰冷',
            py: 'zl',
             id: '肢冷',
          },
          {
            title: '四肢麻木',
            py: 'ztmm',
             id: '肢体麻木',
          },
          {
            title: '感觉四肢沉重',
            py: 'szcz',
             id: '四肢沉重',
          },
          {
            title: '四肢不由自主的抽搐、颤动',
            py: 'szcd',
             id: '四肢掣动',
          },
          {
            title: '四肢无力',
            py: 'szfl',
             id: '四肢乏力',
          },
          {
            title: '双腿浮肿',
            py: 'xzfz',
             id: '下肢浮肿',
          },
          {
            title: '手指轻微颤动',
            py: 'szrd',
             id: '手指蠕动',
          },
          {
            title: '手指麻木',
            py: 'szmm',
             id: '手指麻木',
          },
          {
            title: '下肢软弱无力，不堪任地，甚则肌肉萎缩',
            py: 'lzwr',
             id: '两足痿弱',
          },
        ],
      },
      {
        title: '关节',
        item: [
          {
            title: '关节酸楚',
            py: 'gjsc',
             id: '关节酸楚',
          },
          {
            title: '关节屈伸不便利',
            py: 'gjqsbl',
             id: '关节屈伸不利',
          },
        ],
      },
      {
        title: '爪甲症状',
        item: [
          {
            title: '手指甲、脚趾甲颜色淡',
            py: 'szjjzjysd',
             id: '爪甲淡白',
          },
          {
            title: '手指甲、脚趾甲颜色青紫',
            py: 'szjjzjysqc',
             id: '爪甲青紫',
          },
        ],
      },
    ],
  },
  {
    title: '五官症状',
    menu: [
      {
        title: '耳、目',
        item: [
          {
            title: '耳聋',
            py: 'el',
             id: '耳聋',
          },
          {
            title: '耳鸣',
            py: 'em',
             id: '耳鸣',
          },
          {
            title: '耳内胀闷，有堵塞感，听力减退',
            py: 'enzmydsgtljt',
             id: '耳塞',
          },
          {
            title: '眼睛胀',
            py: 'yjz',
             id: '目胀',
          },
          {
            title: '视物模糊',
            py: 'swmh',
             id: '视物模糊',
          },
          {
            title: '眼睛干涩',
            py: 'yjgs',
             id: '目干涩',
          },
          {
            title: '眼白发红',
            py: 'jbfh',
             id: '目赤',
          },
        ],
      },
      {
        title: '口、鼻、咽、齿',
        item: [
          {
            title: '口中淡而无味',
            py: 'kd',
             id: '口淡',
          },
          {
            title: '口中自觉有苦味',
            py: 'kk',
             id: '口苦',
          },
          {
            title: '口臭',
            py: 'kc',
             id: '口臭',
          },
          {
            title: '口中粘腻',
            py: 'kzn',
             id: '口粘腻',
          },
          {
            title: '嘴唇麻木',
            py: 'kcmm',
             id: '口唇麻木',
          },
          {
            title: '口唇淡白',
            py: 'kcdd',
             id: '口唇淡',
          },
          {
            title: '口唇青紫',
            py: 'kcqc',
             id: '口唇紫',
          },
          {
            title: '经常流鼻血',
            py: 'jcxlb',
             id: '鼻衄',
          },
          {
            title: '自觉咽喉干燥',
            py: 'zjyhgz',
             id: '咽干',
          },
          {
            title: '吐字不清',
            py: 'yybl',
             id: '语言不利',
          },
          {
            title: '不想说话，或者说话后觉得累而不想说话',
            py: 'bxshhzshhjdlebxsh',
             id: '懒言',
          },
          {
            title: '牙龈肿痛',
            py: 'yyzt',
             id: '牙龈肿痛',
          },
        ],
      },
    ],
  },
  {
    title: '消化、饮水症状',
    menu: [
      {
        title: '消化',
        item: [
          {
            title: '进食量虽多，却很容易饿',
            py: 'jslsdqhrye',
             id: '消谷善饥',
          },
          {
            title: '食欲减退或者食欲不好',
            py: 'syjthzsybh',
             id: '食欲不振',
          },
          {
            title: '泛酸水',
            py: 'fss',
             id: '吞酸',
          },
          {
            title: '自觉胃中饥饿感，吃东西后减轻，但不能多吃，不久之后又有这种饥饿感',
            py: 'zjwzjegcdxhjqdbndcbjzhyhzyjeg',
             id: '嘈杂',
          },
          {
            title: '嗝出气体，声音长而缓',
            py: 'gcqtsyceh',
             id: '嗳气',
          },
          {
            title: '打嗝，声音急而短促',
            py: 'dgsyjedc',
             id: '呃逆',
          },
          {
            title: '恶心',
            py: 'ex',
             id: '恶心',
          },
          {
            title: '呕吐',
            py: 'ot',
             id: '呕吐',
          },
          {
            title: '呕吐涎沫',
            py: 'odxm',
             id: '呕多涎沫',
          },
        ],
      },
      {
        title: '饮水',
        item: [
          {
            title: '口干',
            py: 'kg',
             id: '口干',
          },
          {
            title: '口渴',
            py: 'kk',
             id: '口渴',
          },
          {
            title: '口渴喜欢冷的饮品',
            py: 'kkxhldyp',
             id: '渴喜冷饮',
          },
          {
            title: '口渴喜欢热的饮品',
            py: 'kkxhrdyp',
             id: '渴喜热饮',
          },
          {
            title: '口渴，喜饮水但不能多饮',
            py: 'kkxysdbndy',
             id: '渴欲饮水而不能多',
          },
          {
            title: '喝水量多，仍感口渴',
            py: 'hsldrgkk',
             id: '饮多不解渴',
          },
        ],
      },
    ],
  },
  {
    title: '二便症状',
    menu: [
      {
        title: '大便',
        item: [
          {
            title: '大便干结',
            py: 'dbgj',
             id: '大便干',
          },
          {
            title: '便秘',
            py: 'bm',
             id: '大便秘结',
          },
          {
            title: '大便稀薄不成形',
            py: 'dbxbbcx',
             id: '大便溏',
          },
          {
            title: '大便排出不顺畅',
            py: 'dbpcbsc',
             id: '大便不畅',
          },
        ],
      },
      {
        title: '小便',
        item: [
          {
            title: '小便颜色清、量多',
            py: 'xbysqld',
             id: '小便清长',
          },
          {
            title: '小便量少',
            py: 'xbls',
             id: '小便短少',
          },
          {
            title: '小便量少、色黄',
            py: 'xblssh',
             id: '小便短黄',
          },
          {
            title: '小便色黄',
            py: 'xbsh',
             id: '小便黄',
          },
          {
            title: '不管白天、晚上小便次数都增多',
            py: 'bgbtwsxbcsdzd',
             id: '小便频数',
          },
          {
            title: '小便排出不畅',
            py: 'xbpcbc',
             id: '小便涩',
          },
          {
            title: '小便时尿道有灼热感',
            py: 'xbsldyzrg',
             id: '小便灼热',
          },
          {
            title: '白天小便次数正常，晚上小便次数≥3次',
            py: 'btxbcszzwsxbcs',
             id: '夜尿多',
          },
        ],
      },
    ],
  },
  {
    title: '其它症状',
    expand: 'true',
    menu: [
      {
        title: '诱因及环境',
        expand: 'true',
        item: [
          {
            title: '病情冬季加重夏季减轻',
            py: 'bqdjjzxjjq',
             id: '冬重夏轻',
          },
          {
            title: '病情夏季减轻冬季加重',
            py: 'bqxjjqdjjz',
             id: '夏重冬轻',
          },
          {
            title: '生气时病情加重',
            py: 'nzs',
             id: '怒则甚',
          },
          {
            title: '劳累后病情加重',
            py: 'llhbqjz',
             id: '劳则甚',
          },
        ],
      },
      {
        title: '呼吸',
        expand: 'true',
        item: [
          {
            title: '平时痰多或者清晨容易吐痰',
            py: 'td',
             id: '痰多',
          },
          {
            title: '咯痰质地稠厚而粘',
            py: 'tzc',
             id: '痰粘稠',
          },
          {
            title: '呼吸比正常人短、快，自觉气不够用',
            py: 'qd',
             id: '气短',
          },
          {
            title: '呼吸急促，自觉气不够用，呼吸节律较快，呼吸不一定短',
            py: 'qcqc',
             id: '气促、气喘',
          },
        ],
      },
      {
        title: '肌肤',
        expand: 'true',
        item: [
          {
            title: '皮肤干燥粗糙状似鱼鳞',
            py: 'pfgzczzsyl',
             id: '肌肤甲错',
          },
          {
            title: '皮肤容易起瘀斑',
            py: 'pfryqyb',
             id: '紫癜',
          },
        ],
      },
      {
        title: '其它',
        expand: 'true',
        item: [
          {
            title: '不想说话，或者说话后觉得累而不想说话',
            py: 'ly',
             id: '懒言',
          },
          {
            title: '记忆力减退，遇事容易忘记，或容易忘记以前的事情',
            py: 'jyljtysrywjhrywjyqdsq',
             id: '健忘',
          },
          {
            title: '筋肉跳动',
            py: 'jrtd',
             id: '筋惕肉瞤',
          },
        ],
      },
    ],
  },
];
export const SCALE_HYP_MAP = {
  消瘦: '肥胖',
  肥胖: '消瘦',
  心烦: '心胸烦热',
  五心烦热: '心胸烦热;手足心热',
  // 心胸烦热: '手足心热;心烦',
  心胸烦热: '五心烦热;手足心热;心烦',
  手足心热: '五心烦热;心胸烦热;肢冷',
  头刺痛: '头胀痛;头隐痛',
  头胀痛: '头刺痛;头隐痛;头目胀痛',
  头隐痛: '头刺痛;头胀痛',
  胁肋刺痛: '胁肋胀痛',
  胁肋胀痛: '胁肋刺痛',
  疼痛固定不移: '游走痛',
  游走痛: '疼痛固定不移',
  爪甲淡白: '爪甲青紫',
  爪甲青紫: '爪甲淡白',
  小便清长: '小便短少;小便短黄;小便涩',
  小便短少: '小便清长;小便短黄;小便短黄',
  小便短黄: '小便清长;小便短少;小便黄;小便短少',
  面色赤红: '面色苍白;面色萎黄;面色黧黑',
  面色苍白: '面色赤红;面色萎黄;面色黧黑',
  面色萎黄: '面色赤红;面色苍白;面色黧黑',
  面色黧黑: '面色赤红;面色苍白;面色萎黄',
  耳聋: '耳塞',
  耳塞: '耳聋',
  口淡: '口苦',
  口苦: '口淡',
  口唇淡: '口唇紫',
  口唇紫: '口唇淡',
  冬重夏轻: '夏重冬轻',
  夏重冬轻: '冬重夏轻',
  消谷善饥: '食欲不振',
  食欲不振: '消谷善饥',
  渴喜冷饮: '渴喜热饮;口干',
  渴喜热饮: '渴喜冷饮;口干',
  渴欲饮水而不能多: '饮多不解渴;口干',
  饮多不解渴: '渴欲饮水而不能多;口干',
  头目胀痛: '头胀痛',
  腰酸: '腰膝酸软;腰脊酸痛',
  腰膝酸软: '腰酸;腰脊酸痛',
  腰脊酸痛: '腰酸;腰膝酸软',
  畏热: '畏寒',
  畏寒: '畏热',
  大便干: '大便溏',
  大便溏: '大便干;大便秘结',
  大便秘结: '大便溏',
  肢冷: '手足心热',
  小便黄: '小便短黄',
  小便涩: '小便清长',
  汗出偏沮: '自汗',
  自汗: '汗出偏沮',
  倦怠乏力: '四肢乏力',
  四肢乏力: '倦怠乏力',
  身重: '四肢沉重',
  四肢沉重: '身重',
  // 肢体麻木: '手麻木',
  // 手麻木: '肢体麻木',
  头重: '头重脚轻',
  头重脚轻: '头重;头重如裹',
  头重如裹: '头重脚轻',
  口干: '口渴;渴喜热饮;渴喜冷饮;渴欲饮水而不能多;饮多不解渴',
  口渴: '口干',
  气短: '气促、气喘',
  '气促、气喘': '气短',
  眩晕: '头晕',
  头晕: '眩晕',
  颜面浮肿: '下肢浮肿;一身浮肿',
  下肢浮肿: '颜面浮肿;一身浮肿',
  一身浮肿: '下肢浮肿;颜面浮肿',
  手指麻木: '肢体麻木',
  肢体麻木: '手指麻木',
};
export const SCALE_TCM_MAP = {
    '1': '2;202',
    '2': '1;202',
    '202': '2;1',
    '3': '4;5',
    '4': '3;5',
    '5': '3;4',
    '6': '46',
    '46': '6',
    '28': '29',
    '29': '28',
    '48': '49',
    '49': '48',
    '52': '53',
    '53': '52',
    '68': '69;70',
    '69': '68;70',
    '70': '69;68',
    '74': '75',
    '75': '74',
    '81': '82;83;203',
    '82': '81;83;203',
    '83': '82;81;203',
    '203': '82;81;83',
    '84': '306',
    '306': '84',
    '87': '88',
    '88': '87',
    '89': '90',
    '90': '89',
    '91': '92',
    '92': '91',
    '93': '94',
    '94': '93',
    '106': '107;108;109',
    '107': '106;108;109',
    '108': '107;106;109',
    '109': '107;108;106',
    '110': '112;114',
    '111': '114',
    '112': '110;114',
    '113': '114',
    '114': '110;111;112;113'
};
export const SCALE_TCM = [
  {
    title: '全身症状',
    subject: [
      {
        id: 's1',
        title: '您的体型是:',
        item: [
          {
            id: '1',
            title: '胖',
          },
          {
            id: '2',
            title: '瘦',
          },
          {
            id: '202',
            title: '体型正常',
          },
        ],
      },
      {
        id: 's2',
        title: '您是否有以下寒热症状：',
        item: [
          {
            id: '3',
            title: '怕冷',
          },
          {
            id: '37',
            title: '背冷',
          },
          {
            id: '4',
            title: '怕热',
          },
          {
            id: '5',
            title: '既怕冷又怕热',
          },
          {
            id: '6',
            title: '两手心、脚心发热及自觉心胸烦热',
          },
          {
            id: '46',
            title: '手脚心发热',
          },
          {
            id: '7',
            title: '自觉发热',
          },
        ],
      },
      {
        id: 's3',
        title: '您是否有以下出汗症状：',
        item: [
          {
            id: '8',
            title: '活动量稍大就容易出虚汗',
          },
          {
            id: '9',
            title: '睡眠时出汗',
          },
        ],
      },
      {
        id: 's4',
        title: '您是否容易出现以下症状：',
        item: [
          {
            id: '10',
            title: '容易疲劳',
          },
          {
            id: '11',
            title: '容易感冒',
          },
          {
            id: '12',
            title: '容易抽筋',
          },
        ],
      },
      {
        id: 's5',
        title: '您是否有以下情志症状：',
        item: [
          {
            id: '13',
            title: '容易忧思',
          },
          {
            id: '14',
            title: '容易动怒、发脾气',
          },
        ],
      },
    ],
  },
  {
    title: '内脏症状',
    subject: [
      {
        id: 's6',
        title: '您是否有以下症状：',
        item: [
          {
            id: '15',
            title: '气短',
          },
          {
            id: '16',
            title: '喜欢叹气或叹气后觉得舒服',
          },
        ],
      },
      {
        id: 's7',
        title: '您是否有以下症状：',
        item: [
          {
            id: '17',
            title: '心慌',
          },
          {
            id: '18',
            title: '心烦',
          },
          {
            id: '19',
            title: '多梦',
          },
          {
            id: '20',
            title: '睡眠不好',
          },
          {
            id: '21',
            title: '健忘',
          },
        ],
      },
      {
        id: 's8',
        title: '您是否有以下症状：',
        item: [
          {
            id: '22',
            title: '平时痰多或者清晨容易吐痰',
          },
          {
            id: '23',
            title: '自觉口中唾液较多，或者频频不自主吐唾液',
          },
          {
            id: '24',
            title: '睡眠时流涎',
          },
          {
            id: '25',
            title: '睡眠时磨牙',
          },
        ],
      },
      {
        id: 's9',
        title: '您是否有以下症状：',
        item: [
          {
            id: '26',
            title: '嗳气',
          },
          {
            id: '27',
            title: '吞酸',
          },
          {
            id: '28',
            title: '食欲减退或者食欲不好',
          },
          {
            id: '29',
            title: '善食易饥',
          },
        ],
      },
    ],
  },
  {
    title: '头颈肩背腰胸腹症状',
    subject: [
      {
        id: 's10',
        title: '您是否有以下症状：',
        item: [
          {
            id: '30',
            title: '头痛',
          },
          {
            id: '31',
            title: '头昏',
          },
          {
            id: '32',
            title: '头重',
          },
        ],
      },
      {
        id: 's11',
        title: '您是否有以下症状：',
        item: [
          {
            id: '33',
            title: '头皮屑多',
          },
          {
            id: '34',
            title: '容易脱发',
          },
          {
            id: '35',
            title: '须发早白【中年见少量白发，老年发白，是正常生理现象】',
          },
        ],
      },
      {
        id: 's12',
        title: '您是否有以下症状：',
        item: [
          {
            id: '36',
            title: '肩痛',
          },
          {
            id: '38',
            title: '腰酸',
          },
          {
            id: '39',
            title: '腰痛',
          },
          {
            id: '40',
            title: '腰膝软弱无力，不耐久行久立',
          },
        ],
      },
      {
        id: 's13',
        title: '您是否有以下症状：',
        item: [
          {
            id: '41',
            title: '胸闷',
          },
          {
            id: '42',
            title: '腹胀',
          },
          {
            id: '43',
            title: '肠鸣',
          },
        ],
      },
    ],
  },
  {
    title: '四肢(手足、爪甲)症状',
    subject: [
      {
        id: 's14',
        title: '您是否有以下症状：',
        item: [
          {
            id: '44',
            title: '感觉四肢麻木',
          },
          {
            id: '45',
            title: '感觉四肢沉重',
          },
          {
            id: '47',
            title: '手指甲、脚趾甲颜色淡',
          },
        ],
      },
    ],
  },
  {
    title: '二阴（二便、阴部）症状',
    subject: [
      {
        id: 's15',
        title: '您是否有以下症状：',
        item: [
          {
            id: '48',
            title: '小便颜色清、量多',
          },
          {
            id: '49',
            title: '小便颜色黄',
          },
          {
            id: '50',
            title: '夜间小便次数多（≥3次）',
          },
          {
            id: '51',
            title: '尿不尽',
          },
        ],
      },
      {
        id: 's16',
        title: '您是否有以下症状：',
        item: [
          {
            id: '52',
            title: '大便稀薄不成形',
          },
          {
            id: '53',
            title: '便秘',
          },
          {
            id: '54',
            title: '大便粘滞不爽，粘马桶',
          },
          {
            id: '55',
            title: '大便艰难，不容易解出',
          },
        ],
      },
      {
        id: 's17',
        title: '您是否有以下症状：',
        item: [
          {
            id: '56',
            title: '前阴瘙痒',
          },
          {
            id: '57',
            title: '前阴潮湿',
          },
          {
            id: '58',
            title: '肛门生痔疮',
          },
          {
            id: '59',
            title: '肛门瘙痒',
          },
        ],
      },
    ],
  },
  {
    title: '五官（目、耳、鼻、咽喉、口唇、牙齿）症状',
    subject: [
      {
        id: 's18',
        title: '您是否有以下症状：',
        item: [
          {
            id: '61',
            title: '容易眼睛干涩',
          },
          {
            id: '62',
            title: '眼屎较多',
          },
          {
            id: '63',
            title: '容易目流泪水，或见风更多',
          },
        ],
      },
      {
        id: 's19',
        title: '您是否有以下症状：',
        item: [
          {
            id: '64',
            title: '耳鸣',
          },
          {
            id: '65',
            title: '咽干',
          },
          {
            id: '66',
            title: '咽喉有异物感',
          },
        ],
      },
      {
        id: 's20',
        title: '您是否有以下症状：',
        item: [
          {
            id: '67',
            title: '口干',
          },
          {
            id: '68',
            title: '口淡',
          },
          {
            id: '69',
            title: '口苦',
          },
          {
            id: '70',
            title: '口甜',
          },
          {
            id: '71',
            title: '口中粘腻',
          },
          {
            id: '72',
            title: '口臭',
          },
        ],
      },
      {
        id: 's21',
        title: '您是否有以下症状：',
        item: [
          {
            id: '73',
            title: '嘴唇干燥，容易起皮脱屑',
          },
          {
            id: '74',
            title: '嘴唇颜色淡',
          },
          {
            id: '75',
            title: '嘴唇颜色暗沉',
          },
        ],
      },
      {
        id: 's22',
        title: '您是否有以下症状：',
        item: [
          {
            id: '76',
            title: '牙痛',
          },
          {
            id: '77',
            title: '牙龈出血',
          },
          {
            id: '78',
            title: '牙龈肿痛',
          },
          {
            id: '79',
            title: '牙龈萎缩【60岁及以上老年人是正常生理现象】',
          },
          {
            id: '80',
            title: '牙齿浮动【60岁及以上老年人是正常生理现象】',
          },
        ],
      },
    ],
  },
  {
    title: '皮肤症状',
    subject: [
      {
        id: 's23',
        title: '您属于以下哪类肤质：',
        item: [
          {
            id: '81',
            title: '干性皮肤',
          },
          {
            id: '82',
            title: '油性皮肤',
          },
          {
            id: '83',
            title: '混合性皮肤',
          },
          {
            id: '203',
            title: '肤质正常',
          },
        ],
      },
      {
        id: 's24',
        title: '您是否有以下症状：',
        item: [
          {
            id: '84',
            title: '皮肤容易起瘀斑',
          },
          {
            id: '85',
            title: '皮肤容易生疣等赘生物',
          },
          {
            id: '86',
            title: '皮肤容易生痤疮或热疮、疖等',
          },
        ],
      },
    ],
  },
  {
    title: '过敏症状',
    subject: [
      {
        id: 's25',
        title: '您是否有以下症状：',
        item: [
          {
            id: '301',
            title: '容易过敏（药物、食物、气味、花粉、季节交替时、气候变化等）',
          },
        ],
      },
      {
        id: 's26',
        title: '您是否有以下症状：',
        item: [
          {
            id: '302',
            title: '没有感冒也会打喷嚏',
          },
          {
            id: '303',
            title: '没有感冒也会鼻塞、流鼻涕',
          },
          {
            id: '304',
            title: '因季节变化、温度变化或异味等原因而咳喘',
          },
        ],
      },
      {
        id: 's27',
        title: '您是否有以下症状：',
        item: [
          {
            id: '305',
            title: '皮肤起荨麻疹（风团、风疹块、风疙瘩）',
          },
          {
            id: '306',
            title: '皮肤因过敏出现紫癜（紫红色瘀点、瘀斑）',
          },
          {
            id: '307',
            title: '皮肤一抓就红，并出现抓痕',
          },
        ],
      },
    ],
  },
  {
    title: '妇科症状：',
    // （女性专有题目）
    subject: [
      {
        id: 's28',
        title: '您是否有以下症状：',
        item: [
          {
            id: '87',
            title: '带下颜色白、量多',
          },
          {
            id: '88',
            title: '带下颜色黄、量多',
          },
          {
            id: '89',
            title: '带下清稀',
          },
          {
            id: '90',
            title: '带下稠厚',
          },
        ],
      },
      {
        id: 's29',
        title: '您是否有以下症状：',
        item: [
          {
            id: '91',
            title: '月经量少',
          },
          {
            id: '92',
            title: '月经量多',
          },
          {
            id: '93',
            title: '月经颜色淡',
          },
          {
            id: '94',
            title: '月经颜色深',
          },
          {
            id: '95',
            title: '月经有血块',
          },
        ],
      },
      {
        id: 's30',
        title: '您是否有以下症状：',
        item: [
          {
            id: '96',
            title: '月经期间腰酸',
          },
          {
            id: '97',
            title: '月经前或月经期间乳房胀痛',
          },
          {
            id: '98',
            title: '月经前或月经期间腹痛',
          },
          {
            id: '99',
            title: '月经过后腹痛',
          },
          {
            id: '100',
            title: '月经期情志异常改变，如烦躁易怒、悲伤欲哭等',
          },
          {
            id: '101',
            title: '月经前或月经期间面目、四肢浮肿',
          },
        ],
      },
    ],
  },
];
export const SCALE_TCM_C = [
  {
    title: '全身症状',
    subject: [
      {
        id: 's1',
        title: '您的体型是:',
        item: [
          {
            id: '1',
            title: '胖',
          },
          {
            id: '2',
            title: '瘦',
          },
          {
            id: '202',
            title: '体型正常',
          },
        ],
      },
      {
        id: 's2',
        title: '您是否有以下寒热症状：',
        item: [
          {
            id: '3',
            title: '怕冷',
          },
          {
            id: '37',
            title: '背冷',
          },
          {
            id: '4',
            title: '怕热',
          },
          {
            id: '5',
            title: '既怕冷又怕热',
          },
          {
            id: '6',
            title: '两手心、脚心发热及自觉心胸烦热',
          },
          {
            id: '46',
            title: '手脚心发热',
          },
          {
            id: '7',
            title: '自觉发热',
          },
        ],
      },
      {
        id: 's3',
        title: '您是否有以下出汗症状：',
        item: [
          {
            id: '8',
            title: '活动量稍大就容易出虚汗',
          },
          {
            id: '9',
            title: '睡眠时出汗',
          },
        ],
      },
      {
        id: 's4',
        title: '您是否容易出现以下症状：',
        item: [
          {
            id: '10',
            title: '容易疲劳',
          },
          {
            id: '11',
            title: '容易感冒',
          },
          {
            id: '12',
            title: '容易抽筋',
          },
        ],
      },
      {
        id: 's5',
        title: '您是否有以下情志症状：',
        item: [
          {
            id: '13',
            title: '容易忧思',
          },
          {
            id: '14',
            title: '容易动怒、发脾气',
          },
        ],
      },
    ],
  },
  {
    title: '内脏症状',
    subject: [
      {
        id: 's6',
        title: '您是否有以下症状：',
        item: [
          {
            id: '15',
            title: '气短',
          },
          {
            id: '16',
            title: '喜欢叹气或叹气后觉得舒服',
          },
        ],
      },
      {
        id: 's7',
        title: '您是否有以下症状：',
        item: [
          {
            id: '17',
            title: '心慌',
          },
          {
            id: '18',
            title: '心烦',
          },
          {
            id: '19',
            title: '多梦',
          },
          {
            id: '20',
            title: '睡眠不好',
          },
          {
            id: '21',
            title: '健忘',
          },
        ],
      },
      {
        id: 's8',
        title: '您是否有以下症状：',
        item: [
          {
            id: '22',
            title: '平时痰多或者清晨容易吐痰',
          },
          {
            id: '23',
            title: '自觉口中唾液较多，或者频频不自主吐唾液',
          },
          {
            id: '24',
            title: '睡眠时流涎',
          },
          {
            id: '25',
            title: '睡眠时磨牙',
          },
        ],
      },
      {
        id: 's9',
        title: '您是否有以下症状：',
        item: [
          {
            id: '26',
            title: '嗳气',
          },
          {
            id: '27',
            title: '吞酸',
          },
          {
            id: '28',
            title: '食欲减退或者食欲不好',
          },
          {
            id: '29',
            title: '善食易饥',
          },
        ],
      },
    ],
  },
  {
    title: '头颈肩背腰胸腹症状',
    subject: [
      {
        id: 's10',
        title: '您是否有以下症状：',
        item: [
          {
            id: '30',
            title: '头痛',
          },
          {
            id: '31',
            title: '头昏',
          },
          {
            id: '32',
            title: '头重',
          },
        ],
      },
      {
        id: 's11',
        title: '您是否有以下症状：',
        item: [
          {
            id: '33',
            title: '头皮屑多',
          },
          {
            id: '34',
            title: '容易脱发',
          },
          {
            id: '35',
            title: '须发早白【中年见少量白发，老年发白，是正常生理现象】',
          },
        ],
      },
      {
        id: 's12',
        title: '您是否有以下症状：',
        item: [
          {
            id: '36',
            title: '肩痛',
          },
          {
            id: '38',
            title: '腰酸',
          },
          {
            id: '39',
            title: '腰痛',
          },
          {
            id: '40',
            title: '腰膝软弱无力，不耐久行久立',
          },
        ],
      },
      {
        id: 's13',
        title: '您是否有以下症状：',
        item: [
          {
            id: '41',
            title: '胸闷',
          },
          {
            id: '42',
            title: '腹胀',
          },
          {
            id: '43',
            title: '肠鸣',
          },
        ],
      },
    ],
  },
  {
    title: '四肢(手足、爪甲)症状',
    subject: [
      {
        id: 's14',
        title: '您是否有以下症状：',
        item: [
          {
            id: '44',
            title: '感觉四肢麻木',
          },
          {
            id: '45',
            title: '感觉四肢沉重',
          },
          {
            id: '47',
            title: '手指甲、脚趾甲颜色淡',
          },
        ],
      },
    ],
  },
  {
    title: '二阴（二便、阴部）症状',
    subject: [
      {
        id: 's15',
        title: '您是否有以下症状：',
        item: [
          {
            id: '48',
            title: '小便颜色清、量多',
          },
          {
            id: '49',
            title: '小便颜色黄',
          },
          {
            id: '50',
            title: '夜间小便次数多（≥3次）',
          },
          {
            id: '51',
            title: '尿不尽',
          },
        ],
      },
      {
        id: 's16',
        title: '您是否有以下症状：',
        item: [
          {
            id: '52',
            title: '大便稀薄不成形',
          },
          {
            id: '53',
            title: '便秘',
          },
          {
            id: '54',
            title: '大便粘滞不爽，粘马桶',
          },
          {
            id: '55',
            title: '大便艰难，不容易解出',
          },
        ],
      },
      {
        id: 's17',
        title: '您是否有以下症状：',
        item: [
          {
            id: '56',
            title: '前阴瘙痒',
          },
          {
            id: '57',
            title: '前阴潮湿',
          },
          {
            id: '58',
            title: '肛门生痔疮',
          },
          {
            id: '59',
            title: '肛门瘙痒',
          },
          {
            id: '60',
            title: '容易遗精【身体健康者，1-2次/月是正常生理现象】',
          },
        ],
      },
    ],
  },
  {
    title: '五官（目、耳、鼻、咽喉、口唇、牙齿）症状',
    // 
    subject: [
      {
        id: 's18',
        title: '您是否有以下症状：',
        item: [
          {
            id: '61',
            title: '容易眼睛干涩',
          },
          {
            id: '62',
            title: '眼屎较多',
          },
          {
            id: '63',
            title: '容易目流泪水，或见风更多',
          },
        ],
      },
      {
        id: 's19',
        title: '您是否有以下症状：',
        item: [
          {
            id: '64',
            title: '耳鸣',
          },
          {
            id: '65',
            title: '咽干',
          },
          {
            id: '66',
            title: '咽喉有异物感',
          },
        ],
      },
      {
        id: 's20',
        title: '您是否有以下症状：',
        item: [
          {
            id: '67',
            title: '口干',
          },
          {
            id: '68',
            title: '口淡',
          },
          {
            id: '69',
            title: '口苦',
          },
          {
            id: '70',
            title: '口甜',
          },
          {
            id: '71',
            title: '口中粘腻',
          },
          {
            id: '72',
            title: '口臭',
          },
        ],
      },
      {
        id: 's21',
        title: '您是否有以下症状：',
        item: [
          {
            id: '73',
            title: '嘴唇干燥，容易起皮脱屑',
          },
          {
            id: '74',
            title: '嘴唇颜色淡',
          },
          {
            id: '75',
            title: '嘴唇颜色暗沉',
          },
        ],
      },
      {
        id: 's22',
        title: '您是否有以下症状：',
        item: [
          {
            id: '76',
            title: '牙痛',
          },
          {
            id: '77',
            title: '牙龈出血',
          },
          {
            id: '78',
            title: '牙龈肿痛',
          },
          {
            id: '79',
            title: '牙龈萎缩【60岁及以上老年人是正常生理现象】',
          },
          {
            id: '80',
            title: '牙齿浮动【60岁及以上老年人是正常生理现象】',
          },
        ],
      },
    ],
  },
  {
    title: '皮肤症状',
    subject: [
      {
        id: 's23',
        title: '您属于以下哪类肤质：',
        item: [
          {
            id: '81',
            title: '干性皮肤',
          },
          {
            id: '82',
            title: '油性皮肤',
          },
          {
            id: '83',
            title: '混合性皮肤',
          },
          {
            id: '203',
            title: '肤质正常',
          },
        ],
      },
      {
        id: 's24',
        title: '您是否有以下症状：',
        item: [
          {
            id: '84',
            title: '皮肤容易起瘀斑',
          },
          {
            id: '85',
            title: '皮肤容易生疣等赘生物',
          },
          {
            id: '86',
            title: '皮肤容易生痤疮或热疮、疖等',
          },
        ],
      },
    ],
  },
  {
    title: '过敏症状',
    subject: [
      {
        id: 's25',
        title: '您是否有以下症状：',
        item: [
          {
            id: '301',
            title: '容易过敏（药物、食物、气味、花粉、季节交替时、气候变化等）',
          },
        ],
      },
      {
        id: 's26',
        title: '您是否有以下症状：',
        item: [
          {
            id: '302',
            title: '没有感冒也会打喷嚏',
          },
          {
            id: '303',
            title: '没有感冒也会鼻塞、流鼻涕',
          },
          {
            id: '304',
            title: '因季节变化、温度变化或异味等原因而咳喘',
          },
        ],
      },
      {
        id: 's27',
        title: '您是否有以下症状：',
        item: [
          {
            id: '305',
            title: '皮肤起荨麻疹（风团、风疹块、风疙瘩）',
          },
          {
            id: '306',
            title: '皮肤因过敏出现紫癜（紫红色瘀点、瘀斑）',
          },
          {
            id: '307',
            title: '皮肤一抓就红，并出现抓痕',
          },
        ],
      },
    ],
  },
];
export const SCALE_TCM_VALUE = [
  {
    id: '1',
    title: '胖',
  },
  {
    id: '2',
    title: '瘦',
  },
  {
    id: '202',
    title: '体型正常',
  },
  {
    id: '3',
    title: '怕冷',
  },
  {
    id: '37',
    title: '背冷',
  },
  {
    id: '4',
    title: '怕热',
  },
  {
    id: '5',
    title: '既怕冷又怕热',
  },
  {
    id: '6',
    title: '两手心、脚心发热及自觉心胸烦热',
  },
  {
    id: '46',
    title: '手脚心发热',
  },
  {
    id: '7',
    title: '自觉发热',
  },
  {
    id: '8',
    title: '活动量稍大就容易出虚汗',
  },
  {
    id: '9',
    title: '睡眠时出汗',
  },
  {
    id: '10',
    title: '容易疲劳',
  },
  {
    id: '11',
    title: '容易感冒',
  },
  {
    id: '12',
    title: '容易抽筋',
  },
  {
    id: '13',
    title: '容易忧思',
  },
  {
    id: '14',
    title: '容易动怒、发脾气',
  },
  {
    id: '15',
    title: '气短',
  },
  {
    id: '16',
    title: '喜欢叹气或叹气后觉得舒服',
  },
  {
    id: '17',
    title: '心慌',
  },
  {
    id: '18',
    title: '心烦',
  },
  {
    id: '19',
    title: '多梦',
  },
  {
    id: '20',
    title: '睡眠不好',
  },
  {
    id: '21',
    title: '健忘',
  },
  {
    id: '22',
    title: '平时痰多或者清晨容易吐痰',
  },
  {
    id: '23',
    title: '自觉口中唾液较多，或者频频不自主吐唾液',
  },
  {
    id: '24',
    title: '睡眠时流涎',
  },
  {
    id: '25',
    title: '睡眠时磨牙',
  },
  {
    id: '26',
    title: '嗳气',
  },
  {
    id: '27',
    title: '吞酸',
  },
  {
    id: '28',
    title: '食欲减退或者食欲不好',
  },
  {
    id: '29',
    title: '善食易饥',
  },
  {
    id: '30',
    title: '头痛',
  },
  {
    id: '31',
    title: '头昏',
  },
  {
    id: '32',
    title: '头重',
  },
  {
    id: '33',
    title: '头皮屑多',
  },
  {
    id: '34',
    title: '容易脱发',
  },
  {
    id: '35',
    title: '须发早白【中年见少量白发，老年发白，是正常生理现象】',
  },
  {
    id: '36',
    title: '肩痛',
  },
  {
    id: '38',
    title: '腰酸',
  },
  {
    id: '39',
    title: '腰痛',
  },
  {
    id: '40',
    title: '腰膝软弱无力，不耐久行久立',
  },
  {
    id: '41',
    title: '胸闷',
  },
  {
    id: '42',
    title: '腹胀',
  },
  {
    id: '43',
    title: '肠鸣',
  },
  {
    id: '44',
    title: '感觉四肢麻木',
  },
  {
    id: '45',
    title: '感觉四肢沉重',
  },
  {
    id: '47',
    title: '手指甲、脚趾甲颜色淡',
  },
  {
    id: '48',
    title: '小便颜色清、量多',
  },
  {
    id: '49',
    title: '小便颜色黄',
  },
  {
    id: '50',
    title: '夜间小便次数多（≥3次）',
  },
  {
    id: '51',
    title: '尿不尽',
  },
  {
    id: '52',
    title: '大便稀薄不成形',
  },
  {
    id: '53',
    title: '便秘',
  },
  {
    id: '54',
    title: '大便粘滞不爽，粘马桶',
  },
  {
    id: '55',
    title: '大便艰难，不容易解出',
  },
  {
    id: '56',
    title: '前阴瘙痒',
  },
  {
    id: '57',
    title: '前阴潮湿',
  },
  {
    id: '58',
    title: '肛门生痔疮',
  },
  {
    id: '59',
    title: '肛门瘙痒',
  },
  {
    id: '60',
    title: '容易遗精【身体健康者，1-2次/月是正常生理现象】',
  },
  {
    id: '61',
    title: '容易眼睛干涩',
  },
  {
    id: '62',
    title: '眼屎较多',
  },
  {
    id: '63',
    title: '容易目流泪水，或见风更多',
  },
  {
    id: '64',
    title: '耳鸣',
  },
  {
    id: '65',
    title: '咽干',
  },
  {
    id: '66',
    title: '咽喉有异物感',
  },
  {
    id: '67',
    title: '口干',
  },
  {
    id: '68',
    title: '口淡',
  },
  {
    id: '69',
    title: '口苦',
  },
  {
    id: '70',
    title: '口甜',
  },
  {
    id: '71',
    title: '口中粘腻',
  },
  {
    id: '72',
    title: '口臭',
  },
  {
    id: '73',
    title: '嘴唇干燥，容易起皮脱屑',
  },
  {
    id: '74',
    title: '嘴唇颜色淡',
  },
  {
    id: '75',
    title: '嘴唇颜色暗沉',
  },
  {
    id: '76',
    title: '牙痛',
  },
  {
    id: '77',
    title: '牙龈出血',
  },
  {
    id: '78',
    title: '牙龈肿痛',
  },
  {
    id: '79',
    title: '牙龈萎缩【60岁及以上老年人是正常生理现象】',
  },
  {
    id: '80',
    title: '牙齿浮动【60岁及以上老年人是正常生理现象】',
  },
  {
    id: '81',
    title: '干性皮肤',
  },
  {
    id: '82',
    title: '油性皮肤',
  },
  {
    id: '83',
    title: '混合性皮肤',
  },
  {
    id: '203',
    title: '肤质正常',
  },
  {
    id: '84',
    title: '皮肤容易起瘀斑',
  },
  {
    id: '85',
    title: '皮肤容易生疣等赘生物',
  },
  {
    id: '86',
    title: '皮肤容易生痤疮或热疮、疖等',
  },
  {
    id: '301',
    title: '容易过敏（药物、食物、气味、花粉、季节交替时、气候变化等）',
  },
  {
    id: '302',
    title: '没有感冒也会打喷嚏',
  },
  {
    id: '303',
    title: '没有感冒也会鼻塞、流鼻涕',
  },
  {
    id: '304',
    title: '因季节变化、温度变化或异味等原因而咳喘',
  },
  {
    id: '305',
    title: '皮肤起荨麻疹（风团、风疹块、风疙瘩）',
  },
  {
    id: '306',
    title: '皮肤因过敏出现紫癜（紫红色瘀点、瘀斑）',
  },
  {
    id: '307',
    title: '皮肤一抓就红，并出现抓痕',
  },
  {
    id: '87',
    title: '带下颜色白、量多',
  },
  {
    id: '88',
    title: '带下颜色黄、量多',
  },
  {
    id: '89',
    title: '带下清稀',
  },
  {
    id: '90',
    title: '带下稠厚',
  },
  {
    id: '91',
    title: '月经量少',
  },
  {
    id: '92',
    title: '月经量多',
  },
  {
    id: '93',
    title: '月经颜色淡',
  },
  {
    id: '94',
    title: '月经颜色深',
  },
  {
    id: '95',
    title: '月经有血块',
  },
  {
    id: '96',
    title: '月经期间腰酸',
  },
  {
    id: '97',
    title: '月经前或月经期间乳房胀痛',
  },
  {
    id: '98',
    title: '月经前或月经期间腹痛',
  },
  {
    id: '99',
    title: '月经过后腹痛',
  },
  {
    id: '100',
    title: '月经期情志异常改变，如烦躁易怒、悲伤欲哭等',
  },
  {
    id: '101',
    title: '月经前或月经期间面目、四肢浮肿',
  },
  {
    id: '102',
    title: '舌胖',
  },
  {
    id: '103',
    title: '齿痕舌',
  },
  {
    id: '104',
    title: '舌裂',
  },
  {
    id: '105',
    title: '舌边尖红',
  },
  {
    id: '106',
    title: '舌苔白',
  },
  {
    id: '107',
    title: '舌苔白腻',
  },
  {
    id: '108',
    title: '舌苔黄',
  },
  {
    id: '109',
    title: '舌苔黄腻',
  },
  {
    id: '110',
    title: '脉紧',
  },
  {
    id: '111',
    title: '脉弦',
  },
  {
    id: '112',
    title: '脉滑',
  },
  {
    id: '113',
    title: '脉数',
  },
  {
    id: '114',
    title: '脉结代',
  },
];

export const SCALE_DIA = [
  {
    title: '全身症状(体型、寒热、汗出、情志、睡眠、乏力)',
    subject: [
      {
        id: 's1',
        title: '体型：',
        item: [
          {
            id: '76',
            title: '消瘦',
          },
          {
            id: '62',
            title: '肥胖',
          },
        ],
      },
      {
        id: 's2',
        title: '寒热：',
        item: [
          {
            id: '72',
            title: '畏寒'
          },
          {
            id: '191',
            title: '畏风'
          },
          {
            id: '105',
            title: '肢冷'
          },
          {
            id: '1',
            title: '潮热'
          },
          {
            id: '19',
            title: '烘热'
          },
          {
            id: '73',
            title: '畏热'
          },
          {
            id: '75',
            title: '五心烦热'
          },
          {
            id: '58',
            title: '手足心热'
          },
          {
            id: '130',
            title: '自觉发热'
          },
          {
            id: '144',
            title: '心胸烦热'
          }
        ],
      },
      {
        id: 's3',
        title: '汗出',
        item: [
          {
            id: '10',
            title: '盗汗',
          },
          {
            id: '111',
            title: '自汗',
          },
        ],
      },
      {
        id: 's4',
        title: '情志',
        item: [
          {
            id: '21',
            title: '急躁易怒',
          },
          {
            id: '52',
            title: '善叹息',
          },
          {
            id: '132',
            title: '善惊恐',
          },
          {
            id: '133',
            title: '善忧思',
          },
        ],
      },
      {
        id: 's5',
        title: '睡眠',
        item: [
          {
            id: '13',
            title: '多梦',
          },
          {
            id: '54',
            title: '失眠'
          },
        ],
      },
      {
        id: 's6',
        title: '乏力',
        item: [
          {
            id: '36',
            title: '懒言'
          },
          {
            id: '26',
            title: '倦怠乏力'
          },
          {
            id: '53',
            title: '神疲'
          },
          {
            id: '183',
            title: '嗜睡（嗜卧）'
          },
          {
            id: '100',
            title: '容易疲劳'
          },
        ],
      },
    ],
  },
  {
    title: '感觉异常症状（疼痛、麻木、酸重）',
    subject: [
      
      {
        id: 's8',
        title: '疼痛性质',
        item: [
          {
            id: '4',
            title: '游走痛'
          },
          {
            id: '63',
            title: '疼痛固定不移'
          },
          {
            id: '186',
            title: '疼痛拒按'
          },
          {
            id: '187',
            title: '疼痛喜按'
          },
        ],
      },
      { 
          id: 's9',
          title: '头面',
          item: [
            {
              id: '65',
              title: '头痛'
            },
            {
              id: '68',
              title: '头胀痛'
            },
            {
              id: '49',
              title: '偏头痛'
            },
            {
              id: '11',
              title: '巅顶痛'
            },
            {
              id: '91',
              title: '牙龈肿痛'
            },
        ],
      },
      { 
          id: 's10',
          title: '胸',
          item: [
            {
              id: '90',
              title: '胸痛'
            },
            {
              id: '88',
              title: '胸刺痛'
            },
          ],
      },
      {
          id: 's11',
          title: '胁肋',
          item: [
            {
              id: '85',
              title: '胁痛'
            },
            {
              id: '82',
              title: '胁肋刺痛'
            },
            {
              id: '84',
              title: '胁肋胀痛'
            },
          ],
      },
      {
          id: 's12',
          title: '腰',
          item: [
            {
              id: '95',
              title: '腰痛'
            },
            {
              id: '197',
              title: '腰腿酸痛'
            },
          ],
      },
      {
          id: 's13',
          title: '脘腹',
          item: [
            {
              id: '189',
              title: '脘腹胀痛'
            },
            {
              id: '190',
              title: '脘腹灼痛'
            },
            {
              id: '143',
              title: '胃脘痛'
            },
          ],
      },
      { 
          id: 's14',
          title: '肢体',
          item: [
            {
              id: '200',
              title: '肢体刺痛'
            },
            {
              id: '201',
              title: '肢体木痛'
            },
          ],
      },
      {
          id: 's15',
          title: '麻木',
          item: [
            {
              id: '134',
              title: '头麻木'
            }, 
            {
              id: '148',
              title: '手指麻木'
            }, 
            {
              id: '116',
              title: '口唇麻木'
            }, 
            {
              id: '108',
              title: '肢体麻木'
            },       
          ],
      },
      {
          id: 's16',
          title: '酸重',
          item: [
            {
              id: '94',
              title: '腰酸'
            }, 
            {
              id: '96',
              title: '腰膝酸软'
            }, 
            {
              id: '147',
              title: '身重'
            }, 
            {
              id: '69',
              title: '头重'
            },  
            {
              id: '107',
              title: '肢体困重'
            },       
          ],
      },

    ],
  },
  {
    title: '头面症状（头部、面部、口唇部）',
    subject: [
      {
        id: 's17',
        title: '头部',
        item: [
           {
              id: '65',
              title: '头痛'
           },
           {
              id: '68',
              title: '头胀痛'
           }, 
           {
              id: '49',
              title: '偏头痛'
           }, 
           {
              id: '11',
              title: '巅顶痛'
           },,
           {
              id: '64',
              title: '头昏'
           },
           {
              id: '66',
              title: '眩晕'
           }, 
           {
              id: '67',
              title: '头胀'
           }, 
           {
              id: '69',
              title: '头重'
           },
           {
              id: '134',
              title: '头麻木'
           },
           {
              id: '91',
              title: '牙龈肿痛'
           },
           {
              id: '17',
              title: '脱发'
           },
        ],
      },
      {
        id: 's18',
        title: '面部',
        item: [
          {
            id: '38',
            title: '面红'
          },
           {
            id: '39',
            title: '颜面浮肿'
          },
           {
            id: '40',
            title: '面色苍白'
          },
           {
            id: '41',
            title: '面色黧黑'
          },
           {
            id: '42',
            title: '面色萎黄'
          },
           {
            id: '123',
            title: '颧红'
          },
           {
            id: '175',
            title: '面色无华'
          },
        ],
      },
      {
        id: 's19',
        title: '口唇',
        item: [
          {
            id: '29',
            title: '口臭'
          }, 
          {
            id: '34',
            title: '口苦'
          }, 
          {
            id: '35',
            title: '口甜'
          },
          {
            id: '118',
            title: '口淡'
          },
          {
            id: '32',
            title: '口粘腻'
          },
          {
            id: '119',
            title: '口舌生疮'
          },
          {
            id: '115',
            title: '口唇干燥'
          },
          {
            id: '116',
            title: '口唇麻木'
          },
          {
            id: '117',
            title: '口唇色淡'
          },
          {
            id: '3',
            title: '口唇紫'
          },
          {
            id: '167',
            title: '口唇红'
          },
        ],
      }
    ],
  },
  {
    title: '腰背胸腹症状（腰背部、胸部、腹部、胁肋部）',
    subject: [
      {
        id: 's18',
        title: '心胸部',
        item: [
          {
            id: '86',
            title: '心烦'
          },
          {
            id: '87',
            title: '心悸'
          },
          {
            id: '88',
            title: '胸刺痛'
          },
          {
            id: '89',
            title: '胸闷'
          },
          {
            id: '90',
            title: '胸痛'
          },
          {
            id: '144',
            title: '心胸烦热'
          },
        ],
      },
      {
        id: 's19',
        title: '脘腹部',
        item: [
          {
            id: '143',
            title: '胃脘痛'
          }, 
          {
            id: '71',
            title: '脘腹痞满'
          }, 
          {
            id: '188',
            title: '脘腹坚满'
          }, 
          {
            id: '189',
            title: '脘腹胀痛'
          }, 
          {
            id: '190',
            title: '脘腹灼痛'
          }, 
          {
            id: '18',
            title: '腹胀'
          }, 
          {
            id: '55',
            title: '食后痞胀'
          }, 
          {
            id: '136',
            title: '肠鸣'
          }, 
          {
            id: '137',
            title: '腹痛，喜温喜按'
          }, 
          {
            id: '170',
            title: '腹胀痛'
          }, 
          {
            id: '178',
            title: '少腹疼痛'
          },
          {
            id: '179',
            title: '少腹胀满'
          },
          {
            id: '180',
            title: '少腹胀痛'
          },
          {
            id: '194',
            title: '小腹冷痛'
          },
        ],
      },
      {
        id: 's20',
        title: '胁肋部',
        item: [
          {
            id: '85',
            title: '胁痛'
          }, 
          {
            id: '82',
            title: '胁肋刺痛'
          }, 
          {
            id: '84',
            title: '胁肋胀痛'
          }, 
          {
            id: '83',
            title: '胁肋胀满'
          },
        ],
      },  {
        id: 's21',
        title: '腰背部',
        item: [
          {
            id: '94',
            title: '腰酸'
          }, 
          {
            id: '196',
            title: '腰冷'
          }, 
          {
            id: '95',
            title: '腰痛'
          }, 
          {
            id: '197',
            title: '腰腿酸痛'
          },
          {
            id: '96',
            title: '腰膝酸软'
          },
          {
            id: '145',
            title: '背冷'
          },
          {
            id: '163',
            title: '背痛  '
          },
        ],
      },
    ],
  },
  {
    title: '四肢关节症状（四肢、爪甲）',
    subject: [
      {
        id: 's22',
        title: '四肢',
        item: [
          {
            id: '59',
            title: '四肢乏力'
          },
          {
            id: '105',
            title: '肢冷'
          },
          {
            id: '107',
            title: '肢体困重'
          },
          {
            id: '108',
            title: '肢体麻木'
          },
          {
            id: '199',
            title: '肢体颤抖'
          },
          {
            id: '200',
            title: '肢体刺痛'
          }, 
          {
            id: '201',
            title: '肢体木痛'
          },
          {
            id: '106',
            title: '肢体浮肿'
          },
          {
            id: '146',
            title: '两足痿弱'
          },
        ],
      },
      {
        id: 's23',
        title: '爪甲',
        item: [
          {
            id: '109',
            title: '爪甲少华'
          }, 
           {
            id: '110',
            title: '爪甲青紫'
          }, 
        ],
      },
    ],
  },
  {
    title: '五官症状（耳、目、口、齿、咽、鼻）',
    subject: [
      {
        id: 's24',
        title: '耳、目',
        item: [
          {
            id: '15',
            title: '耳聋'
          },
          {
            id: '16',
            title: '耳鸣'
          },
          {
            id: '169',
            title: '耳轮枯焦'
          },
          {
            id: '43',
            title: '目赤'
          },
          {
            id: '122',
            title: '目胀'
          },
          {
            id: '44',
            title: '目干涩'
          }, 
          {
            id: '57',
            title: '视物模糊'
          },
          {
            id: '121',
            title: '眼屎较多'
          },
          {
            id: '125',
            title: '流泪'
          },
        ],
      },
      {
        id: 's25',
        title: '口、齿',
        item: [
          {
            id: '29',
            title: '口臭'
          }, 
          {
            id: '34',
            title: '口苦'
          }, 
          {
            id: '35',
            title: '口甜'
          }, 
          {
            id: '118',
            title: '口淡'
          }, 
          {
            id: '32',
            title: '口粘腻'
          },  
          {
            id: '119',
            title: '口舌生疮'
          },  
          {
            id: '115',
            title: '口唇干燥'
          },  
          {
            id: '116',
            title: '口唇麻木'
          },  
          {
            id: '117',
            title: '口唇色淡'
          },  
          {
            id: '3',
            title: '口唇紫'
          },  
          {
            id: '167',
            title: '口唇红'
          },  
          {
            id: '2',
            title: '牙齿浮动'
          },  
          {
            id: '91',
            title: '牙龈肿痛'
          }, 
        ],
      },
      {
        id: 's26',
        title: '鼻、咽',
        item: [
          {
            id: '164',
            title: '鼻鼾'
          }, 
          {
            id: '92',
            title: '咽干'
          },
          {
            id: '120',
            title: '咽喉异物感'
          },
        ],
      },
    ],
  },
  {
    title: '消化、饮水症状',
    subject: [
      {
        id: 's27',
        title: '消化',
        item: [
          {
            id: '0',
            title: '嗳气'
          },
          {
            id: '14',
            title: '恶心'
          },
          {
            id: '47',
            title: '呕吐'
          },
          {
            id: '51',
            title: '善食易饥'
          },
          {
            id: '56',
            title: '食欲不振'
          },
          {
            id: '135',
            title: '嘈杂'
          }, 
          {
            id: '138',
            title: '呃逆'
          },
          {
            id: '141',
            title: '吞酸'
          },
          {
            id: '173',
            title: '饥不能食'
          },
        ],
      },
      {
        id: 's28',
        title: '饮水',
        item: [
          {
            id: '31',
            title: '口渴'
          }, 
          {
            id: '27',
            title: '渴喜冷饮'
          }, 
          {
            id: '28',
            title: '渴喜热饮'
          }, 
          {
            id: '30',
            title: '口干'
          }, 
          {
            id: '33',
            title: '渴欲饮水而不能多'
          },  
          {
            id: '101',
            title: '饮不解渴'
          },  
          {
            id: '165',
            title: '不欲饮'
          },  
          {
            id: '174',
            title: '口渴欲饮但欲饮水不欲咽'
          },  
        ],
      },
    ],
  },
  {
    title: '痰、唾症状',
    subject: [
      {
        id: 's29',
        title: '痰',
        item: [
          {
            id: '37',
            title: '咯痰'
          },
          {
            id: '60',
            title: '痰多'
          },
          {
            id: '181',
            title: '痰少'
          },
          {
            id: '61',
            title: '痰滑易咯'
          },
          {
            id: '127',
            title: '痰液清稀'
          },
          {
            id: '128',
            title: '痰不易咯出'
          }, 
          {
            id: '129',
            title: '痰粘稠'
          },
        ],
      },
      {
        id: 's30',
        title: '唾',
        item: [
          {
            id: '124',
            title: '多唾'
          }, 
          {
            id: '126',
            title: '流涎'
          }, 
        ],
      },
    ],
  },
  {
    title: '二便症状（大便、小便、二阴）',
    subject: [
      {
        id: 's31',
        title: '大便',
        item: [
          {
            id: '5',
            title: '大便干燥'
          },
          {
            id: '6',
            title: '大便秘结'
          },
          {
            id: '7',
            title: '大便溏薄'
          },
          {
            id: '8',
            title: '大便粘腻不爽'
          },
          {
            id: '184',
            title: '溏结不调'
          },
          {
            id: '74',
            title: '五更泄泻'
          }, 
          {
            id: '168',
            title: '大便泄泻'
          },  
          {
            id: '139',
            title: '食寒腹泻'
          },  
          {
            id: '140',
            title: '矢气频频'
          },
          {
            id: '142',
            title: '完谷不化'
          },
        ],
      },
      {
        id: 's32',
        title: '小便',
        item: [
          {
            id: '45',
            title: '小便余沥'
          }, 
          {
            id: '46',
            title: '小便频数'
          }, 
          {
            id: '77',
            title: '小便短黄'
          }, 
          {
            id: '78',
            title: '小便短少'
          }, 
          {
            id: '79',
            title: '小便黄赤'
          }, 
          {
            id: '80',
            title: '小便清长'
          }, 
          {
            id: '81',
            title: '小便涩'
          }, 
          {
            id: '97',
            title: '夜尿多'
          }, 
          {
            id: '176',
            title: '小便量多'
          }, 
          {
            id: '192',
            title: '小便浑浊'
          }, 
          {
            id: '193',
            title: '小便失禁'
          },
        ],
      },
      {
        id: 's33',
        title: '肛门及二阴',
        item: [
          {
            id: '149',
            title: '肛门瘙痒'
          },
          {
            id: '70',
            title: '阴部瘙痒'
          },
          {
            id: '150',
            title: '阴部潮湿'
          },
        ],
      }
    ],
  },
  {
    title: '妇科症状',
    subject: [
      {
        id: 's34',
        title: '白带',
        item: [
          {
            id: '9',
            title: '带下清稀'
          },
          {
            id: '152',
            title: '带下稠厚'
          },
          {
            id: '151',
            title: '白带量多'
          },
          {
            id: '153',
            title: '黄带'
          },
        ],
      },
      {
        id: 's35',
        title: '月经',
        item: [
          {
            id: '24',
            title: '月经量少'
          }, 
          {
            id: '154',
            title: '月经量多'
          }, 
          {
            id: '155',
            title: '月经色淡'
          }, 
          {
            id: '25',
            title: '经血紫暗'
          }, 
          {
            id: '103',
            title: '月经后期'
          }, 
          {
            id: '104',
            title: '经血夹块'
          }, 
          {
            id: '156',
            title: '经间期出血'
          }, 
          {
            id: '157',
            title: '经后腹痛'
          }, 
          {
            id: '158',
            title: '经前、经行腹痛'
          }, 
          {
            id: '159',
            title: '经前乳房胀痛'
          }, 
          {
            id: '160',
            title: '经行浮肿'
          },
          {
            id: '161',
            title: '经行情志异常'
          },
          {
            id: '162',
            title: '经行腰酸'
          },
        ],
      },
      {
        id: 's36',
        title: '其它',
        item: [
          {
            id: '166',
            title: '不孕'
          },
          {
            id: '195',
            title: '性欲减低'
          },
        ],
      }
    ],
  },
  {
    title: '皮肤症状',
    subject: [
      {
        id: 's38',
        title: '皮肤症状',
        item: [
          {
            id: '20',
            title: '肌肤甲错'
          }, 
          {
            id: '23',
            title: '皮肤痤疮或疮疖'
          }, 
          {
            id: '48',
            title: '皮肤干燥'
          }, 
          {
            id: '102',
            title: '皮肤瘀斑瘀点'
          }, 
        ],
      },
    ],
  },
  {
    title: '西医检测指标',
    subject: [
      {
        id: 's39',
        title: '西医检测指标',
        item: [
          {
            id: '220',
            title: '血脂高'
          }, 
          {
            id: '223',
            title: '血流变高凝状态'
          }, 
          {
            id: '172',
            title: '血糖（尿糖）高'
          }, 
        ],
      },
    ],
  },
  {
    title: '其它症状',
    subject: [
      {
        id: 's40',
        title: '诱因及加重因素',
        item: [
          {
            id: '112',
            title: '常因情志不畅诱发或加重'
          },
          {
            id: '113',
            title: '劳则甚'
          },
          {
            id: '114',
            title: '遇冷尤甚'
          },
          {
            id: '177',
            title: '入夜尤甚'
          },
          {
            id: '12',
            title: '动则加重'
          },
          {
            id: '185',
            title: '疼痛常夜间加剧'
          },
        ],
      },
      {
        id: 's41',
        title: '其它',
        item: [
          {
            id: '22',
            title: '健忘'
          }, 
          {
            id: '50',
            title: '气短'
          }, 
          {
            id: '182',
            title: '身痒'
          }, 
          {
            id: '99',
            title: '容易感冒'
          }, 
          {
            id: '131',
            title: '容易抽筋'
          },  
        ],
      },
    ],
  },
];
export const SCALE_DIA_C = [
  {
    title: '全身症状(体型、寒热、汗出、情志、睡眠、乏力)',
    subject: [
      {
        id: 's1',
        title: '体型：',
        item: [
          {
            id: '76',
            title: '消瘦',
          },
          {
            id: '62',
            title: '肥胖',
          },
        ],
      },
      {
        id: 's2',
        title: '寒热：',
        item: [
          {
            id: '72',
            title: '畏寒'
          },
          {
            id: '191',
            title: '畏风'
          },
          {
            id: '105',
            title: '肢冷'
          },
          {
            id: '1',
            title: '潮热'
          },
          {
            id: '19',
            title: '烘热'
          },
          {
            id: '73',
            title: '畏热'
          },
          {
            id: '75',
            title: '五心烦热'
          },
          {
            id: '58',
            title: '手足心热'
          },
          {
            id: '130',
            title: '自觉发热'
          },
          {
            id: '144',
            title: '心胸烦热'
          }
        ],
      },
      {
        id: 's3',
        title: '汗出',
        item: [
          {
            id: '10',
            title: '盗汗',
          },
          {
            id: '111',
            title: '自汗',
          },
        ],
      },
      {
        id: 's4',
        title: '情志',
        item: [
          {
            id: '21',
            title: '急躁易怒',
          },
          {
            id: '52',
            title: '善叹息',
          },
          {
            id: '132',
            title: '善惊恐',
          },
          {
            id: '133',
            title: '善忧思',
          },
        ],
      },
      {
        id: 's5',
        title: '睡眠',
        item: [
          {
            id: '13',
            title: '多梦',
          },
          {
            id: '54',
            title: '失眠'
          },
        ],
      },
      {
        id: 's6',
        title: '乏力',
        item: [
          {
            id: '36',
            title: '懒言'
          },
          {
            id: '26',
            title: '倦怠乏力'
          },
          {
            id: '53',
            title: '神疲'
          },
          {
            id: '183',
            title: '嗜睡（嗜卧）'
          },
          {
            id: '100',
            title: '容易疲劳'
          },
        ],
      },
    ],
  },
  {
    title: '感觉异常症状（疼痛、麻木、酸重）',
    subject: [
      
      {
        id: 's8',
        title: '疼痛性质',
        item: [
          {
            id: '4',
            title: '游走痛'
          },
          {
            id: '63',
            title: '疼痛固定不移'
          },
          {
            id: '186',
            title: '疼痛拒按'
          },
          {
            id: '187',
            title: '疼痛喜按'
          },
        ],
      },
      { 
          id: 's9',
          title: '头面',
          item: [
            {
              id: '65',
              title: '头痛'
            },
            {
              id: '68',
              title: '头胀痛'
            },
            {
              id: '49',
              title: '偏头痛'
            },
            {
              id: '11',
              title: '巅顶痛'
            },
            {
              id: '91',
              title: '牙龈肿痛'
            },
        ],
      },
      { 
          id: 's10',
          title: '胸',
          item: [
            {
              id: '90',
              title: '胸痛'
            },
            {
              id: '88',
              title: '胸刺痛'
            },
          ],
      },
      {
          id: 's11',
          title: '胁肋',
          item: [
            {
              id: '85',
              title: '胁痛'
            },
            {
              id: '82',
              title: '胁肋刺痛'
            },
            {
              id: '84',
              title: '胁肋胀痛'
            },
          ],
      },
      {
          id: 's12',
          title: '腰',
          item: [
            {
              id: '95',
              title: '腰痛'
            },
            {
              id: '197',
              title: '腰腿酸痛'
            },
          ],
      },
      {
          id: 's13',
          title: '脘腹',
          item: [
            {
              id: '189',
              title: '脘腹胀痛'
            },
            {
              id: '190',
              title: '脘腹灼痛'
            },
            {
              id: '143',
              title: '胃脘痛'
            },
          ],
      },
      { 
          id: 's14',
          title: '肢体',
          item: [
            {
              id: '200',
              title: '肢体刺痛'
            },
            {
              id: '201',
              title: '肢体木痛'
            },
          ],
      },
      {
          id: 's15',
          title: '麻木',
          item: [
            {
              id: '134',
              title: '头麻木'
            }, 
            {
              id: '148',
              title: '手指麻木'
            }, 
            {
              id: '116',
              title: '口唇麻木'
            }, 
            {
              id: '108',
              title: '肢体麻木'
            },       
          ],
      },
      {
          id: 's16',
          title: '酸重',
          item: [
            {
              id: '94',
              title: '腰酸'
            }, 
            {
              id: '96',
              title: '腰膝酸软'
            }, 
            {
              id: '147',
              title: '身重'
            }, 
            {
              id: '69',
              title: '头重'
            },  
            {
              id: '107',
              title: '肢体困重'
            },       
          ],
      },

    ],
  },
  {
    title: '头面症状（头部、面部、口唇部）',
    subject: [
      {
        id: 's17',
        title: '头部',
        item: [
           {
              id: '65',
              title: '头痛'
           },
           {
              id: '68',
              title: '头胀痛'
           }, 
           {
              id: '49',
              title: '偏头痛'
           }, 
           {
              id: '11',
              title: '巅顶痛'
           },,
           {
              id: '64',
              title: '头昏'
           },
           {
              id: '66',
              title: '眩晕'
           }, 
           {
              id: '67',
              title: '头胀'
           }, 
           {
              id: '69',
              title: '头重'
           },
           {
              id: '134',
              title: '头麻木'
           },
           {
              id: '91',
              title: '牙龈肿痛'
           },
           {
              id: '17',
              title: '脱发'
           },
        ],
      },
      {
        id: 's18',
        title: '面部',
        item: [
          {
            id: '38',
            title: '面红'
          },
           {
            id: '39',
            title: '颜面浮肿'
          },
           {
            id: '40',
            title: '面色苍白'
          },
           {
            id: '41',
            title: '面色黧黑'
          },
           {
            id: '42',
            title: '面色萎黄'
          },
           {
            id: '123',
            title: '颧红'
          },
           {
            id: '175',
            title: '面色无华'
          },
        ],
      },
      {
        id: 's19',
        title: '口唇',
        item: [
          {
            id: '29',
            title: '口臭'
          }, 
          {
            id: '34',
            title: '口苦'
          }, 
          {
            id: '35',
            title: '口甜'
          },
          {
            id: '118',
            title: '口淡'
          },
          {
            id: '32',
            title: '口粘腻'
          },
          {
            id: '119',
            title: '口舌生疮'
          },
          {
            id: '115',
            title: '口唇干燥'
          },
          {
            id: '116',
            title: '口唇麻木'
          },
          {
            id: '117',
            title: '口唇色淡'
          },
          {
            id: '3',
            title: '口唇紫'
          },
          {
            id: '167',
            title: '口唇红'
          },
        ],
      }
    ],
  },
  {
    title: '腰背胸腹症状（腰背部、胸部、腹部、胁肋部）',
    subject: [
      {
        id: 's18',
        title: '心胸部',
        item: [
          {
            id: '86',
            title: '心烦'
          },
          {
            id: '87',
            title: '心悸'
          },
          {
            id: '88',
            title: '胸刺痛'
          },
          {
            id: '89',
            title: '胸闷'
          },
          {
            id: '90',
            title: '胸痛'
          },
          {
            id: '144',
            title: '心胸烦热'
          },
        ],
      },
      {
        id: 's19',
        title: '脘腹部',
        item: [
          {
            id: '143',
            title: '胃脘痛'
          }, 
          {
            id: '71',
            title: '脘腹痞满'
          }, 
          {
            id: '188',
            title: '脘腹坚满'
          }, 
          {
            id: '189',
            title: '脘腹胀痛'
          }, 
          {
            id: '190',
            title: '脘腹灼痛'
          }, 
          {
            id: '18',
            title: '腹胀'
          }, 
          {
            id: '55',
            title: '食后痞胀'
          }, 
          {
            id: '136',
            title: '肠鸣'
          }, 
          {
            id: '137',
            title: '腹痛，喜温喜按'
          }, 
          {
            id: '170',
            title: '腹胀痛'
          }, 
          {
            id: '178',
            title: '少腹疼痛'
          },
          {
            id: '179',
            title: '少腹胀满'
          },
          {
            id: '180',
            title: '少腹胀痛'
          },
          {
            id: '194',
            title: '小腹冷痛'
          },
        ],
      },
      {
        id: 's20',
        title: '胁肋部',
        item: [
          {
            id: '85',
            title: '胁痛'
          }, 
          {
            id: '82',
            title: '胁肋刺痛'
          }, 
          {
            id: '84',
            title: '胁肋胀痛'
          }, 
          {
            id: '83',
            title: '胁肋胀满'
          },
        ],
      },  {
        id: 's21',
        title: '腰背部',
        item: [
          {
            id: '94',
            title: '腰酸'
          }, 
          {
            id: '196',
            title: '腰冷'
          }, 
          {
            id: '95',
            title: '腰痛'
          }, 
          {
            id: '197',
            title: '腰腿酸痛'
          },
          {
            id: '96',
            title: '腰膝酸软'
          },
          {
            id: '145',
            title: '背冷'
          },
          {
            id: '163',
            title: '背痛  '
          },
        ],
      },
    ],
  },
  {
    title: '四肢关节症状（四肢、爪甲）',
    subject: [
      {
        id: 's22',
        title: '四肢',
        item: [
          {
            id: '59',
            title: '四肢乏力'
          },
          {
            id: '105',
            title: '肢冷'
          },
          {
            id: '107',
            title: '肢体困重'
          },
          {
            id: '108',
            title: '肢体麻木'
          },
          {
            id: '199',
            title: '肢体颤抖'
          },
          {
            id: '200',
            title: '肢体刺痛'
          }, 
          {
            id: '201',
            title: '肢体木痛'
          },
          {
            id: '106',
            title: '肢体浮肿'
          },
          {
            id: '146',
            title: '两足痿弱'
          },
        ],
      },
      {
        id: 's23',
        title: '爪甲',
        item: [
          {
            id: '109',
            title: '爪甲少华'
          }, 
           {
            id: '110',
            title: '爪甲青紫'
          }, 
        ],
      },
    ],
  },
  {
    title: '五官症状（耳、目、口、齿、咽、鼻）',
    subject: [
      {
        id: 's24',
        title: '耳、目',
        item: [
          {
            id: '15',
            title: '耳聋'
          },
          {
            id: '16',
            title: '耳鸣'
          },
          {
            id: '169',
            title: '耳轮枯焦'
          },
          {
            id: '43',
            title: '目赤'
          },
          {
            id: '122',
            title: '目胀'
          },
          {
            id: '44',
            title: '目干涩'
          }, 
          {
            id: '57',
            title: '视物模糊'
          },
          {
            id: '121',
            title: '眼屎较多'
          },
          {
            id: '125',
            title: '流泪'
          },
        ],
      },
      {
        id: 's25',
        title: '口、齿',
        item: [
          {
            id: '29',
            title: '口臭'
          }, 
          {
            id: '34',
            title: '口苦'
          }, 
          {
            id: '35',
            title: '口甜'
          }, 
          {
            id: '118',
            title: '口淡'
          }, 
          {
            id: '32',
            title: '口粘腻'
          },  
          {
            id: '119',
            title: '口舌生疮'
          },  
          {
            id: '115',
            title: '口唇干燥'
          },  
          {
            id: '116',
            title: '口唇麻木'
          },  
          {
            id: '117',
            title: '口唇色淡'
          },  
          {
            id: '3',
            title: '口唇紫'
          },  
          {
            id: '167',
            title: '口唇红'
          },  
          {
            id: '2',
            title: '牙齿浮动'
          },  
          {
            id: '91',
            title: '牙龈肿痛'
          }, 
        ],
      },
      {
        id: 's26',
        title: '鼻、咽',
        item: [
          {
            id: '164',
            title: '鼻鼾'
          }, 
          {
            id: '92',
            title: '咽干'
          },
          {
            id: '120',
            title: '咽喉异物感'
          },
        ],
      },
    ],
  },
  {
    title: '消化、饮水症状',
    subject: [
      {
        id: 's27',
        title: '消化',
        item: [
          {
            id: '0',
            title: '嗳气'
          },
          {
            id: '14',
            title: '恶心'
          },
          {
            id: '47',
            title: '呕吐'
          },
          {
            id: '51',
            title: '善食易饥'
          },
          {
            id: '56',
            title: '食欲不振'
          },
          {
            id: '135',
            title: '嘈杂'
          }, 
          {
            id: '138',
            title: '呃逆'
          },
          {
            id: '141',
            title: '吞酸'
          },
          {
            id: '173',
            title: '饥不能食'
          },
        ],
      },
      {
        id: 's28',
        title: '饮水',
        item: [
          {
            id: '31',
            title: '口渴'
          }, 
          {
            id: '27',
            title: '渴喜冷饮'
          }, 
          {
            id: '28',
            title: '渴喜热饮'
          }, 
          {
            id: '30',
            title: '口干'
          }, 
          {
            id: '33',
            title: '渴欲饮水而不能多'
          },  
          {
            id: '101',
            title: '饮不解渴'
          },  
          {
            id: '165',
            title: '不欲饮'
          },  
          {
            id: '174',
            title: '口渴欲饮但欲饮水不欲咽'
          },  
        ],
      },
    ],
  },
  {
    title: '痰、唾症状',
    subject: [
      {
        id: 's29',
        title: '痰',
        item: [
          {
            id: '37',
            title: '咯痰'
          },
          {
            id: '60',
            title: '痰多'
          },
          {
            id: '181',
            title: '痰少'
          },
          {
            id: '61',
            title: '痰滑易咯'
          },
          {
            id: '127',
            title: '痰液清稀'
          },
          {
            id: '128',
            title: '痰不易咯出'
          }, 
          {
            id: '129',
            title: '痰粘稠'
          },
        ],
      },
      {
        id: 's30',
        title: '唾',
        item: [
          {
            id: '124',
            title: '多唾'
          }, 
          {
            id: '126',
            title: '流涎'
          }, 
        ],
      },
    ],
  },
  {
    title: '二便症状（大便、小便、二阴）',
    subject: [
      {
        id: 's31',
        title: '大便',
        item: [
          {
            id: '5',
            title: '大便干燥'
          },
          {
            id: '6',
            title: '大便秘结'
          },
          {
            id: '7',
            title: '大便溏薄'
          },
          {
            id: '8',
            title: '大便粘腻不爽'
          },
          {
            id: '184',
            title: '溏结不调'
          },
          {
            id: '74',
            title: '五更泄泻'
          }, 
          {
            id: '168',
            title: '大便泄泻'
          },  
          {
            id: '139',
            title: '食寒腹泻'
          },  
          {
            id: '140',
            title: '矢气频频'
          },
          {
            id: '142',
            title: '完谷不化'
          },
        ],
      },
      {
        id: 's32',
        title: '小便',
        item: [
          {
            id: '45',
            title: '小便余沥'
          }, 
          {
            id: '46',
            title: '小便频数'
          }, 
          {
            id: '77',
            title: '小便短黄'
          }, 
          {
            id: '78',
            title: '小便短少'
          }, 
          {
            id: '79',
            title: '小便黄赤'
          }, 
          {
            id: '80',
            title: '小便清长'
          }, 
          {
            id: '81',
            title: '小便涩'
          }, 
          {
            id: '97',
            title: '夜尿多'
          }, 
          {
            id: '176',
            title: '小便量多'
          }, 
          {
            id: '192',
            title: '小便浑浊'
          }, 
          {
            id: '193',
            title: '小便失禁'
          },
        ],
      },
      {
        id: 's33',
        title: '肛门及二阴',
        item: [
          {
            id: '149',
            title: '肛门瘙痒'
          },
          {
            id: '70',
            title: '阴部瘙痒'
          },
          {
            id: '150',
            title: '阴部潮湿'
          },
        ],
      }
    ],
  },
  {
    title: '男科症状',
    subject: [
      {
        id: 's37',
        title: '男科症状',
        item: [
          {
            id: '93',
            title: '阳痿'
          }, 
          {
            id: '98',
            title: '遗精'
          }, 
          {
            id: '171',
            title: '滑精'
          }, 
          {
            id: '195',
            title: '性欲减低'
          }, 
          {
            id: '198',
            title: '早泄'
          },
        ],
      },
    ],
  },
  {
    title: '皮肤症状',
    subject: [
      {
        id: 's38',
        title: '皮肤症状',
        item: [
          {
            id: '20',
            title: '肌肤甲错'
          }, 
          {
            id: '23',
            title: '皮肤痤疮或疮疖'
          }, 
          {
            id: '48',
            title: '皮肤干燥'
          }, 
          {
            id: '102',
            title: '皮肤瘀斑瘀点'
          }, 
        ],
      },
    ],
  },
  {
    title: '西医检测指标',
    subject: [
      {
        id: 's39',
        title: '西医检测指标',
        item: [
          {
            id: '220',
            title: '血脂高'
          }, 
          {
            id: '223',
            title: '血流变高凝状态'
          }, 
          {
            id: '172',
            title: '血糖（尿糖）高'
          }, 
        ],
      },
    ],
  },
  {
    title: '其它症状',
    subject: [
      {
        id: 's40',
        title: '诱因及加重因素',
        item: [
          {
            id: '112',
            title: '常因情志不畅诱发或加重'
          },
          {
            id: '113',
            title: '劳则甚'
          },
          {
            id: '114',
            title: '遇冷尤甚'
          },
          {
            id: '177',
            title: '入夜尤甚'
          },
          {
            id: '12',
            title: '动则加重'
          },
          {
            id: '185',
            title: '疼痛常夜间加剧'
          },
        ],
      },
      {
        id: 's41',
        title: '其它',
        item: [
          {
            id: '22',
            title: '健忘'
          }, 
          {
            id: '50',
            title: '气短'
          }, 
          {
            id: '182',
            title: '身痒'
          }, 
          {
            id: '99',
            title: '容易感冒'
          }, 
          {
            id: '131',
            title: '容易抽筋'
          },  
        ],
      },
    ],
  },
];
export const SCALE_DIA_ITEM = {
  '62': '76',
  '76': '62',
  '75': '58;144',
  '58': '75;144',
  '144': '75;58;86',
  '86': '144',
  '26': '59',
  '59': '26',
  '4': '63',
  '63': '4',
  '186': '187',
  '187': '186',
  '65': '49;11;68',
  '68': '65',
  '49': '65;11',
  '11': '65;49',
  '90': '88',
  '88': '90',
  '85': '82;84',
  '82': '85;84',
  '84': '82;85',
  '96': '197',
  '197': '96',
  '143': '189;190',
  '71': '188',
  '188': '71',
  '189': '190;143;170',
  '190': '189;143',
  '18': '170',
  '170': '18;180;189',
  '180': '170;178',
  '178': '180',
  '200': '201',
  '201': '200',
  '38': '40;41;42;123',
  '40': '38;41;42',
  '41': '38;40;42',
  '42': '38;40;41',
  '123': '38',
  '34': '35;118',
  '35': '34;118',
  '118': '34;35',
  '117': '3;167',
  '3': '117;167',
  '167': '117;3',
  '110': '109',
  '109': '110',
  '51': '56;173',
  '56': '51',
  '173': '51',
  '60': '181',
  '181': '60',
  '61': '128',
  '128': '61',
  '127': '129',
  '129': '127',
  '168': '7;6;5;184',
  '7': '168;5;6;184',
  '6': '168;7;184',
  '5': '168;7;184',
  '184': '168;5;7;6',
 
  '80': '79;78;81;77',
  '79': '80',
  '78': '80;176',
  '81': '80',
  '77': '80;176',
  '176': '77;78',
  '9': '152',
  '152': '9',
  '24': '154',
  '154': '24',
  '155': '25',
  '25': '155',
  '202': '203',
  '203': '202',
  '206': '207',
  '207': '206',
  '210': '211',
  '211': '210',
  '2016': '217',
  '217': '2016;216',
  '216': '217',
  '214': '218',
  '218': '214',
  '219': '215',
  '215': '219',
  '30': '31;27;28;33;101;165',
  '31': '165;30',
  '27': '28;30;165',
  '28': '27;30;165',
  '33': '30;101;165',
  '101': '30;33;165',
  '165': '31;27;28;30;33;101',
};

export const SCALE_DIA_VALUE =[
  {
    id: '76',
    title: '消瘦',
  },
  {
    id: '62',
    title: '肥胖',
  },
  {
    id: '72',
    title: '畏寒'
  },
  {
    id: '191',
    title: '畏风'
  },
  {
    id: '105',
    title: '肢冷'
  },
  {
    id: '1',
    title: '潮热'
  },
  {
    id: '19',
    title: '烘热'
  },
  {
    id: '73',
    title: '畏热'
  },
  {
    id: '75',
    title: '五心烦热'
  },
  {
    id: '58',
    title: '手足心热'
  },
  {
    id: '130',
    title: '自觉发热'
  },
  {
    id: '144',
    title: '心胸烦热'
  },
  {
    id: '10',
    title: '盗汗',
  },
  {
    id: '111',
    title: '自汗',
  },
  {
    id: '21',
    title: '急躁易怒',
  },
  {
    id: '52',
    title: '善叹息',
  },
  {
    id: '132',
    title: '善惊恐',
  },
  {
    id: '133',
    title: '善忧思',
  },
  {
    id: '13',
    title: '多梦',
  },
  {
    id: '54',
    title: '失眠'
  },
  {
    id: '36',
    title: '懒言'
  },
  {
    id: '26',
    title: '倦怠乏力'
  },
  {
    id: '53',
    title: '神疲'
  },
  {
    id: '183',
    title: '嗜睡（嗜卧）'
  },
  {
    id: '100',
    title: '容易疲劳'
  },
  {
    id: '4',
    title: '游走痛'
  },
  {
    id: '63',
    title: '疼痛固定不移'
  },
  {
    id: '186',
    title: '疼痛拒按'
  },
  {
    id: '187',
    title: '疼痛喜按'
  },
  {
    id: '65',
    title: '头痛'
  },
  {
    id: '68',
    title: '头胀痛'
  },
  {
    id: '49',
    title: '偏头痛'
  },
  {
    id: '11',
    title: '巅顶痛'
  },
  {
    id: '91',
    title: '牙龈肿痛'
  },  
  {
    id: '90',
    title: '胸痛'
  },
  {
    id: '88',
    title: '胸刺痛'
  },
  {
    id: '85',
    title: '胁痛'
  },
  {
    id: '82',
    title: '胁肋刺痛'
  },
  {
    id: '84',
    title: '胁肋胀痛'
  },
  {
    id: '95',
    title: '腰痛'
  },
  {
    id: '197',
    title: '腰腿酸痛'
  },
  {
    id: '189',
    title: '脘腹胀痛'
  },
  {
    id: '190',
    title: '脘腹灼痛'
  },
  {
    id: '200',
    title: '肢体刺痛'
  },
  {
    id: '201',
    title: '肢体木痛'
  },
  {
    id: '134',
    title: '头麻木'
  }, 
  {
    id: '148',
    title: '手指麻木'
  }, 
  {
    id: '116',
    title: '口唇麻木'
  }, 
  {
    id: '108',
    title: '肢体麻木'
  },       
  {
    id: '94',
    title: '腰酸'
  }, 
  {
    id: '96',
    title: '腰膝酸软'
  }, 
  {
    id: '147',
    title: '身重'
  }, 
  {
    id: '69',
    title: '头重'
  },  
  {
    id: '107',
    title: '肢体困重'
  },   
   {
      id: '68',
      title: '头胀痛'
   }, 
   {
      id: '11',
      title: '巅顶痛'
   },
   {
      id: '64',
      title: '头昏'
   },
   {
      id: '66',
      title: '眩晕'
   }, 
   {
      id: '67',
      title: '头胀'
   }, 
   {
      id: '69',
      title: '头重'
   },
   {
      id: '134',
      title: '头麻木'
   },
   {
      id: '91',
      title: '牙龈肿痛'
   },
   {
      id: '17',
      title: '脱发'
   },
    {
      id: '38',
      title: '面红'
    },
     {
      id: '39',
      title: '颜面浮肿'
    },
     {
      id: '40',
      title: '面色苍白'
    },
     {
      id: '41',
      title: '面色黧黑'
    },
     {
      id: '42',
      title: '面色萎黄'
    },
     {
      id: '123',
      title: '颧红'
    },
     {
      id: '175',
      title: '面色无华'
    },

    {
      id: '29',
      title: '口臭'
    }, 
    {
      id: '34',
      title: '口苦'
    }, 
    {
      id: '35',
      title: '口甜'
    },
    {
      id: '118',
      title: '口淡'
    },
    {
      id: '32',
      title: '口粘腻'
    },
    {
      id: '119',
      title: '口舌生疮'
    },
    {
      id: '115',
      title: '口唇干燥'
    },
    {
      id: '116',
      title: '口唇麻木'
    },
    {
      id: '117',
      title: '口唇色淡'
    },
    {
      id: '3',
      title: '口唇紫'
    },
    {
      id: '167',
      title: '口唇红'
    },  
    {
      id: '86',
      title: '心烦'
    },
    {
      id: '87',
      title: '心悸'
    },
    {
      id: '88',
      title: '胸刺痛'
    },
    {
      id: '89',
      title: '胸闷'
    },
    {
      id: '90',
      title: '胸痛'
    },
    {
      id: '144',
      title: '心胸烦热'
    },

    {
      id: '143',
      title: '胃脘痛'
    }, 
    {
      id: '71',
      title: '脘腹痞满'
    }, 
    {
      id: '188',
      title: '脘腹坚满'
    }, 
    {
      id: '189',
      title: '脘腹胀痛'
    }, 
    {
      id: '190',
      title: '脘腹灼痛'
    }, 
    {
      id: '18',
      title: '腹胀'
    }, 
    {
      id: '55',
      title: '食后痞胀'
    }, 
    {
      id: '136',
      title: '肠鸣'
    }, 
    {
      id: '137',
      title: '腹痛，喜温喜按'
    }, 
    {
      id: '170',
      title: '腹胀痛'
    }, 
    {
      id: '178',
      title: '少腹疼痛'
    },
    {
      id: '179',
      title: '少腹胀满'
    },
    {
      id: '180',
      title: '少腹胀痛'
    },
    {
      id: '194',
      title: '小腹冷痛'
    },
  
    {
      id: '85',
      title: '胁痛'
    }, 
    {
      id: '82',
      title: '胁肋刺痛'
    }, 
    {
      id: '84',
      title: '胁肋胀痛'
    }, 
    {
      id: '83',
      title: '胁肋胀满'
    },
  
    {
      id: '94',
      title: '腰酸'
    }, 
    {
      id: '196',
      title: '腰冷'
    }, 
    {
      id: '95',
      title: '腰痛'
    }, 
    {
      id: '197',
      title: '腰腿酸痛'
    },
    {
      id: '96',
      title: '腰膝酸软'
    },
    {
      id: '145',
      title: '背冷'
    },
    {
      id: '163',
      title: '背痛  '
    },
    
    {
      id: '59',
      title: '四肢乏力'
    },
    {
      id: '105',
      title: '肢冷'
    },
    {
      id: '107',
      title: '肢体困重'
    },
    {
      id: '108',
      title: '肢体麻木'
    },
    {
      id: '199',
      title: '肢体颤抖'
    },
    {
      id: '200',
      title: '肢体刺痛'
    }, 
    {
      id: '201',
      title: '肢体木痛'
    },
    {
      id: '106',
      title: '肢体浮肿'
    },
    {
      id: '146',
      title: '两足痿弱'
    },
      
    {
      id: '109',
      title: '爪甲少华'
    }, 
     {
      id: '110',
      title: '爪甲青紫'
    }, 
    {
      id: '15',
      title: '耳聋'
    },
    {
      id: '16',
      title: '耳鸣'
    },
    {
      id: '169',
      title: '耳轮枯焦'
    },
    {
      id: '43',
      title: '目赤'
    },
    {
      id: '122',
      title: '目胀'
    },
    {
      id: '44',
      title: '目干涩'
    }, 
    {
      id: '57',
      title: '视物模糊'
    },
    {
      id: '121',
      title: '眼屎较多'
    },
    {
      id: '125',
      title: '流泪'
    },

    {
      id: '29',
      title: '口臭'
    }, 
    {
      id: '34',
      title: '口苦'
    }, 
    {
      id: '35',
      title: '口甜'
    }, 
    {
      id: '118',
      title: '口淡'
    }, 
    {
      id: '32',
      title: '口粘腻'
    },  
    {
      id: '119',
      title: '口舌生疮'
    },  
    {
      id: '115',
      title: '口唇干燥'
    },  
    {
      id: '116',
      title: '口唇麻木'
    },  
    {
      id: '117',
      title: '口唇色淡'
    },  
    {
      id: '3',
      title: '口唇紫'
    },  
    {
      id: '167',
      title: '口唇红'
    },  
    {
      id: '2',
      title: '牙齿浮动'
    },  
    {
      id: '91',
      title: '牙龈肿痛'
    }, 
  
    {
      id: '164',
      title: '鼻鼾'
    }, 
    {
      id: '92',
      title: '咽干'
    },
    {
      id: '120',
      title: '咽喉异物感'
    },      
    {
      id: '0',
      title: '嗳气'
    },
    {
      id: '14',
      title: '恶心'
    },
    {
      id: '47',
      title: '呕吐'
    },
    {
      id: '51',
      title: '善食易饥'
    },
    {
      id: '56',
      title: '食欲不振'
    },
    {
      id: '135',
      title: '嘈杂'
    }, 
    {
      id: '138',
      title: '呃逆'
    },
    {
      id: '141',
      title: '吞酸'
    },
    {
      id: '173',
      title: '饥不能食'
    },

      {
        id: '31',
        title: '口渴'
      }, 
      {
        id: '27',
        title: '渴喜冷饮'
      }, 
      {
        id: '28',
        title: '渴喜热饮'
      }, 
      {
        id: '30',
        title: '口干'
      }, 
      {
        id: '33',
        title: '渴欲饮水而不能多'
      },  
      {
        id: '101',
        title: '饮不解渴'
      },  
      {
        id: '165',
        title: '不欲饮'
      },  
      {
        id: '174',
        title: '口渴欲饮但欲饮水不欲咽'
      },  
      
        {
          id: '37',
          title: '咯痰'
        },
        {
          id: '60',
          title: '痰多'
        },
        {
          id: '181',
          title: '痰少'
        },
        {
          id: '61',
          title: '痰滑易咯'
        },
        {
          id: '127',
          title: '痰液清稀'
        },
        {
          id: '128',
          title: '痰不易咯出'
        }, 
        {
          id: '129',
          title: '痰粘稠'
        },
      
        {
          id: '124',
          title: '多唾'
        }, 
        {
          id: '126',
          title: '流涎'
        }, 
      
        {
          id: '5',
          title: '大便干燥'
        },
        {
          id: '6',
          title: '大便秘结'
        },
        {
          id: '7',
          title: '大便溏薄'
        },
        {
          id: '8',
          title: '大便粘腻不爽'
        },
        {
          id: '184',
          title: '溏结不调'
        },
        {
          id: '74',
          title: '五更泄泻'
        }, 
        {
          id: '168',
          title: '大便泄泻'
        },  
        {
          id: '139',
          title: '食寒腹泻'
        },  
        {
          id: '140',
          title: '矢气频频'
        },
        {
          id: '142',
          title: '完谷不化'
        },
        {
          id: '45',
          title: '小便余沥'
        }, 
        {
          id: '46',
          title: '小便频数'
        }, 
        {
          id: '77',
          title: '小便短黄'
        }, 
        {
          id: '78',
          title: '小便短少'
        }, 
        {
          id: '79',
          title: '小便黄赤'
        }, 
        {
          id: '80',
          title: '小便清长'
        }, 
        {
          id: '81',
          title: '小便涩'
        }, 
        {
          id: '97',
          title: '夜尿多'
        }, 
        {
          id: '176',
          title: '小便量多'
        }, 
        {
          id: '192',
          title: '小便浑浊'
        }, 
        {
          id: '193',
          title: '小便失禁'
        },

        {
          id: '149',
          title: '肛门瘙痒'
        },
        {
          id: '70',
          title: '阴部瘙痒'
        },
        {
          id: '150',
          title: '阴部潮湿'
        },
      
        {
          id: '9',
          title: '带下清稀'
        },
        {
          id: '152',
          title: '带下稠厚'
        },
        {
          id: '151',
          title: '白带量多'
        },
        {
          id: '153',
          title: '黄带'
        },
        {
          id: '24',
          title: '月经量少'
        }, 
        {
          id: '154',
          title: '月经量多'
        }, 
        {
          id: '155',
          title: '月经色淡'
        }, 
        {
          id: '25',
          title: '经血紫暗'
        }, 
        {
          id: '103',
          title: '月经后期'
        }, 
        {
          id: '104',
          title: '经血夹块'
        }, 
        {
          id: '156',
          title: '经间期出血'
        }, 
        {
          id: '157',
          title: '经后腹痛'
        }, 
        {
          id: '158',
          title: '经前、经行腹痛'
        }, 
        {
          id: '159',
          title: '经前乳房胀痛'
        }, 
        {
          id: '160',
          title: '经行浮肿'
        },
        {
          id: '161',
          title: '经行情志异常'
        },
        {
          id: '162',
          title: '经行腰酸'
        },
        {
          id: '166',
          title: '不孕'
        },
        {
          id: '195',
          title: '性欲减低'
        },
        {
          id: '93',
          title: '阳痿'
        }, 
        {
          id: '98',
          title: '遗精'
        }, 
        {
          id: '171',
          title: '滑精'
        }, 
        {
          id: '195',
          title: '性欲减低'
        }, 
        {
          id: '198',
          title: '早泄'
        },
     
        {
          id: '20',
          title: '肌肤甲错'
        }, 
        {
          id: '23',
          title: '皮肤痤疮或疮疖'
        }, 
        {
          id: '48',
          title: '皮肤干燥'
        }, 
        {
          id: '102',
          title: '皮肤瘀斑瘀点'
        }, 
  
        {
          id: '220',
          title: '血脂高'
        }, 
        {
          id: '221',
          title: '血流变高凝状态'
        }, 
        {
          id: '172',
          title: '血糖（尿糖）高'
        }, 
        {
          id: '112',
          title: '常因情志不畅诱发或加重'
        },
        {
          id: '113',
          title: '劳则甚'
        },
        {
          id: '114',
          title: '遇冷尤甚'
        },
        {
          id: '177',
          title: '入夜尤甚'
        },
        {
          id: '12',
          title: '动则加重'
        },
        {
          id: '185',
          title: '疼痛常夜间加剧'
        },
        {
          id: '50',
          title: '气短'
        }, 
        {
          id: '182',
          title: '身痒'
        }, 
        {
          id: '99',
          title: '容易感冒'
        }, 
        {
          id: '131',
          title: '容易抽筋'
        },  
        {
          id: '202',
          title: '舌胖'
        },
        {
          id: '203',
          title: '舌瘦'
        },
        {
          id: '204',
          title: '齿痕舌'
        },
        {
          id: '205',
          title: '舌裂'
        },
        {
          id: '206',
          title: '舌赤红'
        },
        {
          id: '207',
          title: '舌暗红'
        },
        {
          id: '208',
          title: '舌紫'
        },
        {
          id: '209',
          title: '舌有瘀斑或瘀点'
        }, 
        {
          id: '210',
          title: '舌苔白'
        }, 
        {
          id: '211',
          title: '舌苔黄'
        }, 
        {
          id: '212',
          title: '舌苔腻'
        }, 
        {
          id: '213',
          title: '舌苔腐'
        },
        {
          id: '214',
          title: '脉弦'
        }, 
        {
          id: '215',
          title: '脉滑'
        }, 
        {
          id: '216',
          title: '脉数'
        }, 
        {
          id: '217',
          title: '脉迟'
        }, 
        {
          id: '218',
          title: '虚脉'
        }, 
        {
          id: '219',
          title: '脉结代'
        },  
];
export const SCALE_PARTUM =[
  {
    title: '五官及其他症状',
    subject: [
      {
        id: 's1',
        title: '我近来：',
        item: [
          {
            id: '80',
            title: '面红赤'
          },
          {
            id: '81',
            title: '脸色暗沉'
          },
        ],
      },
      {
        id: 's2',
        title: '我觉得：',
        item: [
          {
            id: '32',
            title: '口干'
          },
          {
            id: '33',
            title: '口渴'
          },
          {
            id: '34',
            title: '口苦'
          },
          {
            id: '35',
            title: '口甜'
          },
        ]
      },
      {
        id: 's3',
        title: '我会：',
        item: [
          {
            id: '16',
            title: '鼻干'
          },
          {
            id: '17',
            title: '长期口唇肿胀'
          },
          {
            id: '18',
            title: '唇齿干燥'
          },
          {
            id: '19',
            title: '唇干裂'
          },
          {
            id: '20',
            title: '口舌生疮'
          },
          {
            id: '21',
            title: '耳鸣或听力下降'
          },
          {
            id: '22',
            title: '声音嘶哑或不能出声（排除感冒）'
          },
        ],
      },
      {
        id: 's4',
        title: '我会：', 
        item: [
          {
            id: '23',
            title: '咳嗽'
          },
          {
            id: '24',
            title: '咳嗽，严重时感觉恶心'
          },
          {
            id: '25',
            title: '咳嗽咯痰'
          },
          {
            id: '26',
            title: '咳嗽气喘'
          },
          {
            id: '27',
            title: '呛咳'
          },
          {
            id: '28',
            title: '气喘气促'
          },
          {
            id: '29',
            title: '洗浴后即气喘'
          },
          {
            id: '30',
            title: '痰稠'
          },
          {
            id: '31',
            title: '痰多'
          },
        ],
      },
    ],
  },
  {
    title: '饮食及其他症状',
    subject: [
      {
        id: 's5',
        title: '我近来：',
        item: [
          {
            id: '36',
            title: '食少'
          },
          {
            id: '37',
            title: '看见食物即觉恶心'
          },
           {
            id: '38',
            title: '食量倍增'
          },
           {
            id: '39',
            title: '食量大，吃肉较多'
          },
          {
            id: '40',
            title: '天热时吃瓜果或冷饮偏多'
          },
          {
            id: '41',
            title: '吃饭不香'
          },
        ],
      },
      {
        id: 's6',
        title: '我会出现：',
        item: [
          {
            id: '42',
            title: '恶心'
          },
          {
            id: '43',
            title: '干呕'
          },
          {
            id: '44',
            title: '反酸，或从胃里往上反气，带有食物的味道'
          },
          {
            id: '45',
            title: '经常打嗝'
          },
          {
            id: '46',
            title: '呕吐'
          },
        ]
      },
      {
        id: 's7',
        title: '我近来：',
        item: [
          {
            id: '47',
            title: '进食后即感腹胀'
          },
          {
            id: '48',
            title: '胃痛时作'
          },
          {
            id: '49',
            title: '胃胀，有饱闷感'
          },
          {
            id: '50',
            title: '胃中不适，嘈杂，俗称“烧心”'
          },
        ],
      },
       {
        id: 's8',
        title: '我近来：',
        item: [
          {
            id: '116',
            title: '时不时打寒战'
          },
          {
            id: '117',
            title: '感觉全身发热或身体某些部位发热，人不舒服但体温正常'
          },
          {
            id: '118',
            title: '下午或夜间不适症状加重'
          },
          {
            id: '119',
            title: '皮肤痒'
          },
        ],
      },
    ],
  },
  {
    title: '情绪及其他症状',
    subject: [
      {
        id: 's9',
        title: '我近来：',
        item: [
          {
            id: '62',
            title: '头痛'
          },
          {
            id: '63',
            title: '头晕'
          },
          {
            id: '64',
            title: '头晕，视物不清'
          },
          {
            id: '65',
            title: '眼花'
          },
        ],
      },
      {
        id: 's10',
        title: '我近来：',
        item: [
          {
            id: '56',
            title: '气短'
          },
          {
            id: '57',
            title: '心痛'
          },
          {
            id: '58',
            title: '胸闷'
          },
          {
            id: '59',
            title: '心慌心跳快，感觉心脏搏动强烈，伴心前区不适感'
          },
          {
            id: '60',
            title: '心慌心跳快，同时伴汗出'
          },
          {
            id: '61',
            title: '易受惊吓，受惊后心跳剧烈'
          },
        ],
      },
      {
        id: 's11',
        title: '我感觉：',
        item: [
          {
            id: '66',
            title: '体倦乏力'
          },
          {
            id: '67',
            title: '四肢乏力'
          },
          {
            id: '68',
            title: '精神疲倦'
          },
          {
            id: '69',
            title: '精神萎靡不振'
          },
        ],
      },
      {
        id: 's12',
        title: '我近来：',
        item: [
          {
            id: '70',
            title: '入睡困难'
          },
          {
            id: '71',
            title: '嗜睡'
          },
          {
            id: '72',
            title: '彻夜不眠'
          },
          {
            id: '73',
            title: '能睡着但睡不安稳，睡眠质量差'
          },
        ],
      },
      {
        id: 's13',
        title: '我近来：',
        item: [
          {
            id: '74',
            title: '易受惊吓'
          },
          {
            id: '75',
            title: '心烦'
          },
          {
            id: '76',
            title: '易怒'
          },
          {
            id: '77',
            title: '心神不宁'
          },
          {
            id: '79',
            title: '常常悲伤欲哭，难以自制'
          },
          {
            id: '78',
            title: '感觉精神恍惚，注意力难以集中'
          },
        ],
      },
    ],
  },
  {
    title: '身体感觉及其他症状',
    subject: [
      {
        id: 's14',
        title: '我会出现：',
        item: [
          {
            id: '82',
            title: '汗多湿衣'
          },
          {
            id: '83',
            title: '头汗多'
          },
          {
            id: '84',
            title: '睡觉时出汗'
          },
          {
            id: '85',
            title: '容易出汗但量不多'
          },
        ],
      },
      {
        id: 's15',
        title: '我感觉：',
        item: [
          {
            id: '86',
            title: '手麻'
          },
          {
            id: '87',
            title: '手指颤振'
          },
          {
            id: '88',
            title: '身体发麻'
          },
        ],
      },
      {
        id: 's16',
        title: '我近来：',
        item: [
          {
            id: '98',
            title: '手不能举'
          },
          {
            id: '99',
            title: '难于转侧'
          },
          {
            id: '100',
            title: '上肢屈伸不利，提不了重物'
          },
        ],
      },
      {
        id: 's17',
        title: '我感觉：',
        item: [
          {
            id: '89',
            title: '肢体或颜面浮肿'
          },
          {
            id: '90',
            title: '容易抽筋'
          },
          {
            id: '91',
            title: '筋脉中有收束刺痛感'
          },
          {
            id: '92',
            title: '筋肉不自主的跳动'
          },
        ],
      },
      {
        id: 's18',
        title: '我近来：',
        item: [
          {
            id: '109',
            title: '腹中冷'
          },
          {
            id: '110',
            title: '畏寒喜暖'
          },
          {
            id: '111',
            title: '感觉骨中热'
          },
          {
            id: '112',
            title: '感觉关节发冷疼痛'
          },
          {
            id: '113',
            title: '两足冷'
          }, 
          {
            id: '114',
            title: '手足冷'
          }, 
          {
            id: '115',
            title: '手足冷，过肘膝'
          },
        ],
      },
    ],
  },
  {
    title: '疼痛及相关症状',
    subject: [
      {
        id: 's19',
        title: '我觉得：',
        item: [
          {
            id: '51',
            title: '腹满腹胀'
          },
          {
            id: '52',
            title: '腹痛'
          },
          {
            id: '53',
            title: '腹胀痛'
          },
          {
            id: '54',
            title: '两肋向上连及乳房皆痛'
          }, 
          {
            id: '55',
            title: '胁肋部胀或疼痛'
          },
        ],
      },
      {
        id: 's20',
        title: '我感觉：',
        item: [
          {
            id: '101',
            title: '腰痛'
          },
          {
            id: '102',
            title: '腰脊酸痛'
          },
          {
            id: '103',
            title: '腰疼连及两大腿痛'
          },
          {
            id: '104',
            title: '腰、大腿麻木疼痛，连及胯腹'
          }, 
          {
            id: '105',
            title: '腰部无力'
          },
        ],
      },
      {
        id: 's21',
        title: '我感觉：',
        item: [
          {
            id: '106',
            title: '大腿刺痛'
          },
          {
            id: '107',
            title: '两大腿作痛'
          },
          {
            id: '108',
            title: '膝软'
          },
        ],
      },
      {
        id: 's22',
        title: '我近来：',
        item: [
          {
            id: '93',
            title: '周身关节有酸楚不适感，伴或不伴疼痛'
          },
          {
            id: '94',
            title: '周身疼痛（排除感冒）'
          },
          {
            id: '95',
            title: '周身疼痛，以手按之痛甚'
          }, 
          {
            id: '96',
            title: '四肢窜痛 '
          },  
          {
            id: '97',
            title: '关节疼痛 '
          },
        ],
      },  
    ],
  },
  {
    title: '妇科及二便症状',
    subject: [
      {
        id: 's23',
        title: '我的白带和月经：',
        item: [
          {
            id: '6',
            title: '白带量极少'
          },
          {
            id: '7',
            title: '白带清稀量多'
          },
          {
            id: '8',
            title: '经来多血块'
          },
          {
            id: '9',
            title: '经来十日半月不止'
          },
          {
            id: '10',
            title: '经血稠粘深红'
          },
          {
            id: '11',
            title: '经血色淡'
          },
          {
            id: '12',
            title: '经血色淡，月经周期大于34天'
          },
        ],
      },
      {
        id: 's24',
        title: '我感觉：',
        item: [
          {
            id: '13',
            title: '小腹两侧或单侧疼痛'
          },
          {
            id: '14',
            title: '小腹时痛'
          },
          {
            id: '15',
            title: '小腹胀满，痛或不痛'
          },
        ],
      },
      {
        id: 's25',
        title: '我近来：',
        item: [
          
          {
            id: '121',
            title: '大便数日未解，欲排便而无力'
          },
          {
            id: '122',
            title: '大便稀，不成形'
          },
          {
            id: '123',
            title: '便秘'
          },
          {
            id: '124',
            title: '便血'
          },
          {
            id: '125',
            title: '易腹泻'
          },
          {
            id: '126',
            title: '泄泻'
          },
          {
            id: '127',
            title: '肛门灼热'
          },
        ],
      },
      {
        id: 's26',
        title: '我的小便：',
        item: [
          {
            id: '128',
            title: '小便量少，颜色深'
          },
          {
            id: '129',
            title: '小便憋不住，咳嗽或打喷嚏时会自己流出'
          },
          {
            id: '130',
            title: '小便频，量多'
          },
          {
            id: '131',
            title: '小便热'
          },
          {
            id: '132',
            title: '产后小便不通'
          },
        ],
      }, 
    ],
  },
];
export const SCALE_PARTUM_ITEM = {
  '36': '38;39',
  '38': '36',
  '39': '36',
  '6': '7',
  '7': '6',
  '10': '11',
  '11': '10',
  '114': '115',
  '115': '114'
};

export const SCALE_PARTUM_VALUE = [
  // {id:'1',title:'哺乳期'},
  // {id:'2',title:'有产后子宫复旧不全病史'},
  // {id:'3',title:'生产过程不顺利'},
  {id:'4',title:'怀孕四次及以上'},
  {id:'5',title:'消瘦'},
  {id:'6',title:'白带量极少'},
  {id:'7',title:'白带清稀量多'},
  {id:'8',title:'经来多血块'},
  {id:'9',title:'经来十日半月不止'},
  {id:'10',title:'经血稠粘深红'},
  {id:'11',title:'经血色淡'},
  {id:'12',title:'经血色淡，月经周期大于34天'},
  {id:'13',title:'小腹两侧或单侧疼痛'},
  {id:'14',title:'小腹时痛'},
  {id:'15',title:'小腹胀满，痛或不痛'},
  {id:'16',title:'鼻干'},
  {id:'17',title:'长期口唇肿胀'},
  {id:'18',title:'唇齿干燥'},
  {id:'19',title:'唇干裂'},
  {id:'20',title:'口舌生疮'},
  {id:'21',title:'耳鸣或听力下降'},
  {id:'22',title:'声音嘶哑或不能出声（排除感冒）'},
  {id:'23',title:'咳嗽'},
  {id:'24',title:'咳嗽，严重时感觉恶心'},
  {id:'25',title:'咳嗽咯痰'},
  {id:'26',title:'咳嗽气喘'},
  {id:'27',title:'呛咳'},
  {id:'28',title:'气喘气促'},
  {id:'29',title:'洗浴后即气喘'},
  {id:'30',title:'痰稠'},
  {id:'31',title:'痰多'},
  {id:'32',title:'口干'},
  {id:'33',title:'口渴'},
  {id:'34',title:'口苦'},
  {id:'35',title:'口甜'},
  {id:'36',title:'食少'},
  {id:'37',title:'看见食物即觉恶心'},
  {id:'38',title:'食量倍增'},
  {id:'39',title:'食量大，吃肉较多'},
  {id:'40',title:'天热时吃瓜果或冷饮偏多'},
  {id:'41',title:'吃饭不香'},
  {id:'42',title:'恶心'},
  {id:'43',title:'干呕'},
  {id:'44',title:'反酸，或从胃里往上反气，带有食物的味道'},
  {id:'45',title:'经常打嗝'},
  {id:'46',title:'呕吐'},
  {id:'47',title:'进食后即感腹胀'},
  {id:'48',title:'胃痛时作'},
  {id:'49',title:'胃胀，有饱闷感'},
  {id:'50',title:'胃中不适，嘈杂，俗称“烧心”'},
  {id:'51',title:'腹满腹胀'},
  {id:'52',title:'腹痛'},
  {id:'53',title:'腹胀痛'},
  {id:'54',title:'两肋向上连及乳房皆痛'},
  {id:'55',title:'胁肋部胀或疼痛'},
  {id:'56',title:'气短'},
  {id:'57',title:'心痛'},
  {id:'58',title:'胸闷'},
  {id:'59',title:'心慌心跳快，感觉心脏搏动强烈，伴心前区不适感'},
  {id:'60',title:'心慌心跳快，同时伴汗出'},
  {id:'61',title:'易受惊吓，受惊后心跳剧烈'},
  {id:'62',title:'头痛'},
  {id:'63',title:'头晕'},
  {id:'64',title:'头晕，视物不清'},
  {id:'65',title:'眼花'},
  {id:'66',title:'体倦乏力'},
  {id:'67',title:'四肢乏力'},
  {id:'68',title:'精神疲倦'},
  {id:'69',title:'精神萎靡不振'},
  {id:'70',title:'入睡困难'},
  {id:'71',title:'嗜睡'},
  {id:'72',title:'彻夜不眠'},
  {id:'73',title:'能睡着但睡不安稳，睡眠质量差'},
  {id:'74',title:'易受惊吓'},
  {id:'75',title:'心烦'},
  {id:'76',title:'易怒'},
  {id:'77',title:'心神不宁'},
  {id:'78',title:'感觉精神恍惚，注意力难以集中'},
  {id:'79',title:'常常悲伤欲哭，难以自制'},
  {id:'80',title:'面红赤'},
  {id:'81',title:'脸色暗沉'},
  {id:'82',title:'汗多湿衣'},
  {id:'83',title:'头汗多'},
  {id:'84',title:'睡觉时出汗'},
  {id:'85',title:'容易出汗但量不多'},
  {id:'86',title:'手麻'},
  {id:'87',title:'手指颤振'},
  {id:'88',title:'身体发麻'},
  {id:'89',title:'肢体或颜面浮肿'},
  {id:'90',title:'容易抽筋'},
  {id:'91',title:'筋脉中有收束刺痛感'},
  {id:'92',title:'筋肉不自主的跳动'},
  {id:'93',title:'周身关节有酸楚不适感，伴或不伴疼痛'},
  {id:'94',title:'周身疼痛（排除感冒）'},
  {id:'95',title:'周身疼痛，以手按之痛甚'},
  {id:'96',title:'四肢窜痛'},
  {id:'97',title:'关节疼痛'},
  {id:'98',title:'手不能举'},
  {id:'99',title:'难于转侧'},
  {id:'100',title:'上肢屈伸不利、提不了重物'},
  {id:'101',title:'腰痛'},
  {id:'102',title:'腰脊酸痛'},
  {id:'103',title:'腰疼连及两大腿痛'},
  {id:'104',title:'腰、大腿麻木疼痛，连及胯腹'},
  {id:'105',title:'腰部无力'},
  {id:'106',title:'大腿刺痛'},
  {id:'107',title:'两大腿作痛'},
  {id:'108',title:'膝软'},
  {id:'109',title:'腹中冷'},
  {id:'110',title:'畏寒喜暖'},
  {id:'111',title:'感觉骨中热'},
  {id:'112',title:'感觉关节发冷疼痛'},
  {id:'113',title:'两足冷'},
  {id:'114',title:'手足冷'},
  {id:'115',title:'手足冷，过肘膝'},
  {id:'116',title:'时不时打寒战'},
  {id:'117',title:'感觉全身发热或身体某些部位发热，人不舒服但体温正常'},
  {id:'118',title:'下午或夜间不适症状加重'},
  {id:'119',title:'皮肤痒'},
  // {id:'120',title:'产后奶水极少或没奶无法正常哺乳'},
  {id:'121',title:'大便数日未解，欲排便而无力'},
  {id:'122',title:'大便稀，不成形'},
  {id:'123',title:'便秘'},
  {id:'124',title:'便血'},
  {id:'125',title:'易腹泻'},
  {id:'126',title:'泄泻'},
  {id:'127',title:'肛门灼热'},
  {id:'128',title:'小便量少，颜色深'},
  {id:'129',title:'小便憋不住，咳嗽或打喷嚏时会自己流出'},
  {id:'130',title:'小便频，量多'},
  {id:'131',title:'小便热'},
  {id:'132',title:'产后小便不通'},
  {id:'133',title:'舌红'},
  {id:'134',title:'舌绛'},
  {id:'135',title:'舌无苔'},
  {id:'136',title:'苔黄'},
  {id:'137',title:'脉滑'},
  {id:'138',title:'脉弦'},
];
export const SCALE_PREPRE =[
  {
    title: '月经及相关症状',
    subject: [
      {
        id: 's1',
        title: '我的月经周期：',
        item: [
          {
            id: '18',
            title: '月经周期大于34天'
          },
          {
            id: '19',
            title: '月经周期小于25天'
          },
          {
            id: '20',
            title: '经来十日半月不止',
          }
        ],
      },
      {
        id: 's2',
        title: '我的月经量：',
        item: [
          {
            id: '21',
            title: '月经过多'
          },
          {
            id: '22',
            title: '月经量少'
          },
          {
            id: '23',
            title: '月经量极少，点滴而已'
          },
        ]
      },
      {
        id: 's3',
        title: '我的月经呈：',
        item: [
          {
            id: '24',
            title: '经血粘稠深红'
          },
          {
            id: '25',
            title: '经血色淡'
          },
          {
            id: '26',
            title: '经来多血块'
          },
          {
            id: '27',
            title: '月经下黑水如黑豆汁'
          },
          {
            id: '28',
            title: '月经或流赤白黄水'
          },
          {
            id: '29',
            title: '月经血块多，色暗或夹白色'
          },
        ],
      },
      {
        id: 's4',
        title: '我经期前后的身体变化：', 
        item: [
          {
            id: '30',
            title: '经行或经前口渴'
          },
          {
            id: '31',
            title: '经前皮肤易起皮疹或长痘痘'
          },
          {
            id: '32',
            title: '经行时感皮肤热'
          },
          {
            id: '33',
            title: '经期或经后，感觉身体异常疲惫'
          },
        ],
      },
      {
        id: 's5',
        title: '我经期时有以下状况：', 
        item: [
          {
            id: '34',
            title: '经期，常有呕吐'
          },
          {
            id: '35',
            title: '经期常有轻微腹泻'
          },
          {
            id: '36',
            title: '经来感觉手或足肿胀，或是眼睑浮肿'
          },
          {
            id: '37',
            title: '经来或经后身痛'
          },
           {
            id: '38',
            title: '痛经'
          },
           {
            id: '39',
            title: '经期第三天后有小腹两侧或单侧疼痛'
          },
        ],
      },
    ],
  },
  {
    title: '其他妇科症状',
    subject: [
      {
        id: 's6',
        title: '我的白带：',
        item: [
          {
            id: '12',
            title: '白带量多清稀'
          },
          {
            id: '13',
            title: '白带量多，淡黄或偶带青绿色'
          },
          {
            id: '14',
            title: '白带呈咖啡色',
          },
          {
            id: '15',
            title: '白带呈红色（非月经期）',
          },
          {
            id: '16',
            title: '带下腥臭',
          },
          {
            id: '17',
            title: '白带粘稠臭秽',
          },
        ],
      },
      {
        id: 's7',
        title: '我有时有下列情况：',
        item: [
          {
            id: '44',
            title: '脐周冷痛'
          },
          {
            id: '45',
            title: '脐旁左右一筋疼'
          },
          {
            id: '46',
            title: '脐下牵痛'
          },
        ]
      },
      {
        id: 's8',
        title: '我近来感觉：',
        item: [
          {
            id: '47',
            title: '小腹两侧或单侧疼痛'
          },
          {
            id: '48',
            title: '小腹冷（尤其是小腹两侧皮肤发凉）'
          },
          {
            id: '49',
            title: '小腹硬'
          },
          {
            id: '50',
            title: '小腹坠胀'
          },
        ],
      },
      {
        id: 's9',
        title: '我近来出现：', 
        item: [
          {
            id: '41',
            title: '外阴红肿'
          },
          {
            id: '42',
            title: '外阴皮肤瘙痒疼痛，分泌物增多'
          },
          {
            id: '43',
            title: '外阴瘙痒'
          },
        ],
      },
      {
        id: 's10',
        title: '我的情况是：', 
        item: [
          {
            id: '40',
            title: '性生活后，阴道少量出血'
          },
          {
            id: '132',
            title: '当老公/男友想做爱时我总是能配合'
          },
          {
            id: '1002',
            title: '当老公/男友想做爱时我有时不能配合'
          },
        ],
      },
    ],
  },
  {
    title: '情绪及相关症状',
    subject: [
      {
        id: 's11',
        title: '我近来觉得：',
        item: [
          {
            id: '69',
            title: '心烦'
          },
          {
            id: '70',
            title: '易怒'
          },
          {
            id: '71',
            title: '心神不宁',
          },
          {
            id: '72',
            title: '情绪莫名低落',
          },
          {
            id: '136',
            title: '情绪不稳，容易哭',
          },
        ],
      },
      {
        id: 's12',
        title: '我近来有：',
        item: [
          {
            id: '68',
            title: '思虑过度/压力大'
          },
          {
            id: '73',
            title: '失眠'
          },
          {
            id: '74',
            title: '易受惊吓'
          },
          {
            id: '135',
            title: '多梦'
          },
          {
            id: '137',
            title: '精神疲倦'
          },
        ]
      },
      {
        id: 's13',
        title: '我近来感觉：',
        item: [
          {
            id: '75',
            title: '心慌心跳快，感觉心脏搏动强烈，伴心前区不适感'
          },
          {
            id: '76',
            title: '胸闷'
          },
          {
            id: '77',
            title: '胸痛'
          },
          {
            id: '78',
            title: '气短'
          },
        ],
      },
      {
        id: 's14',
        title: '我近来感觉：', 
        item: [
          {
            id: '91',
            title: '头晕，视物不清'
          },
          {
            id: '92',
            title: '头晕，头痛'
          },
          {
            id: '124',
            title: '眉棱骨痛，或可及太阳穴'
          },
          {
            id: '125',
            title: '耳内或耳后项侧作痛',
          },
        ],
      },
      {
        id: 's15',
        title: '我近来：', 
        item: [
          {
            id: '89',
            title: '咽中若有一核，吞吐不利'
          },
          {
            id: '131',
            title: '常因生气而诱发头、耳、颈项、咽喉、牙、乳房等部位疼痛'
          },
        ],
      },
    ],
  },
  {
    title: '五官及其他症状',
    subject: [
      {
        id: 's16',
        title: '我的面色：',
        item: [
          {
            id: '79',
            title: '两颧发红'
          },
          {
            id: '80',
            title: '面红'
          },
          {
            id: '81',
            title: '脸色不好，没有光泽',
          },
          {
            id: '138',
            title: '有黑眼圈',
          },
        ],
      },
      {
        id: 's17',
        title: '我近来感觉：',
        item: [
          {
            id: '93',
            title: '视物不清'
          },
          {
            id: '94',
            title: '耳鸣'
          },
          {
            id: '95',
            title: '听力下降'
          },
          {
            id: '96',
            title: '目干涩'
          }
        ]
      },
      {
        id: 's18',
        title: '我近来感觉：',
        item: [
          {
            id: '82',
            title: '口干，口渴'
          },
          {
            id: '83',
            title: '咽干'
          },
          {
            id: '84',
            title: '口苦'
          },
          {
            id: '85',
            title: '口内不时辛辣'
          },
          {
            id: '86',
            title: '口燥，但欲漱水，不欲咽'
          },
          {
            id: '87',
            title: '口水多，总是不自主的吐或咽'
          },
        ],
      },
      {
        id: 's19',
        title: '我近来出现：', 
        item: [
          {
            id: '139',
            title: '鼻干'
          },
          {
            id: '140',
            title: '唇干裂'
          },
          {
            id: '115',
            title: '口唇麻木'
          },
          {
            id: '102',
            title: '易发口角炎或溃疡'
          },
          {
            id: '126',
            title: '牙龈萎缩'
          },
          {
            id: '141',
            title: '舌无苔'
          },
        ],
      },
      {
        id: 's20',
        title: '我近来有以下状况：', 
        item: [
          {
            id: '88',
            title: '咳嗽吐沫'
          },
          {
            id: '90',
            title: '痰多'
          },
          {
            id: '142',
            title: '干咳少痰或无痰'
          },
        ],
      }
    ],
  },
  {
    title: '身体感觉及其他症状',
    subject: [
      {
        id: 's21',
        title: '我近来感觉：',
        item: [
          {
            id: '97',
            title: '身子沉，懒于活动'
          },
          {
            id: '98',
            title: '腰以下重'
          },
          {
            id: '99',
            title: '体倦乏力',
          },
          {
            id: '100',
            title: '劳累后感耳鸣头晕',
          },
          {
            id: '101',
            title: '脱发',
          }
        ],
      },
      {
        id: 's22',
        title: '我近来出现：',
        item: [
          {
            id: '108',
            title: '容易汗出，有时睡着也会出'
          },
          {
            id: '109',
            title: '不耐热'
          },
          {
            id: '110',
            title: '畏寒喜暖'
          },
          {
            id: '111',
            title: '两足发热'
          },
          {
            id: '112',
            title: '手足冷',
          },
          {
            id: '113',
            title: '两足常冷'
          },
          {
            id: '114',
            title: '下身冷'
          }
        ]
      },
      {
        id: 's23',
        title: '我近来：',
        item: [
          {
            id: '127',
            title: '容易抽筋'
          },
          {
            id: '128',
            title: '感觉全身发热或身体某些部位发热，人不舒服但体温正常'
          },
          {
            id: '130',
            title: '下午或夜间不适症状加重'
          },
          {
            id: '129',
            title: '感觉手或足肿胀，或是眼睑浮肿'
          },
        ],
      },
      {
        id: 's24',
        title: '我的情况是 ：', 
        item: [
          {
            id: '56',
            title: '饭量不少但人偏瘦'
          },
          {
            id: '57',
            title: '常常已经吃饱了但还是会再吃一点'
          },
          {
            id: '58',
            title: '喜食辛辣之物'
          },
          {
            id: '59',
            title: '经行期间或产后不忌生冷'
          },
        ],
      },
      {
        id: 's25',
        title: '我的皮肤 ：', 
        item: [
          {
            id: '103',
            title: '易发皮疹等过敏性问题'
          },
          {
            id: '104',
            title: '手或足易起疹子或小水泡'
          },
          {
            id: '105',
            title: '皮肤起白屑'
          },
          {
            id: '106',
            title: '皮肤瘙痒，抓之肤见赤痕'
          },
          {
            id: '107',
            title: '皮肤干，发痒'
          }
        ],
      },
    ],
  },
  {
    title: '二便及其他症状',
    subject: [
      {
        id: 's26',
        title: '我近来感觉：',
        item: [
          {
            id: '116',
            title: '感觉手指或足趾发麻'
          },
          {
            id: '117',
            title: '肢体酸麻'
          },
          {
            id: '118',
            title: '两肋有紧束感',
          },
          {
            id: '119',
            title: '胁肋部胀或疼痛',
          },
        ],
      },
      {
        id: 's27',
        title: '我近来感觉：',
        item: [
          {
            id: '120',
            title: '四肢酸痛'
          },
          {
            id: '121',
            title: '腰酸'
          },
          {
            id: '122',
            title: '腰痛'
          },
          {
            id: '123',
            title: '足跟痛或足底痛'
          },
        ]
      },
      {
        id: 's28',
        title: '我近来：',
        item: [
          {
            id: '65',
            title: '腹满腹胀，吃饭稍多就不舒服'
          },
          {
            id: '66',
            title: '反酸，或从胃里往上反气，带有食物的味道'
          },
          {
            id: '67',
            title: '胃痛时作'
          },
        ],
      },
      {
        id: 's29',
        title: '我的小便情况：', 
        item: [
          {
            id: '51',
            title: '尿色深黄'
          },
          {
            id: '52',
            title: '小便混浊'
          },
          {
            id: '53',
            title: '小便频'
          },
          {
            id: '54',
            title: '小便憋不住，咳嗽或打喷嚏时会自己流出'
          },
          {
            id: '55',
            title: '欲小便而排出得不顺畅，或排完后感觉似乎还有'
          },
        ],
      },
      {
        id: 's30',
        title: '我的大便情况：', 
        item: [
          {
            id: '60',
            title: '便秘'
          },
          {
            id: '61',
            title: '排便不畅'
          },
          {
            id: '62',
            title: '大便时秘时溏泄'
          },
          {
            id: '63',
            title: '易腹泻'
          },
          {
            id: '64',
            title: '大便不成形'
          }
        ],
      },
      // {
      //   id: 's31',
      //   title: '我的脉象：', 
      //   item: [
      //     {
      //       id: '143',
      //       title: '脉虚'
      //     },
      //     {
      //       id: '144',
      //       title: '脉弦'
      //     },
      //     {
      //       id: '145',
      //       title: '脉细'
      //     },
      //   ],
      // },
    ],
  },
];
export const SCALE_PREPRE_ITEM = {
  '18':'19',
  '19':'18',
  '21': '22;23',
  '22': '23;21',
  '23': '21;22',
  '24': '25;27',
  '27': '24',
  '25': '24',
  '60': '64',
  '64': '60',
  '86': '87',
  '87': '86',
  '111': '112;113',
  '112': '111;113',
  '113': '111;112',
  '90': '142',
  '142': '90',
  '91': '93',
  '93': '91',
  '132': '1002',
  '1002': '132'
};
export const SCALE_PREPRE_VALUE = [
  
  {
    id: '18',
    title: '月经周期大于34天'
  },
  {
    id: '19',
    title: '月经周期小于25天'
  },
  {
    id: '20',
    title: '经来十日半月不止',
  },
  {
    id: '21',
    title: '月经过多'
  },
  {
    id: '22',
    title: '月经量少'
  },
  {
    id: '23',
    title: '月经量极少，点滴而已'
  },
  {
    id: '24',
    title: '经血粘稠深红'
  },
  {
    id: '25',
    title: '经血色淡'
  },
  {
    id: '26',
    title: '经来多血块'
  },
  {
    id: '27',
    title: '月经下黑水如黑豆汁'
  },
  {
    id: '28',
    title: '月经或流赤白黄水'
  },
  {
    id: '29',
    title: '月经血块多，色暗或夹白色'
  },
  {
    id: '30',
    title: '经行或经前口渴'
  },
  {
    id: '31',
    title: '经前皮肤易起皮疹或长痘痘'
  },
  {
    id: '32',
    title: '经行时感皮肤热'
  },
  {
    id: '33',
    title: '经期或经后，感觉身体异常疲惫'
  },
  {
    id: '34',
    title: '经期，常有呕吐'
  },
  {
    id: '35',
    title: '经期常有轻微腹泻'
  },
  {
    id: '36',
    title: '经来感觉手或足肿胀，或是眼睑浮肿'
  },
  {
    id: '37',
    title: '经来或经后身痛'
  },
   {
    id: '38',
    title: '痛经'
  },
  {
    id: '39',
    title: '经期第三天后有小腹两侧或单侧疼痛'
  },
  {
    id: '12',
    title: '白带量多清稀'
  },
  {
    id: '13',
    title: '白带量多，淡黄或偶带青绿色'
  },
  {
    id: '14',
    title: '白带呈咖啡色',
  },
  {
    id: '15',
    title: '白带呈红色（非月经期）',
  },
  {
    id: '16',
    title: '带下腥臭',
  },
  {
    id: '17',
    title: '白带粘稠臭秽',
  },
  {
    id: '44',
    title: '脐周冷痛'
  },
  {
    id: '45',
    title: '脐旁左右一筋疼'
  },
  {
    id: '46',
    title: '脐下牵痛'
  },
  {
    id: '47',
    title: '小腹两侧或单侧疼痛'
  },
  {
    id: '48',
    title: '小腹冷（尤其是小腹两侧皮肤发凉）'
  },
  {
    id: '49',
    title: '小腹硬'
  },
  {
    id: '50',
    title: '小腹坠胀'
  },
  {
    id: '41',
    title: '外阴红肿'
  },
  {
    id: '42',
    title: '外阴皮肤瘙痒疼痛，分泌物增多'
  },
  {
    id: '43',
    title: '外阴瘙痒'
  },
  {
    id: '40',
    title: '性生活后，阴道少量出血'
  },
  {
    id: '132',
    title: '当老公/男友想做爱时我总是能配合'
  },
  {
    id: '1002',
    title: '当老公/男友想做爱时我有时不能配合'
  },
  {
    id: '69',
    title: '心烦'
  },
  {
    id: '70',
    title: '易怒'
  },
  {
    id: '71',
    title: '心神不宁',
  },
  {
    id: '72',
    title: '情绪莫名低落',
  },
  {
    id: '136',
    title: '情绪不稳，容易哭',
  },
  {
    id: '68',
    title: '思虑过度/压力大'
  },
  {
    id: '73',
    title: '失眠'
  },
  {
    id: '74',
    title: '易受惊吓'
  },
  {
    id: '135',
    title: '多梦'
  },
  {
    id: '137',
    title: '精神疲倦'
  },
  {
    id: '75',
    title: '心慌心跳快，感觉心脏搏动强烈，伴心前区不适感'
  },
  {
    id: '76',
    title: '胸闷'
  },
  {
    id: '77',
    title: '胸痛'
  },
  {
    id: '78',
    title: '气短'
  },
  {
    id: '91',
    title: '头晕，视物不清'
  },
  {
    id: '92',
    title: '头晕，头痛'
  },
  {
    id: '124',
    title: '眉棱骨痛，或可及太阳穴'
  },
  {
    id: '125',
    title: '耳内或耳后项侧作痛',
  },
  {
    id: '89',
    title: '咽中若有一核，吞吐不利'
  },
  {
    id: '131',
    title: '常因生气而诱发头、耳、颈项、咽喉、牙、乳房等部位疼痛'
  },
  {
    id: '79',
    title: '两颧发红'
  },
  {
    id: '80',
    title: '面红'
  },
  {
    id: '81',
    title: '脸色不好，没有光泽',
  },
  {
    id: '138',
    title: '有黑眼圈',
  },
  {
    id: '93',
    title: '视物不清'
  },
  {
    id: '94',
    title: '耳鸣'
  },
  {
    id: '95',
    title: '听力下降'
  },
  {
    id: '96',
    title: '目干涩'
  },
  {
    id: '82',
    title: '口干，口渴'
  },
  {
    id: '83',
    title: '咽干'
  },
  {
    id: '84',
    title: '口苦'
  },
  {
    id: '85',
    title: '口内不时辛辣'
  },
  {
    id: '86',
    title: '口燥，但欲漱水，不欲咽'
  },
  {
    id: '87',
    title: '口水多，总是不自主的吐或咽'
  },
  {
    id: '139',
    title: '鼻干'
  },
  {
    id: '140',
    title: '唇干裂'
  },
  {
    id: '115',
    title: '口唇麻木'
  },
  {
    id: '102',
    title: '易发口角炎或溃疡'
  },
  {
    id: '126',
    title: '牙龈萎缩'
  },
  {
    id: '141',
    title: '舌无苔'
  },
  {
    id: '88',
    title: '咳嗽吐沫'
  },
  {
    id: '90',
    title: '痰多'
  },
  {
    id: '142',
    title: '干咳少痰或无痰'
  },
  {
    id: '97',
    title: '身子沉，懒于活动'
  },
  {
    id: '98',
    title: '腰以下重'
  },
  {
    id: '99',
    title: '体倦乏力',
  },
  {
    id: '100',
    title: '劳累后感耳鸣头晕',
  },
  {
    id: '101',
    title: '脱发',
  },
  {
    id: '108',
    title: '容易汗出，有时睡着也会出'
  },
  {
    id: '109',
    title: '不耐热'
  },
  {
    id: '110',
    title: '畏寒喜暖'
  },
  {
    id: '111',
    title: '两足发热'
  },
  {
    id: '112',
    title: '手足冷',
  },
  {
    id: '113',
    title: '两足常冷'
  },
  {
    id: '114',
    title: '下身冷'
  },
  {
    id: '127',
    title: '容易抽筋'
  },
  {
    id: '128',
    title: '感觉全身发热或身体某些部位发热，人不舒服但体温正常'
  },
  {
    id: '130',
    title: '下午或夜间不适症状加重'
  },
  {
    id: '129',
    title: '感觉手或足肿胀，或是眼睑浮肿'
  },
  {
    id: '56',
    title: '饭量不少但人偏瘦'
  },
  {
    id: '57',
    title: '常常已经吃饱了但还是会再吃一点'
  },
  {
    id: '58',
    title: '喜食辛辣之物'
  },
  {
    id: '59',
    title: '经行期间或产后不忌生冷'
  },
  {
    id: '103',
    title: '易发皮疹等过敏性问题'
  },
  {
    id: '104',
    title: '手或足易起疹子或小水泡'
  },
  {
    id: '105',
    title: '皮肤起白屑'
  },
  {
    id: '106',
    title: '皮肤瘙痒，抓之肤见赤痕'
  },
  {
    id: '107',
    title: '皮肤干，发痒'
  },
  {
    id: '116',
    title: '感觉手指或足趾发麻'
  },
  {
    id: '117',
    title: '肢体酸麻'
  },
  {
    id: '118',
    title: '两肋有紧束感',
  },
  {
    id: '119',
    title: '胁肋部胀或疼痛',
  },
  {
    id: '120',
    title: '四肢酸痛'
  },
  {
    id: '121',
    title: '腰酸'
  },
  {
    id: '122',
    title: '腰痛'
  },
  {
    id: '123',
    title: '足跟痛或足底痛'
  },
  {
    id: '65',
    title: '腹满腹胀，吃饭稍多就不舒服'
  },
  {
    id: '66',
    title: '反酸，或从胃里往上反气，带有食物的味道'
  },
  {
    id: '67',
    title: '胃痛时作'
  },
  {
    id: '51',
    title: '尿色深黄'
  },
  {
    id: '52',
    title: '小便混浊'
  },
  {
    id: '53',
    title: '小便频'
  },
  {
    id: '54',
    title: '小便憋不住，咳嗽或打喷嚏时会自己流出'
  },
  {
    id: '55',
    title: '欲小便而排出得不顺畅，或排完后感觉似乎还有'
  },
  {
    id: '60',
    title: '便秘'
  },
  {
    id: '61',
    title: '排便不畅'
  },
  {
    id: '62',
    title: '大便时秘时溏泄'
  },
  {
    id: '63',
    title: '易腹泻'
  },
  {
    id: '64',
    title: '大便不成形'
  },
  { id: '143',
    title: '脉虚'
  },
  {
    id: '144',
    title: '脉弦'
  },
  {
    id: '145',
    title: '脉细'
  }

];
export const PREPRE_DATA = [{ label: '大失血病史（大于400ml）', id: '9' },{ label: '子宫内膜异位症', id: '10' }, { label: '子宫肌瘤', id: '11' },{ label: '其他', id: '1001' }];
export const SCALE_WZXY = [
  {
    subject: [
        {
          id: 's1',
          title: "您最近是否有下列症状：",
          item: [
              {
                  title: "比较容易生气，但不一定会发作出来",
                  id: "1"
              },
              {
                  title: "很容易被吓到",
                  id: "2"
              },
              {
                  title: "看到电视中感人的情节比别人容易掉眼泪",
                  id: "3"
              },
              {
                  title: "感觉焦虑，老担心有不好的事发生",
                  id: "4"
              },
              {
                  title: "感觉人很烦躁",
                  id: "5"
              }
          ]
      },
      {
          id: 's2',
          title: "您最近是否有下列症状：",
          item: [
              {
                  title: "常常会感到脸或是耳朵会发热",
                  id: "6"
              },
              {
                  title: "比别人容易出汗",
                  id: "7"
              },
              {
                  title: "怕风",
                  id: "8"
              }
          ]
      },
        {
          id: 's3',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "困、喜睡",
              id: "9"
            },
            {
              title: "入睡困难、睡后易醒或夜梦纷纭",
              id: "10"
            }
          ]
        },
        {
          id: 's4',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "常感鼻塞或嗅觉不灵敏",
              id: "11"
            },
            {
              title: "咳嗽",
              id: "12"
            },
            {
              title: "嗓子不舒服，发干",
              id: "13"
            },
            {
              title: "感觉咽部有东西似的，不舒服，有时候甚至想干呕",
              id: "14"
            },
            {
              title: "眼睛怕光",
              id: "15"
            }
          ]
        },
        {
          id: 's5',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "老是长出气",
              id: "16"
            },
            {
              title: "感觉出气不顺",
              id: "17"
            },
            {
              title: "气短",
              id: "18"
            },
            {
              title: "感觉胸闷",
              id: "19"
            },
            {
              title: "会一阵阵心跳加快",
              id: "20"
            }
          ]
        },
        {
          id: 's6',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "容易腹泄",
              id: "21"
            },
            {
              title: "不想吃东西",
              id: "22"
            },
            {
              title: "感到肚子有点胀胀的感觉，不舒服",
              id: "23"
            },
            {
              title: "感觉两肋下面有东西抵着似的，有时候还疼",
              id: "24"
            },
            {
              title: "胃胀、胃痛，长期有胃病，反反复复",
              id: "25"
            }
          ]
        },
        {
          id: 's7',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "小便不利",
              id: "26"
            },
            {
              title: "小腿前面或是脚踝常常水肿",
              id: "27"
            },
            {
              title: "肢体沉重",
              id: "28"
            }
          ]
        },
        {
          id: 's8',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "胁下痛",
              id: "29"
            },
            {
              title: "小腹两侧疼痛",
              id: "30"
            },
            {
              title: "肩胛间或两臂内侧疼痛",
              id: "31"
            },
            {
              title: "脚底痛",
              id: "32"
            },
            {
              title: "腰痛",
              id: "33"
            },
            {
              title: "关节痛",
              id: "34"
            }
          ]
        },
        {
          id: 's9',
          title: "您最近是否有下列症状：",
          item: [
            {
              title: "手指或手臂发麻",
              id: "35"
            },
            {
              title: "有时会不自觉摇动手脚",
              id: "36"
            },
            {
              title: "眼皮经常莫名其妙的跳",
              id: "37"
            },
            {
              title: "感到脖子发紧",
              id: "38"
            },
            {
              title: "月经不正常，要么晚要么早，不能按时而来",
              id: "39"
            }
          ]
        }
    ]
  }
]

export const SCALE_WZXY_MUSIC = [
  {
      "Id": 1,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m00.mp3",
      "Time": "0:27",
      "Title": "曲目·甲",
  }, 
  {
      "Id": 2,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m01.mp3",
      "Time": "0:35",
      "Title": "曲目·乙"
  }, 
  {
      "Id": 3,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m02.mp3",
      "Time": "0:53",
      "Title": "曲目·丙"
  }, 
  {
      "Id": 4,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m03.mp3",
      "Time": "0:40",
      "Title": "曲目·丁"
  }, 
  {
      "Id": 5,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m04.mp3",
      "Time": "0:47",
      "Title": "曲目·戊"
  }, 
  {
      "Id": 6,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m05.mp3",
      "Time": "0:55",
      "Title": "曲目·己"
  }, 
  {
      "Id": 7,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m06.mp3",
      "Time": "1:08",
      "Title": "曲目·庚"
  }, 
  {
      "Id": 8,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m07.mp3",
      "Time": "1:10",
      "Title": "曲目·辛"
  }, 
  {
      "Id": 9,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m08.mp3",
      "Time": "0:46",
      "Title": "曲目·壬"
  }, 
  {
      "Id": 10,
      "Choicekey": "喜欢|一般|不喜欢",
      "Choicevalue": "1|2|3",
      "Address": "http://gyhl.oss-cn-shanghai.aliyuncs.com/music/m09.mp3",
      "Time": "1:13",
      "Title": "曲目·癸"
  }
]