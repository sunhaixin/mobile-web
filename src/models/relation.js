import * as services from '../services/relation' ;
import * as account from '../services/account' ;
import { routerRedux } from 'dva/router';
import { Toast } from 'antd-mobile';
import { moduleshandle, fetchErrorMsg } from "routes/SettingAge";

export default {
    namespace: 'relation',

    state: {
      text:{},
      modules: "",
      drawertype: "",
      drawerlist: [],
      openDrawer: false,
      isDisabled: false
    },

    subscriptions: {
      setup({ dispatch, history }) { 
          return history.listen(({ pathname, ...rest}) => {
              if(pathname=="/IndexPage"){
                  dispatch({type:"postModules", payload:{ hospitalId: localStorage.getItem("hid") }});
              }
          });
      },
    },

    effects: {
      * create({ payload: { idNumber, name, gender, birthday, type, hid } }, { call, put }) {
        localStorage.removeItem("Datas");
        localStorage.setItem("Datas", JSON.stringify({ idNumber: idNumber, type: undefined===type? 0 : type }));
        let nextCreate = yield call(services.queryCreate, idNumber, name, gender, birthday, type, hid );
        if(undefined === fetchErrorMsg(nextCreate)){
          return fetchErrorMsg(nextCreate)
        }else{
          if(nextCreate.code == 200){
            yield put({ type: "save", payload: { text: nextCreate.data}});
            sessionStorage.removeItem("state");
            sessionStorage.removeItem("token");
            sessionStorage.setItem("state",JSON.stringify(nextCreate.data));
            sessionStorage.setItem("token",JSON.stringify(nextCreate.data.token));		
            yield put(routerRedux.push("/IndexPage"))	
          }else{
            Toast.fail(nextCreate.message,1);
          }
        }
      },

      * findbyid({ payload: { idNumber, type, hid }},{ call, put }){
        sessionStorage.removeItem("state");
        sessionStorage.removeItem("token");
        localStorage.removeItem("Datas");
        localStorage.setItem("Datas", JSON.stringify({ idNumber: idNumber, type: undefined===type? 0 : parseInt(type) }));
        const data = yield call(account.queryAuthentication, idNumber, type, hid);
        if(undefined === fetchErrorMsg(data)){
          return fetchErrorMsg(data)
        }else{
          if(!data.data) {
            yield put(routerRedux.push( '/regitser'));
          }else{
            sessionStorage.setItem("state",JSON.stringify(data.data));
            sessionStorage.setItem("token",JSON.stringify(data.data.token));
            const personal = JSON.parse(sessionStorage.getItem('state'));
            if (personal.idNumber || personal.idNumber.split("@")[1] == idNumber) {
              yield put(routerRedux.push("/IndexPage"))
            }
          }
        }
      },
      
      * updatePatient({ payload: data }, { call, put, select }) {
        let reset = yield call(account.updatePatient, data);
        if(undefined === fetchErrorMsg(reset)){
          return fetchErrorMsg(reset)
        }else{
          if(reset.code == 200){
            Toast.success('修改成功', 1);  
            sessionStorage.removeItem("state");
            sessionStorage.setItem("state",JSON.stringify(reset.data));
            const modules = yield call(services.queryGetModules, localStorage.getItem("hid"));
            if(undefined === fetchErrorMsg(modules)){
              return fetchErrorMsg(modules)
            }else{
              if(modules.code == 200){
                const personal = JSON.parse(sessionStorage.getItem('state'));
                let arr = modules.data.split(";");
                let list = moduleshandle(personal,arr);
                yield put(routerRedux.push("/IndexPage"))
                yield put ({type: "save", payload: { drawerlist: list }});
                yield put ({type: "scale/save", payload: { scalevalue: {}, step: 1, steps: 0 }});
              }
            }
            }else{
              Toast.fail(reset.message,1);
            }
        }
      },

      * postModules({ payload: { hospitalId } }, { call, put }){
        const modules = yield call(services.queryGetModules, hospitalId);
        if(undefined === fetchErrorMsg(modules)){
          return fetchErrorMsg(modules)
        }else{
          if(modules.code == 200){
            const personal = JSON.parse(sessionStorage.getItem('state'));
            let arr = modules.data.split(";");
            let list = moduleshandle(personal,arr);
            yield put ({type: "save", payload: { drawerlist: list }});
          }
        }
      },
    },

    reducers: {
      save(state, action) {
        return { ...state, ...action.payload };
      },
    },

};
