import * as services from '../services/report';
import { Toast } from 'antd-mobile';
import { fetchErrorMsg } from "routes/SettingAge";

export default {

  namespace: 'report',

  state: {
    report: {},
    programs: {},
    comment: "",
    PatientValue: "",
    reportList: [],
    title: ""
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
        return history.listen(({ pathname, ...rest}) => {
            
            if(pathname=="/report"){
                dispatch({type:"fetchReportList"});
            }
            if (/report\/\w+/.test(pathname) ) {
                window.document.title = "体检报告";
                let id  = rest.search.split('?')[1].split('&')[0];
                let eva = rest.search.split('?')[1].split('&')[2].split('=')[1];
                const payload = { uid: id };
                switch (parseInt(eva, 10)) {
                    case 1:
                      payload.type = 'old';
                      break;
                    case 2:
                      payload.type = 'nine';
                      break;
                    case 3:
                      payload.type = 'phy';
                      break;
                    case 4:
                      payload.type = 'pre';
                      break;
                    case 5:
                      payload.type = 'hyp';
                      break;
                    case 6:
                      payload.type = 'chd';
                      break;
                    case 7:
                      payload.type = 'alpha';
                      break;
                    case 8:
                      payload.type = 'pulse';
                      break;
                    case 9:
                      payload.type = 'evaTongue';
                      break;
                    case 10:
                      payload.type = 'face';
                      break;
                    case 11:
                      payload.type = 'dia';
                      break;
                    case 12:
                      payload.type = 'tcm';
                      break;
                    case 13:
                      payload.type = 'partum';
                      break;
                    case 14:
                      payload.type = 'prepre';
                      break;
                    case 16:
                      payload.type = 'normal';
                      break; 
                    case 17:
                      payload.type = 'phy2';
                      break; 
                    case 19:
                      payload.type = 'chd2';
                      break; 
                    case 20:
                      payload.type = 'wzxy';
                      break; 
                    default:
                      // ...
                }
                // dispatch({ type: 'fetchReportDetail', payload});
            }
        });
    },
  },

  effects: {
    
     * fetchReportList({ payload },{ call, put }) {
        const data = yield call(services.queryReportList);
        if(undefined === fetchErrorMsg(data)){
          return fetchErrorMsg(data)
        }else{
          if(data.code == 200){
            yield put({ type: 'save', payload: { reportList: data.data || [] } });
          }else{
            Toast.fail(data.message, 1)
          }
        }
      }, 
     * fetchReportDetail({ payload:{ type, uid }}, { call, put }){
        let detail;
        if (type == "evaTongue" || type == "pulse" || type == "face" || type == "alpha") {
          detail = yield call(services.ReportDetail, type, uid);
          if(detail.code == 200){
            yield put({type: 'save',payload: {report: detail.data}});
          }else{
            Toast.fail(detail.message, 1);
          }
          
       }else if(type =="prepre" || type == "normal"){
        detail = yield call(services.ReportDetail, type, uid);
        yield put({ 
          type: 'save', 
          payload: {
            programs:{
              Programe: detail.data.result.programe,
              type: type,
              recType: detail.data.result.recType
            },
            report:{
               report1: detail.data.result.report1,
               report2: detail.data.result.report2,
               report3: detail.data.result.report3,
               title: detail.data.title,
               vp: detail.data.vp,
            },
            comment: detail.data.comment,
            PatientValue:{
               patientValue: detail.data.patientValue,
            },
         }});
       }else{
          detail = yield call(services.ReportDetail, type, uid);
          if(detail.code == 200){
             yield put({ 
              type: 'save', 
              payload: { 
                programs:{
                  programeInt:detail.data.programeInt, 
                  programeRec:detail.data.programeRec, 
                  programeIntHtml:detail.data.programeIntHtml, 
                  programeRecHtml:detail.data.programeRecHtml, 
                  Programe:detail.data.programe,
                  IntDetail: detail.data.intDetail,
                  type: type, recType: detail.data.recType,
                  RecDetail:detail.data.recDetail,
                }, 
                comment:detail.data.comment, 

                PatientValue:{
                  allTP: detail.data.allTP,
                  patientValue: detail.data.patientValue,
                  patientValue1:detail.data.patientValue1, 
                  patientValue2:detail.data.patientValue2,
                  patientValue3:detail.data.patientValue3,
                },
                report:{
                  vp: detail.data.vp,
                  report1:detail.data.report1,
                  report2:detail.data.report2,
                  report3:detail.data.report3,
                  report4:detail.data.report4,
                  report5:detail.data.report5,
                  isTrust: detail.data.isTrust,
                  hint: detail.data.hint,
                  conclusion: detail.data.conclusion,
                  explanation: detail.data.explanation,
                  performance: detail.data.performance,
                  scores: detail.data.scores
                },

            }});
          }else{
             Toast.fail(detail.message, 1);
          }

      }
      },

     * reset({ payload }, { call, put }) {
       yield put({ type: 'save', payload: { step: 1, scalevalue: {} } });
     },
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },

};
