import * as services from '../services/scale' ;
import { Toast } from 'antd-mobile'; 
import { sheetType } from "const";
import { routerRedux } from 'dva/router';
import { fetchErrorMsg } from "routes/SettingAge";

export default {
 
  namespace: 'scale',

  state: {
    recordList: [],
    record: [],
    scalevalue: {},
    detail: {},
    localtype:["5"],
    open: false,
    step: 1,
    steps: 0,
    isDisabled: false,  
    btn: "获取验证码",
    wait: 120, 
    isDisabled_: false,
    clock: '',
    backGround: "#fff",
    fontSizecolor: "#aa4929",
    IndexPageSize: 'black',
    recordSize: 'black',
    reportSize: 'black',
    IndexPageBottom: '2px solid red',
    recordBottom: '2px solid red',
    reportBottom: '2px solid red'
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      return history.listen(({ pathname, ...rest}) => {
        if(pathname == "/"){
           let hid_ = parseInt(window.location.search.split("&")[0].split("?")[1]);
            if(hid_ != localStorage.getItem("hid")){
              localStorage.removeItem("Datas");
            }
            dispatch({ type: 'reset'});
            dispatch({ type: 'getW3CardType', payload: { hid: hid_ }})
        }
        if(pathname=="/scale_record"){
            dispatch({type:"fetchScaleList"});
        }
        if(pathname == "/IndexPage" || /scale_record\/\w+/.test(pathname) || /report\/\w+/.test(pathname)){
          dispatch({ type: 'reset'});
        }
        if(pathname == "/IndexPage/old"|| "/IndexPage/nine"){
            dispatch({type: "save", payload:{ record: [] }});
        }
        if (/scale_record\/\w+/.test(pathname) ) {
          let eva = rest.search.split("&")[1].split("=")[1];
          let uid = rest.search.split("&")[0].split("?")[1];
          dispatch({ type: 'reset'});
          dispatch({ 
            type: 'fetchScaleDetail', 
            payload:{
              type: sheetType[eva],
              uid:  uid, 
          }
        });
        }
      })
    }  
  },

  effects: {
     * submitQuestions({ payload: {birth, isSick, disease, diseaseTime, type, patientId, hospitalId, patientValue, jw, sc, hy, height, weight, isReturn, isMiscarriage, isAbortion, } }, { call, put }){
        let report;
        if(type === 'partum'){
          report = yield call(services.submitScaleByUser,'','','','',type, patientId, hospitalId, patientValue, jw, sc);
        }else if(type === 'chd2'){
          report = yield call(services.submitScaleByUser, birth, isSick, disease, diseaseTime, type, patientId, hospitalId, patientValue,);
        }else if(type === 'prepre' || type === 'normal'){
          report = yield call(services.submitScaleByUser,'','','','',type, patientId, hospitalId, patientValue, jw, sc, hy, height, weight, isReturn, isMiscarriage, isAbortion);
        }else{ 
          report = yield call(services.submitScaleByUser,'','','','',type, patientId, hospitalId, patientValue);
        }
        if(report.code == 200){
          Toast.success('提交成功',1);
          yield put(routerRedux.replace({ 
            pathname: '/scale_record/submitdetail', 
            search:`${report.data.uid}&eva=${sheetType[type]}`
          }));
          window.scrollTo(0, 0);
        }else{
          Toast.fail(report.message,1);
        }
     },

     * fetchScaleList({ payload },{ call, put }) {
        const data = yield call(services.queryScaleList);
        if(undefined === fetchErrorMsg(data)){
          return fetchErrorMsg(data)
        }else{
          if(data.code == 200){
            data.data? data.data.sort((a,b) => {
                return a.createTime > b.createTime ? -1 : 1;
            }):""
            yield put({ type: 'save', payload: { recordList: data.data || [] } });
          }else{
            Toast.fail(data.message, 1)
          }
        }
      },

     * fetchScaleDetail({ payload:{ type, uid } }, { call, put }){
        const detail = yield call(services.queryScaleDetail,type, uid);
        if(detail.code == 200){
          if(detail.data){
            yield put({ type: 'save', payload: { record: detail.data || [] } });
          }else{
            Toast.fail(detail.message, 1)
          }
       }else{
         Toast.fail(detail.message, 1)
       }
     },

     * submitHypQuestions({ payload: { type, patientValue1, patientValue2, patientValue3, ...rest }}, { call, put }){
        const report = yield call(services.submitHypQuestions, type,rest.hospitalId, rest.patientId, patientValue1, patientValue2, patientValue3);;
        if(report.code == 200){
          Toast.success('提交成功',1);
          yield put(routerRedux.replace({ 
            pathname: '/scale_record/submitdetail', 
            search:`${report.data.uid}&eva=${sheetType[type]}`
          }));
          window.scrollTo(0, 0);
        }else{
          Toast.fail(report.message,1);
        }
     },

     * getW3CardType({ payload: { hid }}, { call, put }){
        const data = yield call(services.getW3CardType, hid);
        if(undefined === fetchErrorMsg(data)){
          return fetchErrorMsg(data)
        }else{
          if(data.code == 200){
            sessionStorage.removeItem("w3CardType");
            sessionStorage.setItem("w3CardType",JSON.stringify(data.data));
            let Type_ = "";
            if(data.data){
              if(data.data.w3CardType !== null){
                Type_ = [""+data.data.w3CardType+""]
              }else{
                Type_ = ["5"];
              }
            }
            yield put({ type: 'save', payload: { W3T: data.data, localtype: Type_} });
          }else{
            if(data.code == 104){
              Toast.fail("不存在该医院", 1)
            }else if(data.code == 302){
              Toast.fail("未获取到医院编号", 1)
            }else{
              Toast.fail(data.message, 1)
            }
          }
        }
     },
     
     * reset({ payload },{ call, put }){
        yield put({ type: 'save',  payload: { scalevalue: {}, step: 1, steps: 0, codecode: ""} });
     },
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
  },

};
