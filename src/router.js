import React from 'react';
import { Router, Route, Switch} from 'dva/router';
import { UserAgree } from 'components';
import { 
  IndexPage, Moblie,
  UpdateMyDetail, 
  ScaleModules,
  ScaleNineTemplate,
  ScaleOldTemplate, 
  ScaleNineOld,

  ScaleChddetail,
  ScaleChdTemplate, 
  ScaleChd2detail,
  ScaleChd2Template,

  ScalePreSheet,
  ScalePreTemplate,

  ScalehypTemplate, 
  ScaleHypSheet, 
  ScaleHypDetail,

  ScalePhySheet,
  ScalePhyTemplate, 
  ScalePhy2Template,

  ScaleTcmSheet,
  ScaleTcmTemplate, 

  ScaleDiaSheet,
  ScaleDiaTemplate, 
  
  ScalePartumSheet,
  ScalePartumTemplate, 

  ScalePrepreNormal, 
  ScalePrepreNormalSheet,

  ScaleWzxyTemplate,

  ScaleRecordListPage,
  ScaleRecordDetailPage,

  ReportListPage,
  ReportDetailPage
} from 'routes';

export default function Router_Config({ history }) {
  return (
    <Router history={history}>
      <Switch>
            <Route path="/" exact component={IndexPage} />
            <Route path="/useragree" exact component={UserAgree} />
            <Route path="/regitser" exact component={Moblie} />
            <Route path="/IndexPage" exact component={ScaleModules} />
            <Route path="/IndexPage/detail" exact component={UpdateMyDetail} />
            <Route path="/IndexPage/old" exact component={ScaleOldTemplate} />
            <Route path="/IndexPage/nine" exact component={ScaleNineTemplate} />
            <Route path="/IndexPage/chd" exact component={ScaleChdTemplate} />
            <Route path="/IndexPage/chd2" exact component={ScaleChd2Template} />
            <Route path="/IndexPage/hyp" exact component={ScalehypTemplate} />
            <Route path="/IndexPage/hyp/hypsheet" exact component={ScaleHypSheet} />
            <Route path="/IndexPage/pre" exact component={ScalePreTemplate} />
            <Route path="/IndexPage/phy" exact component={ScalePhyTemplate} />
            <Route path="/IndexPage/phy2" exact component={ScalePhy2Template} />
            <Route path="/IndexPage/tcm" exact component={ScaleTcmTemplate} />
            <Route path="/IndexPage/dia" exact component={ScaleDiaTemplate} />
            <Route path="/IndexPage/partum" exact component={ScalePartumTemplate} />
            <Route path="/IndexPage/prepre" exact component={ScalePrepreNormal} />
            <Route path="/IndexPage/normal" exact component={ScalePrepreNormal} />
            <Route path="/IndexPage/wzxy" exact component={ScaleWzxyTemplate} />
            <Route path="/scale_record" exact component={ScaleRecordListPage} />
            <Route path="/scale_record/qrcode" exact component={ScaleRecordDetailPage} />
            <Route path="/scale_record/submitdetail" exact component={ScaleRecordDetailPage} />
            <Route path="/scale_record/qrcode/sheet" exact component={ScaleNineOld} />
            <Route path="/scale_record/qrcode/sheet/dia" exact component={ScaleDiaSheet} />
            <Route path="/scale_record/qrcode/sheet/tcm" exact component={ScaleTcmSheet} />
            <Route path="/scale_record/qrcode/sheet/prepre" exact component={ScalePrepreNormalSheet} />
            <Route path="/scale_record/qrcode/sheet/normal" exact component={ScalePrepreNormalSheet} />
            <Route path="/scale_record/qrcode/sheet/pre" exact component={ScalePreSheet} />
            <Route path="/scale_record/qrcode/sheet/phy" exact component={ScalePhySheet} />
            <Route path="/scale_record/qrcode/sheet/phy2" exact component={ScalePhySheet} />
            <Route path="/scale_record/qrcode/sheet/partum" exact component={ScalePartumSheet} />
            <Route path="/scale_record/qrcode/sheet/hyp" exact component={ScaleHypDetail} />
            <Route path="/scale_record/qrcode/sheet/chd" exact component={ScaleChddetail} />
            <Route path="/scale_record/qrcode/sheet/chd2" exact component={ScaleChd2detail} />
            <Route path="/resultqrcode" exact component={ScaleRecordDetailPage} />
            <Route path="/report" exact component={ReportListPage} />
            <Route path="/report/detail" exact component={ReportDetailPage} />
      </Switch>
    </Router>
  );
}