import React from 'react';
import { connect } from 'dva';
import { Grid, WhiteSpace } from 'antd-mobile';
import { routerRedux } from 'dva/router';
import MemberInfo from "components/MemberInfo";


class ScaleModules extends React.Component {
    constructor(props) {
        super(props);
        window.scrollTo(0, 0);
        window.document.title = '选择测评';
    }

    goto(sheet) {
        this.props.dispatch(
            routerRedux.replace({
              pathname: `IndexPage/${sheet.type}`,
            }),
          );
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <MemberInfo pathname={this.props.location.pathname}/>
                <WhiteSpace/>
                {this.props.drawerlist &&  this.props.drawerlist.length !=0? 
                <div style={{position: 'relative', top: 85}}>
                <Grid data={this.props.drawerlist} columnNum={2} 
                onClick={(sheet, i) => this.goto(sheet)} /></div>: "" }
            </div>
        );
    }
}

export default connect(({ relation }) => ({...relation }))(ScaleModules);