import { connect } from 'dva';
import { createForm } from 'rc-form';
import React from 'react';
import { DateTimes } from 'const';
import { WingBlank, WhiteSpace, Flex, List, InputItem, DatePicker, Picker, Button, Toast } from 'antd-mobile';

const nowTimeStamp = Date.now();
const now = new Date(nowTimeStamp);
const minDate = new Date(1900, 1, 1);
const maxDate = now;
const genderList = [
    { label: '男', value: 1 },
    { label: '女', value: 2 },
  ];

const UpdateMyDetail = ({ dispatch, form }) => {
  const { getFieldProps, validateFields, getFieldError, getFieldValue } = form;
  let personal =  JSON.parse(sessionStorage.getItem("state"));
  let INames, ImaxLength, Imessage, IdNums;
  window.document.title = "个人信息";
  let localid = JSON.parse(localStorage.getItem("Datas"));
  if(personal.idNumber){
    if(personal.idNumber.indexOf('#c') != -1){
      INames="就诊卡号";
      Imessage = "请填写就诊号";
      ImaxLength = 30;
      IdNums = personal.idNumber.split("@")[1];
    }else if(personal.idNumber.indexOf('#b') != -1){
      INames="社保卡号";
      Imessage = "请填写社保号";
      ImaxLength = 30;
      IdNums = personal.idNumber.split("@")[1]
    }else if(personal.idNumber.indexOf('#a') != -1){
      INames="体检卡号";
      Imessage = "请填写体检号";
      ImaxLength = 30;
      IdNums = personal.idNumber.split("@")[1];
    }else if(localid && localid.type == 5){
      INames = "手机卡号";
      Imessage = "请填写手机号";
      ImaxLength = 11;
      IdNums = personal.idNumber
    }else{
      INames = "身份证号";
      Imessage = "请填写身份证号";
      ImaxLength = 18;
      IdNums = personal.idNumber
    }
  }

  const updatePatient = () =>
    validateFields((errors, value) => {
      if (errors) {
        const fieldNames = Object.keys(errors);
        if (fieldNames[0]) {
          return Toast.fail(getFieldError(fieldNames[0]));
        }
      }else{
          dispatch({
            type: "relation/updatePatient",
            payload: {
              name: getFieldValue('name'),
              gender: getFieldValue('gender'),
              birthday: DateTimes(value.birthday).split(" ")[0],
              hid: localStorage.getItem("hid")
            },
          });
      }
  })
  return (
    <WingBlank size="md">
      <WhiteSpace />
      <List>
          <InputItem
          {...getFieldProps('idNumber', {
            initialValue: IdNums,
            validate: [{
              trigger: false,
              rules: [{ required: true, message: {Imessage} }],
            }],
          })}
          disabled={true}
          type="string"
          maxLength={ImaxLength}
        ><div style={{color:'black'}}>{INames}</div>
        </InputItem>
        <InputItem
              {...getFieldProps('name', {
                initialValue: personal.name ? personal.name: "",
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请输入姓名' }],
                }],
              })}
              placeholder="请输入姓名"
        >姓名</InputItem>
         <Picker
              cols={1}
              data={genderList}
              {...getFieldProps('gender', {
                initialValue: personal.gender? [personal.gender]: "",
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请选择性别' }],
                }],
              })}
            >
              <List.Item arrow="horizontal">性别</List.Item>
        </Picker>
        <DatePicker
              {...getFieldProps('birthday', {
                initialValue: personal.birthday? new Date(personal.birthday.slice(0,11).replace(/-/g, "/")): "",
                validate: [{
                  trigger: false,
                  rules: [{ required: true, message: '请选择出生年月' }],
                }],
              })}
              mode="date"
              title="请选择"
              minDate={minDate}
              maxDate={maxDate}
            >
              <List.Item arrow="horizontal">出生年月</List.Item>
        </DatePicker>
        <WhiteSpace size="xl" />
        <WingBlank>
          <Flex>
            <Flex.Item>
               <Button type="primary" onClick={updatePatient}>保存</Button>
            </Flex.Item>
          </Flex>
        </WingBlank>
        <WhiteSpace size="xl" />
      </List>
    </WingBlank>
  );
};

export default connect(({ relation }) => ({ ...relation }))(createForm()(UpdateMyDetail));
