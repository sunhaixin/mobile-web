import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import { WingBlank, WhiteSpace } from 'antd-mobile';
import { Clickdlogin } from 'components';

const IndexPage = ({location}) => {
  // 取hid得值为hid等于后面的整数
  let hid_url = "";
  if(window.location.search){
    hid_url = parseInt(window.location.search.split("&")[0].split("?")[1]);
  }else if(location.search){
    hid_url = location.search.split("?")[1].split("=")[1];
  }
  localStorage.removeItem("hid");
  localStorage.setItem("hid", hid_url);
  return (
       <div style={{ position: 'absolute', top: '20%', width: '100%'}}>
            <WingBlank><div style={{ fontSize: '0.22rem'}}>欢迎使用智能云中医</div></WingBlank>
            <WhiteSpace size="xl" />
            <WhiteSpace size="xl" />
          <Clickdlogin/>
       </div>
  );
};

export default connect(({ relation }) => ({ ...relation }))(createForm()(IndexPage));