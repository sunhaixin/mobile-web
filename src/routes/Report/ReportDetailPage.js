import React from 'react';
import { connect } from 'dva';
import { WhiteSpace, Flex, WingBlank } from 'antd-mobile';
import {
    ScaleReport,
    ChdScaleReport,
    Chd2ScaleReport,
    ScaleProgrameSelect,
  	HypScaleReport,
  	XinScaleReport,
  	TcmScaleReport,
  	PreScaleReport,
  	PartumScaleReport,
  	DiaScaleReport,
    PrePreScaleReport,
    WzxyScaleReport,
  	ScaleComment,

  	AlphaReport,
  	FaceReport,
  	PulseReport,
  	TongueReport,
} from "components/scale";
import MemberInfo from "components/MemberInfo";

function ReportDetailPage({ location, report, programs, PatientValue, comment }) {
    let eva = location.search.split('?')[1].split('&')[2].split('=')[1];

    let hypRP, Prompt = "", Prompts = "";
    if(report){
        report.PatientValue = PatientValue;
        hypRP = report.report1;
    }else{
		hypRP = "";
    }
    if(report.vp == 0 && eva != 7) {
		Prompt = "请到所属医院获取详细报告及调养方案";
	}else if(report.vp == 1) {
		Prompt = "请到所属医院获取调养方案";
	}else if(report.vp == 0 && eva == 7){
		Prompt = "请到所属医院获取详细报告";
	}
    if(report &&!report.isTrust){
		Prompts = "您好,程序显示您的测试结果可信度不足,请您重新如实填写问卷。以下为您五态人格测验的测试结果，请您酌情参考。";
    }
    const hypreport = {PatientValue, report1: hypRP};
    const BlackSpace = () =>{
        return(
            <div>
                <WhiteSpace size="lg"/>
                <WhiteSpace size="lg"/>
                <WhiteSpace size="lg"/>
                <WhiteSpace size="lg"/>
                <WhiteSpace size="lg"/>
                <WhiteSpace size="lg"/>
            </div>
        )
    }
    if(eva == 1){
        return(
            <div>
                 <MemberInfo pathname={location.pathname}/>
                 <WhiteSpace/>
                 {report.vp == 3 ?
                 <div>
                     {BlackSpace()}
                    <ScaleReport {...report} title="老年人体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div>  
                     {BlackSpace()}
                    <ScaleReport {...report} title="老年人体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                     {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <ScaleReport {...report} title="老年人体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                     {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""
                 }
            </div>
        )
    }else if(eva == 2){
        return(
            <div>
                 <MemberInfo pathname={location.pathname}/>
                 <WhiteSpace/>
                 {report.vp == 3 ?
                 <div>
                    {BlackSpace()}
                    <ScaleReport {...report} title="中医体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <ScaleReport {...report} title="中医体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <ScaleReport {...report} title="中医体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
            </div>
        )
    }else if(eva == 3){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 3 ?
                 <div>
                    {BlackSpace()}
                    <XinScaleReport {...report} title="五态性格问卷测评(简)报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <XinScaleReport {...report} title="五态性格问卷测评(简)报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <XinScaleReport {...report} title="五态性格问卷测评(简)报告"/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 4){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 2?
                <div>  
                     {BlackSpace()}
                    <PreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                     {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <PreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                     {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 5){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 3 ?
                 <div>
                    {BlackSpace()}
                    <HypScaleReport {...hypreport} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <HypScaleReport {...hypreport} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <HypScaleReport {...hypreport} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 6){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <ChdScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <ChdScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 7){
        return (
			<div>
                {BlackSpace()}
			    <MemberInfo pathname={location.pathname}/>
			    <WhiteSpace />
				<AlphaReport {...report} />
			</div>
		)
    }else if(eva == 8){
        return (
			<div>
                {BlackSpace()}
				<MemberInfo pathname={location.pathname}/>
				<WhiteSpace />
				<PulseReport {...report} />
		 </div>
        )
    }else if(eva == 9){
        return (
			<div>
                {BlackSpace()}
			    <MemberInfo pathname={location.pathname}/>
			    <WhiteSpace />
			    <TongueReport {...report} />
			</div>
		)
    }else if(eva == 10){
        return (
			<div>
               {BlackSpace()}
			   <MemberInfo pathname={location.pathname}/>
			   <WhiteSpace />
               <FaceReport {...report} />
			</div>
		)
    }else if(eva == 11){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 3 ?
                 <div >
                    {BlackSpace()}
                    <DiaScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div >  
                    {BlackSpace()}
                    <DiaScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <DiaScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 12){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 3 ?
                 <div>
                    {BlackSpace()}
                    <TcmScaleReport {...report} title="四诊合参体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                 </div>
                 :report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <TcmScaleReport {...report} title="四诊合参体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs} vp={report.vp}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <TcmScaleReport {...report} title="四诊合参体质辨识报告"/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 13){
        return (
			<div>
			    <MemberInfo pathname={location.pathname}/>
			    <WhiteSpace />
                {report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <PartumScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <PartumScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
			</div>
		)
    }else if(eva == 14){
        return (
			<div>
                <MemberInfo pathname={location.pathname}/>
			    <WhiteSpace />
                {report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <PrePreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <PrePreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
			</div>
		)
    }else if(eva == 16){
        return (
            <div>
                <MemberInfo pathname={location.pathname}/>
                <WhiteSpace />
                {report.vp == 2?
                <div >  
                    {BlackSpace()}
                    <PrePreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <PrePreScaleReport {...report}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
            </div>
        )
    }else if(eva == 17){
        return (
            <div>
                <MemberInfo pathname={location.pathname}/>
                <WhiteSpace />
                {report.vp == 2?
                <div>  
                    {BlackSpace()}
                    <WingBlank>
                        <Flex justify="center"><div style={{color: '#aa4929', margin: '0 auto', fontSize: '0.13rem'}}>{Prompts}</div></Flex>
                    </WingBlank>
                    <XinScaleReport {...report} title="五态性格问卷测评报告" />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <WingBlank>
                        <Flex justify="center"><div style={{color: '#aa4929', margin: '0 auto', fontSize: '0.13rem'}}>{Prompts}</div></Flex>
                    </WingBlank>
                    <WhiteSpace />
                    <XinScaleReport {...report} title="五态性格问卷测评报告" />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div>  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 19){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {report.vp == 2?
                <div >  
                    {BlackSpace()}
                    <Chd2ScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleProgrameSelect list={programs}/>
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                :report.vp == 1 ?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <Chd2ScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""}
           </div>
       )
    }else if(eva == 20){
        return (
            <div>
               <MemberInfo pathname={location.pathname}/>
               <WhiteSpace />
               {/* {report.vp == 2? */}
                <div >  
                    {BlackSpace()}
                    <WzxyScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>
                {/* :report.vp == 1 ?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                    <WhiteSpace />
                    <WzxyScaleReport {...report} />
                    <WhiteSpace />
                    <ScaleComment comment={comment} />
                </div>  
                :report.vp == 0?
                <div >  
                    {BlackSpace()}
                    <Flex justify="center" style={{color: '#aa4929',}}>{Prompt}</Flex>
                </div>  
                :""} */}
           </div>
       )
    }
}

export default connect(({ report }) => ({
    ...report,
  }))(ReportDetailPage);
