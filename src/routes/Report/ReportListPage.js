import React, { Component } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import MemberInfo from "components/MemberInfo";
import {  Card, WhiteSpace, WingBlank, Flex, ActivityIndicator } from 'antd-mobile';

// 通过dva 中的 connect进行绑定数据
// Route Component 会有额外的 props 用以获取路由信息。 location, params, children

class ReportListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isLoading: false,
      });
    }, 600);
  }

  componentWillMount() {
    this.forceUpdate();
  }

  render() {
    const { location:{ pathname }, reportList  } = this.props
    window.scrollTo(0, 0); window.document.title = "体检报告";
    reportList? reportList.sort(function(a,b) {
      return Date.parse(b.createTime.replace(/-/g,"/"))-Date.parse(a.createTime.replace(/-/g,"/"));
    }): ''
    return (
     <div>
        <MemberInfo pathname={pathname}/>
        <WingBlank size="md" style={{position: 'relative', top: 85}}>
        <WhiteSpace/>
        {this.state.isLoading ?  
          <Flex justify="center" style={{ margin: "50px 0" }} >
                 <ActivityIndicator size="large" animating />
          </Flex> : 
            reportList && reportList.length == 0? 
            <div>
              <Flex justify="center" style={{ margin: "30px 0" }} >
                <img style={{width: 100, height: 100}} src={require('../../assets/nodata.png')} /> 
              </Flex> 
              <Flex justify="center" style={{ color: '#aa4929' }} >
                暂无报告记录
              </Flex> 
            </div>
            :reportList.map((report, i) =>
              <Link key={report.uid} to={{
            pathname: `${pathname}/detail`, 
            search:`${report.evaUid}&${Date.parse(new Date(report.createTime.replace(/-/g,"/")))}&eva=${report.eva}` 
              }}>
                <Card className="record-card">
                  <Card.Header
                    title={<div style={{ fontSize: '.14rem'}}>
                      {report.eva == 3? "五态性格问卷测评(简)":
                      report.eva == 7 ? "中成药辅助辨证":
                      report.eva == 17? "五态性格问卷测评": 
                      report.eva == 12? "四诊合参体质辨识": 
                      report.eva == 16? "常态女性健康状态测评": 
                      report.title}
                    </div>}
                    extra={report.createTime}
                    />
                  <Card.Footer
                    content={report.result? <div style={{ width: 290,height: 16, overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>{report.result}</div>:  <div style={{ height: 16}}>暂无结论</div>}
                    />
              </Card>
              <WhiteSpace/>
            </Link>
        )}
        </WingBlank>
     </div>
    );
  }
}

export default connect(({ report }) => ({
  ...report,
}))(ReportListPage);