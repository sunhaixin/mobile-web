import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, InputItem, Flex, List, } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_CHD, SCALE_CHD_C } from 'const';
import MemberInfo from "components/MemberInfo";

const parentSickData = [{ label: '哮喘病史', value: 'parentSick_1' }, { label: '过敏性疾病', value: 'parentSick_2' }, { label: '易感冒史', value: 'parentSick_3' }, { label: '无上述病史', value: 'parentSick_0' }];

function ScaleChdTemplate({ dispatch, location, form, scalevalue, record, ...rest }) {
  let question;
  const { getFieldProps } = form;
  let Title = SCALE_CHD[0].title;
  record.patientValue = record.patientValue? record.patientValue.split(";"): "";
  if(record.gender == 1){
     question = SCALE_CHD_C[0];
  }else{
     question = SCALE_CHD[0];
  }

  let Patient = {}, disease= "" , born = "", caesarean = "" , dataSick = "";
  for (var i = record.patientValue.length - 1; i >= 0; i--) {
    Patient[record.patientValue[i].substring(0,record.patientValue[i].indexOf(":"))] = record.patientValue[i].substring(record.patientValue[i].indexOf(":")+1,record.patientValue[i].length)
  }
  // Patient[100]:住院时间, Patient[97]和Patient[96]:出生史, Patient[98]:是否剖腹产, Patient[105]: 体重
  // Patient[104]: 身高 , Patient[99]: 出生体重
  if (Patient[100] == undefined) {
    disease = "无";
  } else if(Patient[100] == 1){
    disease = "有";
    dataSick = "1周以内"; 
  } else if(Patient[100] == 2){
    disease = "有";
    dataSick = "1-2周"; 
  }  else if(Patient[100] == 3){
    disease = "有";
    dataSick = "2周以上"; 
  } 
  if (Patient[96]) {
    born = "足月";
  } else if (Patient[97]) {
    born = "早产";
  } 
  if (Patient[98]) {
    caesarean = "是";
  } else {
    caesarean = "否";
  }
  function isActive(key, value, i) {
    if (Patient[101] && i == 0){
      return true;
    }
     if (Patient[102] && i == 1){
      return true;
    }
    if (Patient[103] && i == 2){
      return true;
    }
    if ( (!Patient[101] && !Patient[102] && !Patient[103]) && i == 3){
      return true;
    }
     
  }
  function isChecked (type, code , i, ii){
    if(type == 1){
      let check = i.split("|");
      if (check.indexOf(Patient[code]) != -1){
        if(check.indexOf(Patient[code]) ==ii){
          return true;
        }
      }
    }
    if(type == 2){
      let check = code.split("|");
      for (var j = check.length - 1; j >= 0; j--) {
        if(Patient[check[j]]){
          if(j == ii){
            return true;
          }
        }
      }
    }
  }
  function isCheck (type, code , i, ii){
    if(type == 1){
      let check = i.split("|");
      if (check.indexOf(Patient[code]) != -1){
        if(check.indexOf(Patient[code]) ==ii){
          return true;
        }
      }
    }
    if(type == 2){
      let check = code.split("|");
      for (var j = check.length - 1; j >= 0; j--) {
        if(Patient[check[j]]){
          if(j == ii){
            return true;
          }
        }
      }
    }
  }
  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
      <WhiteSpace />
        <List>
        <InputItem
            value={born}
            editable={false}
          >
            出生史
          </InputItem>
           <InputItem
            value={caesarean}
            editable={false}
          >
            是否剖腹产
          </InputItem>
           <InputItem
            value={Patient[99]}
            editable={false}
          >
            出生体重(kg)
          </InputItem>
           <InputItem
            value={disease}
            editable={false}
          >
            新生儿疾病
          </InputItem>
          {disease=="有"?(<InputItem
            value ={dataSick}
            editable={false}
          >
            住院时间
          </InputItem>:""):""}
         <List.Item key="parentSick">
            父母方面的疾病
            <List.Item.Brief>
              <Flex wrap="wrap" className={styles.questionOptions}>
                {parentSickData.map((option, i) => {
                  const key = option.value;
                  const value = option.label;
                  return (
                    <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i) })}>
                      <input {...getFieldProps(`${key}`)} name={key} value={value} checked={isActive(key, value, i)}  />
                      {value}
                    </label>
                  );
                })}
              </Flex>
            </List.Item.Brief>
          </List.Item>
         <InputItem
            value={Patient[105]}
            editable={false}
          >
            目前体重(kg)
          </InputItem>
          <InputItem
            value={Patient[104]}
            editable={false}
          >
            目前身高(cm)
          </InputItem>
        </List>
        <WhiteSpace size="md" />
      <List.Item>
        <Flex justify="center">{Title}</Flex>
      </List.Item>
      {question.subject.map((option, i) => (
          <List.Item wrap key={i}>
                {`${i + 1}. ${option.title}`}
            <List.Item.Brief>
                <Flex wrap="wrap" className={styles.questionOptions}>
                  {
                    option.choiceKey?  option.choiceKey.split("|").map((item,ii) => (
                        <label 
                          key={item}
                          className={
                          cx(styles.questionOption, styles.questionOptionNormal, 
                          { [styles.questionOptionActive]: isChecked( option.type, option.code, option.choiceValue, ii) })}>
                          <input/>
                          {item}
                        </label>
                      ))

                    : option.subject.map((item, ii) => (
                    <div key={ii}>
                        {item.title}
                        <List.Item>
                          {
                            item.choiceKey.split("|").map((it,iii) => (
                                <label key={it}  
                                  className={
                                  cx(styles.questionOption, styles.questionOptionNormal, 
                                  { [styles.questionOptionActive]: isCheck( item.type, item.code, item.choiceValue, iii) } )}>
                                  <input/>
                                  {it}
                                </label>
                            ))
                          }
                        </List.Item>
                    </div>
                    ))
                  }
                   </Flex>
                </List.Item.Brief>
          </List.Item>
              ))}
            <WhiteSpace size="lg" />

    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleChdTemplate));
