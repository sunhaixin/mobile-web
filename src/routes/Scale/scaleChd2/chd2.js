import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Picker, InputItem, Flex, List, Button, Toast } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_CHD2, SCALE_CHD2_Item } from 'const';
import { isInArray } from '../../SettingAge';
import MemberInfo from "components/MemberInfo";

function Chd2({ dispatch, location, form, scalevalue, step, detail, isDisabled,  ...rest }) {
  let personal =  JSON.parse(sessionStorage.getItem("state"));
  let patientId = personal.patientId ? personal.patientId: personal.id;
  let birthday = personal.birthday ? personal.birthday.split(" ")[0]: "";
  let stepQuestions = SCALE_CHD2[0].subject; window.document.title = "中医儿童体质辨识";
  
  const { getFieldProps, getFieldValue, setFieldsValue, validateFields, getFieldError } = form;

  if (!Object.keys(scalevalue).length) {
    scalevalue = [[], []];
    dispatch({ type: 'scale/save', payload: { scalevalue } });
  }

  let infos = scalevalue[0] || []; //用户信息
  let scores = scalevalue[1] || []; //症状

  const birthSickData = [{ label: '良好', value: '0' }, { label: '生病', value: '1' }];

  function handleChange({ target: { type, name, value } }) {
    if (Array.from(scores).find(item => item === name)) {
        setFieldsValue({ [name]: value }); //form实际值与状态不符，用赋值保持相符
        scores = Array.from(scores).filter(item => item !== name); //从scores中去除
    }
    if (getFieldValue(name) === value) {
        setFieldsValue({ [name]: null });	
    }else {
        setFieldsValue({ [name]: value });
    }
    
    validateFields((err, values) => {
      if (err) {
        const fieldNames = Object.keys(err);
        if (fieldNames[0]) {
          return Toast.fail(getFieldError(fieldNames[0]));
        }
      } else {
          Object.keys(values).forEach((key) => {
              const birthSick = getFieldValue('birthSick');
              const bingname = getFieldValue('bingname');
              const huanbingyear = getFieldValue('huanbingyear');
              infos = [birthSick,bingname,huanbingyear];
              if (values[key] && isInArray(scores, key) == false) {
                  if(key !== "birthSick" || key !== "bingname" || key !==  "huanbingyear"){
                    scores.push(key);
                  }
                  if(SCALE_CHD2_Item[name]){
                      SCALE_CHD2_Item[name].split(";").forEach((e, i) =>{
                          setFieldsValue({ [e]: null });
                          if (scores.find(one => one === e)) {
                              scores = scores.filter(one => one !== e);
                            }
                      })	
                      Array.from(scores).forEach((item, i) => {
                          if (SCALE_CHD2_Item[item]) {
                              SCALE_CHD2_Item[item].split(';').forEach((item, i) => {
                              setFieldsValue({ [item]: null });
                              if (scores.find(one => one === item)) {
                                  scores.push(item);
                              }
                            });
                          }
                      });
                  }
              }
          });
      } 
    });
    dispatch({ type: 'scale/save', payload: { scalevalue: [infos ,scores] } });
  }
  function isActive(key, value, i) {
    if(Array.from(scores).find(item => item === key)) {
        return true;
    }
  }

  function submitHandler() {
      validateFields((err, value) =>{
          if(err){
              const fieldNames = Object.keys(err);
              if(fieldNames[0]){
                   return Toast.fail(getFieldError(fieldNames[0]));
              }
          }else{
            // console.log(scores)
            if(scores.length == 0){
                Toast.fail('选择的症状信息过少，请重新选择', 1);
              }else{
                const patientValue = scores.join(';');
                let isSick, disease, diseaseTime;
                if(infos[0] == 0){
                  isSick = 0;disease ="";diseaseTime ="";
                }else{
                  isSick = 1;disease = infos[1];diseaseTime = infos[2];
                }
                dispatch(({ type: 'scale/save', payload:{
                  isDisabled: true
                }}))
                setTimeout(() => {
                    dispatch(({ type: 'scale/save', payload:{
                      isDisabled: false
                  }}))
                }, 3000);
                dispatch({
                  type: 'scale/submitQuestions',
                  payload: {
                    birth: birthday,
                    isSick, disease, diseaseTime,
                    type: 'chd2',
                    patientId: patientId,
                    hospitalId: localStorage.getItem("hid"),
                    patientValue,
                    ...rest,
                  },
              });
              }
          }
      })
  }

  return (
   <div>
      <MemberInfo pathname={location.pathname}/>
      <WhiteSpace/>
      <WingBlank size="md" style={{ position: 'relative', top: 85}}>
      <List>
        <List.Item>
            <Flex justify="center">儿童体质辨识</Flex>
            <Flex justify="center" style={{marginTop: '0.1rem', fontSize: 12}}>请根据您的孩子“长期（一年及以上）的身体状况”进行回答。</Flex>
            <Flex justify="center">回答问题前,请仔细阅读问题，对其充分理解后再作答</Flex>
            <Flex justify="center">请不要空题或漏答</Flex>
        </List.Item>
        <Picker
        cols={1}
        data={birthSickData}
        {...getFieldProps('birthSick', {
            initialValue: infos[0] || '',
            validate: [
            {
                trigger: false,
                rules: [{ required: true, message: '请选择儿童现在健康状态' }],
            },
            ],
        })}
        >
        <List.Item arrow="horizontal">儿童现在健康状态</List.Item>
        </Picker>
        { getFieldValue('birthSick') == 1?
        <div>
            <InputItem
        {...getFieldProps('bingname', {
        initialValue: infos[1] || '',
        validate: [{
            trigger: false,
            rules: [{ required: true, message:  "请输入病名" }],
        }],
        })}
        placeholder="请输入病名"
        > 病名
        </InputItem>
        <InputItem
        {...getFieldProps('huanbingyear', {
          initialValue: infos[2] || '',
          validate: [
            {
              trigger: false,
              rules: [{ required: true, message: '请输入患病时间' }],
            },
          ],
          normalize:(v, prev) => {
            if (v && !/^(([1-9]\d*)|0)(\.\d{0,1}?)?$/.test(v)) {
              if (v === '.') {
                return '0.';
              }
              return prev;
            }
            return v;
          },
        })}
        placeholder="请输入患病时间"
        maxLength={3}
        extra="年"
      >
        患病时间
      </InputItem>
        </div>
        :""}
       {stepQuestions.map((question, i) => (
            <List.Item wrap multipleLine key={`s${i + 1}`} >
              {`${i + 1}. ${question.title}`}
              <List.Item.Brief>
                {question.type === '3'
                  ? //包含子题
                  question.subject.map((question, i) => {
                      let Ans;
                      if (question.type === '2') {
                        //子题，多选题
                        Ans = (
                          <Flex wrap="wrap" className={styles.questionOptions} key={`ans_2_${i + 1}`}>
                            {question.choiceKey.split('|').map((option, i) => {
                              const key = question.code.split('|')[i];
                              const value = question.choiceKey.split('|')[i];
                              return (
                                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                    <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)} onChange={handleChange} />
                                    {option}
                                </label>
                              );
                            })}
                          </Flex>
                        );
                      } else {
                        //子题，单选题
                        Ans = (
                          <Flex wrap="wrap" className={styles.questionOptions} key={`ans_1_${i + 1}`}>
                            {question.choiceKey.split('|').map((option, iii) => {
                               const key = question.choiceValue.split('|')[iii];
                               const value = question.choiceKey.split('|')[iii];
                               return (
                                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                    <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)} onChange={handleChange} />
                                    {option}
                                </label>
                              );
                            })}
                          </Flex>
                        );
                      }
                      const Sub = (
                        <div key={`div${i + 1}`}>
                          <Flex direction="row" key={`${i + 1}`}>
                            <Flex.Item key={`${i + 1}`}>
                              {question.title}
                            </Flex.Item>
                          </Flex>
                          <WhiteSpace size="xs" />
                          {Ans}
                          <WhiteSpace size="md" />
                        </div>
                      );

                      return Sub;
                    })
                  : question.type === '2'
                      ? //多选题
                        <Flex wrap="wrap" className={styles.questionOptions} key={`1_${i + 1}`}>
                          {question.choiceKey.split('|').map((option, i) => {
                            const key = question.code.split('|')[i];
                            const value = question.choiceKey.split('|')[i];
                            return (
                            <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)} onChange={handleChange} />
                                {option}
                            </label>
                            );
                          })}
                        </Flex>
                      : //单选题
                        <Flex wrap="wrap" className={styles.questionOptions} key={`2_${i + 1}`}>
                          {question.choiceKey.split('|').map((option, ii) => {
                            const key = question.choiceValue.split('|')[ii];
                            const value = question.choiceKey.split('|')[ii];
                            return (
                              <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                  <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)} onChange={handleChange} />
                                  {option}
                              </label>
                            );
                          })}
                        </Flex>}
              </List.Item.Brief>
            </List.Item>
          ))}
          <WhiteSpace size="xl" />
          <WingBlank>
            <Flex>
            <Flex.Item>
                <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
            </Flex.Item>
            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
      </List>
      </WingBlank>
   </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(Chd2));
