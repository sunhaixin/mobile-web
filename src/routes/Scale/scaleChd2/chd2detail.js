import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, InputItem, Flex, List, } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_CHD2 } from 'const';
import MemberInfo from "components/MemberInfo";

function ScaleChd2Template({ dispatch, location, form, scalevalue, record, ...rest }) {
    let stepQuestions = SCALE_CHD2[0].subject;
    const { getFieldProps } = form;
    let patientValue = record.patientValue? record.patientValue.split(";"): "";

    function isActive(key, value, i) {
        if (Array.from(patientValue).find(item => item === key)) {
            return true;
        }
    }
    return (
    <div>
        <MemberInfo pathname={location.pathname}/>
        <WingBlank size="md" style={{ position: 'relative', top: 85}}>
        <WhiteSpace />
        <List>
        <InputItem value={!record.isSick? "良好": "生病"}>
        健康状态
        </InputItem>
        {!record.isSick? "": 
        <div>
        <InputItem value={!record.disease? "" :record.disease}>
        病名
        </InputItem>
        <InputItem value={!record.diseaseTime? "" :record.diseaseTime+ " " +"年"}>
        患病时间
        </InputItem>
        </div>
       }
        </List>
        <WhiteSpace size="md" />
        {stepQuestions.map((question, i) => (
            <List.Item wrap multipleLine key={`s${i + 1}`} >
              {`${i + 1}. ${question.title}`}
              <List.Item.Brief>
                {question.type === '3'
                  ? //包含子题
                  question.subject.map((question, i) => {
                      let Ans;
                      if (question.type === '2') {
                        //子题，多选题
                        Ans = (
                          <Flex wrap="wrap" className={styles.questionOptions} key={`ans_2_${i + 1}`}>

                            {question.choiceKey.split('|').map((option, i) => {
                              let key, value;
                              if(question.code){
                                key = question.code.split('|')[i];
                              }
                              if(question.choiceKey){
                                value = question.choiceKey.split('|')[i];
                              }
                              return (
                                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                    <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)}  />
                                    {option}
                                </label>
                              );
                            })}
                          </Flex>
                        );
                      } else {
                        //子题，单选题
                        Ans = (
                          <Flex wrap="wrap" className={styles.questionOptions} key={`ans_1_${i + 1}`}>
                            {question.choiceKey.split('|').map((option, i) => {
                              let key, value;
                              if(question.choiceValue){
                                key = question.choiceValue.split('|')[i];
                              }
                              if(question.choiceKey){
                                value = question.choiceKey.split('|')[i];
                              }
                              return (
                                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                    <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)}  />
                                    {option}
                                </label>
                              );
                            })}
                          </Flex>
                        );
                      }
                      const Sub = (
                        <div key={`div${i + 1}`}>
                          <Flex direction="row" key={`${i + 1}`}>
                            <Flex.Item key={`${i + 1}`}>
                              {question.title}
                            </Flex.Item>
                          </Flex>
                          <WhiteSpace size="xs" />
                          {Ans}
                          <WhiteSpace size="md" />
                        </div>
                      );

                      return Sub;
                    })
                  : question.type === '2'
                      ? //多选题
                        <Flex wrap="wrap" className={styles.questionOptions} key={`1_${i + 1}`}>
                          {question.choiceKey.split('|').map((option, i) => {
                            let key, value;
                            if(question.code){
                              key = question.code.split('|')[i];
                            }
                            if(question.choiceKey){
                              value = question.choiceKey.split('|')[i];
                            }
                            return (
                            <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)}  />
                                {option}
                            </label>
                            );
                          })}
                        </Flex>
                      : //单选题
                        <Flex wrap="wrap" className={styles.questionOptions} key={`2_${i + 1}`}>
                          {question.choiceKey.split('|').map((option, i) => {
                            let key, value;
                            if(question.choiceValue){
                              key = question.choiceValue.split('|')[i];
                            }
                            if(question.choiceKey){
                              value = question.choiceKey.split('|')[i];
                            }
                            return (
                            <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
                                <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)}  />
                                {option}
                            </label>
                            );
                          })}
                        </Flex>}
              </List.Item.Brief>
            </List.Item>
          ))}
        <WhiteSpace size="lg" />
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleChd2Template));
