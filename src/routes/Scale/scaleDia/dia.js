import { SCALE_DIA, SCALE_DIA_C, SCALE_DIA_ITEM} from 'const';
import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List, Button, Toast } from 'antd-mobile';
import { routerRedux } from 'dva/router';
import styles from '../styles.less';
import { isInArray } from '../../SettingAge';
import MemberInfo from "components/MemberInfo";

function dia ({ dispatch, location, form, scalevalue, step, detail, isDisabled, ...rest }) {
	  let questions; window.document.title = "糖尿病辅助辨证";
	  let personal =  JSON.parse(sessionStorage.getItem("state"));
	  let patientId = personal.patientId ? personal.patientId: personal.id;
	  const { getFieldProps, getFieldValue, setFieldsValue, validateFields, getFieldError } = form;

	  let maxstep = 13;
	  if(personal.gender == 1){
		questions = SCALE_DIA_C;
	  }else{
		questions = SCALE_DIA;
	  }

	  let nextStep = step + 1;
	  const category = step - 1;
	  let scores = scalevalue[0] || [];
	  let title = '';
	  if (step === 1) {
	    title = '完善用户信息';
	  }

	  let stepQuestions = [];
	  if (category >= 0 && category < maxstep) {
	    stepQuestions = questions[category].subject;
	    title = questions[category].title;
	  }

      const validateForm = () => {
	    validateFields((err, values) => {
	      if (err) {
	        const fieldNames = Object.keys(err);
	        if (fieldNames[0]) {
	          return Toast.fail(getFieldError(fieldNames[0]));
	        }
	      } else {
	        if (nextStep <= maxstep) {
			dispatch(({ type: 'scale/save', payload:{
				step: nextStep
			}}))
	          let pathname = location.pathname;
				dispatch(
					routerRedux.replace({
						pathname,
						state: {
							type: 'dia',
							patientId: patientId,
							step: nextStep,
						},
					}),
				);
	        }else {
	          if(scores.length < 5){
	              Toast.fail('选择的症状信息过少，请重新选择');
	          }else{
		            const patientValue = scores.join(',');
					dispatch(({ type: 'scale/save', payload:{
						isDisabled: true
					}}))
					setTimeout(() => {
						dispatch(({ type: 'scale/save', payload:{
						isDisabled: false
					}}))
					}, 3000);
					dispatch({
					type: 'scale/submitQuestions',
					payload: {
						type: 'dia',
						patientId: patientId,
						hospitalId: localStorage.getItem("hid"),
						patientValue,
						...rest,
					},
				});
	          }
	        }
	      }
	    });
	  };
	  function isActive(key, value, i) {
	    if (Array.from(scores).find(item => item === key)) {
			return true;
		}
	    if (!getFieldValue(key)) {
	      return false;
	    }
    	return getFieldValue(key) === value;
	  }
	  
	  function handleChange({ target: { type, name, value } }) {
		if (Array.from(scores).find(item => item === name)) {
			setFieldsValue({ [name]: value }); //form实际值与状态不符，用赋值保持相符
			scores = Array.from(scores).filter(item => item !== name); //从scores中去除
		}
		if (getFieldValue(name) === value) {
			setFieldsValue({ [name]: null });	
		}else {
			setFieldsValue({ [name]: value });
		}
		validateFields((err, values) => {
			Object.keys(values).forEach((key) => {
				if (values[key] && isInArray(scores, key) == false) {
					scores.push(key);
					if(SCALE_DIA_ITEM[name]){
						SCALE_DIA_ITEM[name].split(";").forEach((e, i) =>{
							setFieldsValue({ [e]: null });
							if (scores.find(one => one === e)) {
								scores = scores.filter(one => one !== e);
							  }
						})	
						Array.from(scores).forEach((item, i) => {
							if (SCALE_DIA_ITEM[item]) {
								SCALE_DIA_ITEM[item].split(';').forEach((item, i) => {
								setFieldsValue({ [item]: null });
								if (scores.find(one => one === item)) {
									scores.push(item);
								}
							  });
							}
						});
					}
				} 
			  });
		})
		dispatch({ type: 'scale/save', payload: { scalevalue: [scores] } });
	  }

	  function nextHandler() {
	    window.scrollTo(0, 0);
	    nextStep = step + 1;
	    validateForm();
	  }

	  function prevHandler() {
	    window.scrollTo(0, 0);
	    nextStep = step - 1;
	    validateForm();
	  }

	  function submitHandler() {
	    validateForm();
	  }

	  return (
    	<div>
		<MemberInfo pathname={location.pathname}/>
		<WhiteSpace/>
		<WingBlank size="md" style={{ position: 'relative', top: 85}}>
      	<List>
			<List.Item>
			<Flex justify="center">{`${step}/${maxstep} ${title}`}</Flex>
			<Flex justify="center" style={{marginTop: '0.1rem'}}>在近3个月内您是否频繁出现以下症状</Flex>
			<Flex justify="center">（有则选, 无可跳过, 也可以多选）</Flex>
			</List.Item>
			{stepQuestions.map((question, i) => (
				<List.Item wrap multipleLine key={question.id}>
				{`${i + 1}. ${question.title}`}
				<List.Item.Brief>
					<Flex wrap="wrap" className={styles.questionOptions}>
					{question.item.map((option, i) => {
						const key = option.id;
						const value = option.title;
						return (
						<label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
							<input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   checked={isActive(key, value, i)} onChange={handleChange} />
							{value}
						</label>
						);
					})}
					</Flex>
				</List.Item.Brief>
								{step==1?(i==0?personal.gender== 1?
					<Flex>身高(cm)-105=标准体重(正常范围为标准体重±10%)</Flex>:
					<Flex>身高(cm)-100=标准体重(正常范围为标准体重±10%)</Flex>
				:""):""}
				</List.Item>
			))}
        	<div>
          	<WhiteSpace size="xl" />
			<WingBlank>
				<Flex>
				{step === 1
					? null
					: <Flex.Item>
					<Button onClick={prevHandler}>上一页</Button>
					</Flex.Item>}

				{step < maxstep
					? <Flex.Item>
					<Button type="primary" onClick={nextHandler}>下一页</Button>
					</Flex.Item>
					: <Flex.Item>
					<Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
					</Flex.Item>}

				</Flex>
			</WingBlank>
			<WhiteSpace size="xl" />
				</div>
			</List>
			<WhiteSpace size="xl" />
			</WingBlank>
		</div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(dia));