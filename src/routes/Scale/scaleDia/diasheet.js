import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_DIA, SCALE_DIA_C } from 'const';
import MemberInfo from "components/MemberInfo";

function ScaleTemplate({ dispatch, location, form, scalevalue, record, ...rest }) {

  let questions; 
  const { getFieldProps } = form;

  if(record.gender == 1){
      questions = SCALE_DIA_C;
      questions[0].subject[0].Remarks = "身高(cm)-105=标准体重(正常范围为标准体重±10%)";
  }else{
      questions = SCALE_DIA;
      questions[0].subject[0].Remarks = "身高(cm)-100=标准体重(正常范围为标准体重±10%)";
  }

  function isDisable(name, value, i){
    if(record.patientValue){
      for(let i in record.patientValue.split(",")){
        if(record.patientValue.split(",")[i] === name){
          return true;
        }
      }
    }
    return false;
  }

  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 95}}>
        {questions.map((question, id) => (
          <div key={id}>
          <List>
            <List.Item>
              {question.title}
            </List.Item>
            {question.subject.map((question, i) => (
              <List.Item wrap key={question.id}>
                {`${i + 1}. ${question.title}`}
                <List.Item.Brief>
                <WhiteSpace size="md" />
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                      return (
                        <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal,{ [styles.questionOptionActive]: isDisable(key, value, i) })}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
                <div>{question.Remarks}</div>
                <WhiteSpace size="md" />
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="md" />
            </div>
        ))}
     </WingBlank>
     </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));
