import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List, Button } from 'antd-mobile';
import { SCALE_HYP_MAP, SCALE_HYP } from 'const';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";
import { isInArray, isResult } from '../../SettingAge';

function hyp({ dispatch, location, form, steps, scalevalue, ...rest }) {

  const { getFieldProps, getFieldValue, setFieldsValue, validateFields } = form;
  window.document.title = "高血压病辅助辨证";
  let patientId = JSON.parse(sessionStorage.getItem("state")).patientId;
  let restState = { type: 'hyp', patientId };

  const title = SCALE_HYP[steps].title;
  const menus = SCALE_HYP[steps].menu;

  if (!Object.keys(scalevalue).length) {
    scalevalue = [[], [], []];
    dispatch({ type: 'scale/save', payload: { scalevalue } });
  
  }

  let normal = scalevalue[0] || []; //症状
  let main = scalevalue[1] || []; //主症
  let tp = scalevalue[2] || []; //舌脉症状

  function handleChange({ target: { type, name, value, id } }) {

    if (Array.from(normal).find(item => item.id === id)) {
      setFieldsValue({ [name]: id }); //form实际值与状态不符，用赋值保持相符
      normal = Array.from(normal).filter(item => item.id !== id); //从normal中去除
    }
    if (Array.from(main).find(item => item.id === id)) {
      setFieldsValue({ [name]: id }); //form实际值与状态不符，用赋值保持相符
      main = Array.from(main).filter(item => item.id !== id); //从main中去除
    }
    if (Array.from(tp).find(item => item.id === id)) {
      setFieldsValue({ [name]: id }); //form实际值与状态不符，用赋值保持相符
      tp = Array.from(tp).filter(item => item.id !== id); //从tp中去除
    }
    if (getFieldValue(name) === id) {
      setFieldsValue({ [name]: null });	
    }else {
      setFieldsValue({ [name]: id });
      
    }
    validateFields((err, values) => {
      Object.keys(values).forEach((key) => {
        if (values[key] && isInArray(normal, key) == false) {
          normal.push({id:values[key],title:key});
          if(SCALE_HYP_MAP[id]){
            SCALE_HYP_MAP[id].split(";").forEach((e, i) =>{
              setFieldsValue({ [e]: null });
              if (normal.find(one => one.id === e)) {
                normal = normal.filter(one => one.id !== e);
                }
            })	
            Array.from(normal).forEach((item, i) => {
              if (SCALE_HYP_MAP[item]) {
                SCALE_HYP_MAP[item].split(';').forEach((item, i) => {
                setFieldsValue({ [item]: null });
                if (normal.find(one => one.id === item)) {
                  normal.push(item);
                }
                });
              }
            });
          }
        } 
        });
    })
    dispatch({ type: 'scale/save', payload: { scalevalue: [isResult(normal), main, tp] } });
  } 
  
  function nextHandler() {
    window.scrollTo(0, 0);
    if (steps < 9) {
      
      let pathname = location.pathname;
      let nextType = steps + 1;
      dispatch(({ type: 'scale/save', payload:{
        steps: nextType
      }}))
        dispatch(
        routerRedux.replace({
          pathname,
          state: {
            steps: nextType,
            restState,
          },
        }),
      );
    } else {
      let nextType = steps + 1;
      dispatch(({ type: 'scale/save', payload:{
        steps: nextType
      }}))
      dispatch(
        routerRedux.replace({
          pathname:`/IndexPage/hyp/hypsheet`,
          state: {
            steps: nextType,
            restState,
            scalevalue,
          },
        }),
      );
    }
  }

  function prevHandler() {
    window.scrollTo(0, 0);
    if (steps > 0) {
      let pathname = location.pathname;
      let nextType = steps - 1;
      dispatch(({ type: 'scale/save', payload:{
        steps: nextType
      }}))
      dispatch(
        routerRedux.replace({
          pathname,
          state: {
            steps: nextType,
            restState,
          },
        }),
      );
    }
  }

  function isActive(key, value, i, id) {
    // 选中状态下的颜色
    if (Array.from(normal).find(item => item.id === id)) {
      return true;
    }
    if (Array.from(main).find(item => item.id === id)) {
      return true;
    }
    if (Array.from(tp).find(item => item.id === id)) {
      return true;
    }

    if (!getFieldValue(key)) {
      return false;
    }

    return getFieldValue(key) === id;
  }

  return menus
    ? <div>
        <MemberInfo pathname={location.pathname}/>
        <WhiteSpace/>
        <WingBlank size="md" style={{ position: 'relative', top: 85}}>
        <List>
            <List.Item>
              <Flex justify="center">{`${steps+1}/10 ${title}`}</Flex>
              <Flex justify="center" style={{marginTop: '0.1rem'}}>在近3个月内您是否频繁出现以下症状</Flex>
              <Flex justify="center">（有则选，无可不选，可以多选）</Flex>
            </List.Item>
            {menus.map((menu, i) => (
              <List.Item key={i}>
                {menu.title}
                <WhiteSpace size="md" />
                <Flex wrap="wrap" className={styles.questionOptions}>
                  {menu.item.map((item, i) => {
                    const key = item.title;
                    const value = item.title;
                    const id = item.id;
                    return (
                      <label
                        key={key}
                        className={cx(styles.questionOption, styles.questionOptionNormal, {
                          [styles.questionOptionActive]: isActive(key, value, i,id),
                        })}
                      >
                        <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} id={id} checked={isActive(key, value, i, id)} onChange={handleChange} />
                        {value}
                      </label>
                    );
                  })}
                </Flex>
                {steps==0?(i==0?JSON.parse(sessionStorage.getItem("state")).gender== 1?
                  <Flex>身高(cm)-105=标准体重(正常范围为标准体重±10%)</Flex>:
                  <Flex>身高(cm)-100=标准体重(正常范围为标准体重±10%)</Flex>
                :""):""}
              </List.Item>
            ))}
            <div>
              <WhiteSpace size="xl" />
              <WingBlank>
                <Flex>
                  {steps === 0
                    ? null
                    : <Flex.Item>
                      <Button onClick={prevHandler}>上一页</Button>
                    </Flex.Item>}
                  <Flex.Item>
                    <Button type="primary" onClick={nextHandler}>下一页</Button>
                  </Flex.Item>
                </Flex>
              </WingBlank>
              <WhiteSpace size="xl" />
            </div>
          </List>
        <WhiteSpace size="xl" />
      </WingBlank>
      </div>
    : null;
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(hyp));