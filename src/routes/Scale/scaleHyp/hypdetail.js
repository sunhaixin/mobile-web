import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List  } from 'antd-mobile';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";
import { SCALE_HYP, SCALE_HYP_TITLE } from 'const';

function ScaleHypSheet({ dispatch, scalevalue,form, record, ...rest }) {
   const { getFieldProps, getFieldValue } = form;
   let normal =[], main=[];
   let normal_v = record.patientValue? record.patientValue.split("|")[0].split(";"): "";
   let main_v = record.patientValue? record.patientValue.split("|")[1].split(";"): "";
   SCALE_HYP_TITLE.map((x) =>{
       if(normal_v){
            normal_v.map((y) =>{
                if(y == x.id){
                normal.push({id:x.id,title:x.title})
                }
            })
       }
       if(main_v){
            main_v.map((y) =>{
                if(y == x.id){
                    main.push({id:x.id,title:x.title})
                }
            })
       }
   });
   if(record.gender == 1){
      SCALE_HYP[0].menu[0].Remarks = "身高(cm)-105=标准体重(正常范围为标准体重±10%)";
   }else{
      SCALE_HYP[0].menu[0].Remarks = "身高(cm)-100=标准体重(正常范围为标准体重±10%)";
   }

   function isActive(key, value, i, id) {

    if (Array.from(normal).find(item => item.title === value)) {
      return true;
    }
    if (Array.from(main).find(item => item.title === value)) {
      return true;
    }
    if (!getFieldValue(key)) {
      return false;
    }

    return getFieldValue(key) === id;
  }
   

  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WhiteSpace/>
    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
        {SCALE_HYP.map((question, id) => (
          <div key={id}>
          
          <List>
            <List.Item>
              {question.title}
            </List.Item>
            {question.menu.map((question, i) => (
              <List.Item wrap key={i}>
                {`${i + 1}. ${question.title}`}
                <WhiteSpace size="md" />
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                      return (
                        <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal,{ [styles.questionOptionActive]: isActive(key, value, i) })}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
                <div>{question.Remarks}</div>
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="lg" />
            </div>
        ))}
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleHypSheet));
