import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List, Button, Toast } from 'antd-mobile';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";

function hypsheet({ dispatch, location, form, scalevalue, isDisabled, ...rest }) {

  const { getFieldProps } = form;
  window.document.title = "高血压病辅助辨证";
  let personal =  JSON.parse(sessionStorage.getItem("state"));
  let patientId = personal.patientId ? personal.patientId: personal.id;

  if (!Object.keys(scalevalue).length) {
    scalevalue = [[], [], []];
    dispatch({ type: 'scale/save', payload: { scalevalue } });
    
  }
  function handleNormal({ target: { type, name, value,id } }) {
      let normal = scalevalue[0]; //症状
      const main = scalevalue[1]; //主症
      const tp = scalevalue[2]; //舌脉症状
      if (Array.from(normal).find(item => item.title === value)) {
        if(main.length < 3 ) {
          normal = Array.from(normal).filter(item => item.title !== value);
          main.push({id:id,title:value});
        }
      }
      dispatch({ type: 'scale/save', payload: { scalevalue: [normal, main, tp] } });
  }
  function handleMain({ target: { type, name, value, id } }) {
  
      const normal = scalevalue[0]; //症状
      let main = scalevalue[1]; //主症
      const tp = scalevalue[2]; //舌脉症状

      if (Array.from(main).find(item => item.title === value)) {
      
        main = Array.from(main).filter(item => item.title !== value);
        normal.push({id:id,title:value});//
      }
      dispatch({ type: 'scale/save', payload: { scalevalue: [normal, main, tp] } });
  }

  function isDisable () {
    const main = scalevalue[1];
    if (main.length > 2) {
      return true;
    }
  }

  function submit() {
    let normal = [], main = [], tp = [];
    scalevalue[0]? scalevalue[0].filter(item => normal.push(item.id)): "";
    scalevalue[1]? scalevalue[1].filter(item => main.push(item.id)): "";
    scalevalue[2]? scalevalue[2].filter(item => tp.push(item.id)): "";
    if (main.length < 1 || main.length > 3) {
        Toast.fail('请选择1至3个主症', 1);
    } else {
        dispatch(({ type: 'scale/save', payload:{
          isDisabled: true
        }}))
        setTimeout(() => {
            dispatch(({ type: 'scale/save', payload:{
              isDisabled: false
          }}))
        }, 3000);
        dispatch({
          type: 'scale/submitHypQuestions',
          payload: {
            type: 'hyp',
            patientId: patientId,
            hospitalId: localStorage.getItem("hid"),
            patientValue1: normal.join(';'),
            patientValue2: main.join(';'),
            patientValue3: "",
            ...rest,
          }
        });
    }
  }
  function submitHandler() {
      submit();
  }

  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WhiteSpace/>
    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
      <WhiteSpace />
      <List>
        <List.Item>
          <Flex justify="center">辨证分型</Flex>
          <WhiteSpace size="xl" />
          <Flex>一般症状</Flex>
          <WhiteSpace />
          <Flex wrap="wrap" className={styles.questionOptions} key="normal">
            {scalevalue[0].map((item, i) => {
              const key = item.id;
              const value = item.title;
              return (
                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal,{[styles.questionOptionDisable]: isDisable()})}>
                  <input {...getFieldProps(`${key}`)} type="radio" name={key} value={item.title} id={item.id} onChange={handleNormal} />
                  {value}
                </label>
              );
            })} 
          </Flex>
        </List.Item>

        <List.Item>
          <Flex>主诉<small>（请在一般症状中选择1~3项较为严重的症状）</small></Flex>
          <WhiteSpace />
          <Flex wrap="wrap" className={styles.questionOptions} key="main">
             {scalevalue[1].map((item, i) => {
              const key = item.id;
              const value = item.title;
              return (
                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal)}>
                  <input {...getFieldProps(`${key}`)} type="radio" name={key} value={item.title} id={item.id} onChange={handleMain} />
                  {value}
                </label>
              );
            })}
          </Flex>
        </List.Item>
      </List>
      <WhiteSpace />
      <List>
        <List.Item>
          <WingBlank>
            <Flex>
              <Flex.Item>
                <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
              </Flex.Item>
            </Flex>
          </WingBlank>
        </List.Item>
      </List>
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(hypsheet));