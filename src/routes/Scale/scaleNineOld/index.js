import { SCALE_NINE, SCALE_NINE_X, SCALE_OLD} from 'const';
import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import MemberInfo from "components/MemberInfo";
import { WingBlank, WhiteSpace, Flex, List } from 'antd-mobile';
import styles from '../styles.less';

function ScaleTemplate({ dispatch, location, form, record, isDisabled, ...rest }) {

    	const { getFieldProps, getFieldValue } = form;
		let question_ ,personal =  JSON.parse(sessionStorage.getItem("state"));
		if(location.pathname == "/scale_record/qrcode/sheet"){
			let eva = location.search.split("&")[1].split("=")[1];
			if(eva == 1){
				question_ = SCALE_OLD
			}else if(eva == 2){
				if(personal.gender == 1){
					question_ = SCALE_NINE;
				}else{
					question_ = SCALE_NINE_X;
				} 
			}
		}
		const check = record.patientValue;
		function isActive(key, value, i) {
			if (record && record.patientValue) {
				return check.split('')[i] === value;
			}

			if (!getFieldValue(key)) {
				return false;
			}

			return getFieldValue(key) === value;
		}
	
	  return (
	    <div>
		  <MemberInfo pathname={location.pathname}/>
		  <WingBlank size="md" style={{ position: 'relative', top: 85}}>
	      <WhiteSpace />
	      <List>
	      	<List.Item>
            <Flex justify="center" style={{marginTop: '0.1rem'}}>请根据一年来的体验和感觉，回答以下问题。</Flex>
          </List.Item>
	      	{question_.map((question, i) => (
	          <List.Item wrap multipleLine key={question.Id}>
	            {`${i + 1}. ${question.Title}`}
	            <List.Item.Brief>
	              <Flex justify="between" className={styles.amlistcontent}>
	                {question.Choicekey.split('|').map((option, ii) => {
	                  const value = question.Choicevalue.split('|')[ii];
	                  const key = question.Id;
	                  return (
	                    <Flex.Item key={value}>
	                      <label
	                        className={cx(styles.questionOption, styles.questionOptionNormal, {
	                          [styles.questionOptionActive]: isActive(key, value, i),
	                        })}
	                      >
	                        <input {...getFieldProps(`${key}`)} type="radio" name={key} value={value} checked={isActive(key, value, i)} />
	                        {option}
	                      </label>
	                    </Flex.Item>
	                  );
	                })}
	              </Flex>
	            </List.Item.Brief>
	          </List.Item>
	        ))}
	      </List>
	    <WhiteSpace size="xl" />
	    </WingBlank>
			</div>
	  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));
