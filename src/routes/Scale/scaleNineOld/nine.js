import React from 'react';
import cx from 'classnames';
import { connect } from 'dva';
import { SCALE_NINE, SCALE_NINE_X } from 'const';
import styles from '../styles.less';
import { createForm } from 'rc-form';
import MemberInfo from "components/MemberInfo";
import { WingBlank, WhiteSpace, Flex, List, Button } from 'antd-mobile';

function ScaleTemplate({ dispatch, location, form, isDisabled, ...rest }) {

      const { getFieldProps, getFieldValue, setFieldsValue, validateFields } = form;
      let personal =  JSON.parse(sessionStorage.getItem("state"));
      let patientId = personal.patientId ? personal.patientId: personal.id;
      window.document.title = "中医体质辨识"; let question_;
	  if(personal.gender == 1){
            question_ = SCALE_NINE;
      }else{
            question_ = SCALE_NINE_X;
      } 

	  function submitHandler() {
	    validateFields((err, values) => {
          const ret = [];
	      Object.keys(values).forEach((key) => {
	        if (!values[key]) {
	          values[key] = 1;
	        }
	        ret.push(values[key]);
	      });
	      if (!err) {
			dispatch(({ type: 'scale/save', payload:{
				isDisabled: true
			}}))
			setTimeout(() => {
					dispatch(({ type: 'scale/save', payload:{
						isDisabled: false
				}}))
			}, 3000);
            dispatch({
                type: 'scale/submitQuestions',
                payload: {
                    type: "nine",
                    patientId: patientId,
                    patientValue: ret.join(''),
                    hospitalId: localStorage.getItem("hid"),
                    ...rest,
                },
            });
	      }
	    });
	}
    function isActive(key, value, i) {
        if (!getFieldValue(key)) {
            return false;
        }

        return getFieldValue(key) === value;
    }

    function handleChange({ target: { type, name, value } }) {
        setFieldsValue({ [name]: value });
	  }
	
    return (
        <div>
            <MemberInfo pathname={location.pathname}/>
		    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
            <WhiteSpace />
            <List>
                <List.Item>
                    <Flex justify="center" style={{marginTop: '0.1rem'}}>请根据一年来的体验和感觉，回答以下问题。</Flex>
                </List.Item>
                {question_.map((question, i) => (
                <List.Item wrap multipleLine key={question.Id}>
                    {`${i + 1}. ${question.Title}`}
                    <List.Item.Brief>
                    <Flex justify="between" className={styles.amlistcontent}>
                        {question.Choicekey.split('|').map((option, ii) => {
                        const value = question.Choicevalue.split('|')[ii];
                        const key = question.Id;
                        return (
                            <Flex.Item key={value}>
                            <label
                                className={cx(styles.questionOption, styles.questionOptionNormal, {
                                [styles.questionOptionActive]: isActive(key, value, i),
                                })}
                            >
                                <input {...getFieldProps(`${key}`)} type="radio" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
                                {option}
                            </label>
                            </Flex.Item>
                        );
                        })}
                    </Flex>
                    </List.Item.Brief>
                </List.Item>
                ))}
            <WhiteSpace size="xl" />
            <WingBlank>
                <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
            </WingBlank>
            <WhiteSpace size="xl" />
            </List>
            <WhiteSpace size="xl" />
            </WingBlank>
        </div>
    );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));
