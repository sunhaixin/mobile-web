import { SCALE_PARTUM, todayPartum, SCALE_PARTUM_ITEM } from 'const';
import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, DatePicker, Picker, InputItem, Flex, List, Button, Toast } from 'antd-mobile';
import { routerRedux } from 'dva/router';
import styles from '../styles.less';
import { isInArray } from '../../SettingAge';
import MemberInfo from "components/MemberInfo";

const nowTimeStamp = Date.now();
const now = new Date(nowTimeStamp);

let dt = new Date();
dt.setMonth( dt.getMonth()-6 );
let minDate = new Date(dt);
let maxDate = now;

function partum({dispatch, location, form, scalevalue, step, detail, isDisabled, ...rest }){

		let questions = SCALE_PARTUM; window.document.title = "产后女性健康状态测评";
    let personal =  JSON.parse(sessionStorage.getItem("state"));
    let patientId = personal.patientId ? personal.patientId: personal.id;
		let UpdateDate = new Date(personal.birthday.slice(0,11).replace(/-/g, "/")).getFullYear();

    const { getFieldProps, getFieldValue, setFieldsValue, validateFields, getFieldError } = form;

    if (!Object.keys(scalevalue).length) {
	    scalevalue = [[], [], []];
	    dispatch({ type: 'scale/save', payload: { scalevalue } });
    }
	  let infos = scalevalue[0] || []; //用户信息
		let scores = scalevalue[1] || []; //症状
	  let nextStep = step + 1;
	  const category = step - 2;

	  let title = '';
	  if (step === 1) {
	    title = '完善用户信息';
	  }

	  let stepQuestions = [];
	  if (category >= 0 && category < 6) {

	    stepQuestions = questions[category].subject;
	    title = questions[category].title;
	  }

	   const isbrqData = [{ label: '是', value: '1' }, { label: '不是', value: '0' }];
     const jwData = [{ label: '有产后子宫复旧不全病史', id: '2' }, { label: '生产过程不顺利', id: '3' }, { label: '产后奶水极少或没奶,无法正常哺乳', id: '120' }];

     const validateForm = () => {

	    validateFields((err, values) => {

	      if (err) {
	        const fieldNames = Object.keys(err);
	        if (fieldNames[0]) {
	          return Toast.fail(getFieldError(fieldNames[0]));
	        }
	      } else {
	        if (step === 1) {
	          if (getFieldValue("hy") < getFieldValue("sc")) {
	            setFieldsValue({["hy"]: null});
	            setFieldsValue({["sc"]: null});
	            return Toast.fail("怀孕次数不得小于生产次数", 1);
	          }
	          const hy = getFieldValue('hy');// 怀孕次数
	          const sc = getFieldValue('sc');// 生产次数
	          const brq = typeof(getFieldValue('brq'))=="string"?getFieldValue('brq'):getFieldValue('brq').join(""); // 哺乳期
	          const height = getFieldValue('height'); // 身高
	          const weight = getFieldValue('weight'); // 体重
	          // 国医慧联设定7岁及以上女性, 需要设定一下女性年龄范围
	          const production = getFieldValue('production');
	          let UpdateBday = production.getFullYear();
            let difference = UpdateBday - UpdateDate;
            if(difference<7){
              setFieldsValue({["production"]: null});
              return Toast.fail("对不起，您选的女性应不小于7岁",1);
            }
            const jw = [];
	          for (var i in jwData) {
	            if (values[jwData[i].id]) {
                jw.push(jwData[i].id);
	              scores.push(jwData[i].id);
                
	            }
            }
	          infos = [hy, brq, height, weight, production , sc, jw];
	        }
	        dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores, []] } });
	        if (nextStep <=7) {
            dispatch(({ type: 'scale/save', payload:{
							step: nextStep
						}}))
	          let pathname = location.pathname;
	          dispatch(
	            routerRedux.replace({
	              pathname,
	              state: {
	                type: 'partum',
	                patientId: patientId,
                  step: nextStep,
                  birthday: personal.birthday.slice(0,11).replace(/-/g, "/")
	              },
	            }),
	          );
	        } else {
	          // 最后一页'提交'
	   		if(scores.length <= 3){
              Toast.fail('选择的症状信息过少，请重新选择');
            }else if(scores.length>3){
	          	  let lactation = "1";
	              if( infos[1]== 1){
	                  scores.push(lactation);//哺乳期
	              }
	          	  let pregnant = "4";
		          if(infos[0] >=6){
		              scores.push(pregnant);//怀孕次数
		          }
		         
		          let BWI = infos[3]/infos[2];
		          let baric_index = "5";
		          if (BWI<18.5){
		              scores.push(baric_index);//体重指数BMI
		          }
	          	infos[4]  = infos[4].getFullYear() + '/' + (infos[4].getMonth() + 1) + '/' + infos[4].getDate() + ' '+ todayPartum[infos[4].getDay()] + ' ' + infos[4].getHours() + ':' + infos[4].getMinutes() + ':' + infos[4].getSeconds();
              const patientValue = `${infos[0]}|${infos[1]}|${infos[2]}|${infos[3]}|${infos[4]}|${scores.join(';')}`;
              dispatch(({ type: 'scale/save', payload:{
                isDisabled: true
              }}))
              setTimeout(() => {
                  dispatch(({ type: 'scale/save', payload:{
                    isDisabled: false
                }}))
              }, 3000);
              dispatch({
                type: 'scale/submitQuestions',
                payload: {
                  type: 'partum',
                  patientId: patientId,
                  hospitalId: localStorage.getItem("hid"),
                  patientValue, 
                  jw: infos[6].join(';'), 
                  sc: infos[5],
                  ...rest,
                },
            });
		         
		      
			  }
	        }
	      }
	    });
    };
	
	function isActive(key, value, i) {
		if (Array.from(scores).find(item => item === key)) {
			return true;
		}

		if (!getFieldValue(key)) {
			return false;
		}

		return getFieldValue(key) === value;
	}
	function handleChange({ target: { type, name, value } }) {
    if (Array.from(scores).find(item => item === name)) {
      setFieldsValue({ [name]: value }); //form实际值与状态不符，用赋值保持相符
      scores = Array.from(scores).filter(item => item !== name); //从scores中去除
    }
    if (getFieldValue(name) === value) {
      setFieldsValue({ [name]: null });	
    }else {
      setFieldsValue({ [name]: value });
    }
    if(step > 1){
      validateFields((err, values) => {
        Object.keys(values).forEach((key) => {
          if (values[key] && isInArray(scores, key) == false) {
            scores.push(key);
            if(SCALE_PARTUM_ITEM[name]){
              SCALE_PARTUM_ITEM[name].split(";").forEach((e, i) =>{
                setFieldsValue({ [e]: null });
                if (scores.find(one => one === e)) {
                  scores = scores.filter(one => one !== e);
                  }
              })	
              Array.from(scores).forEach((item, i) => {
                if (SCALE_PARTUM_ITEM[item]) {
                  SCALE_PARTUM_ITEM[item].split(';').forEach((item, i) => {
                  setFieldsValue({ [item]: null });
                  if (scores.find(one => one === item)) {
                    scores.push(item);
                  }
                  });
                }
              });
            }
          } 
          });
      })
    }
    dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores, []] } });
  }
  
	function nextHandler() {
	    window.scrollTo(0, 0);
	    nextStep = step + 1;
	    validateForm();
	}

	function prevHandler() {
	    window.scrollTo(0, 0);
	    nextStep = step - 1;
	    validateForm();
	}

	function submitHandler() {
	    validateForm();
	}
	function isBlur(type,value){
	    if (type == "height") {
	      if (value < 0 || value > 300) {
	        setFieldsValue({[type]: null});
	        return Toast.fail("请输入正确的身高0~300cm！", 1);
	      }
	    }
	    if (type == "weight") {
	      if (value < 20 || value > 200) {
	        setFieldsValue({[type]: null});
	        return Toast.fail("请输入正确的体重20~200kg！", 1);
	      }
	    }
	    if (type == "hy") {
	      if (value <1) {
	        // 之前是需要判断1-4次, 因为产后症状对应表的原因。
	        setFieldsValue({[type]: null});
	        return Toast.fail("请输入正确的怀孕次数1次以上！", 1);
	      }
	    }
	    if (type == "sc") {
	      if (value <1) {
	        setFieldsValue({[type]: null});
	        return Toast.fail("生产次数大于等于1并小于等于怀孕次数", 1);
	      }
	    }
	}

	return (
    <div>
      <MemberInfo pathname={location.pathname}/>
      <WhiteSpace/>
      <WingBlank size="md" style={{ position: 'relative', top: 85}}>
      <List>
        <List.Item>
          <Flex justify="center">{`${step}/7 ${title}`}</Flex>
          {step!==1?
            <div>
            <Flex justify="center" style={{marginTop: '0.1rem'}}>您近期（1-2个月）是否出现以下症状</Flex>
            <Flex justify="center">（有即选，无则不选，可以多选）</Flex>
            </div>:""}
        </List.Item>
        {step === 1
          ? /*第1页自定义表单*/
            <div>
              <InputItem
                {...getFieldProps('height', {
                  initialValue: infos[2],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入身高' }],
                    },
                  ],
                  normalize:(v, prev) => {
                    if (v && !/^(([1-9]\d*)|0)(\.\d{0,1}?)?$/.test(v)) {
                      if (v === '.') {
                        return '0.';
                      }
                      return prev;
                    }
                    return v;
                  },
                })}
                placeholder="请输入身高"
                type="digit" 
                onBlur={() => isBlur("height",getFieldValue('height'))}
              >
                身高(cm)
              </InputItem>
               <InputItem
                {...getFieldProps('weight', {
                  initialValue: infos[3],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入体重' }],
                    },
                  ],
                  normalize:(v, prev) => {
                    if (v && !/^(([1-9]\d*)|0)(\.\d{0,1}?)?$/.test(v)) {
                      if (v === '.') {
                        return '0.';
                      }
                      return prev;
                    }
                    return v;
                  },
                })}
                placeholder="请输入体重"
                type="digit" 
                onBlur={() => isBlur("weight",getFieldValue('weight'))}
              >
                体重(kg)
              </InputItem>
              <InputItem
                {...getFieldProps('hy', {
                  initialValue: infos[0],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入怀孕次数' }],
                    },
                  ],
                })}
                placeholder="请输入怀孕次数"
                type="number"
                onBlur={() => isBlur("hy",getFieldValue('hy'))}
              >
                怀孕次数
              </InputItem>
              <InputItem
                {...getFieldProps('sc', {
                  initialValue: infos[5],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入生产次数' }],
                    },
                  ],
                })}
                placeholder="请输入生产次数"
                type="number"
                onBlur={() => isBlur("sc",getFieldValue('sc'))}
              >
                生产次数
              </InputItem>
              <DatePicker
                {...getFieldProps('production', {
                  initialValue: infos[4],
                  validate: [{
                    trigger: false,
                    rules: [{ required: true, message: '请选择生产日期' }],
                  }],
                })}
                mode="date"
                title="请选择"
                minDate={minDate}
                maxDate={maxDate}
              >
                <List.Item arrow="horizontal">生产日期</List.Item>
              </DatePicker>
              <Picker
                cols={1}
                data={isbrqData}
                {...getFieldProps('brq', {
                  initialValue: infos[1],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择是／不是' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">哺乳期</List.Item>
              </Picker>
              <List.Item>
                既往史
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {jwData.map((option, i) => {
                      const key = option.id;
                      const value = option.label;
                      return (
                        <label 
                         key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { 
                         [styles.questionOptionActive]: isActive(key, value, i) })}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            </div>
            : /*第2-7页动态题目*/
            stepQuestions.map((question, i) => (
              <List.Item wrap multipleLine key={question.id}>
                {`${i + 1}. ${question.title}`}
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, i) => {
                      const key = option.id;
                      const value = option.title;
                      return (
                        <label 
                          key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { 
                            [styles.questionOptionActive]: isActive(key, value, i),})}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            ))}
        <div>
          <WhiteSpace size="xl" />
          <WingBlank>
            <Flex>
              {step === 1
                ? null
                : <Flex.Item>
                  <Button onClick={prevHandler}>上一页</Button>
                </Flex.Item>}

              {step < 7 
                ? <Flex.Item>
                  <Button type="primary" onClick={nextHandler}>下一页</Button>
                </Flex.Item>
                : <Flex.Item>
                  <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
                </Flex.Item>}

            </Flex>
          </WingBlank>
          <WhiteSpace size="xl" />
        </div>
      </List>
      <WhiteSpace size="xl" />
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(partum));