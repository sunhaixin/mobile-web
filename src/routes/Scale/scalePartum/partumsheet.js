import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, InputItem, Flex, List } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_PARTUM } from 'const';
import MemberInfo from "components/MemberInfo";

const jwData = [{ label: '有产后子宫复旧不全病史', id: '2' }, { label: '生产过程不顺利', id: '3' }, { label: '产后奶水极少或没奶,无法正常哺乳', id: '120' }];

function ScalePartumTemplate({ dispatch, location, form, scalevalue, record, ...rest }) {

  let inp, radio;
  if(record.patientValue){
    inp = record.patientValue.split("|");
    radio = inp[inp.length-1].split(';');
  }
  function isActive(key, value, i) {
    if(radio){
      if (Array.from(radio).find(item => item === key)) {
       return true;
     }
   }

    if (!form.getFieldValue(key)) {
      return false;
    }

    return form.getFieldValue(key) === value;
  }
  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 95}}>
      <WhiteSpace size="md" />
      <List>
        <InputItem
          value={inp? inp[0]: ""}
          editable={false}
        >
          怀孕次数
        </InputItem>
        <InputItem
          value={record.sc}
          editable={false}
        >
          生产次数
        </InputItem>
        <InputItem
          value={inp? inp[1]== 1 ? "是" : "不是": ""}
          editable={false}
        >
          哺乳期 
        </InputItem>
        <InputItem
          value={inp?inp[2]: ""}
          editable={false}
        >
          身高(cm) 
        </InputItem>
        <InputItem
          value={inp?inp[3]: ""}
          editable={false}
        >
          体重(kg) 
        </InputItem>
         <InputItem
          value={inp? inp[4].split(" ")[0]: ""}
          editable={false}
        >
          生产日期
        </InputItem>
      </List>
       <List.Item>
        既往史
        <List.Item.Brief>
          <Flex wrap="wrap" className={styles.questionOptions}>
            {jwData.map((option, i) => {
              const key = option.id;
              const value = option.label;
              return (
                <label key={key} 
                className={cx(styles.questionOption, styles.questionOptionNormal, 
                { [styles.questionOptionActive]: isActive(key, value, i) })}>
                  <input type="checkbox" name={key} value={value} checked={isActive(key, value, i)}/>
                  {value}
                </label>
              );
            })}
        </Flex>
        </List.Item.Brief>
      </List.Item>
      <WhiteSpace size="md" />
        {SCALE_PARTUM.map((question, id) => (
           <div key={id}>
          <List>
            <List.Item>
              {question.title}
            </List.Item>
              {question.subject.map((question, i) => (
              <List.Item wrap key={i}>
                {`${i + 1}. ${question.title}`}
                <WhiteSpace size="md" />
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                      const item = option.id;
                      return (
                        <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, 
                          { [styles.questionOptionActive]: isActive(key, value, item) })}>
                          <input  type="checkbox" value={value}   />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="lg" />
            </div>
        ))}
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScalePartumTemplate));
