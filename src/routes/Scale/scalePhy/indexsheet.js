import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List } from 'antd-mobile';
import { SCALE_PHY, SCALE_PHY2 } from 'const';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";

function ScalePhyTemplate({ dispatch, location, form, record, ...rest }) {
  const { getFieldProps, getFieldValue, } = form;
  let Questions;
  if(location.search.split("&")[1].split("=")[1] == 3){
    Questions = SCALE_PHY
  }else if(location.search.split("&")[1].split("=")[1] == 17){
    Questions = SCALE_PHY2
  }
  function isActive(key, value, i) {
    if (record.patientValue) {
      if(record.patientValue.indexOf("|") != -1){
        let check1 = record.patientValue.split("|");
        check1 = check1[1].split("");
        return check1[i] === value;
      }else{
        return record.patientValue[i] === value;
      }
    }

    if (!getFieldValue(key)) {
      return false;
    }

    return getFieldValue(key) === value;
  }
  // 提交按钮根据record参数显示
  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
      <WhiteSpace />
      <List>
        {Questions.map((question, i) => (
          <List.Item wrap key={question.Id}>
            {`${i + 1}. ${question.Title}`}
            <WhiteSpace size="md"/>
            <List.Item.Brief>
              <Flex justify="between">
                {question.Choicekey.split('|').map((option, ii) => {
                  const value = question.Choicevalue.split('|')[ii];
                  const key = question.Id;
                  return (
                    <Flex.Item key={value}>
                      <label
                        className={cx(styles.questionOption, styles.questionOptionNormal, {
                          [styles.questionOptionActive]: isActive(key, value, i),
                        })}
                      >
                        <input {...getFieldProps(`${key}`)} type="radio" name={key} value={value} checked={isActive(key, value, i)} required />
                        {option}
                      </label>
                    </Flex.Item>
                  );
                })}
              </Flex>
            </List.Item.Brief>
          </List.Item>
        ))}
      </List>
      <WhiteSpace size="xl" />
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScalePhyTemplate));
