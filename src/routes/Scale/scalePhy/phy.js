import React from 'react';
import cx from 'classnames';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import styles from '../styles.less';
import { routerRedux } from 'dva/router';
import { SCALE_PHY } from 'const';
import MemberInfo from "components/MemberInfo";
import { WingBlank, WhiteSpace, Flex, List, Button, Toast } from 'antd-mobile';

function ScaleTemplate({ dispatch, location, form, scalevalue, steps, detail, isDisabled, ...rest }) {
    let personal =  JSON.parse(sessionStorage.getItem("state"));
    let patientId = personal.patientId ? personal.patientId: personal.id;
    const { getFieldProps, getFieldValue, validateFields, getFieldError } = form;
    window.document.title = "五态性格问卷测评(简)";
    if (!Object.keys(scalevalue).length) {
        scalevalue = [[], []];
        dispatch({ type: 'scale/save', payload: { scalevalue } });
    }
    let infos = scalevalue[0] || []; //用户信息
    let scores = scalevalue[1] || []; //症状
    let nextStep = steps + 1;

    const validateForm = () => {
        validateFields((err, values) => {
          if (err) {
            const fieldNames = Object.keys(err);
            if (fieldNames[0]) {
              return Toast.fail(getFieldError(fieldNames[0]));
            }
          } else {
            dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores] } });
            if (nextStep <= 42) {
              // '下一页'
              dispatch(({ type: 'scale/save', payload:{
                steps: nextStep
              }}))
              let pathname = location.pathname;
              dispatch(
                routerRedux.replace({
                  pathname,
                  state: {
                    type: "phy",
                    patientId: patientId,
                    steps: nextStep,
                  },
                }),
              );
            } else {
              // 最后一页'提交'
               if(scores.length < 43){
                  return Toast.fail('请完成所有题目', 1);
               }else{
                  if(scores[42]){
                     for(var i = 0 ;i<scores.length;i++){
                       if(!scores[i] ) {
                           return Toast.fail('请完成所有题目', 1);
                       }
                     }
                  }
                  const patientValue = `${scores.join('')}`;
                  dispatch(({ type: 'scale/save', payload:{
                    isDisabled: true
                  }}))
                  setTimeout(() => {
                      dispatch(({ type: 'scale/save', payload:{
                        isDisabled: false
                    }}))
                  }, 3000);
                    dispatch({
                    type: 'scale/submitQuestions',
                    payload: {
                      type: "phy",
                      patientId: patientId,
                      hospitalId: localStorage.getItem("hid"),
                      patientValue,
                      ...rest,
                    },
                  });
               }
            }
          }
        });
      };
    
      function isActive(key, value, i) {
        if (scores[i]&&scores[i]===value) {
          return true;
        }
        if (!getFieldValue(key-1)) {
          return false;
        }
    
        if(getFieldValue(key -1) !==value){
          return false;
        }
        return getFieldValue(key-1) === value;
      }
    
      function handleChange({ target: { type, name, value } }) {
        name = name - 1;
        if (getFieldValue(name) !== value) {
            if(scores.length < 44  ){
               scores[name] = value;
            }
        }
        dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores] } });
      }
      
      function nextHandler() {
        if(steps<42){
        window.scrollTo(0, 0);
        nextStep = steps + 1;
        validateForm();
        }
      }
    
      function prevHandler() {
        window.scrollTo(0, 0);
        nextStep = steps - 1;
        validateForm();
      }
    
      function submitHandler() {
          validateForm();
      } 
     
      return (
        <div>
          <MemberInfo pathname={location.pathname}/>
          <WhiteSpace/>
          <WingBlank size="md" style={{ position: 'relative', top: 85}}>
          <List>
            <List.Item>
               <Flex justify="center" style={{marginTop: '0.1rem'}}>请您仔细阅读题目，凭第一感觉尽快、如实地回答</Flex>
               <Flex justify="center">不需费时间考虑，尽量不要修改</Flex>
               <Flex justify="center">全部符合您的情况时答"是",只有部分符合只能答"否"</Flex>
            </List.Item>
            <List.Item wrap multipleLine >
                {`${steps+1}. ${SCALE_PHY[steps].Title}`}
                <List.Item.Brief>
                  <Flex justify="between">
                    {SCALE_PHY[steps].Choicekey.split('|').map((option, ii) => {
                      const value = SCALE_PHY[steps].Choicevalue.split('|')[ii];
                      const key = SCALE_PHY[steps].Id;
                      return (
                        <Flex.Item key={value}>
                          <label
                            className={cx(styles.questionOption, styles.questionOptionNormal, {
                              [styles.questionOptionActive]: isActive(key, value, steps),
                            })}
                          >
                            <input {...getFieldProps(`${key}`)} type="radio" name={key} value={value} checked={isActive(key, value,name)} onClick={nextHandler} onChange={handleChange} required />
                            {option}
                          </label>
                        </Flex.Item>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
    
            {!Object.keys(detail).length
              ? <div>
                <WhiteSpace size="xl" />
                <WingBlank>
                    <Flex>
                  {steps === 0
                    ? null
                    : <Flex.Item>
                      <Button onClick={prevHandler}>上一页</Button>
                    </Flex.Item>}
    
                  {steps < 42
                    ? <Flex.Item>
                      <Button type="primary" onClick={nextHandler}>下一页</Button>
                      </Flex.Item>
                    : <Flex.Item>
                      <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
                    </Flex.Item>}
    
                </Flex>
                  </WingBlank>
                <WhiteSpace size="xl" />
              </div>
              : null}
          </List>
          <WhiteSpace size="xl" />
        </WingBlank>
        </div>
      );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));