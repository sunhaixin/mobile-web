import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, InputItem, Flex, List } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_PRE } from 'const';
import MemberInfo from "components/MemberInfo";

function ScalePreTemplate({ dispatch, location, form, scalevalue, record, ...rest }) {
  let inp, radio;
  if(record.patientValue){
    inp = record.patientValue.split("|");
    radio = inp[inp.length-1].split(",");
  }
  
  function isActive(key, value, i) {
    if (radio && radio.indexOf(i) != -1) {
      return true;
    }
    if (!form.getFieldValue(key)) {
      return false;
    }
  }

  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 95}}>
      <WhiteSpace size="md" />
      <List>
        <List.Item >{inp? inp[2] == 1 ? "复诊" : "初诊": ""}</List.Item>
        <InputItem
          value={inp?inp[4]: ""}
          editable={false}
        >
          身高(cm)
        </InputItem>
        <InputItem
          value={inp?inp[5]: ""}
          editable={false}
        >
          体重(kg)
        </InputItem>
        <InputItem
          value={inp?inp[0]: ""}
          editable={false}
        >
          怀孕次数
        </InputItem>
        <InputItem
          value={inp?inp[1]: ""}
          editable={false}
        >
          生产次数
        </InputItem>
        <InputItem
          value={inp?inp[3]: ""}
          editable={false}
        >
          孕周
        </InputItem>        
      </List>  
        <WhiteSpace size="md" />
        {SCALE_PRE.map((question, id) => (
          <div key={id}>
          
          <List>
            <List.Item>
              {question.title}
            </List.Item>
         {question.subject.map((question, i) => (
              <List.Item wrap key={question.id}>
                {`${i + 1}. ${question.title}`}
                <WhiteSpace size="md" />
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                      const item = option.id;
                      return (
                        <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, item) })}>
                          <input {...form.getFieldProps(`${key}`)} type="checkbox" name={key} value={value}   />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="lg" />
            </div>
        ))}
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScalePreTemplate));
