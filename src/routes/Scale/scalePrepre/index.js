import React from 'react';
import { SCALE_PREPRE, SCALE_PREPRE_ITEM, PREPRE_DATA } from 'const';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Picker, InputItem, Flex, List, Button, Toast } from 'antd-mobile';
import { routerRedux } from 'dva/router';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";
import { isInArray, isRemove } from '../../SettingAge';

function ScaleTemplate({ dispatch, location, form, scalevalue, step, detail, isDisabled, ...rest }) {
  let questions = SCALE_PREPRE, xtype;
  let personal =  JSON.parse(sessionStorage.getItem("state"));
  let patientId = personal.patientId ? personal.patientId: personal.id;

  const { getFieldProps, getFieldValue, setFieldsValue, validateFields, getFieldError } = form;

  if (!Object.keys(scalevalue).length) {
    scalevalue = [[], []];
    dispatch({ type: 'scale/save', payload: { scalevalue } });
  }
  if(location.pathname == "/IndexPage/prepre"){
    xtype = 'prepre'; 
    window.document.title = "备孕女性健康状态测评";
  }else if(location.pathname == "/IndexPage/normal"){
    xtype = 'normal'; 
    window.document.title = "常态女性健康状态测评";
  }
  let infos = scalevalue[0] || []; //用户信息
  let scores = scalevalue[1] || []; //症状

  let nextStep = step + 1;
  const category = step - 2;

  let title = '';
  if (step === 1) {
    title = '完善用户信息';
  }

  let stepQuestions = [];
  if (category >= 0 && category < 6) {
    stepQuestions = questions[category].subject;
    title = questions[category].title;
  }

  const iszrData = [{ label: '有', value: '1' }, { label: '无', value: '0' }];
  const isReturnData = [{ label: '初诊', value: '0' }, { label: '复诊', value: '1' }];

  const validateForm = () => {

	validateFields((err, values) => {
		if (err) {
		    const fieldNames = Object.keys(err);
        if (fieldNames[0]) {
          return Toast.fail(getFieldError(fieldNames[0]));
        }
    } else {
    	if (step === 1) {
        if (getFieldValue("hy") < getFieldValue("sc")) {
          setFieldsValue({["hy"]: null});
          setFieldsValue({["sc"]: null});
          return Toast.fail("怀孕次数不得小于生产次数", 1);
        }
        const isReturn = getFieldValue('isReturn');
    		const height = getFieldValue('height'); // 身高
 		    const weight = getFieldValue('weight'); // 体重
 		    const hy = getFieldValue('hy');// 怀孕次数
  			const sc = getFieldValue('sc');// 生产次数
        const natural = getFieldValue('natural'); // 自然流产史
        const artificial = getFieldValue('artificial'); // 人工流产史
        const jw = [];
          for (var i = PREPRE_DATA.length - 1; i >= 0; i--) {
            if (values[PREPRE_DATA[i].id]) {
              jw.push(PREPRE_DATA[i].id);
              scores.push(PREPRE_DATA[i].id);
            }
          }
	      infos = [hy, natural, height, weight, artificial , sc, isReturn, jw];
    	 }
        dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores] } });
        if (nextStep <= 7) {
          dispatch(({ type: 'scale/save', payload:{
            step: nextStep
          }}))
          let pathname = location.pathname;
          dispatch(
            routerRedux.replace({
              pathname,
              state: {
                type: 'prepre',
                patientId: patientId,
                step: nextStep,
              },
            }),
          );
        } else {
          //年龄（虚岁）= 以测试年- 出生年 + 1
           if(scores.length <= 3){
              Toast.fail('选择的症状信息过少，请重新选择');
            }else if(scores.length>3){
              
              let BWI = infos[3]/infos[2];
              if (BWI<18.5){
                  scores.push("5");//肥胖
              }
              if (BWI>25){
                  scores.push("6");//消瘦
              }
              let liucan = infos[0] - infos[5];
              if(liucan >2){
                  scores.push("8");//流产大于两次 
              }
              let lactation = "7";
              if( infos[1]== 0){
                  scores.push(lactation);//人工流产史
              }
              let time = new Date();  let times = new Date(personal.birthday.slice(0,11).replace(/-/g, "/"));
              let newYear = time.getFullYear(), oldYear = times.getFullYear();
              let year = newYear - oldYear;
              if(year<=41 && year>=35){
                 scores.push("2");//35≤年龄 ≤41
              }
              if(year<=52 && year>=42){
                 scores.push("3");//42≤年龄 ≤52 
              }

              if(isInArray(scores,'19') && isInArray(scores,'23')){
                 isRemove(scores,"19");
                 isRemove(scores,"23");
                 scores.push("133");
              }
              if(isInArray(scores,'21') && isInArray(scores,'25')){
                 isRemove(scores,"21");
                 isRemove(scores,"25");
                 scores.push("134");
              }
            const patientValue = `${scores.join(',')}`;
            dispatch(({ type: 'scale/save', payload:{
              isDisabled: true
            }}))
            setTimeout(() => {
                dispatch(({ type: 'scale/save', payload:{
                  isDisabled: false
              }}))
            }, 3000);
            dispatch({
              type: 'scale/submitQuestions',
              payload: {
                type: xtype,
                patientId: patientId,
                hospitalId: localStorage.getItem("hid"),
                patientValue,
                jw: infos[7].join(','), 
                hy: infos[0],
                sc: infos[5],
                height: infos[2],
                weight: infos[3],   
                isReturn: infos[6],
                isMiscarriage: infos[1],
                isAbortion: infos[4],
                ...rest,
              },
            });
            }
           }
         }
	   });
   };

   function isActive(key, value, i) {
   	 if (Array.from(scores).find(item => item === key)) {
        return true;
      }

      if (!getFieldValue(key)) {
        return false;
      }

      return getFieldValue(key) === value;
   }

   function handleChange({ target: { type, name, value } }) {
    
    if (Array.from(scores).find(item => item === name)) {
      setFieldsValue({ [name]: value }); //form实际值与状态不符，用赋值保持相符
      scores = Array.from(scores).filter(item => item !== name); //从scores中去除
    }
    if (getFieldValue(name) === value) {
      setFieldsValue({ [name]: null });	
    }else {
      setFieldsValue({ [name]: value });
      
    }
    if(step > 1){
      validateFields((err, values) => {
        Object.keys(values).forEach((key) => {
          if (values[key] && isInArray(scores, key) == false) {
            scores.push(key);
            if(SCALE_PREPRE_ITEM[name]){
              SCALE_PREPRE_ITEM[name].split(";").forEach((e, i) =>{
                setFieldsValue({ [e]: null });
                if (scores.find(one => one === e)) {
                  scores = scores.filter(one => one !== e);
                  }
              })	
              Array.from(scores).forEach((item, i) => {
                if (SCALE_PREPRE_ITEM[item]) {
                  SCALE_PREPRE_ITEM[item].split(';').forEach((item, i) => {
                  setFieldsValue({ [item]: null });
                  if (scores.find(one => one === item)) {
                    scores.push(item);
                  }
                  });
                }
              });
            }
          } 
          });
      })
    }
    dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores] } });         
   }
   function nextHandler() {
     window.scrollTo(0, 0);
     nextStep = step + 1;
     validateForm();
   }

   function prevHandler() {
     window.scrollTo(0, 0);
     nextStep = step - 1;
     validateForm();
   }

   function submitHandler() {
     validateForm();
   }
   function isBlur(type,value){
    if (type == "height") {
      if (value < 0 || value > 300) {
        setFieldsValue({[type]: null});
        return Toast.fail("请输入正确的身高0~300cm！", 1);
      }
    }
    if (type == "weight") {
      if (value < 20 || value > 200) {
        setFieldsValue({[type]: null});
        return Toast.fail("请输入正确的体重20~200kg！", 1);
      }
    }
    if (type == "hy") {
      if (value < 0) {
        // 之前是需要判断1-4次, 因为产后症状对应表的原因。
        setFieldsValue({[type]: null});
        return Toast.fail("请输入正确的怀孕次数1次以上！", 1);
      }
    }
    if (type == "sc") {
      if (value < 0) {
        setFieldsValue({[type]: null});
        return Toast.fail("生产次数大于等于1并小于等于怀孕次数", 1);
      }
    }
  }

  return (
  	<div>
      <MemberInfo pathname={location.pathname}/>
      <WhiteSpace/>
      <WingBlank size="md" style={{ position: 'relative', top: 85}}>
  		<List>
  			  <List.Item>
	          <Flex justify="center">{`${step}/7 ${title}`}</Flex>
            {step!==1?
            <div><Flex justify="center" style={{marginTop: '0.1rem'}}>请您按近期3个月的身体状况进行选择</Flex>
            <Flex justify="center">（有即选，无则不选，可以多选）</Flex></div>:""}
	        </List.Item>
	        {step === 1
          ? /*第1页自定义表单*/
            <div>
              <Picker
                cols={1}
                data={isReturnData}
                {...getFieldProps('isReturn', {
                  initialValue: infos[6] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择初诊/复诊' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">初诊/复诊</List.Item>
              </Picker>
              <InputItem
                {...getFieldProps('height', {
                  initialValue: infos[2] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入身高' }],
                    },
                  ],
                  normalize:(v, prev) => {
                    if (v && !/^(([1-9]\d*)|0)(\.\d{0,1}?)?$/.test(v)) {
                      if (v === '.') {
                        return '0.';
                      }
                      return prev;
                    }
                    return v;
                  },
                })}
                placeholder="请输入身高"
                type="digit" 
                onBlur={() => isBlur("height",getFieldValue('height'))}
              >
                身高(cm)
              </InputItem>
               <InputItem
                {...getFieldProps('weight', {
                  initialValue: infos[3] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入体重' }],
                    },
                  ],
                  normalize:(v, prev) => {
                    if (v && !/^(([1-9]\d*)|0)(\.\d{0,1}?)?$/.test(v)) {
                      if (v === '.') {
                        return '0.';
                      }
                      return prev;
                    }
                    return v;
                  },
                })}
                placeholder="请输入体重"
                type="digit" 
                onBlur={() => isBlur("weight",getFieldValue('weight'))}
              >
                体重(kg)
              </InputItem>
              <InputItem
                {...getFieldProps('hy', {
                  initialValue: infos[0] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入怀孕次数' }],
                    },
                  ],
                })}
                placeholder="请输入怀孕次数"
                type="number"
                onBlur={() => isBlur("hy",getFieldValue('hy'))}
              >
                怀孕次数
              </InputItem>
              <InputItem
                {...getFieldProps('sc', {
                  initialValue: infos[5] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请输入生产次数' }],
                    },
                  ],
                })}
                placeholder="请输入生产次数"
                type="number"
                onBlur={() => isBlur("sc",getFieldValue('sc'))}
              >
                生产次数
              </InputItem>
              { getFieldValue('sc') - getFieldValue('hy') == 0 || getFieldValue('hy') == 0?
              <div>
              <Picker
                cols={1}
                data={[iszrData[1]]}
                {...getFieldProps('natural', {
                  initialValue: infos[1] || ['0'],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">自然流产史</List.Item>
              </Picker>
              <Picker
                cols={1}
                data={[iszrData[1]]}
                {...getFieldProps('artificial', {
                  initialValue: infos[4] || ['0'],
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">人工流产史</List.Item>
              </Picker></div>:(
                getFieldValue('hy') - getFieldValue('sc') == 1?  <div>
              <Picker
                cols={1}
                data={iszrData}
                {...getFieldProps('natural', {
                  initialValue: infos[1] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">自然流产史</List.Item>
              </Picker>
              <Picker
                cols={1}
                data={getFieldValue('natural')== 1 ? [iszrData[1]] : [iszrData[0]]}
                {...getFieldProps('artificial', {
                  initialValue: infos[4] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">人工流产史</List.Item>
              </Picker></div>: (
                getFieldValue('hy') - getFieldValue('sc') >= 2? <div>
              <Picker
                cols={1}
                data={iszrData}
                {...getFieldProps('natural', {
                  initialValue: infos[1] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">自然流产史</List.Item>
              </Picker>
              <Picker
                cols={1}
                data={getFieldValue('natural')== 1 ? iszrData : [iszrData[0]]}
                {...getFieldProps('artificial', {
                  initialValue: infos[4] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">人工流产史</List.Item>
              </Picker></div>:<div>
              <Picker
                cols={1}
                data={iszrData}
                {...getFieldProps('natural', {
                  initialValue: infos[1] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">自然流产史</List.Item>
              </Picker>
              <Picker
                cols={1}
                data={iszrData}
                {...getFieldProps('artificial', {
                  initialValue: infos[4] || '',
                  validate: [
                    {
                      trigger: false,
                      rules: [{ required: true, message: '请选择有／无' }],
                    },
                  ],
                })}
              >
                <List.Item arrow="horizontal">人工流产史</List.Item>
              </Picker></div>)
              )}
              <List.Item key="jw">
                既往病史
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {PREPRE_DATA.map((option, i) => {
                      const key = option.id;
                      const value = option.label;
                      return (
                        <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i) })}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            </div>
            : /*第2-7页动态题目*/
            stepQuestions.map((question, i) => (
              <List.Item wrap multipleLine key={question.id}>
                {`${i + 1}. ${question.title}`}
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, i) => {
                      const key = option.id;
                      const value = option.title;
                      return (
                        <label 
                          key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { 
                            [styles.questionOptionActive]: isActive(key, value, i)})}>
                          <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
                          {value}
                        </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            ))}
	        <div>
	          <WhiteSpace size="xl" />
	          <WingBlank>
	            <Flex>
	              {step === 1
	                ? null
	                : <Flex.Item>
	                  <Button onClick={prevHandler}>上一页</Button>
	                </Flex.Item>}

	              {step < 7 
	                ? <Flex.Item>
	                  <Button type="primary" onClick={nextHandler}>下一页</Button>
	                </Flex.Item>
	                : <Flex.Item>
	                  <Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
	                </Flex.Item>}

	            </Flex>
	          </WingBlank>
	          <WhiteSpace size="xl" />
	        </div>
  		</List>
  		<WhiteSpace size="xl" />
  	</WingBlank>
    </div>
  )

}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));