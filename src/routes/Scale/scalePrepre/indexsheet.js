import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, InputItem, Flex, List } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_PREPRE, PREPRE_DATA } from 'const';
import MemberInfo from "components/MemberInfo";

function ScalePrePreTemplate({ dispatch, location, form, scalevalue, record,...rest }) {
  const { getFieldProps, getFieldValue } = form;
  const { patientHeight, patientWeight, patientHy, patientSc, isMiscarriage,  isAbortion, isReturn, patientValue } = record;
 
  let optionvalues;
  if(patientValue){
     optionvalues = patientValue.split(',') || [];
    //  if(optionvalues.includes('133')){
     if(optionvalues.indexOf('133') !== -1){
        optionvalues = optionvalues.filter(( id ) => id!=133);
        optionvalues.push("19");
        optionvalues.push("23");
    }
    if(optionvalues.indexOf('134') !== -1){
       optionvalues = optionvalues.filter(( id ) => id!=134);
       optionvalues.push("21");
       optionvalues.push("25");
    }
  }
  function isActive(key, value, i) {
    let scores = scalevalue[1] || [];
    let xuanxiang = optionvalues? optionvalues.join(','): "";
    if (xuanxiang ) {
      return xuanxiang.split('')[i] === value;
    }
    if (Array.from(scores).find(item => item === key)) {
      return true;
    }
    if (!getFieldValue(key)) {
      return false;
    }

    return getFieldValue(key) === value;
  }
  function isDisable(name, value, i){
    if(optionvalues){
      for(let i in optionvalues){
        if(optionvalues[i] === name){
          return true;
        }
      }
    }
    return false;
  }

  return (
    <div>
      <MemberInfo pathname={location.pathname}/>
      <WingBlank size="md" style={{ position: 'relative', top: 95}}>
      <WhiteSpace size="md" />
       <List>
        <List.Item >{ isReturn  == 1 ? "复诊" : "初诊"}</List.Item>
        <InputItem
         value={patientHy}
         editable={false}
       >
         怀孕次数
       </InputItem>
       <InputItem
         value={patientSc}
         editable={false}
       >
         生产次数
       </InputItem>
       <InputItem
         value={patientHeight}
         editable={false}
       >
         身高(cm) 
       </InputItem>
       <InputItem
         value={patientWeight}
         editable={false}
       >
         体重(kg) 
       </InputItem>
       <InputItem
         value={ isMiscarriage == 0 ? "无" : "有"}
         editable={false}
       >
         自然流产史
       </InputItem>
        <InputItem
         value={ isAbortion == 0 ? "无" : "有"}
         editable={false}
       >
         人工流产史
       </InputItem>
     </List>
       <List.Item>
        既往病史
        <WhiteSpace size="md" />
        <List.Item.Brief>
          <Flex wrap="wrap" className={styles.questionOptions}>
            {PREPRE_DATA.map((option, i) => {
              const key = option.id;
              const value = option.label;
              return (
                <label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isDisable(key, value, i) })}>
                  <input type="checkbox" name={key} value={value} checked={isActive(key, value, i)}/>
                  {value}
                </label>
              
              );
            })}
        </Flex>
        </List.Item.Brief>
      </List.Item>
      <WhiteSpace size="md" />
        {SCALE_PREPRE.map((question, id) => (
          <div key={id}>
          <List>
            <List.Item>
              {question.title}
            </List.Item>
            {question.subject.map((question, i) => (
              <List.Item wrap key={i}>
                {`${i + 1}. ${question.title}`}
                <WhiteSpace size="md" />
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                      return (
                         <label key={key} 
                          className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]:  isDisable(key, value, i), })}>
                            <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)}/>
                            {value}
                          </label>
                      );
                    })}
                  </Flex>
                </List.Item.Brief>
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="lg" />
        </div>
        ))}
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScalePrePreTemplate));
