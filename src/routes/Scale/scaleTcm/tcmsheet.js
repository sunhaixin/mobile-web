import React from 'react';
import { connect } from 'dva';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List } from 'antd-mobile';
import styles from '../styles.less';
import { SCALE_TCM, SCALE_TCM_C } from 'const';
import MemberInfo from "components/MemberInfo";

function ScaleTemplate({ dispatch, location, form, scalevalue, record, ...rest }) {
  let questions;
  let scores = scalevalue[0] || [];
  const { getFieldProps, getFieldValue } = form;
  if(record.gender == 1 ){
      questions = SCALE_TCM_C;
      questions[0].subject[0].Remarks = "身高(cm)-105=标准体重(正常范围为标准体重±10%)";

  }else{
      questions =SCALE_TCM;
      questions[0].subject[0].Remarks = "身高(cm)-100=标准体重(正常范围为标准体重±10%)";
  }

  function isDisable(name, value, i){
    if (record.patientValue) {
      let arr = record.patientValue.split(",");
      for (var z = arr.length - 1; z >= 0; z--) {
        if(arr[z] === name){
          return true;
        }
      }

    }

    return false;
  }
  function isActive(key, value, i) {
    if (record.patientValue) {
      return record.patientValue.split('')[i] === value;
    }
    if (Array.from(scores).find(item => item === key)) {
      return true;
    }

    if (!getFieldValue(key)) {
      return false;
    }

    return getFieldValue(key) === value;
  }

  return (
    <div>
    <MemberInfo pathname={location.pathname}/>
    <WingBlank size="md" style={{ position: 'relative', top: 85}}>
        <WhiteSpace size="md" />
        {questions.map((question, id) => (
          <div key={id}>
          <List>
            <List.Item>
              {question.title}
            </List.Item>
            {question.subject.map((question, i) => (
              <List.Item wrap multipleLine key={question.id}>
                {`${i + 1}. ${question.title}`}
                <List.Item.Brief>
                  <Flex wrap="wrap" className={styles.questionOptions}>
                    {question.item.map((option, ii) => {
                      const key = option.id;
                      const value = option.title;
                       return (
                          <label key={key} 
                          className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]:  isDisable(key, value, i), })}>
                            <input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)}/>
                            {value}
                          </label>
                        );
                    })}
                  </Flex>
                </List.Item.Brief>
                <div>{question.Remarks}</div>
              </List.Item>
            ))}
          </List>
          <WhiteSpace size="lg" />
            </div>
        ))}
    </WingBlank>
    </div>
  );
}

export default connect(({ scale }) => ({ ...scale }))(createForm()(ScaleTemplate));
