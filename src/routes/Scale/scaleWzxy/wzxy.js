import React, {Component} from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { createForm } from 'rc-form';
import cx from 'classnames';
import { WingBlank, WhiteSpace, Flex, List, Button, Toast } from 'antd-mobile';
import styles from '../styles.less';
import MemberInfo from "components/MemberInfo";
import { isInArray } from '../../SettingAge';
import { SCALE_WZXY, SCALE_WZXY_MUSIC } from 'const';

class wzxy extends Component {
	constructor(props) {
        super(props);
		this.playmusic = this.playmusic.bind(this);
	}
	state = {
        isPlayed:false,
        currentMusic:{},
	};
	playmusic(){
        const { currentMusic } = this.state;
        let audio = this.refs.audio;
        if(audio.paused && currentMusic.Address){
            audio.play()
            this.setState({
                isPlayed:true
            })
        }else{
            audio.pause()
            this.setState({
                isPlayed:false
            })
		}
		audio.addEventListener('timeupdate',()=>{
			if(audio.ended){
				if(!currentMusic.Address){
					return
				}
				let current = ""
				SCALE_WZXY_MUSIC.forEach((v,i)=>{
					if (v.Address === currentMusic.Address){
						current = i
					}
				});
				if(current<SCALE_WZXY_MUSIC.length-1){
					this.setState({
						currentMusic:SCALE_WZXY_MUSIC[current+1]
					},()=>{
						this.playmusic()
					})
				}else{
					this.setState({
						currentMusic:SCALE_WZXY_MUSIC[0]
					},()=>{
						this.playmusic()
					})
				}
            }
		})
	}
    playThis(i){
        this.setState({
            currentMusic: SCALE_WZXY_MUSIC[i]
        },()=>{
            this.playmusic()
        })
    }
    onCheckColor(e,z,i){
        const { currentMusic } = this.state;
            return (
                <div>
                    {currentMusic.Address== e.Address&&this.state.isPlayed?
                    <div><img src={require('../../../assets/ic_bofan.png')} />
                    &nbsp;&nbsp;<img src={require('../../../assets/ic_stop.png')} 
                    onClick={this.playThis.bind(z,i)} /></div>:
                    <img src={require('../../../assets/ic_play.png')} 
                    onClick={this.playThis.bind(z,i)} />}
                </div>
            )
        
    }
    render(){
	   const { currentMusic } = this.state;
	   const { dispatch, location, form, scalevalue, step, isDisabled } = this.props;
	   let questions = SCALE_WZXY; window.document.title = "五脏相音健康养生测评";
	   let personal =  JSON.parse(sessionStorage.getItem("state"));
	   let patientId = personal.patientId ? personal.patientId: personal.id;
	   const { getFieldProps, getFieldValue, setFieldsValue, validateFields, getFieldError } = form;
	   let maxstep = 2;
	   let nextStep = step + 1;
	   let infos = scalevalue[0] || []; //歌曲选项
	   let scores = scalevalue[1] || []; //症状
	   let stepQuestions = questions[0].subject;

	   const validateForm = () => {
	     validateFields((err, values) => {
	      if (err) {
	        const fieldNames = Object.keys(err);
	        if (fieldNames[0]) {
	          return Toast.fail(getFieldError(fieldNames[0]));
	        }
	      } else {
				if (nextStep <= maxstep) {
					dispatch(({ type: 'scale/save', payload:{
						step: nextStep
					}}))
					let pathname = location.pathname;
					dispatch(
						routerRedux.replace({
						pathname,
						state: {
							type: 'wzxy',
							patientId: patientId,
							step: nextStep,
						},
						}),
					);
				} else {// 最后一页'提交'

				}
	      }
        });
	   };
		function isActive(key, value, i) {	
			if(infos && Array.from(infos).find(item => item.split(':')[0] == key && item.split(':')[1] == value)) {
				return true;
			}
			if (Array.from(scores).find(item => item === key)) {
				return true;
			}
			if (!getFieldValue(key)) {
				return false;
			}
			return getFieldValue(key) === value;
	    }
	   	function handleRadioChange({ target: { type, name, value } }) {
			 setFieldsValue({ [name]: value }); 
			 validateFields((err, values) => {
				let Radiolist = [];
				Object.keys(values).forEach((key) => {	
					Radiolist.push(`${key}:${values[key]}`);
				});
				dispatch({ type: 'scale/save', payload: { scalevalue: [Radiolist, scores] } });		
			})			
		}
		function handleChange({ target: { type, name, value } }) {
			if (Array.from(scores).find(item => item === name)) {
				setFieldsValue({ [name]: value }); //form实际值与状态不符，用赋值保持相符
				scores = Array.from(scores).filter(item => item !== name); //从scores中去除
			}
			if (getFieldValue(name) === value) {
				setFieldsValue({ [name]: null });	
			}else {
				setFieldsValue({ [name]: value });
			}
			validateFields((err, values) => {
				Object.keys(values).forEach((key) => {
					if (values[key] && isInArray(scores, key) == false) {
						scores.push(key);
					} 
				});
			})
			dispatch({ type: 'scale/save', payload: { scalevalue: [infos, scores] } });			
		}

		function nextHandler() {
			window.scrollTo(0, 0);
			nextStep = step + 1;
			validateForm();
		}

		function prevHandler() {
			window.scrollTo(0, 0);
			nextStep = step - 1;
			validateForm();
		}

		function submitHandler() {
			validateForm();
		}
	    return(
			<div>
				<MemberInfo pathname={location.pathname}/>
				<WhiteSpace/>
				<WingBlank size="md" style={{ position: 'relative', top: 85}}>
				<List>
					{step === 1 ?
					<List.Item>
						<Flex justify="center" style={{marginTop: '0.1rem'}}>请仔细试听以下曲目，凭第一感觉</Flex>
						<Flex justify="center">如实勾选喜欢程度。共10首曲目，每个曲目均需勾选</Flex>
					</List.Item>:
					<List.Item>
						<Flex justify="center" style={{marginTop: '0.1rem'}}>请根据您近期的真实情况作答。</Flex>
						<Flex justify="center">以下各题，有则选，无则不选，可以多选</Flex>
					</List.Item>}
					{step === 1 
					? /*第1页动态歌曲*/
					SCALE_WZXY_MUSIC.map((question, i) => (
						<List.Item wrap multipleLine key={question.Id}>
						<List.Item.Brief>
						<Flex wrap="wrap">
							<img src={require("../../../assets/ic_music.png")} />
							&nbsp;<div style={{ color: '#000'}}>{`${question.Title}`}</div>
							&nbsp;&nbsp;<div style={{ fontSize: 11, color: '#888'}}>时长:
							&nbsp;{question.Time}</div>
								<div style={{ position: 'absolute', top: 20, right: 30}}>
									{this.onCheckColor(question,this,i)}
									<audio src={currentMusic.Address?currentMusic.Address:""} ref="audio"/> 
								</div>
						</Flex>
						</List.Item.Brief>
						<WhiteSpace/>
						<List.Item.Brief>
						<Flex wrap="wrap" className={styles.questionOptions}>
							{question.Choicekey.split('|').map((option, ii) => {
							const value = question.Choicevalue.split('|')[ii];
							const key = question.Id;

							return (
								<label key={`${key}_${value}`} className={cx(styles.questionOption, styles.questionOptionNormal, {[styles.questionOptionActive]: isActive(key, value, i)})}>
									<input {...getFieldProps(`${key}`)} type="radio" name={key} value={value} checked={isActive(key, value, i)} onChange={handleRadioChange} />
									{option}
								</label>
							)})}
						</Flex>
						</List.Item.Brief>
					</List.Item>
				))
					: /*第2页动态题目*/
					stepQuestions.map((question, i) => (
						<List.Item wrap multipleLine key={question.id}>
						{`${question.title}`}
						<List.Item.Brief>
							<Flex wrap="wrap" className={styles.questionOptions}>
							{question.item.map((option, i) => {
								let key = option.id; 
								let value = option.title;
								return (
								<div key={key}>
								{ key == 39 && personal.gender == 1?"":
								<label key={key} className={cx(styles.questionOption, styles.questionOptionNormal, { [styles.questionOptionActive]: isActive(key, value, i), })}>
									<input {...getFieldProps(`${key}`)} type="checkbox" name={key} value={value} checked={isActive(key, value, i)} onChange={handleChange} />
									{value}
								</label>}
								</div>
								);
							})}
							</Flex>
						</List.Item.Brief>
						</List.Item>))}
					<div>
					<WhiteSpace size="xl" />
					<WingBlank>
						<Flex>
						{step === 1
							? null
							: <Flex.Item>
							<Button onClick={prevHandler}>上一页</Button>
							</Flex.Item>}

						{step < maxstep
							? <Flex.Item>
							<Button type="primary" onClick={nextHandler}>下一页</Button>
							</Flex.Item>
							: <Flex.Item>
							<Button type="primary" disabled={isDisabled} onClick={submitHandler}>提交</Button>
							</Flex.Item>}

						</Flex>
					</WingBlank>
					<WhiteSpace size="xl" />
					</div>
				</List>
	      		</WingBlank>
			</div>
		)
    }
}
export default connect(({ scale }) => ({ ...scale }))(createForm()(wzxy));