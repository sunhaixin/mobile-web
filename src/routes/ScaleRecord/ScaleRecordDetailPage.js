import React from 'react';
import { connect } from 'dva';
import { Link, routerRedux } from 'dva/router';
import MemberInfo from "components/MemberInfo";
import { WingBlank, WhiteSpace, Flex, Card, List, Button } from 'antd-mobile';

const ScalerecordPage = ({ dispatch, location, record }) => {
    let recordEva = "", recordUid = "";
    let w3CardType = JSON.parse(sessionStorage.getItem("w3CardType"));
    if(location.search && location.pathname !== "/resultqrcode"){
        recordEva = location.search.split("&")[1].split("=")[1];
        recordUid = location.search.split("&")[0].split("?")[1];
    }
    let mypathname = "";
    if(recordEva == 1){
        mypathname = "qrcode/sheet";
    }else if(recordEva == 2) {
        mypathname = "qrcode/sheet";
    }else if(recordEva == 3) {
        mypathname = "qrcode/sheet/phy";
    }else if(recordEva == 4) {
        mypathname = "qrcode/sheet/pre";
    }else if(recordEva == 5) {
        mypathname = "qrcode/sheet/hyp";
    }else if(recordEva == 6) {
        mypathname = "qrcode/sheet/chd";
    }else if(recordEva == 11) {
        mypathname = "qrcode/sheet/dia";
    }else if(recordEva == 12) {
        mypathname = "qrcode/sheet/tcm";
    }else if(recordEva == 13) {
        mypathname = "qrcode/sheet/partum"
    }else if(recordEva == 14) {
        mypathname = "qrcode/sheet/prepre";
    }else if(recordEva == 16) {
        mypathname = "qrcode/sheet/normal";
    }else if(recordEva == 17) {
        mypathname = "qrcode/sheet/phy2";
    }else if(recordEva == 19){
        mypathname = "qrcode/sheet/chd2";
    }
    const AgainScale = () =>{
        dispatch(routerRedux.replace(`/IndexPage`));
    }

    return(
        <div>
            <MemberInfo pathname={location.pathname}/>
            <div style={{position: 'relative', top: 85}}>
            <WhiteSpace size="md" />
            {w3CardType && w3CardType.w3Tips? 
            <div style={{ color: '#aa4829'}}>
                <Flex justify="center" style={{marginTop: '0.1rem'}}>{w3CardType.w3Tips}</Flex>
                <WhiteSpace size="md" />
            </div>:""
            }
            <WingBlank size="md" 
            key={recordEva} >
                <Card className="record-card">
                <Card.Header
                title={<span>{record?record.title:''}</span>}
                extra={<span>{record?record.createTime:''}</span>} />
                <Card.Body style={{padding: 0}}>
                    <img
                    className="qrcode"
                    src={record?record.qrCode ||record.qrcode :''} style={{width: "100%"}}  key="icon"/>
                    <Flex justify="center">
                    <h3 className="code"> { record?record.code: ''}</h3>
                    </Flex>
                </Card.Body>
                </Card>
                <WhiteSpace size="md" />
                {location.pathname == "/resultqrcode"?"":
                <Link to={{
                    pathname: mypathname,
                    search: `${recordUid}&eva=${recordEva}`
                    }}>
                        <List>
                            <List.Item
                            arrow="horizontal">
                            {"测评详情"}
                            </List.Item>
                        </List>
                    </Link>
                }
                <WhiteSpace size="lg" />
                <Button
                onClick={AgainScale}
                style={{ fontSize: 15, color: '#aa4929'}}>返回测评</Button>
            </WingBlank>
            </div>
        </div>
    )
}

export default connect(({ scale }) => ({ ...scale }))(ScalerecordPage);
