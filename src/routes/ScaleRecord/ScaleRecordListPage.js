import React, { Component } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import MemberInfo from "components/MemberInfo";
import { WingBlank, WhiteSpace, Card, ActivityIndicator, Flex } from 'antd-mobile';

class ScaleRecordListPage extends Component{
  constructor(props) {
    super(props);
    this.state= {
      isLoading: true
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0); window.document.title = "测评记录";
    setTimeout(() => {
      this.setState({
        isLoading: false,
      });
    }, 600);
  }

  componentWillMount(){
      this.forceUpdate();
  } 

  render(){
    const { location:{ pathname }, recordList  } = this.props;
    window.scrollTo(0, 0); window.document.title = "测评记录";
    return (
      <div> 
          <MemberInfo pathname={pathname}/>
          <WingBlank 
          style={{position: 'relative', top: 85}}
          size="md">
          <WhiteSpace/>
          {this.state.isLoading ?  
            <Flex justify="center" style={{ margin: "50px 0" }} >
                  <ActivityIndicator size="large" animating />
            </Flex> : 
              recordList&& recordList.length == 0? 
              <div>
                <Flex justify="center" style={{ margin: "30px 0" }} >
                  <img style={{width: 100, height: 100}} src={require('../../assets/nodata.png')} /> 
                </Flex> 
                <Flex justify="center" style={{ color: '#aa4929' }} >
                  暂无测评记录
                </Flex> 
              </div>
              :recordList.map((record, i) =>
              <Link key={record.evaUid} to={{pathname: `${pathname}/qrcode`, search:`${record.evaUid}&eva=${record.eva}` }}>
                  <Card className="record-card">
                  <Card.Header
                      title={
                        <div style={{ fontSize: '.14rem'}}>{record.title}</div>
                      }
                      extra={record.createTime}
                      />
                    <Card.Footer
                      content={<div>{record.code}</div>} 
                      />
                </Card>
                <WhiteSpace/>
              </Link>
            )} 
        </WingBlank>
      </div>
      
    );
  }
}

const mapStateToProps = ({ scale, ...rest }) => ({ ...scale, ...rest });

export default connect(mapStateToProps)(ScaleRecordListPage);