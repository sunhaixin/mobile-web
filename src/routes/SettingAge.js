import { sheets } from "const";
import { Toast } from 'antd-mobile';

export const IdNumverReg = (idnumber) =>{
  // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
  const regIdNo = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  let idnumber_err = "";
  if(!regIdNo.test(idnumber)){
    idnumber_err = 404;
    Toast.fail("身份证长度不正确", 1)
  }else{
    var year, month, day, temp_date; 
    if (15 == idnumber.length) {
            year = idnumber.substring(6, 8);
            month = idnumber.substring(8, 10);
            day = idnumber.substring(10, 12);
            temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
            // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法  
            if (temp_date.getFullYear() > new Date().getFullYear() || temp_date.getMonth() != parseFloat(month) - 1 || temp_date.getDate() != parseFloat(day)) {
                idnumber_err = 404;
                Toast.fail("请填写正确的出生日期", 1)
            }
   } else if (18 == idnumber.length) {
            year = idnumber.substring(6, 10);
            month = idnumber.substring(10, 12);
            day = idnumber.substring(12, 14);
            temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
            // 这里用getFullYear()获取年份，避免千年虫问题    
            if (temp_date.getFullYear() > new Date().getFullYear() || temp_date.getMonth() != parseFloat(month) - 1 || temp_date.getDate() != parseFloat(day)) {
                idnumber_err = 404;
                Toast.fail("请填写正确的出生日期", 1)
            }
      }
  }
  return idnumber_err;
}

//采取周岁计算方式：每到公历生日的当天便增龄一岁
export const AgeUtil = (birthday,now) =>{
    const getLastDayOfMonth = (year,month)=>{
        let isLeapYear = year => (year%4 == 0 && year%100 != 0) || year%400 == 0;
        let daysOfmonth2 = isLeapYear(year)?29:28;
        let days_31 = {
            1:31,
            2:daysOfmonth2,
            3:31,
            4:30,
            5:31,
            6:30,
            7:31,
            8:31,
            9:30,
            10:31,
            11:30,
            12:31
        };
        return days_31[month];
    };
      let year,month,day = 0;
      let yearDiff = now.getYear() - birthday.getYear();
      let monthDiff =now.getMonth() - birthday.getMonth();
      let dayDiff = now.getDate() - birthday.getDate();
      let ActualToday = getLastDayOfMonth(birthday.getYear(),(birthday.getMonth()+1));
      year = yearDiff;
      month = monthDiff;
      day = dayDiff;
          
      if(year == 0){
          if(monthDiff == 0){
            return day+"天";
          }else if(dayDiff>=0 && monthDiff >0){
            return month+"月";
          }else if(dayDiff<0 && monthDiff == 1){
            return day +  ActualToday +"天"
          }else if(dayDiff<0 && monthDiff>1){
            return (month-1)+"月"
          }
        }else if(year == 1){
            if(monthDiff > 0 || (monthDiff == 0 && dayDiff>=0)){
                return Math.abs(year)+"岁";
            }else if(dayDiff<0 && monthDiff>-11){
                return (month+12-1)+"月"
            }else if(dayDiff<0 && monthDiff==-11){
                return day + ActualToday +"天"
            }else if(day>=0 && monthDiff<0){
                return  (month+12)+"月"
            }
        }else{
            if(monthDiff>0 || (monthDiff==0 && dayDiff>=0)){
                return year +"岁"
            }else{
                return (year-1)+"岁"
            }
        }
}

export const isInArray =(arr,value) =>{
  for(var i = 0; i < arr.length; i++){
      if(value === arr[i]){
          return true;
      }
  }
  return false;
}

export const isResult = (arr) =>{
  var result = [];
  var obj = {};
  for(var i =0; i<arr.length; i++){
         if(!obj[arr[i].id]){
          result.push(arr[i]);
          obj[arr[i].id] = true;
        }
   }
   return result;
}

export const isRemove = (arr,value) =>{
    var index = arr.indexOf(value); 
    if (index > -1) { 
        arr.splice(index, 1); 
    }
}

export const unique2 = (arr) =>{
    arr.sort(); //先排序
    var res = [arr[0]];
    for(var i = 1; i < arr.length; i++){
      if(arr[i] !== res[res.length - 1]){
      res.push(arr[i]);
      }
    }
    return res;
}

export const moduleshandle = (personal, arr) =>{
    let assess= [], evalist=[];
    const Ages = personal.birthday ?AgeUtil(new Date(personal.birthday.slice(0,11).replace(/-/g, "/")),new Date()) : '';
    const gender = personal.gender? personal.gender: "";
    arr.forEach((n) =>{
        if(Ages.indexOf("岁")!== -1){
            switch (parseInt(n, 10)) {
              case 1:
                if(parseInt(Ages)>= 65){
                  assess.push("1")
                }
                break;
              case 2: //7-64
                if(parseInt(Ages)>6 && parseInt(Ages) < 65){
                    assess.push("2")
                }
                break;
              case 3:
                if(parseInt(Ages) >=18){
                  assess.push("3")
                }
                break;
              case 4:
                if(gender == 2){
                  if(parseInt(Ages) >19 && parseInt(Ages) < 57){
                    assess.push("4")
                  }
                }
                break;
              case 5:
                if(parseInt(Ages) >54){
                    assess.push("5")
                }
                break;
              case 6:
                if(parseInt(Ages) <7){
                    assess.push("6")
                }
                // else if(parseInt(Ages) >6 && parseInt(Ages) <14){
                //     assess.push("19")
                // }
                break;
              case 11:
                if(parseInt(Ages) >17){
                    assess.push("11")
                }
                break;
              case 12:
                if(parseInt(Ages) >17){
                    assess.push("12")
                }
                break;
              case 13:
                if(gender == 2){
                  if(parseInt(Ages) >19 && parseInt(Ages) < 57){
                    assess.push("13")
                  }
                }
                break;
              case 14:
                if(gender == 2){
                  if(parseInt(Ages) >19 && parseInt(Ages) < 57){
                    assess.push("14")
                  }
                }
                break;
              case 17:
                if(parseInt(Ages) >17){
                  assess.push("17")
                }
                break;
              // case 20:
              //   if(parseInt(Ages) >15){
              //     assess.push("20")
              //   }
              //   break;
            }
        }else{
          assess = ["6"];
        }
    })  
    sheets.forEach((y) =>{
      assess ? assess.forEach((x) =>{
        if (y.module == x) {
          evalist.push(y)
        }
      }):""
    }) 
    return evalist;
}

export const fetchErrorMsg = (data) =>{
  if(undefined === data || "Network Error" === data){
      return Toast.fail("网络已断开,请连接后重试", 1)
  }else{
      return data.data;
  }
}