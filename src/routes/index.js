import IndexPage from './Department/IndexPage';
import Moblie from './Department/moblie';
import UpdateMyDetail from './Account/UpdateMyDetail'
import ScaleModules from './Account/ScaleModules';
import ScaleNineTemplate from './Scale/scaleNineOld/nine';
import ScaleOldTemplate from './Scale/scaleNineOld/old';
import ScaleNineOld from './Scale/scaleNineOld';


import ScaleChdTemplate from './Scale/scaleChd/chd';
import ScaleChddetail from './Scale/scaleChd/chddetail';

import ScaleChd2Template from './Scale/scaleChd2/chd2';
import ScaleChd2detail from './Scale/scaleChd2/chd2detail';

import ScalePreTemplate from './Scale/scalePre/pre';
import ScalePreSheet from './Scale/scalePre/presheet';


import ScalehypTemplate from './Scale/scaleHyp/hyp';
import ScaleHypSheet from './Scale/scaleHyp/hypsheet';
import ScaleHypDetail from './Scale/scaleHyp/hypdetail';


import ScalePhyTemplate from './Scale/scalePhy/phy';
import ScalePhy2Template from './Scale/scalePhy/phy2';
import ScalePhySheet from './Scale/scalePhy/indexsheet';


import ScaleTcmSheet from './Scale/scaleTcm/tcmsheet';
import ScaleTcmTemplate from './Scale/scaleTcm/tcm';

import ScaleDiaSheet from "./Scale/scaleDia/diasheet";
import ScaleDiaTemplate from './Scale/scaleDia/dia';

import ScalePartumTemplate from './Scale/scalePartum/partum';
import ScalePartumSheet from './Scale/scalePartum/partumsheet';

import ScalePrepreNormal from './Scale/scalePrepre';
import ScalePrepreNormalSheet from './Scale/scalePrepre/indexsheet';

import ScaleWzxyTemplate from './Scale/scaleWzxy/wzxy';

import ScaleRecordListPage from './ScaleRecord/ScaleRecordListPage';
import ScaleRecordDetailPage from './ScaleRecord/ScaleRecordDetailPage';

import ReportListPage from './Report/ReportListPage';
import ReportDetailPage from './Report/ReportDetailPage';

export {
	IndexPage,
	Moblie,
	UpdateMyDetail,
	ScaleModules,
	ScaleNineTemplate,
	ScaleOldTemplate,
	ScaleNineOld,

	ScaleChddetail,
	ScaleChdTemplate,
	ScaleChd2detail,
	ScaleChd2Template,
	ScalePreSheet,
	ScalePreTemplate,
	ScalehypTemplate,
	ScaleHypSheet,
	ScaleHypDetail,

	ScalePhySheet,
	ScalePhyTemplate,
	ScalePhy2Template,

	ScaleTcmSheet,
	ScaleTcmTemplate,

	ScaleDiaSheet,
	ScaleDiaTemplate,

	ScalePartumSheet,
	ScalePartumTemplate,

	ScalePrepreNormal,
	ScalePrepreNormalSheet,

	ScaleWzxyTemplate,
	
	ScaleRecordListPage,
	ScaleRecordDetailPage,

	ReportListPage,
	ReportDetailPage
	
}