import request from '../utils/request/request';
// 通过身份证号码认证
export function queryAuthentication(idNumber, type, hid){
	const body = new FormData();
    if(!type){
        body.append('idNumber', idNumber);
        body.append('hid', hid);
    }else if(type == 5){
        body.append('idNumber', idNumber);
        body.append('mobile', idNumber);
        body.append('type', type);
        body.append('hid', hid);
    }else{
        body.append('idNumber', idNumber);
        body.append('type', type);
        body.append('hid', hid);
    }
    return request.post('rbac/wap/login', { body });
}


export function updatePatient(data){
    // name, gender, birthday, call
    const body = new FormData();
    Object.keys(data).forEach(key => body.append(key, data[key]));
    return request.post('wapeva/patient/update', { body });
}