import request from '../utils/request/request';

// 完善用户信息页面
export function queryCreate(idNumber, name, gender, birthday, type, hid){
	const body = new FormData();
	if(!type){
		body.append('idNumber',idNumber);
		body.append('name', name);
		body.append('gender', gender);
		body.append('birthday',birthday);
		body.append('hid', hid);
	}else if(type == 5){
		body.append('idNumber',idNumber);
		body.append('name', name);
		body.append('gender', gender);
		body.append('birthday',birthday);
		body.append('type', type);
		body.append('hid', hid);
		body.append('mobile', idNumber);
	}else if(type == 1 || type ==2 || type == 3){
		body.append('idNumber',idNumber);
		body.append('name', name);
		body.append('gender', gender);
		body.append('birthday',birthday);
		body.append('type', type);
		body.append('hid', hid);
	}
	return request.post('rbac/wap/regist', { body });
}

// 获取测评模块
export function queryGetModules (hospitalId){
  return request("wapeva/hospital/getModules?"+"hospitalId="+hospitalId,{
    method: "GET",
  })
}