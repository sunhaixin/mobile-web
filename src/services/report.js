import request from '../utils/request/request';

// 获取体检记录
export function queryReportList(hospitalId, patinetId, isFinish, page, size) {
    let personal =  JSON.parse(sessionStorage.getItem("state"));
	  let fId = personal.patientId ? personal.patientId: personal.id;
    let isf = 1, Page = 0, Size = 1000; let hId = localStorage.getItem("hid");
    return request("wapeva/eva/list?"+"hospitalId="+hId+"&patinetId="+fId+"&isFinish="+isf+"&page="+Page+"&size="+Size,{
      method: "GET"
    })
  }

  
// 报告详情
export function ReportDetail(type, uid ) {
    let personal =  JSON.parse(sessionStorage.getItem("state"));
    let fId = personal.patientId ? personal.patientId: personal.id;
    return request(`wapeva/${type}/reportDetail?`+"uid="+uid+"&patientId="+fId,{
      method: "GET"
    })
 }