import request from '../utils/request/request';

// 微信和app的备孕和孕中用b字段,暂时用备用的0.
// 提交测评（移动web端患者）
export function submitScaleByUser(birth, isSick, disease, diseaseTime, type, patientId, hospitalId, patientValue, jw, sc, hy, height, weight, isReturn, isMiscarriage, isAbortion,) {
  const body = new FormData();
  if(type === 'pre'){
  	  body.append('b', '0');
  }
  if(type === 'partum'){
      body.append('jw', jw);
      body.append('sc', sc);
  }
  if(type === 'chd2'){
      body.append('birth',birth);
      body.append('isSick',isSick);
      body.append('disease',disease);
      body.append('diseaseTime',diseaseTime);
  }
  if(type === 'prepre' || type === 'normal'){
      body.append('jw', jw);
      body.append('b', '0');
      body.append('sc', sc);
      body.append('height', height);
      body.append('weight', weight);
      body.append('hy', hy);
      body.append('isReturn', isReturn);
      body.append('isMiscarriage', isMiscarriage);
      body.append('isAbortion', isAbortion);
  }
  body.append('patientId', patientId);
  body.append('hospitalId',hospitalId);
  body.append('patientValue', patientValue);
  return request.post(`wapeva/${type}/submit`, { body });
}
//hyp
export function submitHypQuestions(type, hospitalId,  patientId, patientValue1, patientValue2, patientValue3){
  const body = new FormData();
  if(type === 'hyp'){
     body.append('patientValue1', patientValue1);
     body.append('patientValue2', patientValue2);
     body.append('patientValue3', patientValue3);
  }
  body.append('hospitalId',hospitalId);
  body.append('patientId', patientId);
  return request.post(`wapeva/${type}/submit`, { body });

}
// 获取测评记录
export function queryScaleList(hospitalId, patinetId, isFinish, page, size) {
  let hId = localStorage.getItem("hid"); let isf = 0, Page = 0, Size = 1000;
  let personal =  JSON.parse(sessionStorage.getItem("state"));
	let fId = personal.patientId ? personal.patientId: personal.id;
  return request("wapeva/eva/list?"+"hospitalId="+hId+"&patinetId="+fId+"&isFinish="+isf+"&page="+Page+"&size="+Size,{
    method: "GET"
  })
}

//测评详情
export function queryScaleDetail(type,uid){
  let personal =  JSON.parse(sessionStorage.getItem("state"));
	let fId = personal.patientId ? personal.patientId: personal.id;
  if(type == "chd"){
    return request(`wapeva/${type}/detail?`+"uid="+uid+"&familyId="+fId, {
      method: "GET",
    })
  }else{
    return request(`wapeva/${type}/detail?`+"uid="+uid+"&patientId="+fId, {
      method: "GET",
    })
  }
}

export function getW3CardType(hid){
  return request(`rbac/wap/getW3CardType?`+"hid="+hid, {
    method: "GET",
  })
}