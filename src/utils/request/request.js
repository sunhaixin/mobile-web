import fetch from 'dva/fetch';
import { isEmpty } from '../lang';
import serialize from './helpers/serialize';
import combineURL from './helpers/combineURL';
import isAbsoluteURL from './helpers/isAbsoluteURL';
import { apiBaseUrl } from '../../config';

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

const timeout = (p, ms = 30 * 1000) =>
  Promise.race([
    p,
    wait(ms).then(() => {
      const error = new Error(`Connection timed out after ${ms} ms`);
      error.statusCode = 408;
      throw error;
    }),
  ]);

// Request factory
function request(url, options, method) {
  const { endpoint, ...rest } = interceptRequest(url, options, method);
  const xhr = fetch(endpoint, rest).then(interceptResponse);
  return timeout(xhr, request.defaults.timeout).catch((error) => {
    // return Promise.reject(error);
    
  });
}

request.defaults = {
  baseURL: apiBaseUrl,
  timeout: 10 * 5000,
  headers: {
    Accept: 'application/json',
  },
};

// Headers factory
const createHeaders = () => {
  const headers = {
    ...request.defaults.headers,
  };

  const Token = JSON.parse(sessionStorage.getItem("token"));
  if (Token) {
    headers.Authorization = Token;
  }
    return headers;
};

// Request interceptor
function interceptRequest(url, options, method) {
  let endpoint;
  if (isAbsoluteURL(url)) {
    endpoint = url;
  } else {
    endpoint = combineURL(request.defaults.baseURL, url);
  }

  let data = {
    method,
    endpoint,
    headers: createHeaders(),
  };

  if (!isEmpty(options)) {
    data = {
      ...data,
      ...options,
    };

    if (options.json) {
      data.headers['Content-Type'] = 'application/json;charset=utf-8';
      data.body = JSON.stringify(options.json);
    }

    if (options.form) {
      data.headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
      data.body = serialize(options.form);
    }

    if (options.body) {
      data.body = options.body;
    }

    if (options.params) {
      endpoint += `?${serialize(options.params)}`;
      data.endpoint = endpoint;
    }
  }

  return data;
}
// Response interceptor
/* eslint-disable consistent-return */
function interceptResponse(response) {
  return new Promise((resolve, reject) => {
    const emptyCodes = [204, 205];

    // Don't attempt to parse 204 & 205
    if (emptyCodes.indexOf(response.status) !== -1) {
      return resolve(response.ok);
    }

    if (response.ok) {
      const contentType = response.headers.get('Content-Type');
      // if (contentType.includes('application/json')) {
      if (contentType && contentType.indexOf('application/json') !== -1) {

        resolve(response.json());
      }

      resolve(response);
    }
    
    //如果返回的是false说明当前操作系统是手机端，如果返回的是true则说明当前的操作系统是电脑端
    var userAgentInfo = navigator.userAgent;
    var isAndroid = userAgentInfo.indexOf('Android') > -1 || userAgentInfo.indexOf('Linux') > -1; //g
    var isIOS = !!userAgentInfo.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    var wechatInfo = navigator.userAgent.match(/MicroMessenger\/([\d\.]+)/i) ;
    var System_ = "";
    if (isAndroid) {
        //这个是安卓操作系统
        System_ = "Android"
    }else if (isIOS) {
        System_ = "iPhone"
　　　　//这个是ios操作系统
    }else{
        System_ = "电脑端"
    }
   if (response.status === 401 || response.status == 429) {
     setTimeout(function(){
           throw new Error(response.url);
     }, 1000)
     setTimeout(function(){
       localStorage.removeItem('auth'+sessionStorage.getItem("hospitalid"));
       // sessionStorage.removeItem('token');
       location.reload();
     }, 1500)
     window.onerror = function(message, source, lineno, colno, error){ 
       const body = new FormData();
       body.append('codeVersion', "微信3__1.1.2 pre-release");
       body.append('system', System_);
       body.append('wechatVersion', wechatInfo? "微信"+ wechatInfo[1]: System_);//微信版本号
       body.append('networkLatency', undefined === window.TimeLatency ? 0: window.TimeLatency) ;//网络延迟
       body.append('lossRate', 1); //丢包率
       body.append('internetSpeedAvg', undefined === window.speedTime ? 0: window.speedTime); // 平均网速// 平均网速
       body.append('message', message);
       body.append('source', source);
       body.append('lineno', lineno);
       body.append('colno', colno);
       body.append('error', error);
       request.post("rbac/logjs/save",{ body });
       return false
     }
   }

    const error = new Error(response.statusText);
    try {
      response.clone().json().then((result) => {
        error.body = result;
        error.response = response;
        reject(error);
      });
    } catch (e) {
      error.response = response;
      reject(error);
    }
  });
}
/* eslint-enable consistent-return */

// suger
request.get = (url, options) => request(url, options, 'GET');

request.head = (url, options) => request(url, options, 'HEAD');

request.options = (url, options) => request(url, options, 'OPTIONS');

request.post = (url, options) => request(url, options, 'POST');

request.put = (url, options) => request(url, options, 'PUT');

request.delete = (url, options) => request(url, options, 'DELETE');

request.del = request.delete;

export default request;
