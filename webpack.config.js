const path = require('path');

module.exports = (webpackConfig, env) => {
  // 别名配置
  webpackConfig.resolve.alias = {
    'components': path.join(__dirname, 'src', 'components'),
    'const': path.join(__dirname, 'src', 'constants'),
    'routes': path.join(__dirname, 'src', 'routes'),
  }
  return webpackConfig;
}